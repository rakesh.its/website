<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'abyl' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'Password@!321##20' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'OxD<PRs xdwh|+$5]^h`FZ@`z9@Xy[WmGG[X`Y;GVfdqV^r0z?S]U_Z64<|!aF2r' );
define( 'SECURE_AUTH_KEY',  'p9$;Wca(?u`]lQ/o-A[mp|N;,pUCY/5KXH]{/dB]~Q7WlBO1[2KAm8@/4G!%4#;[' );
define( 'LOGGED_IN_KEY',    '-!RV,?|BtnkBwvv1O|;yk0/FExP#!bdWfz7!(lg?p!!:bk6W& G0hE/~;af-rNh ' );
define( 'NONCE_KEY',        'b5w m}hr=60#q(eLwL6vvogVB43.pqt,>6HSo ]L/ij}k<w</#+x/jp:(hA8ip`(' );
define( 'AUTH_SALT',        '5!3[r =<KcJ=u[7)PraF{M3EQ*89~1K/3%48^2Rq#qB3x,w#Aez<kQ7|#C90~!kP' );
define( 'SECURE_AUTH_SALT', '~B+&*G? kL+6<X4@T!{iBf1UZ?1Y8I%*J.}1N7=tJHv!i6KJg,K-tummS9,eM/>A' );
define( 'LOGGED_IN_SALT',   '<2Dects/s}]53W;W,]`CwG`+6X1JQw5ZpsUM-3H9Q]n@2el_2A`&}Q^sm!qi-_TU' );
define( 'NONCE_SALT',       '@.1(6u({SP$`BeZ:=:X=}96$6#+qmRIfw+KB,8 JNS;cF?5w3DOqZy`O{%-.#`Ns' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
