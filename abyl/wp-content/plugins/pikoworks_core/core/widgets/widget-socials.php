<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
    exit;
} 

/**
 * widgets social icon
 * @author  themepiko
 * @link http://themepiko.com/
 */
 
class pikoworks_widgets_socials extends WP_Widget {     
  
    
    function __construct() {
		parent::__construct( 'piko-socials-icon', esc_html__('Socials icon', 'pikoworks_core'), // Name
			array( 'description' => esc_html__( 'Social link load from theme option', 'pikoworks_core' ),  ) // Args
		);
	}
    
    
    public function widget( $args, $instance ) {
        global $post, $pikoworks;
        
        $title = apply_filters( 'widget_title', $instance['title'] ); 
        
        
        // before and after widget defined by themes file 
        echo $args['before_widget'];   
        
        if ( trim( $title ) != '' ) {
            if ( isset( $args['before_title']) ){
                echo $args['before_title'];
                echo $title;
                echo $args['after_title'];
            }    
        }
        
        if ( trim( $pikoworks['twitter'] . $pikoworks['facebook'] . $pikoworks['googleplus'] . $pikoworks['dribbble'] . 
            $pikoworks['behance'] . $pikoworks['tumblr'] . $pikoworks['instagram'] . $pikoworks['pinterest'] .  $pikoworks['soundcloud'] .
            $pikoworks['youtube'] . $pikoworks['vimeo'] . $pikoworks['linkedin'] . $pikoworks['flickr'] ) != '' ) {
                echo '<div class="social-layout-2"><div class="social-page-icon">';
                get_template_part( 'template-parts/social', 'items' );
                echo '</div></div><!-- /.social-wrap -->';
        }        
        echo $args['after_widget']; 
    }
    
    public function form( $instance ) {
        if ( isset( $instance['title'] )) { 
            $title = $instance['title'];  
        }
        else { 
            $title = esc_html__('Socials', 'pikoworks_core'); 
        }        
        // Widget admin form
        ?> 
        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php esc_html_e( 'Title', 'pikoworks_core' ); ?>: </label> 
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>"  />
        </p>
        
        <?php 
    }
    
    public function update( $new_instance, $old_instance ) {
        $instance = array(); 
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        return $instance;
    }    
}
