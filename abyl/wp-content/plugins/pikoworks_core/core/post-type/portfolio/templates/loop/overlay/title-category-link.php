<?php
/**
 * @author  Themepiko
 */
$cat = '';
foreach ( $terms as $term ){
    $cat .= $term->name.', ';
}
$cat = rtrim($cat,', ');

$disable_link = isset( $GLOBALS['pikoworks']['portfolio_disable_link_detail'] ) ? $GLOBALS['pikoworks']['portfolio_disable_link_detail'] : '1';

?>
<figure class="entry-thumbnail">
    <img width="<?php echo esc_attr($width) ?>" height="<?php echo esc_attr($height) ?>" src="<?php echo esc_url($thumbnail_url) ?>" alt="<?php echo get_the_title() ?>"/>
    <div class="entry-thumbnail-hover">
        <div class="entry-hover-wrapper">
            <div class="entry-hover-inner">
                <?php if ( $disable_link =='1'){?>
                    <h5><?php the_title() ?></h5>
                <?php } else{?>
                    <h5><a href="<?php echo get_permalink(get_the_ID()) ?>" class="title"><?php the_title() ?></a></h5>
                <?php }?>
                    <span class="category"><?php echo wp_kses_post($cat) ?></span><br>
                <span class="link-button">
                     <?php if ($disable_link){?>
                         <a class="link"  href="<?php echo get_permalink(get_the_ID()) ?>" title="<?php echo get_the_title() ?>">
                             <span class="lnr lnr-link" aria-hidden="true"></span>
                         </a>
                     <?php } ?>
                    <a class="view-gallery prettyPhoto" href="<?php echo esc_url($url_origin) ?>" data-rel="prettyPhoto[pp_gal_<?php echo get_the_ID() ?>]"  title="<?php echo get_the_title() ?>">
                        <span class="lnr lnr-magnifier" aria-hidden="true"></span>
                    </a>
                </span>
            </div>
        </div>
    </div>
</figure>