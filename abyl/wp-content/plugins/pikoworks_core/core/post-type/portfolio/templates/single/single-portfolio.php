<?php
get_header();
global $pikoworks;
$min_suffix = (isset($pikoworks['enable_minifile']) && $pikoworks['enable_minifile'] == 1) ? '.min' : '';
//wp_enqueue_style('pikoworks-portfolio', PIKOWORKSCORE_CORE_URL . '/post-type/portfolio/assets/css/portfolio'.$min_suffix.'.css', array(),false);




$autoplay = isset($pikoworks['autoplay']) ? $pikoworks['autoplay'] : true;
$navigation = isset($pikoworks['navigation']) ? $pikoworks['navigation'] : true;
$dots = isset($pikoworks['dots']) ? $pikoworks['dots'] : false;
$margin = isset($pikoworks['margin']) ? $pikoworks['margin'] : '';
$loop = isset($pikoworks['loop']) ? $pikoworks['loop'] : false;
$slidespeed = isset($pikoworks['slidespeed']) ? $pikoworks['slidespeed'] : '250';

$data_carousel = array(
    "autoplay"      => esc_html($autoplay),
    "navigation"    => esc_html($navigation),
    "margin"        => esc_html($margin),
    "smartSpeed"    => esc_html($slidespeed),
    "loop"          => esc_html($loop),
    "autoheight"    => "false",
    'nav'           => esc_html($navigation),
    'dots'          => esc_html($dots),
);
$data_carousel['items'] =  1;
      
$single_animation = $pikoworks['sanam'];

if ( have_posts() ) {    
    // Start the Loop.
    while ( have_posts() ) : the_post();
        $post_id = get_the_ID();
        $categories = get_the_terms($post_id, PIKO_PORTFOLIO_CATEGORY_TAXONOMY);
        $client = get_post_meta($post_id, 'portfolio-client', true );
        $location = get_post_meta($post_id, 'portfolio-location', true );
        $embaded =  wp_oembed_get(get_post_meta($post_id, 'portfolio_video', true ));

        $meta_values = get_post_meta( get_the_ID(), 'portfolio-format-gallery', false );
        $imgThumbs = wp_get_attachment_image_src(get_post_thumbnail_id($post_id),'full');
        $cat = '';
        $arrCatId = array();
        if($categories){
            foreach($categories as $category) {
                $cat_link = get_category_link( $category->term_id);
                $cat .= '<a href="' . esc_url($cat_link). '">'.$category->name.'</a>, ';
                $arrCatId[count($arrCatId)] = $category->term_id;
            }
            $cat = trim($cat, ', ');
        } 

        $detail_style =  get_post_meta(get_the_ID(),'portfolio_detail_style',true);
        if (!isset($detail_style) || $detail_style == 'none' || $detail_style == '') {
            $detail_style = $pikoworks['portfolio-single-style'];
        }

        include_once(plugin_dir_path( __FILE__ ).'/'.$detail_style.'.php');

    endwhile;
    }
?>

<?php

if(isset($pikoworks['show_portfolio_related']) && $pikoworks['show_portfolio_related']=='1' )
    include_once(plugin_dir_path( __FILE__ ).'/related.php');

?>

<script type="text/javascript">
    (function($) {
        "use strict";
        $(document).ready(function(){

            $('a','.post-single .share').each(function(){
                $(this).click(function(){
                    var href = $(this).attr('data-href');
                    var leftPosition, topPosition;
                    var width = 400;
                    var height = 300;
                    var leftPosition = (window.screen.width / 2) - ((width / 2) + 20);
                    var topPosition = (window.screen.height / 2) - ((height / 2) + 100);
                    //Open the window.
                    window.open(href, "", "width=350, height=250,left=" + leftPosition + ",top=" + topPosition);
                })
            })

            $("a[data-rel^='prettyPhoto']").prettyPhoto(
                {
                    theme: 'light_rounded',
                    slideshow: 5000,
                    deeplinking: false,
                    social_tools: false
                });
            $('.portfolio-item.hover-dir > div.entry-thumbnail').hoverdir();
        })
    })(jQuery);
</script>

<?php get_footer(); ?>
