<?php
/**
 * @author  themepiko
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <div class="clearfix">
            <div class="col-md-8">
                <div class="owl-wrap">
                    <?php if($embaded!=''):
                            echo '<div id="res_videos" class="embed-responsive embed-responsive-16by9">' . $embaded . '</div>';                         
                        elseif(count($meta_values) > 0): ?>
                        <div class="vertical-image">
                        <?php
                            $index = 0;
                            foreach($meta_values as $image):
                                $urls = wp_get_attachment_image_src($image,'full');
                                $img = '';
                                if(count($urls)>0){
                                    $resize = matthewruddy_image_resize($urls[0],750,560);
                                    if($resize!=null && is_array($resize) )
                                        $img = $resize['url'];
                                }
                                ?>
                                <figure>
                                    <img alt="<?php the_title_attribute(); ?>" src="<?php echo esc_url($img) ?>" />
                                </figure>
                            <?php endforeach; ?>
                        </div>  <!--post-slideshow wrap-->
                        <?php    
                        else:  
                            if(count($imgThumbs)>0) :?>
                            <figure>
                                <img class="single-img" alt="<?php the_title_attribute(); ?>" src="<?php echo esc_url($imgThumbs[0])?>" />
                            </figure>
                        <?php endif;
                        endif; // meta value
                        ?>
                </div>
            </div>
            <div class="col-md-4">
                <div class="single-content-wrap">
                    <header class="entry-header">
                        <h3><?php the_title() ?></h3>
                    </header>
                    <div class="category">
                        <?php echo wp_kses_post($cat) ?>
                    </div>
                    <?php the_content(); ?>
                </div>
                <div class="portfolio-info extra-field">
                    <?php
                    $meta = get_post_meta(get_the_ID(), 'portfolio_custom_fields', TRUE);
                    if(isset($meta) && is_array($meta) && count($meta['portfolio_custom_fields'])>0){
                        for($i=0; $i<count($meta['portfolio_custom_fields']);$i++){
                            ?>
                            <div class="portfolio-info-box">
                                <h6><?php echo wp_kses_post($meta['portfolio_custom_fields'][$i]['custom-field-title']) ?>:</h6>
                                <div class="portfolio-meta"><?php echo wp_kses_post($meta['portfolio_custom_fields'][$i]['custom-field-description']) ?></div>
                            </div>
                        <?php }
                    }
                    ?>
                    <div class="portfolio-info-box">
                        <h6><?php echo esc_html__('Date','pikoworks_core') ?>: </h6>
                        <div class="portfolio-meta "><?php echo date(get_option('date_format'),strtotime($post->post_date)) ?></div>
                    </div>
                    <?php if($client!=''): ?>
                    <div class="portfolio-info-box">                        
                        <h6><?php echo esc_html__('Client','pikoworks_core') ?>: </h6>
                        <div class="portfolio-meta"><?php echo esc_html($client); ?></div>                        
                    </div>
                    <?php endif; ?>
                </div>
                <?php if($GLOBALS['pikoworks']['optn_portfolio_social_shear']== '1'): ?>
                <div class="share ">
                    <h6><?php echo esc_html__('Share','pikoworks_core') ?>:</h6>
                    <ul>
                        <li><a href="javascript:void(0)" data-href="http://www.facebook.com/sharer.php?u=<?php echo get_permalink($post_id);?>"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="javascript:void(0)" data-href="https://twitter.com/home?status=<?php echo get_permalink($post_id);?>" ><i class="fa fa-twitter"></i></a></li>
                        <li><a href="javascript:void(0)" data-href="https://plus.google.com/share?url=<?php echo get_permalink($post_id);?>"><i class="fa fa-google-plus"></i></a></li>
                    </ul>
                </div>
                <?php endif; ?>
            </div>
        </div>
</article>