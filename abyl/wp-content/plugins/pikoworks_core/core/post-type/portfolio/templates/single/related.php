<?php
/**
 * @author  themepiko
 */
$index=0;
$column = 4;
$image_size = '570x460';
$show_pagging = '2';
$item = 4;

$args = array(
    'post__not_in' => array($post_id),
    'posts_per_page'   => -1,
    'orderby'			=> 'rand',
    'post_type'        => PIKO_PORTFOLIO_POST_TYPE,
    'portfolio_category__in'    => $arrCatId,
    'post_status'      => 'publish'
);
$posts_array = new WP_Query( $args );
$total_post = $posts_array->found_posts;
$data_plugin_options = $owl_carousel_class = '';


if ($total_post / $item > 1) {
    $owl_carousel_class = 'owl-carousel';
}
    $data_carousel_related = array(
        'autoplay'      => 'true',
        'loop'          => 'true',
        'autoheight'    => 'false',
        'nav'           => 'true',
        'dots'          => 'false',
        'margin'        => esc_attr($pikoworks['p_related_margin']),
    );
    $arr_related = array(
        '0' => array(
        "items" => esc_attr($pikoworks['p_related_items_mobile'])
    ), 
    '768' => array(
        "items" => esc_attr($pikoworks['p_related_items_tablet'])
    ), 
    '992' => array(
        "items" => esc_attr($pikoworks['p_related_items_desktop'])
    ),
    '1200' => array(
        "items" => esc_attr($pikoworks['p_related_items_large_device'])
    )
    );
    $data_responsive = json_encode($arr_related);
    $data_carousel_related["responsive"] = $data_responsive;

    if( ( $posts_array->post_count <= 1  ) ){
        $data_carousel_related['loop'] = 'false';
    }
    
    

global $pikoworks;
$overlay_style = 'icon';
$column = 'piko-col-md-4';
$overlay_zoom = 'piko-col-md-4';
if(isset($pikoworks['portfolio-related-overlay']))
    $overlay_style = $pikoworks['portfolio-related-overlay'];
if(isset($pikoworks['portfolio-related-column']))
    $column = 'piko-col-md-'.$pikoworks['portfolio-related-column'];
if(isset($pikoworks['portfolio-related-overlay-zoom']))
    $overlay_zoom = 'poverlay-style-2';
$layout = 'title';
if(isset($pikoworks['portfolio_related_style']) && $pikoworks['portfolio_related_style']!='' ){
    $layout = $pikoworks['portfolio_related_style'];
}
if ($overlay_style == 'left-title-excerpt-link'){
    $overlay_align = 'hover-align-left';
}else{
    $overlay_align = 'hover-align-center';
}
?>

<div class="container">
    <div class="related-post">
            <div class="related-title">
                <h3 class="title-relate"><?php echo esc_html__('Related Projects','pikoworks-core'); ?></h3>
            </div>
        <div class="owl-carousel owl-nav-show portfolio-wrapper <?php echo esc_attr($overlay_zoom); ?>" <?php echo  _data_carousel( $data_carousel_related ); ?>>
            <?php


            while ( $posts_array->have_posts() ) : $posts_array->the_post();
                $index++;
                $permalink = get_permalink();
                $title_post = get_the_title();
                $terms = wp_get_post_terms( get_the_ID(), array( PIKO_PORTFOLIO_CATEGORY_TAXONOMY));
                $cat = $cat_filter = '';
                foreach ( $terms as $term ){
                    $cat_filter .= preg_replace('/\s+/', '', $term->name) .' ';
                    $cat .= $term->name.', ';
                }
                $cat = rtrim($cat,', ');
                
                $is_directioanl = '';

                ?>
                <?php include(PIKO_PORTFOLIO_DIR_PATH.'/templates/loop/'.$layout.'-item.php');
                ?>
            <?php
            endwhile;
            wp_reset_postdata();
            ?>
        </div>
    </div>
</div>