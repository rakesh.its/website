<?php
/**
 * @author  Themepiko
 */
$disable_link = isset( $GLOBALS['pikoworks']['portfolio_disable_link_detail'] ) ? $GLOBALS['pikoworks']['portfolio_disable_link_detail'] : '1';

?>
<figure class="entry-thumbnail">
    <img width="<?php echo esc_attr($width) ?>" height="<?php echo esc_attr($height) ?>" src="<?php echo esc_url($thumbnail_url) ?>" alt="<?php echo get_the_title() ?>"/>
    <div class="entry-thumbnail-hover">
        <div class="entry-hover-wrapper">
            <div class="entry-hover-inner">
                <?php if ($disable_link):?>                    
                     <h5><a href="<?php echo get_permalink(get_the_ID()) ?>"><?php the_title() ?></a></h5>
                <?php else:?>
                   <span class="link-button">
                        <a class="link"  href="<?php echo get_permalink(get_the_ID()) ?>" title="<?php echo get_the_title() ?>">
                              <span class="lnr lnr-link" aria-hidden="true"></span>
                         </a>
                        <a class="view-gallery"  href="<?php echo esc_url($url_origin) ?>" data-rel="prettyPhoto[pp_gal_<?php echo get_the_ID() ?>]"  title="<?php echo get_the_title() ?>">
                            <span class="lnr lnr-magnifier" aria-hidden="true"></span>
                        </a>
                    </span>
                    <h5><?php the_title() ?></h5>
                <?php endif; ?>
            </div>
        </div>
    </div>
</figure>