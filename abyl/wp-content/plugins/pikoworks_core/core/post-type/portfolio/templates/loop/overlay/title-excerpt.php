<?php
/**
 * @author  Themepiko
 */
$disable_link = isset( $GLOBALS['pikoworks']['portfolio_disable_link_detail'] ) ? $GLOBALS['pikoworks']['portfolio_disable_link_detail'] : '1';

?>
<figure class="entry-thumbnail">
    <img width="<?php echo esc_attr($width) ?>" height="<?php echo esc_attr($height) ?>"
         src="<?php echo esc_url($thumbnail_url) ?>" alt="<?php echo get_the_title() ?>"/>
    <div class="entry-thumbnail-hover">
        <div class="entry-hover-wrapper">
            <div class="entry-hover-inner">
                <?php if ( $disable_link =='1'){?>
                    <h5><?php the_title() ?></h5>
                <?php } else{?>
                    <h5><a href="<?php echo get_permalink(get_the_ID()) ?>" class="title"><?php the_title() ?></a></h5>
                <?php }?>
                <div class="excerpt-wrap">
                    <span class="excerpt">
                       <?php echo wp_trim_words( get_the_content(), esc_attr( $excerpt ), ' '); ?>
                    </span>
                </div>
            </div>
        </div>
    </div>
</figure>