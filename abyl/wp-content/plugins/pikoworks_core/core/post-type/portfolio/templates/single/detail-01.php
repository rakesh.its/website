<?php
/**
 * @author  themepiko
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
            <div class="row">
                <div class="col-md-12">                    
                    <div class="owl-wrap">
                        <?php if($embaded!=''):
                            echo '<div id="res_videos" class="embed-responsive embed-responsive-16by9">' . $embaded . '</div>';                         
                        elseif(count($meta_values) > 0): ?>
                        <div class="owl-carousel <?php echo esc_attr( $single_animation ) ?>" <?php echo  _data_carousel( $data_carousel ); ?>>
                        <?php
                            $index = 0;
                            foreach($meta_values as $image):
                                $urls = wp_get_attachment_image_src($image,'full');
                                $img = '';
                                if(count($urls)>0){
                                    $resize = matthewruddy_image_resize($urls[0],960,375);
                                    if($resize!=null && is_array($resize) )
                                        $img = $resize['url'];
                                }
                                ?>
                                <figure>
                                    <img alt="<?php the_title_attribute(); ?>" src="<?php echo esc_url($img) ?>" />
                                </figure>
                            <?php endforeach; ?>
                        </div>  <!--post-slideshow wrap-->
                        <?php    
                        else:  
                            if(count($imgThumbs)>0) :?>
                            <figure>
                                <img class="single-img" alt="<?php the_title_attribute(); ?>" src="<?php echo esc_url($imgThumbs[0])?>" />
                            </figure>
                        <?php endif;
                        endif; // meta value
                        ?>
                    </div>
                </div>
            </div>
                
            <div class="clearfix">
            <div class="single-content-wrap">                                
                <div class="col-md-9">                    
                    <h3 class="content-title"><?php echo esc_html__('Project Description','pikoworks_core') ?></h3>
                    <div class="content-info">
                        <?php the_content() ?>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="portfolio-info padding-top">
                        <?php
                        $meta = get_post_meta(get_the_ID(), 'portfolio_custom_fields', TRUE);
                        if(isset($meta) && is_array($meta) && count($meta['portfolio_custom_fields'])>0){
                            for($i=0; $i<count($meta['portfolio_custom_fields']);$i++){
                        ?>
                        <div class="portfolio-info-box">
                            <h6><?php echo wp_kses_post($meta['portfolio_custom_fields'][$i]['custom-field-title']) ?>: </h6>
                            <div class="portfolio-meta"><?php echo wp_kses_post($meta['portfolio_custom_fields'][$i]['custom-field-description']) ?></div>
                        </div>
                        <?php }
                        }
                        ?>
                        <div class="portfolio-info-box">
                            <h6><?php echo esc_html__('Date','pikoworks_core') ?>: </h6>
                            <div class="portfolio-meta"><?php echo date(get_option('date_format'),strtotime($post->post_date)) ?></div>
                        </div>
                        <div class="portfolio-info-box">
                            <h6><?php echo esc_html__('Category','pikoworks_core') ?>: </h6>
                            <div class="portfolio-meta category"><?php echo wp_kses_post($cat); ?></div>
                        </div>
                        <?php if($client!=''): ?>
                        <div class="portfolio-info-box">
                            <h6><?php echo esc_html__('Client','pikoworks_core') ?>: </h6>
                            <div class="portfolio-meta"><?php echo esc_html($client) ?></div>
                        </div>
                        <?php endif; ?>
                       <?php if($GLOBALS['pikoworks']['optn_portfolio_social_shear']== '1'): ?>
                        <div class="share ">
                            <h6><?php echo esc_html__('Share','pikoworks_core') ?>:</h6>
                            <ul>
                                <li><a href="javascript:void(0)" data-href="http://www.facebook.com/sharer.php?u=<?php echo get_permalink($post_id);?>"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="javascript:void(0)" data-href="https://twitter.com/home?status=<?php echo get_permalink($post_id);?>" ><i class="fa fa-twitter"></i></a></li>
                                <li><a href="javascript:void(0)" data-href="https://plus.google.com/share?url=<?php echo get_permalink($post_id);?>"><i class="fa fa-google-plus"></i></a></li>
                            </ul>
                        </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
</article>