<?php
/**
 * Custom post testimonial
 * @author themepiko
 */
if( !defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}
global $pikoworks;
if (!class_exists('piko_Shortcode_testimonial')) {
    class piko_Shortcode_testimonial
    {
        function __construct()
        {
            add_action('init', array($this, 'register_taxonomies'), 5);
            add_action('init', array($this, 'register_post_types'), 5);
            add_shortcode('piko_testimonial', array($this, 'testimonial_shortcode'));
            add_filter('rwmb_meta_boxes', array($this, 'register_meta_boxes'));
            if (is_admin()) {
                add_filter('manage_edit-testimonial_columns', array($this, 'add_columns'));
                add_action('manage_testimonial_posts_custom_column', array($this, 'set_columns_value'), 10, 2);
                add_action('admin_menu', array($this, 'addMenuChangeSlug'));
            }
        }

        function register_taxonomies()
        {
            if (taxonomy_exists('testimonial_category')) {
                return;
            }

            $post_type = 'testimonial';
            $taxonomy_slug = 'testimonial_category';
            $taxonomy_name = 'Testimonial Categories';

            $post_type_slug = get_option('piko-' . $post_type . '-config');
            if (isset($post_type_slug) && is_array($post_type_slug) &&
                array_key_exists('taxonomy_slug', $post_type_slug) && $post_type_slug['taxonomy_slug'] != ''
            ) {
                $taxonomy_slug = $post_type_slug['taxonomy_slug'];
                $taxonomy_name = $post_type_slug['taxonomy_name'];
            }
            register_taxonomy('testimonial_category', 'testimonial',
                array('hierarchical' => true,
                    'label' => $taxonomy_name,
                    'query_var' => true,
                    'rewrite' => array('slug' => $taxonomy_slug))
            );
            flush_rewrite_rules();
        }

        function register_post_types()
        {
            $post_type = 'testimonial';

            if (post_type_exists($post_type)) {
                return;
            }

            $post_type_slug = get_option('piko-' . $post_type . '-config');
            if (!isset($post_type_slug) || !is_array($post_type_slug)) {
                $slug = 'testimonial';
                $name = $singular_name = 'Testimonial';
            } else {
                $slug = $post_type_slug['slug'];
                $name = $post_type_slug['name'];
                $singular_name = $post_type_slug['singular_name'];
            }

            register_post_type($post_type,
                array(
                    'label' => esc_html__('Testimonial', 'pikoworks_core'),
                    'description' => esc_html__('Testimonial Description', 'pikoworks_core'),
                    'labels' => array(
                        'name' => $name,
                        'singular_name' => esc_html($singular_name),
                        'menu_name' => esc_html__($name, 'pikoworks_core'),
                        'parent_item_colon' => esc_html__('Parent Item:', 'pikoworks_core'),
                        'all_items' => esc_html__(sprintf('All %s', $name), 'pikoworks_core'),
                        'view_item' => esc_html__('View Item', 'pikoworks_core'),
                        'add_new_item' => esc_html__(sprintf('Add New  %s', $name), 'pikoworks_core'),
                        'add_new' => esc_html__('Add New', 'pikoworks_core'),
                        'edit_item' => esc_html__('Edit Item', 'pikoworks_core'),
                        'update_item' => esc_html__('Update Item', 'pikoworks_core'),
                        'search_items' => esc_html__('Search Item', 'pikoworks_core'),
                        'not_found' => esc_html__('Not found', 'pikoworks_core'),
                        'not_found_in_trash' => esc_html__('Not found in Trash', 'pikoworks_core'),
                    ),
                    'supports' => array('title', 'excerpt', 'thumbnail', 'revisions'),
                    'public' => true,
                    'show_ui' => true,
                    '_builtin' => false,
                    'has_archive' => true,
                    'rewrite' => array('slug' => $slug, 'with_front' => true),
                    'menu_icon' => 'dashicons-format-quote',
                )
            );
            flush_rewrite_rules();
        }

        function addMenuChangeSlug()
        {
            add_submenu_page('edit.php?post_type=testimonial', 'Setting', 'Settings', 'edit_posts', wp_basename(__FILE__), array($this, 'initPageSettings'));
        }

        function initPageSettings()
        {
            $template_path = ABSPATH . 'wp-content/plugins/pikoworks_core/core/post-type/posttype-settings/settings.php';
            if (file_exists($template_path))
                require_once $template_path;
        }

        function add_columns($columns)
        {
            unset(
                $columns['cb'],
                $columns['title'],
                $columns['date']
            );
            $cols = array_merge(array('cb' => ('')), $columns);
            $cols = array_merge($cols, array('title' => esc_html__('Name', 'pikoworks_core')));
            $cols = array_merge($cols, array('job' => esc_html__('Company Name', 'pikoworks_core')));
            $cols = array_merge($cols, array('thumbnail' => esc_html__('Client Picture', 'pikoworks_core')));
            $cols = array_merge($cols, array('date' => esc_html__('Comment Date', 'pikoworks_core')));
            return $cols;
        }

        function set_columns_value($column, $post_id)
        {
            switch ($column) {
                case 'id': {
                    echo wp_kses_post($post_id);
                    break;
                }
                case 'job': {
                    echo get_post_meta($post_id, 'job', true);
                    break;
                }
                case 'thumbnail': {
                    echo get_the_post_thumbnail($post_id, 'thumbnail');
                    break;
                }                
            }
        }

        function register_meta_boxes($meta_boxes)
        {
            $prefix = 'piko_';
            global $meta_boxes;
            $meta_boxes[] = array(
                'title' => esc_html__('Testimonial Details', 'pikoworks_core'),
                'pages' => array('testimonial'),
                'fields' => array(
                     
                    array(
                        'name' => esc_html__('Designation', 'pikoworks_core'),
                        'id' => $prefix .'company_designation',
                        'type' => 'text',
                    ),
                    array(
                        'name' => esc_html__('Company Name', 'pikoworks_core'),
                        'id' => 'job',
                        'type' => 'text',
                    ),                                      
                    array(
                        'name' => esc_html__('Company Url', 'pikoworks_core'),
                        'desc' => esc_html__('Add company website', 'pikoworks_core'),
                        'id' => $prefix .'company_urls',
                        'type' => 'text',
                        'validation' => 'url',
                    ),
                )
            );
            return $meta_boxes;
        }

        function testimonial_shortcode($atts)
        {
            
            extract(shortcode_atts(array(
            'type' => 'style1',
            'number'        => 7,
            'category'        => '',
            'order' => 'DESC',
            'orderby' => 'date',
            'excerpt'   => '55',            
            'show_company_link' => 'no',
            'el_class' => '',
            //Carousel            
            'autoplay'      => "false",
            'navigation'    => "false",
            'navigation_btn'    => "owl-nav-show-inner",
            'slidespeed'    => 250,
            'loop'          => "false",
            'dots'         => "false",
            'margin'        => 30,                 
            'is_client_image' => '',                 
            //Default
            'use_responsive' => 1,
            'sanam' => '',
            'items_large_device'   => 4,
            'items_desktop'   => 3,
            'items_tablet'   => 2,
            'items_mobile'   => 1,
            'css'           => '',
                
                
            ), $atts));            
            global $meta;            
            $args = array(
                'post_type' => 'testimonial',                
                'post_status' => 'publish',
                'orderby' => esc_attr( $orderby ),
                'order'   => esc_attr( $order ),
                'posts_per_page' => esc_attr( $number ),                
            );
            if ($category != '') {
                $args['tax_query'] = array(
                    array(
                        'taxonomy' => 'testimonial_category',
                        'field' => 'slug',
                        'terms' => explode(',', $category),
                        'operator' => 'IN'
                    )
                );
            }
            
            $query = new WP_Query($args);
            
//            $min_suffix_css = (isset($marin['enable_minifile_css']) && $marin['enable_minifile_css'] == 1) ? '.min' : '';
//            wp_enqueue_style('marin_testimonial_css', PIKOWORKSCORE_CORE_URL . 'post-type/testimonial/assets/css/testimonial' . $min_suffix_css . '.css', array(), false);
           
            ob_start();
            if ( $query->have_posts() ) : 
            $css_class = 'testimonial-wrap '  . $el_class . ' ' . $type . ' ' . $css;
            if ( function_exists( 'vc_shortcode_custom_css_class' ) ):
                $css_class .= ' ' . apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), '', $atts );
            endif;
            
            
            $single_animation = '';
            if($use_responsive == '0'){
                $single_animation = $sanam;
            }
            
            $data_carousel = array(                
                "autoplay"      => esc_html($autoplay),
                "navigation"    => esc_html($navigation),
                "margin"        => esc_html($margin),
                "smartSpeed"    => esc_html($slidespeed),
                "loop"          => esc_html($loop),
                "autoheight"    => "false",
                'nav'           => esc_html($navigation),
                'dots'          => esc_html($dots),
            );                     
            if( $use_responsive == '1' ) {
                $arr = array(
                    '0' => array(
                        "items" => esc_html($items_mobile)
                    ), 
                    '768' => array(
                        "items" => esc_html($items_tablet)
                    ), 
                    '992' => array(
                        "items" => esc_html($items_desktop)
                    ),
                    '1200' => array(
                        "items" => esc_html($items_large_device)
                    )
                );
                $data_responsive = json_encode($arr);
                $data_carousel["responsive"] = $data_responsive;
                
                if( ( $query->post_count <= 1  ) ){
                    $data_carousel['loop'] = 'false';
                }
                
            }else{
                $data_carousel['items'] =  1;
                
            }
            $owl_data_option = '';
            $owl_class = '';             
           
            $owl_class = 'owl-carousel' . ' ' . $single_animation . ' ' .$navigation_btn;
            $owl_data_option = _data_carousel( $data_carousel );
              
                 
            
            ?>
            <div class="<?php echo esc_attr( $css_class ) ?>">
                <div class="<?php echo esc_attr( $owl_class ) ?>" <?php echo  $owl_data_option; ?>>                  
                    
                <?php 
                    
                
                    while ( $query->have_posts() ) : $query->the_post(); 

                        $prefix = 'piko_';
                        $job = get_post_meta(get_the_ID(), 'job', true);
                        $designation = get_post_meta(get_the_ID(), $prefix .'company_designation', true);
                        $urls = get_post_meta(get_the_ID(), $prefix .'company_urls', true);

                        
                        $plugin_path = untrailingslashit(plugin_dir_path(__FILE__));
                        switch ($type) {
                            case 'style3':{
                                include($plugin_path . '/templates/style3.php');
                                break;
                            }
                            case 'style2':
                            {
                                include($plugin_path . '/templates/style2.php');
                                break;
                            }
                            default:
                            {
                                include($plugin_path . '/templates/style1.php');
                            }
                        }
                    ?>        
                 <?php  endwhile; // end of the loop. ?>                 
                </div>                
            </div>                
           <?php   
            endif; //have query post   
            wp_reset_postdata();
            $content = ob_get_clean();
            return $content;
        }
    }
    new piko_Shortcode_testimonial();
}