<?php
/**
 * The template part for displaying content
 */
?> 

<article class="testimonial-3">    
    <div class="comment">        
        <h6><?php the_title(); ?></h6>
        <span class="designation"><?php echo wp_kses_post($designation); ?></span>
        <?php if ( $show_company_link == 'yes' ) : ?>
            - <a href="<?php echo esc_url($urls); ?>" target="_blank"><?php echo wp_kses_post($job); ?></a>
        <?php endif; ?><br>
        <div class="round-wrap">
          <span class="round" aria-hidden="true"></span>
          <span class="icon lnr lnr-arrow-down" aria-hidden="true"></span>
        </div>
    </div>
    <div class="comment-text">        
        <?php
        echo '<p>'. wp_trim_words( get_the_excerpt(), esc_attr($excerpt) ) . '</p>';
        ?>
    </div>
</article>

