<?php
/**
 * The template part for displaying content
 */
?>
<article class="testimonial-2">
    
    <div class="comment-image">
        <div class="box">
            <?php  if($is_client_image == 'yes'): ?>
            <?php the_post_thumbnail(array(100,100)); ?>
            <?php else: ?>
            <span class="lnr lnr-smile" aria-hidden="true"></span>
           <?php endif;?>            
        </div>
        
        <span class="icon-2 lnr lnr-arrow-down" aria-hidden="true"></span>
    </div>
    
    <div class="comment-text">        
        <?php
        echo '<p>'. wp_trim_words( get_the_excerpt(), esc_attr($excerpt) ) . '</p>';
        ?>
    </div>
    <div class="comment">        
        <h6><?php the_title(); ?></h6>
        <span class="designation"><?php echo wp_kses_post($designation); ?></span>
        <?php if ( $show_company_link == 'yes' ) : ?>
            - <a href="<?php echo esc_url($urls); ?>" target="_blank"><?php echo wp_kses_post($job); ?></a>
        <?php endif; ?><br>          
    </div>
</article>


