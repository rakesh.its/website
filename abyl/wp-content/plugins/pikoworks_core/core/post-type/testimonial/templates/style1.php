<?php
/**
 * The template part for displaying content
 */
?>
<div class="testimonial-1">
    <div class="testimonial">
        <span class="quote"><i class="fa fa-quote-left" aria-hidden="true"></i></span>
        <?php echo '<p>'. wp_trim_words( get_the_excerpt(), esc_attr($excerpt) ) . '</p>'; ?>
    </div>
    <div class="testimonial-name">
        <p><?php the_post_thumbnail('', array('class' => 'img-circle')); ?></p>
        <p><span><?php the_title(); ?> </span><br>
         <span class="designation"><?php echo wp_kses_post($designation); ?></span>
         <?php if ( $show_company_link == 'yes' ) : ?>
            - <a href="<?php echo esc_url($urls); ?>" target="_blank"><?php echo wp_kses_post($job); ?></a>
        <?php endif; ?>
        <br><span class="rating">
        <i class="fa fa-star" aria-hidden="true"></i>
        <i class="fa fa-star" aria-hidden="true"></i>
        <i class="fa fa-star" aria-hidden="true"></i>
        <i class="fa fa-star" aria-hidden="true"></i>
        <i class="fa fa-star" aria-hidden="true"></i>
        </span>
        </p>
    </div>                            
</div>



