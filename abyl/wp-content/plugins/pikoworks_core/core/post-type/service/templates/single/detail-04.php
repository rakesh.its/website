<?php
/**
 * @author  themepiko
 */
$prefix = 'piko_';
$chosen_sidebar =  get_post_meta($post_id, $prefix .'pikoworks_detail_sidebar',true);
if (!isset($chosen_sidebar) || $detail_style == 'none' || $chosen_sidebar == '') {
    $chosen_sidebar = $GLOBALS['pikoworks']['service_single_sidebar'];
}


$sidebar_position = isset( $GLOBALS['pikoworks']['service_single_sidebar_pos'] ) ? trim( $GLOBALS['pikoworks']['service_single_sidebar_pos'] ) : 'right';
$secondary_class = pikoworks_secondary_single_service_class();
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="row">
        <div class="col-xs-12">
            <?php the_content(); ?>
        </div>
    </div>    
</article>
<?php if ( $sidebar_position != 'fullwidth' ): ?>
	<aside id="secondary" class="widget-area <?php echo esc_attr( $secondary_class ); ?>" role="complementary">
		<?php dynamic_sidebar( $chosen_sidebar ); ?>
	</aside><!-- .sidebar .widget-area -->
<?php endif; ?>
