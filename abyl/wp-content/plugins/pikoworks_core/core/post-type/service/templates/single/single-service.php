<?php
get_header();
global $pikoworks;
$prefix = 'piko_';
$min_suffix = (isset($pikoworks['enable_minifile']) && $pikoworks['enable_minifile'] == 1) ? '.min' : '';
//wp_enqueue_style('style', get_template_directory_uri() . 'style'.$min_suffix.'.css', array(),false);

$enable_carousel = rwmb_meta($prefix . 'enable_carousel', array());

$autoplay = isset($pikoworks['lb_autoplay']) ? $pikoworks['lb_autoplay'] : true;
$navigation = isset($pikoworks['lb_navigation']) ? $pikoworks['lb_navigation'] : true;
$dots = isset($pikoworks['lb_dots']) ? $pikoworks['lb_dots'] : false;
$margin = isset($pikoworks['lb_margin']) ? $pikoworks['lb_margin'] : '';
$loop = isset($pikoworks['lb_loop']) ? $pikoworks['lb_loop'] : false;
$slidespeed = isset($pikoworks['lb_slidespeed']) ? $pikoworks['lb_slidespeed'] : '250';
$use_responsive = isset($pikoworks['lb_use_responsive']) ? $pikoworks['lb_use_responsive'] : '0';

$items_large_device = isset($pikoworks['lb_items_large_device']) ? $pikoworks['lb_items_large_device'] : '4';
$items_desktop = isset($pikoworks['lb_items_desktop']) ? $pikoworks['lb_items_desktop'] : '3';
$items_tablet = isset($pikoworks['lb_items_tablet']) ? $pikoworks['lb_items_tablet'] : '2';
$items_mobile = isset($pikoworks['lb_items_mobile']) ? $pikoworks['lb_items_mobile'] : '1';


//carousel setting
$owl_data_option = '';
$single_animation = '';
$data_carousel = array(
        "autoplay"      => esc_html($autoplay),
        "margin"        => esc_html($margin),
        "smartSpeed"    => esc_html($slidespeed),
        "loop"          => esc_html($loop),
        "autoheight"    => "false",
        'nav'           => esc_html($navigation),
        'dots'          => esc_html($dots),
    );     
     if( $use_responsive == '1' ) {
        $arr = array(
            '0' => array(
                "items" => esc_html($items_mobile)
            ), 
            '768' => array(
                "items" => esc_html($items_tablet)
            ), 
            '992' => array(
                "items" => esc_html($items_desktop)
            ),
            '1200' => array(
                "items" => esc_html($items_large_device)
            )
        );
        $data_responsive = json_encode($arr);
        $data_carousel["responsive"] = $data_responsive;

        if( count(have_posts())>0 ){
            $data_carousel['loop'] = 'false';
        }

    }else{
        $data_carousel['items'] =  1;

    } 
    $navigation_btn = 'owl-nav-show-hover';
if($enable_carousel == '1'){
    $owl_data_option =  _data_carousel( $data_carousel );
    $single_animation = 'owl-carousel ' . $pikoworks['lb_sanam'] . ' ' . $navigation_btn;
}

if ( have_posts() ) {    
    // Start the Loop.
    while ( have_posts() ) : the_post();
        $post_id = get_the_ID();
        $enable_single_details = rwmb_meta($prefix . 'enable_single_details', array());        
        $first_img = get_post_meta( get_the_ID(), $prefix .'service_first_img', false );        
        $first_id = get_post_meta($post_id,  $prefix .'service_first_product_id', true );
        $first_desc = get_post_meta($post_id, $prefix .'service_first_desc', true );        
        $sec_enable = get_post_meta( get_the_ID(), $prefix .'enable_sec_img', true );        
        $sec_img = get_post_meta( get_the_ID(), $prefix .'service_sec_img', false );        
        $sec_id = get_post_meta($post_id,  $prefix .'service_sec_product_id', true );
        $sec_desc = get_post_meta($post_id, $prefix .'service_sec_desc', true );
        $btn_text = get_post_meta($post_id, $prefix .'service_btn_text', true );        
        $third_img = get_post_meta( get_the_ID(), $prefix .'service_third_img', false );        
        $third_id = get_post_meta($post_id,  $prefix .'service_third_product_id', true );
        $third_desc = get_post_meta($post_id, $prefix .'service_third_desc', true );         
        $detail_ids = get_post_meta($post_id,  $prefix .'service_details_product_id', true );
        
        $embaded =  wp_oembed_get(get_post_meta($post_id, $prefix .'service_video', true ));

        // product id
        if ( class_exists( 'YITH_WCQV_Frontend' ) ):
            $first_ids = $post->ID = $first_id;
            $sec_ids = $post->ID = $sec_id;
            $third_ids = $post->ID = $third_id;
            $details_ids = $post->ID = $detail_ids;
            $label = get_option( 'yith-wcqv-button-label' );
        endif; // YITH_WCQV_Frontend  
        
        $imgThumbs = wp_get_attachment_image_src(get_post_thumbnail_id($post_id),'full');          
          
        
        $detail_style =  get_post_meta($post_id, $prefix .'service_detail_style' ,true);
        if (!isset($detail_style) || $detail_style == 'none' || $detail_style == '') {
            $detail_style = $pikoworks['service-single-style'];
        }
        include_once(plugin_dir_path( __FILE__ ).'/'.$detail_style.'.php');
    endwhile;
    }
?>
<script type="text/javascript">
    (function($) {
        "use strict";
        $(document).ready(function(){
            $('a','.service-single .share').each(function(){
                $(this).click(function(){
                    var href = $(this).attr('data-href');
                    var leftPosition, topPosition;
                    var width = 400;
                    var height = 300;
                    var leftPosition = (window.screen.width / 2) - ((width / 2) + 20);
                    var topPosition = (window.screen.height / 2) - ((height / 2) + 100);
                    //Open the window.
                    window.open(href, "", "width=350, height=250,left=" + leftPosition + ",top=" + topPosition);
                })
            })
            $("a[rel^='prettyPhoto']").prettyPhoto(
                {
                    theme: 'light_rounded',
                    slideshow: 5000,
                    deeplinking: false,
                    social_tools: false
                });               
                
        });
    })(jQuery);
</script>

<?php get_footer(); ?>
