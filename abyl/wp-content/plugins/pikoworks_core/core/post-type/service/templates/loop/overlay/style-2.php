<?php
/**
 * @author  Themepiko
 */
$read_more_btn = '';
if($show_read_more_btn == 'yes'):
    $read_more_btn = pikoworks_read_more_link();
endif;
?>
<div class="row">
    <div class="col-md-4 col-sm-4 col-xs-4">
        <figure>
            <a href="<?php echo get_permalink(get_the_ID()) ?>">
                <img width="<?php echo esc_attr($width) ?>" height="<?php echo esc_attr($height) ?>" src="<?php echo esc_url($thumbnail_url) ?>" alt="<?php echo get_the_title() ?>"/>
            </a>
        </figure> 
    </div>
    <div class="col-md-8 col-sm-8 col-xs-8">
        <div class="block">
            <header>
                <h4><a href="<?php echo get_permalink(get_the_ID()) ?>"><?php echo get_the_title() ?></a></h4>
            </header>
            <?php 
                if ( ! has_excerpt() ) {
                    echo '<p>'. wp_trim_words( get_the_content(), esc_attr($excerpt), '...  ' ) .  $read_more_btn . '</p>';
                    } else { 
                        echo '<p>'. wp_trim_words( get_the_excerpt(), esc_attr($excerpt), '... ' ) . $read_more_btn . '</p>';
                    }
            ?>
        </div>
    </div>
</div>