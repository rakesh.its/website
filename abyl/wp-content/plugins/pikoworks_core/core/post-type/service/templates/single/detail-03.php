<?php
/**
 * @author  themepiko
 */
$prefix = 'piko_';
$chosen_sidebar =  get_post_meta($post_id, $prefix .'pikoworks_detail_sidebar',true);
if (!isset($chosen_sidebar) || $detail_style == 'none' || $chosen_sidebar == '') {
    $chosen_sidebar = $GLOBALS['pikoworks']['service_single_sidebar'];
}


$sidebar_position = isset( $GLOBALS['pikoworks']['service_single_sidebar_pos'] ) ? trim( $GLOBALS['pikoworks']['service_single_sidebar_pos'] ) : 'right';
$secondary_class = pikoworks_secondary_single_service_class();
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="row">        
        <div class="col-md-6 col-sm-6"> 
                <?php                       
               
                if($enable_carousel == '1' && count($sec_img) > 0 ): ?>
                    <div class="slider-wrap">
                        <div class="<?php echo esc_attr( $single_animation ) ?>" <?php echo $owl_data_option ?>>
                            <?php
                            $index = 0;
                            foreach($sec_img as $image):
                                $urls2 = wp_get_attachment_image_src($image,'full'); 
                                ?>
                                <a href="<?php echo esc_url($urls2[0]) ?>" title="<?php echo get_the_title() ?>" data-rel="prettyPhoto[pp_gal_<?php echo get_the_ID() ?>]">
                                    <img alt="<?php the_title_attribute(); ?>" src="<?php echo esc_url($urls2[0]) ?>" />
                                </a> 
                            <?php endforeach; ?>
                        </div>                        
                    </div>
                    <?php
                    else:                                
                     if(count($sec_img) > 0): ?>
                            <?php
                            $index = 0;
                            foreach($sec_img as $image):
                                $urls2 = wp_get_attachment_image_src($image,'full'); 
                                ?>
                                <figure class="image-wrap">
                                    <a href="<?php echo esc_url($urls2[0]) ?>" data-rel="prettyPhoto[pp_gal_<?php echo get_the_ID() ?>]"  title="<?php echo get_the_title() ?>">
                                        <img alt="<?php the_title_attribute(); ?>" src="<?php echo esc_url($urls2[0]) ?>" />
                                    </a>                                    
                                </figure>
                            <?php endforeach; ?>                           
                        <?php
                        endif; // sec img                               

                endif; //enable slider ?>        
        </div>
        <div class="col-md-6 col-sm-6">             
            <?php
                if($embaded !=''){
                    echo '<div id="piko_videos" class="embed-responsive embed-responsive-16by9">' . $embaded . '</div>'; 
                }
            ?> 
        </div>
        <div class="col-xs-12">
            <?php the_content(); ?>
        </div>
    </div>    
</article>
<?php if ( $sidebar_position != 'fullwidth' ): ?>
	<aside id="secondary" class="widget-area <?php echo esc_attr( $secondary_class ); ?>" role="complementary">
		<?php dynamic_sidebar( $chosen_sidebar ); ?>
	</aside><!-- .sidebar .widget-area -->
<?php endif; ?>
