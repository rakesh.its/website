<?php
/**
 * @author  Themepiko
 */
$read_more_btn = '';
if($show_read_more_btn == 'yes'):
    $read_more_btn = pikoworks_read_more_link();
endif;
?>
<figure>
    <?php if($image_overlay){ echo '<div class="entry-thumbnail">';} ?>
    <a href="<?php echo get_permalink(get_the_ID()) ?>">
        <img width="<?php echo esc_attr($width) ?>" height="<?php echo esc_attr($height) ?>" src="<?php echo esc_url($thumbnail_url) ?>" alt="<?php echo get_the_title() ?>"/>
    </a>
    <?php if($image_overlay){ echo '</div>';} ?>
</figure>    
<div class="block">
    <header>
        <h4><a href="<?php echo get_permalink(get_the_ID()) ?>"><?php echo get_the_title() ?></a></h4>
    </header>
    <?php 
        if ( ! has_excerpt() ) {
            echo '<p>'. wp_trim_words( get_the_content(), esc_attr($excerpt), '...  ' ) .  $read_more_btn . '</p>';
            } else { 
                echo '<p>'. wp_trim_words( get_the_excerpt(), esc_attr($excerpt), '... ' ) . $read_more_btn . '</p>';
            }
    ?>
</div>

