<?php
/**
 * Custom post service
 * @author themepiko
 */

$args = array(
    'offset' => $offset,
    'orderby' => 'post__in',
    'post__in' => explode(",", $service_ids),
    'posts_per_page' => $post_per_page,
    'post_type' => PIKOWORKS_SERVICE_POST_TYPE,
    'post_status' => 'publish');

if ($data_source == '') {
    $args = array(
        'offset' => $offset,
        'posts_per_page' => $post_per_page,
        'orderby' => 'post_date',
        'order' => $order,
        'post_type' => PIKOWORKS_SERVICE_POST_TYPE,
        PIKOWORKS_SERVICE_CATEGORY_TAXONOMY => strtolower($category),
        'post_status' => 'publish');
}

$posts_array = new WP_Query($args);
$total_post = $posts_array->found_posts;
$col_class = '';
?>

    <div class="<?php echo esc_attr( $css_class) ?>">        
        <?php        
        $data_carousel_service = array(
                "autoplay"      => $autoplay,
                "navigation"    => $navigation,
                "margin"        => $margin,
                "smartSpeed"    => $slidespeed,
                "loop"          => $loop,
                "autoheight"    => "false",
                'nav'           => $navigation,
                'dots'          => $dots,
            );
            $arr = array(
                '0' => array(
                    "items" => $items_mobile
                ), 
                '768' => array(
                    "items" => $items_tablet
                ), 
                '992' => array(
                    "items" => $items_desktop
                ),
                '1200' => array(
                    "items" => $items_large_device
                )
            );
            $data_responsive_service = json_encode($arr);
            $data_carousel_service["responsive"] = $data_responsive_service;

            if( ( $posts_array->post_count <= 1  ) ){
                $data_carousel_service['loop'] = 'false';
            }
            $owl_class = '';
            if($show_pagging == '2'){
                $owl_class = 'owl-carousel' . ' ' . $navigation_btn;
                $owl_data_option = _data_carousel( $data_carousel_service );
                $padding = '';
            }else{
                if($overlay_style !== 'style-2'){
                    $col_class = 'piko-col-md-' . $column . ' columns-' .$column;
                }                
                $owl_data_option = '';
            }
        
        ?>
        <div class="portfolio-wrapper <?php echo esc_attr( sprintf('%s %s %s %s %s', $col_class, $padding, $layout_type, $owl_class, $is_content_center) )?>" <?php echo $owl_data_option ?> >
            <?php
            $index = 0;

            while ($posts_array->have_posts()) : $posts_array->the_post();
                $index++;
                $permalink = get_permalink();
                $title_post = get_the_title();
                $terms = wp_get_post_terms(get_the_ID(), array(PIKOWORKS_SERVICE_CATEGORY_TAXONOMY));
                $cat = $cat_filter = '';
                foreach ($terms as $term) {
                    $cat_filter .= $term->slug . ' ';
                    $cat .= $term->name . ', ';
                }
                $cat = rtrim($cat, ', ');

                ?>

                <?php
                include(plugin_dir_path(__FILE__) . '/loop/grid-item.php');
                ?>
            <?php
            endwhile;
            wp_reset_postdata();
            ?>
        </div>
    </div>

