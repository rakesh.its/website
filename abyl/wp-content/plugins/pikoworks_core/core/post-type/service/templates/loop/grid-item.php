<div class="portfolio-item">

    <?php
    $post_thumbnail_id = get_post_thumbnail_id(  get_the_ID() );
    $arrImages = wp_get_attachment_image_src( $post_thumbnail_id, 'full' );
    $width = 360;
    $height = 202;
    if(isset($image_size) && $image_size=='590x393')
    {
        $width = 590;
        $height = 393;
    }
    if(isset($image_size) && $image_size=='200x140')
    {
        $width = 200;
        $height = 140;
    }
    if(isset($image_size) &&  $image_size=='570x460')
    {
        $width = 570;
        $height = 460;
    }
    if(isset($image_size) &&  $image_size=='585x585')
    {
        $width = 585;
        $height = 585;
    }
    $thumbnail_url = '';
    if(count($arrImages)>0){
        $resize = matthewruddy_image_resize($arrImages[0],$width,$height);
        if($resize!=null && is_array($resize) )
            $thumbnail_url = $resize['url'];
    }

    $url_origin = $arrImages[0];
    include(plugin_dir_path( __FILE__ ).'/overlay/'.$overlay_style.'.php');
    ?>
</div>
