<div class="outteam_meta_control">
    <p style="float:right">
        <a href="#" class="dodelete-ourteam button">Remove All</a>
    </p>
    <div style="clear: both;margin-bottom: 15px;" ></div>
    <?php while($mb->have_fields_and_multi('ourteam',array('length' => 2, 'limit' => 100))): ?>
        <?php $mb->the_group_open(); ?>
        <a href="#" class="dodelete button">Remove</a>
        <?php $mb->the_field('years'); ?>
        <label><?php esc_html_e('Experience Years', 'pikoworks_core'); ?></label>
        <input class="form-control" type="text" name="<?php esc_attr($mb->the_name()); ?>" value="<?php esc_attr($mb->the_value()); ?>"/>
        <span><?php esc_html_e('Example: 2017-2016', 'pikoworks_core') ;?></span>
        <?php $mb->the_field('company'); ?>
        <label><?php esc_html_e('Company Name', 'pikoworks_core'); ?></label>
        <input class="form-control" type="text" name="<?php esc_attr($mb->the_name()); ?>" value="<?php esc_attr($mb->the_value()); ?>"/>

        <?php $mb->the_field('position'); ?>
        <label><?php esc_html_e('Degination', 'pikoworks_core'); ?></label>
        <input class="form-control input-icon" type="text" name="<?php esc_attr($mb->the_name()); ?>" value="<?php esc_attr($mb->the_value()); ?>"/>
        <span><?php esc_html_e('example: CEO', 'pikoworks_core') ;?> </span>
        
        <?php $mb->the_field('responsible'); ?>
        <label><?php esc_html_e('Responsibility', 'pikoworks_core'); ?></label>
        <input class="form-control input-icon" type="text" name="<?php esc_attr($mb->the_name()); ?>" value="<?php esc_attr($mb->the_value()); ?>"/>
        <span><?php esc_html_e('example: Short description', 'pikoworks_core') ;?> </span>
        <?php $mb->the_group_close(); ?>
    <?php endwhile; ?>
    <div style="clear: both;"></div>
    <p>
        <a href="#" class="docopy-ourteam button"><?php esc_html_e('Add More', 'pikoworks_core'); ?></a>
    </p>
</div>