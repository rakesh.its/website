<div class="outteam_meta_control">
    <p style="float:right">
        <a href="#" class="dodelete-ourteam button">Remove All</a>
    </p>
    <div style="clear: both;margin-bottom: 15px;" ></div>
    <?php while($mb->have_fields_and_multi('ourteam',array('length' => 2, 'limit' => 100))): ?>
        <?php $mb->the_group_open(); ?>
        <a href="#" class="dodelete button">Remove</a>
        <?php $mb->the_field('aboutTitle'); ?>
        <label><?php esc_html_e('Name', 'pikoworks_core'); ?></label>
        <input class="form-control" type="text" name="<?php esc_attr($mb->the_name()); ?>" value="<?php esc_attr($mb->the_value()); ?>"/>
        <span><?php esc_html_e('Example: Date of birth:', 'pikoworks_core'); ?></span>
        
        <?php $mb->the_field('aboutDetails'); ?>
        <label><?php esc_html_e('Details', 'pikoworks_core'); ?></label>
        <input class="form-control input-icon" type="text" name="<?php esc_attr($mb->the_name()); ?>" value="<?php esc_attr($mb->the_value()); ?>"/>
        <span><?php esc_html_e('Example: 02 May 1968', 'pikoworks_core'); ?></span>
        <?php $mb->the_group_close(); ?>
    <?php endwhile; ?>
    <div style="clear: both;"></div>
    <p>
        <a href="#" class="docopy-ourteam button"><?php esc_html_e('Add New Line', 'pikoworks_core'); ?></a>
    </p>
</div>