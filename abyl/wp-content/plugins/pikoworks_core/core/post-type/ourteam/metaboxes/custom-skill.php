<div class="outteam_meta_control">
    <p style="float:right">
        <a href="#" class="dodelete-ourteam button">Remove All</a>
    </p>
    <div style="clear: both;margin-bottom: 15px;" ></div>
    <?php while($mb->have_fields_and_multi('ourteam',array('length' => 2, 'limit' => 100))): ?>
        <?php $mb->the_group_open(); ?>
        <a href="#" class="dodelete button">Remove</a>
        <?php $mb->the_field('skillTitle'); ?>
        <label><?php esc_html_e('Skill Title', 'pikoworks_core'); ?></label>
        <input class="form-control" type="text" name="<?php esc_attr($mb->the_name()); ?>" value="<?php esc_attr($mb->the_value()); ?>"/>
        <span><?php esc_html_e('Example: Wordpress', 'pikoworks_core'); ?></span>
        
        <?php $mb->the_field('skillValue'); ?>
        <label><?php esc_html_e('Skill value 1 to 100', 'pikoworks_core'); ?></label>
        <input class="form-control input-icon" type="text" name="<?php esc_attr($mb->the_name()); ?>" value="<?php esc_attr($mb->the_value()); ?>"/>
        <span><?php esc_html_e('Example: 70', 'pikoworks_core'); ?></span>
        <?php $mb->the_group_close(); ?>
    <?php endwhile; ?>
    <div style="clear: both;"></div>
    <p>
        <a href="#" class="docopy-ourteam button"><?php esc_html_e('Add New Skill', 'pikoworks_core'); ?></a>
    </p>
</div>