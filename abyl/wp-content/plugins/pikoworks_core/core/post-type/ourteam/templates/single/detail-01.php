<?php
/**
 * @author  themepiko
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="clearfix margin-b50">
        <div class="col-sm-6 col-md-3">
            <div class="team-img">
                <?php            
                 if(count($imgThumbs)>0) :?>
                    <figure>
                        <?php the_post_thumbnail( 'full' ); ?>
                    </figure>
                <?php endif; ?>
                <div class="team-name">
                    <h4><?php echo get_the_title() ?></h4>            
                    <span class="position"><?php echo esc_html($job) ?></span>
                </div>
                
            </div>
        </div>
        <div class="col-sm-6 col-md-5">
            <?php if(!empty($about)){
                echo '<h4 class="about">' . esc_html($about) . '</h4>';                
                } ?>
            <div class="team-content">
                <?php 
                  if ( '' !== get_post()->post_content ) {
                        the_content();   
                    } else { 
                           the_excerpt();
                    }                                      
                  ?>
            </div>
        </div>
        <div class="col-md-4">
             <?php 
                $address = get_post_meta(get_the_ID(), 'piko_ourteam_address', TRUE);
                if(isset($address) && !empty($address)): 
            ?>
            <div class="table-responsive">
                <table class="table table-striped">
                    <tbody> 
                <?php                
                foreach ($address['ourteam'] as $col) {
                    $aboutTitle = isset($col['aboutTitle']) ? $col['aboutTitle'] : '';
                    $aboutDetails = isset($col['aboutDetails']) ? $col['aboutDetails'] : '';
                    ?>                
                    <tr> <th scope="row"><?php echo esc_attr($aboutTitle) ?></th> <td colspan="2"><?php echo esc_attr($aboutDetails) ?></td></tr> 
                    <?php
                }
                ?>
                    </tbody> 
                </table>
            </div>
            <?php endif; ?>
            
             <?php 
             //social icon
                $meta = get_post_meta(get_the_ID(), 'piko_ourteam_social', TRUE);
                if(isset($meta) && !empty($meta)): 
            ?>
            <ul>
                <?php                
                foreach ($meta['ourteam'] as $col) {
                    $socialName = isset($col['socialName']) ? $col['socialName'] : '';
                    $socialLink = isset($col['socialLink']) ? $col['socialLink'] : '';
                    $socialIcon = isset($col['socialIcon']) ? $col['socialIcon'] : '';
                    ?>
                <li><a href="<?php echo esc_url($socialLink) ?>" target="_blank" 
                           title="<?php echo esc_attr($socialName) ?>"><i
                                class="<?php echo esc_attr($socialIcon) ?>" aria-hidden="true"></i></a>
                    </li>
                    <?php
                }
                ?>
            </ul>
            <?php endif; ?>
        </div>
    </div>
    <div class="seperator margin-b50"><span><i></i></span></div>
    <div class="clearfix margin-b50">
        <div class="col-md-7">
            <?php 
                $experience = get_post_meta(get_the_ID(), 'pikoworks_ourteam_experience', TRUE);
                if(isset($experience) && !empty($experience)): 
            ?>
            <div class="work-history-wrap">
                <h4><?php echo esc_html($experience_title) ?></h4>
                <?php                
                foreach ($experience['ourteam'] as $col) {
                    $years = isset($col['years']) ? $col['years'] : '';
                    $company = isset($col['company']) ? $col['company'] : '';
                    $position = isset($col['position']) ? $col['position'] : '';
                    $responsible = isset($col['responsible']) ? $col['responsible'] : '';
                    ?>
                    <div class="works-history vertical-align-table">
                        <div class="history-left vertical-align-cell">
                            <div class="label-content">
                                <div class="wow zoomIn" data-wow-duration=".8s" data-wow-delay=".25s">                            
                                    <label><?php echo esc_attr($years) ?></label>
                                </div>
                                <span class="square wow zoomIn" data-wow-duration=".5s" data-wow-delay=".35s"></span>
                            </div>
                        </div>
                        <div class="history-content vertical-align-cell">
                            <h4 class="clearfix">
                                <span class="title"><?php echo esc_attr($company) ?></span>
                                <span class="subtitle"><?php echo esc_attr($position) ?></span>
                            </h4>
                            <p><?php echo esc_attr($responsible) ?></p>
                        </div>
                    </div>
                    <?php
                }
                ?> 
            </div>
             <?php endif; ?>    
        </div>
        <div class="col-md-5">            
            <?php 
                $skill = get_post_meta(get_the_ID(), 'pikoworks_ourteam_skill', TRUE);
                if(isset($skill) && !empty($skill)): 
            ?>
            <div class="progress-wrap">
                <h4><?php echo esc_html($skill_title) ?></h4>
                <?php                
                foreach ($skill['ourteam'] as $col) {
                    $skillTitle = isset($col['skillTitle']) ? $col['skillTitle'] : '';
                    $skillValue = isset($col['skillValue']) ? $col['skillValue'] : '';
                    ?>
                    <div class="progress-container">
                        <h4 class="progress-title"><?php echo esc_attr($skillTitle) ?></h4>
                        <div class="progress progress-sm">
                            <div class="progress-bar progress-animate" role="progressbar" data-width="<?php echo esc_attr($skillValue) ?>" aria-valuenow="<?php echo esc_attr($skillValue) ?>" aria-valuemin="0" aria-valuemax="100">
                                <span class="progress-val"><?php echo esc_attr($skillValue) ?>%</span>
                            </div>
                        </div>
                    </div>
                    <?php
                }
                ?> 
            </div>
            <?php endif; ?>            
        </div>
    </div>
</article>