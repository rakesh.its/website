<?php
get_header();
global $pikoworks;

if ( have_posts() ) {    
    // Start the Loop.
    while ( have_posts() ) : the_post();
        $prefix = 'pikoworks_';
        $post_id = get_the_ID();
        $job = get_post_meta(get_the_ID(), $prefix .'job', true);
        $about = get_post_meta(get_the_ID(), $prefix .'team_single_about', true);
        $experience_title = get_post_meta(get_the_ID(), $prefix .'team_single_experience', true);
        $skill_title = get_post_meta(get_the_ID(), $prefix .'team_single_skill', true);        
        $img_id = get_post_thumbnail_id();
        $img = wpb_getImageBySize(array('attach_id' => $img_id, 'thumb_size' => '270x270'));         
        $imgThumbs = wp_get_attachment_image_src(get_post_thumbnail_id($post_id),'full');
        

        $detail_style =  get_post_meta(get_the_ID(),'pikoworks_team_detail_style',true);
        if (!isset($detail_style) || $detail_style == 'none' || $detail_style == '') {
            $detail_style = $pikoworks['ourteam-single-layout'];
        }

        include_once(plugin_dir_path( __FILE__ ).'/detail-0'.$detail_style.'.php');

    endwhile;
    }
?>

<?php get_footer(); ?>
