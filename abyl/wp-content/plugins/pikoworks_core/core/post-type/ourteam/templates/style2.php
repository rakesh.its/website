<?php
/**
 * The template part for displaying content
 */
?> 
<section class="<?php echo esc_attr($class_col); ?>">
        <div class="avatar image-overlay-wrap">
            <figure class="piko-overflow-hidden">
                <?php echo wp_kses_post($img['thumbnail']);
                
                if(!empty($resume)):
                ?>
                <figcaption>
                    <a href="<?php echo esc_url( get_permalink()); ?>" class="resume-icon"><i class="fa fa-file-text-o" aria-hidden="true"></i> <?php echo esc_html($resume) ?></a>                    
                </figcaption>
                <?php endif;?>
            </figure>
        </div> 
        <div class="style4 person-team">
            <header class="person-team-wrapper">
                <h5><a href="<?php echo esc_url( get_permalink()); ?>" ><?php echo get_the_title() ?></a></h5>
                <span class="position"><?php echo esc_html($job) ?></span>
            </header>
            
            <?php 
                $meta = get_post_meta(get_the_ID(), 'piko_ourteam_social', TRUE);
                if(isset($meta) && !empty($meta)): 
            ?>
            <ul class="person-team-wrapper text-right">
                <?php                
                foreach ($meta['ourteam'] as $col) {
                    $socialName = isset($col['socialName']) ? $col['socialName'] : '';
                    $socialLink = isset($col['socialLink']) ? $col['socialLink'] : '';
                    $socialIcon = isset($col['socialIcon']) ? $col['socialIcon'] : '';
                    ?>
                <li><a href="<?php echo esc_url($socialLink) ?>" target="_blank" 
                           title="<?php echo esc_attr($socialName) ?>"><i
                                class="<?php echo esc_attr($socialIcon) ?>" aria-hidden="true"></i></a>
                    </li>
                    <?php
                }
                ?>
            </ul>
            <?php endif;?>                  
        </div>
        <?php 
          if ( ! has_excerpt() ) {
            echo '<p>'. wp_trim_words( get_the_content(), esc_attr($excerpt)) . '</p>';
            } else { 
                  echo '<p>'. wp_trim_words( get_the_excerpt(), esc_attr($excerpt)) . '</p>';
            }                                      
          ?>
</section>