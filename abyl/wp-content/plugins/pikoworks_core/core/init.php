<?php
// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}
if ( !class_exists( 'RW_Meta_Box' ) ){
/**
 * Load meta-boxe 
 */
 require_once PIKOWORKSCORE_CORE . 'meta-box/meta-box-init.php';
}

 /** 
 * Load widgets
 **/

 require_once PIKOWORKSCORE_CORE . '/widgets/latest-posts.php';
 require_once PIKOWORKSCORE_CORE . '/widgets/widget-socials.php';
 require_once PIKOWORKSCORE_CORE . '/widgets/widget-flickr.php';
 require_once PIKOWORKSCORE_CORE . '/widgets/widget-tabs.php';
 require_once PIKOWORKSCORE_CORE . '/widgets/wp-instagram-widget.php';
 
 function pikoworks_no_core_no_widgets(){
      register_widget('pikoworks_widget_postimage');
      register_widget('pikoworks_widget_tab');
      register_widget('pikoworks_widgets_socials');
      register_widget('pikoworks_Flickr_Widget');
      register_widget('pikoworks_Instagram_Widget');
 }
 add_action('widgets_init', 'pikoworks_no_core_no_widgets');
 
 /**
 * Render data option for carousel
 * 
 * @param $data array. All data for carousel
 * 
 */
if( ! function_exists( '_data_carousel' ) ){
    function _data_carousel( $data ){
        $output = "";
        foreach($data as $key => $val){
            if($val){
                $output .= ' data-'.$key.'="'.esc_attr( $val ).'"';
            }
        }
        return $output;
    }
}

 
/**
 * Initialising Visual Composer
 * 
 */ 
if ( class_exists( 'Vc_Manager', false ) ) {
    
    if ( ! function_exists( 'js_composer_bridge_admin' ) ) {
		function js_composer_bridge_admin( $hook ) {
			wp_enqueue_style( 'js_composer_bridge', PIKOWORKSCORE_CORE_URL . 'js_composer/css/style.css', array() );
		}
	}
    add_action( 'admin_enqueue_scripts', 'js_composer_bridge_admin', 15 );


    require_once PIKOWORKSCORE_CORE.'js_composer/visualcomposer.php';
}

/**
 * Load Post type dynamics
 */

require_once PIKOWORKSCORE_CORE.'post-type/shortcodes.php';