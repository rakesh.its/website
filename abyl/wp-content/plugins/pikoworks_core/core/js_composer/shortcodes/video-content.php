<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
add_action( 'vc_before_init', 'pikoworks_video_content' );
function pikoworks_video_content(){
// Setting shortcode lastest
vc_map( array(
    "name"        => esc_html__( "Video Light box", 'pikoworks_core'),
    "base"        => "video_content",
    "category"    => esc_html__('Pikoworks', 'pikoworks_core' ),
    "icon" => get_template_directory_uri() . "/assets/images/logo/vc-icon.png",
    "description" => esc_html__( "Video light box with content", 'pikoworks_core'),
    "as_parent" => array('except ' => ''),
    "content_element"         => true,
    "show_settings_on_create" => true,
    "params"      => array(
        array(
            'type' => 'attach_image',
            'param_name' => 'image',
            'admin_label' => true,
            'heading' => esc_html__('Video Cover Image', 'pikoworks_core'),
        ), 
        array(
            'type' => 'textfield',
            'param_name' => 'bg_video',
            'heading' => esc_html__('Embaded video Link', 'pikoworks_core'),
            'description'     => sprintf( wp_kses( __( 'Embaded link like as:  <a href="%s" target="__blank"> Click </a>', 'pikoworks_core' ), array( 'a' => array( 'href' => array(), 'target' => array() ) ) ), 'http://www.youtube.com/watch?v=Ur9gQDq7E3o' ),
            'value' => '',
            'admin_label' => true,
        ),
        array(
            "type"        => "textfield",
            "heading"     => esc_html__( "Extra class name", 'pikoworks_core' ),
            "param_name"  => "el_class",
            "description" => esc_html__( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "pikoworks_core" ),
        ),
         array(
            'type'           => 'css_editor',
            'heading'        => esc_html__( 'Css', 'pikoworks_core' ),
            'param_name'     => 'css',
            'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'pikoworks_core' ),
            'group'          => esc_html__( 'Design options', 'pikoworks_core' )
	)
    ),
    "js_view" => 'VcColumnView'
));
}
class WPBakeryShortCode_video_content extends WPBakeryShortCodesContainer { 
    
    protected function content($atts, $content = null) {
        $atts = function_exists( 'vc_map_get_attributes' ) ? vc_map_get_attributes( 'video_content', $atts ) : $atts;
        $atts = shortcode_atts( array(
            'bg_video' => '',
            'image'   => '',
            'el_class'           => '',
            'css'           => '',            
            
        ), $atts );
        extract($atts);
        
        $css_class = 'story-section ' . $el_class;
        if ( function_exists( 'vc_shortcode_custom_css_class' ) ):
            $css_class .= ' ' . apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), '', $atts );
        endif;       
        
        $image_attributes = wp_get_attachment_image_src( $image, 'full' );
        
        ob_start();        
        ?>
        <div class="<?php echo esc_attr($css_class); ?>">
            <div class="story-video-col video-gallery" style='background-image: url(<?php echo balanceTags($image_attributes[0], true ); ?>)'>                   
                   <a href="<?php echo esc_url($bg_video); ?>?width=988&height=556" data-rel="prettyPhoto" class="video-btn smaller"><i class="fa fa-play" aria-hidden="true"></i></a>
            </div><!-- End .story-video-col -->           
            <div class="story-content-col">
                <?php echo do_shortcode($content); ?>
            </div>
        </div>
        <?php        
        $result = ob_get_contents();
        ob_clean();
        return $result;
    }    
    
}