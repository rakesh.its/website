<?php
/**
 * @Time Line work history bar
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
add_action( 'vc_before_init', 'pikoworks_timeline' );
function pikoworks_timeline(){

vc_map( array(
    "name"                    => esc_html__( "TimeLine", 'pikoworks-core'),
    "base"                    => "timeline_history",
    "category"                => esc_html__('Pikoworks', 'pikoworks-core' ),
    "icon" => get_template_directory_uri() . "/assets/images/logo/vc-icon.png",
    "description"             => esc_html__( "Work History", 'pikoworks-core'),
    "as_parent"               => array('only' => 'timeline_section'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
    "content_element"         => true,
    "show_settings_on_create" => true,
    "params"                  => array(
        array(
            "type"        => "textfield",
            "heading"     => esc_html__( "Timeline Heading", 'pikoworks-core' ),
            "param_name"  => "heading_title",
            "admin_label" => true,                
        ),
        array(
            'type'        => 'css_editor',
            'heading'     => esc_html__( 'Css', 'pikoworks-core' ),
            'param_name'  => 'css',
            'group'       => esc_html__( 'Design options', 'pikoworks-core' ),
            'admin_label' => false,
	),
        
    ),
    "js_view" => 'VcColumnView'
));
vc_map( array(
    "name"            => esc_html__("Timeline Tab", 'pikoworks-core'),
    "base"            => "timeline_section",
    "content_element" => true,
    "as_child"        => array('only' => 'timeline_history'), // Use only|except attributes to limit parent (separate multiple values with comma)
    "params"          => array(
        // add params same as with any other content element        
        array(
            "type"        => "textfield",
            "heading"     => esc_html__( "Years", 'pikoworks-core' ),
            "param_name"  => "years",
            "value"       => esc_html__('2018 -2017', 'pikoworks-core'),
            'admin_label' => true
        ),
        array(
            "type"        => "textfield",
            "heading"     => esc_html__( "Company Name", 'pikoworks-core' ),
            "param_name"  => "company",
            "value" => esc_html__('Google INC','pikoworks-core'),
            'admin_label' => true
        ),
        array(
            "type"        => "textfield",
            "heading"     => esc_html__( "Position sub title", 'pikoworks-core' ),
            "param_name"  => "position",
            "value" => esc_html__('Marketing Head','pikoworks-core'),
            'admin_label' => true
        ),
        array(
            "type"        => "textarea",
            "heading"     => esc_html__( "Responsibility", 'pikoworks-core' ),
            "param_name"  => "responsible",
            "value" => esc_html__('Your Responsibility Short description','pikoworks-core'),            
        ),        
        array(
            "type"        => "textfield",
            "heading"     => esc_html__( "Extra class name", "pikoworks-core" ),
            "param_name"  => "el_class",
            "description" => esc_html__( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "pikoworks-core" ),
            'admin_label' => false,
        ),
    )
) );

}
class WPBakeryShortCode_timeline_history extends WPBakeryShortCodesContainer {
    
    protected function content($atts, $content = null) {
        $atts = function_exists( 'vc_map_get_attributes' ) ? vc_map_get_attributes( 'timeline_history', $atts ) : $atts;
        extract( shortcode_atts( array(
            'heading_title'       => '',
            'el_class'       => '',
            'css'           => '',
        ), $atts ) );
        
        $css_class = 'work-history-wrap ' . $el_class;
        if ( function_exists( 'vc_shortcode_custom_css_class' ) ):
            $css_class .= ' ' . apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), '', $atts );
        endif;
        
        $tabs = pikoworks_get_all_attributes( 'timeline_section', $content );
        if($heading_title != ''){
            $heading_title = '<h3>'.esc_attr($heading_title).'</h3>';
        }
        
        if( count( $tabs ) > 0 ):                         
            ob_start();?> 
            <div class="<?php echo esc_attr($css_class); ?>">
                <?php echo balanceTags($heading_title); ?>
                <?php pikoworks_timeline_generate($tabs); ?>
            </div>
            <?php 
            return ob_get_clean();
            
        endif;
    }
}

function pikoworks_timeline_generate( $tabs = array() ){
    foreach( $tabs as $i => $tab ): $class = "tab-nav"; ?>
        <?php
            extract( shortcode_atts( array(
                'years' => esc_html__('2018 -2017', 'pikoworks-core'),
                'company' => esc_html__('Google INC','pikoworks-core'),              
                'position' => esc_html__('Marketing Head','pikoworks-core'),              
                'responsible' => esc_html__('Your Responsibility Short description','pikoworks-core'),              
            ), $tab ) ); 
        ?>
        <div class="works-history vertical-align-table">
            <div class="history-left vertical-align-cell">
                <div class="label-content">
                    <div class="wow zoomIn" data-wow-duration=".8s" data-wow-delay=".25s">                            
                        <label><?php echo esc_attr($years); ?></label>
                    </div>
                    <span class="square wow zoomIn" data-wow-duration=".5s" data-wow-delay=".35s"></span>
                </div>
            </div>
            <div class="history-content vertical-align-cell">
                <h4 class="clearfix">
                    <span class="title"><?php echo esc_attr($company); ?></span>
                    <span class="subtitle"><?php echo esc_attr($position); ?></span>
                </h4>
                <p><?php echo esc_attr($responsible); ?></p>
            </div>
        </div>
    <?php
    endforeach;
}