<?php
/**
 * @author  themepiko
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
add_action( 'vc_before_init', 'pikoworks_newsletter' );
function pikoworks_newsletter(){
 
// Setting shortcode lastest
vc_map( array(
    "name"        => esc_html__( "Newsletter Mailchimp", 'pikoworks_core'),
    "base"        => "pikoworks_newsletter",
    "category"    => esc_html__('Pikoworks', 'pikoworks_core' ),
    "icon" => get_template_directory_uri() . "/assets/images/logo/vc-icon.png",
    "description" => esc_html__( "Newsletter Mailchimp API key", 'pikoworks_core'),
    "params"      => array(
         array(
                    'type'          => 'dropdown',
                    'heading'       => esc_html__( 'Style', 'pikoworks_core' ),
                    'param_name'    => 'style',
                    'value'         => array(
                        esc_html__( 'Style 1', 'pikoworks_core' ) => 'title_before', // Title before form and no bg
                        esc_html__( 'Style 2', 'pikoworks_core' ) => 'title_after' // Title after form with a bg
                    ),
                    'std'           => 'title_before',
                    'admin_label' => true,  
                ),
                array(
                    'type'          => 'textfield',
                    'heading'       => esc_html__( 'Title', 'pikoworks_core' ),
                    'param_name'    => 'subscribe_form_title',
                    'std'           => esc_html__( 'Company Newsletter', 'pikoworks_core' ),
                    'admin_label' => true,
                ),
                array(
                    'type'          => 'textfield',
                    'heading'       => esc_html__( 'Short Description', 'pikoworks_core' ),
                    'param_name'    => 'subscribe_short_desc',
                    'std'           => esc_html__( 'by subscribing to our newsletter', 'pikoworks_core' ),
                ),
                array(
                    'type'          => 'textfield',
                    'heading'       => esc_html__( 'Submit Text', 'pikoworks_core' ),
                    'param_name'    => 'subscribe_submit_text',
                    'std'           => esc_html__( 'Submit', 'pikoworks_core' ),
                ),
                array(
                    'type'          => 'textfield',
                    'heading'       => esc_html__( 'Placeholder Text', 'pikoworks_core' ),
                    'param_name'    => 'subscribe_form_input_placeholder',
                    'std'           => esc_html__( 'Your email address...', 'pikoworks_core' ),
                ),
                array(
                    'type'          => 'textfield',
                    'heading'       => esc_html__( 'Success Message', 'pikoworks_core' ),
                    'param_name'    => 'subscribe_success_message',
                    'std'           => esc_html__( 'Your email added...', 'pikoworks_core' ),
                ),
//                array(
//                    'type'          => 'attach_image',
//                    'heading'       => esc_html__( 'Image', 'pikoworks_core' ),
//                    'param_name'    => 'subscribe_img_id',
//                    'description'   => esc_html__( 'Choose an image as background', 'pikoworks_core' ),
//                    'dependency' => array(
//    				    'element'   => 'style',
//    				    'value'     => array( 'title_after' )
//    			   	),
//                ),                
                array(
                    'type'          => 'dropdown',
                    'heading'       => esc_html__( 'Using Theme Options', 'pikoworks_core' ),
                    'param_name'    => 'using_theme_options_api_settings',
                    'value'         => array(
                        esc_html__( 'Yes', 'pikoworks_core' ) => 'yes',
                        esc_html__( 'No', 'pikoworks_core' ) => 'no'
                    ),
                    'std'           => 'yes',
                    'group'         => esc_html__( 'Mailchimp Settings', 'pikoworks_core' ),
                ),
                array(
                    'type'          => 'textfield',
                    'heading'       => esc_html__( 'Mailchimp API Key', 'pikoworks_core' ),
                    'param_name'    => 'mailchimp_api_key',
                                        'description'     => sprintf( wp_kses( __( '<a href="%s" target="__blank"> Click here to get your Mailchimp API key </a>', 'pikoworks_core' ), array( 'a' => array( 'href' => array(), 'target' => array() ) ) ), 'http://kb.mailchimp.com/accounts/management/about-api-keys' ),
                    'dependency' => array(
    				    'element'   => 'using_theme_options_api_settings',
    				    'value'     => array( 'no' )
    			   	),
                    'group'         => esc_html__( 'Mailchimp Settings', 'pikoworks_core' ),
                ),
                array(
                    'type'          => 'textfield',
                    'heading'       => esc_html__( 'Mailchimp List ID', 'pikoworks_core' ),
                    'param_name'    => 'mailchimp_list_id',
                    'description'     => sprintf( wp_kses( __( '<a href="%s" target="__blank"> How to find Mailchimp list ID </a>', 'pikoworks_core' ), array( 'a' => array( 'href' => array(), 'target' => array() ) ) ), 'http://kb.mailchimp.com/lists/managing-subscribers/find-your-list-id' ),
                    'dependency' => array(
    				    'element'   => 'using_theme_options_api_settings',
    				    'value'     => array( 'no' )
    			   	),
                    'group'         => esc_html__( 'Mailchimp Settings', 'pikoworks_core' ),
                ),      
        array(
            "type"        => "textfield",
            "heading"     => esc_html__( "Extra class name", 'pikoworks_core' ),
            "param_name"  => "el_class",
            "description" => esc_html__( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "pikoworks_core" ),
        ),
         array(
            'type'           => 'css_editor',
            'heading'        => esc_html__( 'Css', 'pikoworks_core' ),
            'param_name'     => 'css',
            'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'pikoworks_core' ),
            'group'          => esc_html__( 'Design options', 'pikoworks_core' )
	)
    )
));
}
class WPBakeryShortCode_pikoworks_newsletter extends WPBakeryShortCode { 
    
    protected function content($atts, $content = null) {
        $atts = function_exists( 'vc_map_get_attributes' ) ? vc_map_get_attributes( 'pikoworks_newsletter', $atts ) : $atts;
        $atts = shortcode_atts( array(
            'style'             =>  'title_before',
            'subscribe_form_title'        =>  '',
            'subscribe_short_desc'        =>  '',
            'subscribe_submit_text'       =>  'submit',
            'subscribe_form_input_placeholder' =>  '',
            'subscribe_success_message'   =>  '',
            'subscribe_img_id'            =>  0,       
            'using_theme_options_api_settings'  =>  'yes',
            'mailchimp_api_key'           =>  '',
            'mailchimp_list_id'           =>  '',
            
            'css_animation'     =>  '',
            'animation_delay'   =>  '0.5',   // In second
            'el_class'           => '',
            'css'           => '',
            
            
        ), $atts );
        extract($atts);
        
        $css_class = 'layout_' . $style. ' ' . $el_class;
        if ( function_exists( 'vc_shortcode_custom_css_class' ) ):
            $css_class .= ' ' . apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), '', $atts );
        endif;
            
        if ( intval( $subscribe_img_id ) > 0 ) {
            $img = wp_get_attachment_image_url( $subscribe_img_id,'full');   
        }
            
        
        $html = '';        
        $figure_html = '';
        $before_form_html = '';
        $after_form_html = '';
        
         if ( trim( $style ) == 'title_before' ) {
            $before_form_html = '<h4>' . sanitize_text_field( $subscribe_form_title ) . '</h4>
		                      <p>' . sanitize_text_field( $subscribe_short_desc ) . '</p>';
            $css_class .= ' ' . esc_attr( $style );
        }
        if ( trim( $style ) == 'title_after' ) {
            $after_form_html = '<h4>' . sanitize_text_field( $subscribe_form_title ) . '</h4>
                                          <p>' . sanitize_text_field( $subscribe_short_desc ) . '</p>';
            $figure_html = '<figure><img src="' . esc_url( $img ) . '" alt=""></figure>';
            $css_class .= ' feature-block ' . esc_attr( $style );
        }
        
        $subscribe_form_html = '<div class="' . esc_attr( $css_class ) . '">
                                    ' . $figure_html . '
                                    <div class="info-block">
                                    ' . $before_form_html . '
                                    <form name="news_letter" class="form-newsletter from-inline">                                                        
                                        <input type="hidden" name="api_key" value="' . esc_html( $mailchimp_api_key ) . '" />
                                        <input type="hidden" name="list_id" value="' . esc_html( $mailchimp_list_id ) . '" />
                                        <input type="hidden" name="success_message" value="' . sanitize_text_field( $subscribe_success_message ) . '" />
                                        <input type="text" id="piko_news" name="email" placeholder="' . sanitize_text_field( $subscribe_form_input_placeholder ) . '">
                                        <span><button type="submit" name="submit_button" class="button_newletter">' . sanitize_text_field( $subscribe_submit_text) . '</button></span>
                                    </form>
                                    ' . $after_form_html . '
                                </div> <!-- end info-block --> 
                                </div> <!-- css class -->';             
       

        
        ob_start();        
        
         $html .= '<div class="piko-newslatter">           				
                          ' . $subscribe_form_html . '                     
                </div>';
        
        echo balanceTags( $html );       
        
        $result = ob_get_contents();
        ob_clean();
        return $result;
    }    
    
}