<?php
/**
 * @author  themepiko
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
add_action( 'vc_before_init', 'pikoworks_blog_post' );
function pikoworks_blog_post(){
$args = array(
    'posts_per_page' => -1,
    'post_type' => 'post',
    'post_status' => 'publish');
$list_post = array();
$post_array = get_posts($args);
foreach ($post_array as $post) : setup_postdata($post);
$list_post[$post->post_title] = $post->ID;
endforeach;
wp_reset_postdata();    
  
// Setting shortcode lastest
vc_map( array(
    "name"        => esc_html__( "Blog Post", 'pikoworks_core'),
    "base"        => "pikoworks_blog_post",
    "category"    => esc_html__('Pikoworks', 'pikoworks_core' ),
    "icon" => get_template_directory_uri() . "/assets/images/logo/vc-icon.png",
    "description" => esc_html__( "Show blog posts", 'pikoworks_core'),
    "params"      => array(         
        array(
            'type'          => 'dropdown',
            'heading'       => esc_html__( 'select style', 'pikoworks_core' ),
            'param_name'    => 'type',
            'value' => array(
                esc_html__('style 1', 'pikoworks_core') => 'style1',
                esc_html__('style 2', 'pikoworks_core') => 'style2',
                esc_html__('style 3', 'pikoworks_core') => 'style3',
            ),
            'std'           => 'style1',
            'admin_label' => true,  
        ),
        array(
            'type' => 'checkbox',
            'heading' => 'Overlay Zoom effect',
            'param_name' => 'hover_zoom',
            'value' => array(esc_html__('Yes', 'pikoworks_core') => 'overlay-style-2'),
//            'dependency' => Array('element' => 'is_directioanl', 'value' => array('', 'hover-animation'))
        ),
        array(
            'type' => 'dropdown',
            'heading' => esc_html__('Target Source', 'pikoworks_core'),
            'param_name' => 'data_source',
            'admin_label' => true,
            'value' => array(
                esc_html__('From Category', 'pikoworks_core') => '',
                esc_html__('From Title(IDs)', 'pikoworks_core') => 'list_id')
        ),

        array(
            'type' => 'pikoworks_taxonomy',
            'heading' => esc_html__('Post Category', 'pikoworks_core'),
            'param_name' => 'category',
            'admin_label' => true,
            "taxonomy"    => 'category',
            "value"       => '',
            'parent'      => 0,
            'multiple'    => true,
            'description' => esc_html__('Select multiple specific Category or all Category', 'pikoworks_core'),
            'dependency' => Array('element' => 'data_source', 'value' => array(''))
        ),
        array(
            'type' => 'pikoworks_title',
            'heading' => esc_html__('Select Post Title', 'pikoworks_core'),
            'param_name' => 'post_names',
            'options' => $list_post,
            'multiple'    => true,
            'description' => esc_html__('Select multiple specific title or all Title IDs', 'pikoworks_core'),
            'dependency' => Array('element' => 'data_source', 'value' => array('list_id'))
        ), 
        array(
            "type"        => "pikoworks_number",
            "heading"     => esc_html__("Number Product load", 'pikoworks_core'),
            "param_name"  => "number",
            "value"       => 7,
            'admin_label' => true,
            "description" => esc_html__('Enter number of Product if you use "-1" load all product load. or Enter specific as you want like as 7, 8,10...', 'pikoworks_core')
        ),    
        array(
            "type"       => "dropdown",
            "heading"    => esc_html__("Image Size", 'pikoworks_core'),
            "param_name" => "size",
            "value"      => array(
        	esc_html__('1770x780px', 'pikoworks_core')     => 'image-full-width',
                esc_html__('870x580px', 'pikoworks_core')       => 'image-sidebar',
                esc_html__('400x267px', 'pikoworks_core')   => 'medium-image',
                esc_html__('570x315px', 'pikoworks_core')     => '2cols-image',
                esc_html__('370x247px', 'pikoworks_core')     => '3cols-image',
                esc_html__('255x147px', 'pikoworks_core') => '4cols-image',
        	),
            'std'         => '3cols-image',
            'admin_label' => true,
        ),
        array(
            "type"       => "dropdown",
            "heading"    => esc_html__("Order by", 'pikoworks_core'),
            "param_name" => "orderby",
            "value"      => array(
        	esc_html__('None', 'pikoworks_core')     => 'none',
                esc_html__('ID', 'pikoworks_core')       => 'ID',
                esc_html__('Author', 'pikoworks_core')   => 'author',
                esc_html__('Name', 'pikoworks_core')     => 'name',
                esc_html__('Date', 'pikoworks_core')     => 'date',
                esc_html__('Modified', 'pikoworks_core') => 'modified',
                esc_html__('Rand', 'pikoworks_core')     => 'rand',
        	),
            'std'         => 'date',
            'admin_label' => true,
            "description" => esc_html__("Select how to sort retrieved posts.",'pikoworks_core')
        ),
        array(
            "type"       => "dropdown",
            "heading"    => esc_html__("Order", 'pikoworks_core'),
            "param_name" => "order",
            "value"      => array(
                esc_html__( 'Descending', 'pikoworks_core' ) => 'DESC',
                esc_html__( 'Ascending', 'pikoworks_core' )  => 'ASC'
        	),
            'std'         => 'DESC',
            'admin_label' => true,
            "description" => esc_html__("Designates the ascending or descending order.",'pikoworks_core')
        ),
         array(
                'type' => 'textfield',
                'param_name' => 'excerpt',
                'heading' => esc_html__('Excerpt word', 'pikoworks_core'),
                'description' => esc_html__('Default use Excerpt 25 words ', 'pikoworks_core'),
                'value' => '25',
                'admin_label' => true,
            ),
        array(
            'type'          => 'dropdown',
            'heading'       => esc_html__( 'Show Read More Button', 'pikoworks_core' ),
            'param_name'    => 'show_read_more_btn',
             'description' => esc_html__('Button text edit go to: themeoption -> archive blog setting-> Read More Button Text  ', 'pikoworks_core'),
            'admin_label' => true,
            'value' => array(
                esc_html__( 'Yes', 'pikoworks_core' ) => 'yes',
                esc_html__( 'No', 'pikoworks_core' ) => 'no',	    
            ),
            'std'           => 'no',
        ),        
        // Carousel
        array(
            'type'  => 'dropdown',
            'value' => array(
                esc_html__( 'Yes', 'pikoworks_core' ) => 'true',
                esc_html__( 'No', 'pikoworks_core' )  => 'false'
            ),
            'std'         => 'false',
            'heading'     => esc_html__( 'AutoPlay', 'pikoworks_core' ),
            'param_name'  => 'autoplay',
            'group'       => esc_html__( 'Carousel settings', 'pikoworks_core' ),
            'admin_label' => true,
		),
        array(
            'type'  => 'dropdown',
            'value' => array(
                esc_html__( 'Yes', 'pikoworks_core' ) => 'true',
                esc_html__( 'No', 'pikoworks_core' )  => 'false'
            ),
            'std'         => 'false',
            'heading'     => esc_html__( 'Navigation', 'pikoworks_core' ),
            'param_name'  => 'navigation',
            'description' => esc_html__( "Show buton 'next' and 'prev' buttons.", 'pikoworks_core' ),
            'group'       => esc_html__( 'Carousel settings', 'pikoworks_core' ),
            'admin_label' => true,
        ),
        array(
            'type'  => 'dropdown',
            'value' => array(
                esc_html__( 'Nav Center', 'pikoworks_core' ) => 'owl-nav-show-inner',
                esc_html__( 'Outside Center', 'pikoworks_core' )  => 'owl-nav-show',
                esc_html__( 'Hover Center', 'pikoworks_core' )  => 'owl-nav-show-hover'
            ),
            'std'         => 'owl-nav-show-hover',
            'heading'     => esc_html__( 'Next/Prev Button', 'pikoworks_core' ),
            'param_name'  => 'navigation_btn',
            'admin_label' => true,
            'group'       => esc_html__( 'Carousel settings', 'pikoworks_core' ),
            'dependency' => array(
                        'element'   => 'navigation',
                        'value'     => array( 'true' ),
                    ),
        ),       
        array(
            'type'  => 'dropdown',
            'value' => array(
                esc_html__( 'Yes', 'pikoworks_core' ) => 'true',
                esc_html__( 'No', 'pikoworks_core' )  => 'false'
            ),
            'std'         => 'false',
            'heading'     => esc_html__( 'Bullets', 'pikoworks_core' ),
            'param_name'  => 'dots',
            'description' => esc_html__( "Show Carousel bullets bottom", 'pikoworks_core' ),
            'group'       => esc_html__( 'Carousel settings', 'pikoworks_core' ),
            'admin_label' => false,
	),
        array(
            'type'  => 'dropdown',
            'value' => array(
                esc_html__( 'Yes', 'pikoworks_core' ) => 'true',
                esc_html__( 'No', 'pikoworks_core' )  => 'false'
            ),
            'std'         => 'false',
            'heading'     => esc_html__( 'Loop', 'pikoworks_core' ),
            'param_name'  => 'loop',
            'admin_label' => true,
            'description' => esc_html__( "Inifnity loop. Duplicate last and first items to get loop illusion.", 'pikoworks_core' ),
            'group'       => esc_html__( 'Carousel settings', 'pikoworks_core' ),
            'admin_label' => false,
            ),
        array(
            "type"        => "pikoworks_number",
            "heading"     => esc_html__("Slide Speed", 'pikoworks_core'),
            "param_name"  => "slidespeed",
            "value"       => "200",
            "suffix"      => esc_html__("milliseconds", 'pikoworks_core'),
            "description" => esc_html__('Slide speed in milliseconds', 'pikoworks_core'),
            'group'       => esc_html__( 'Carousel settings', 'pikoworks_core' ),
            'admin_label' => true,
	  	),
        array(
            "type"        => "pikoworks_number",
            "heading"     => esc_html__("Margin", 'pikoworks_core'),
            "param_name"  => "margin",
            "value"       => "30",
            'admin_label' => true,
            "suffix"      => esc_html__("px", 'pikoworks_core'),
            "description" => esc_html__('Distance( or space) between 2 item', 'pikoworks_core'),
            'group'       => esc_html__( 'Carousel settings', 'pikoworks_core' )
	  	),
        
        array(
            'type'  => 'dropdown',
            'value' => array(
                esc_html__( 'Yes', 'pikoworks_core' ) => 1,
                esc_html__( 'No', 'pikoworks_core' )  => 0
            ),
            'std'         => 1,
            'heading'     => esc_html__( 'Single/Multiple Carousel', 'pikoworks_core' ),
            'param_name'  => 'use_responsive',
            'admin_label' => true,
            'description' => esc_html__( "Yes is Multiple or No is Single Item carosuel", 'pikoworks_core' ),
            'group'       => esc_html__( 'Multi Columns', 'pikoworks_core' ),           
	),
        array(
            'type'  => 'dropdown',
            'value' => array(
                esc_html__( '--- No Animation ---', 'pikoworks_core' ) => '',
                esc_html__( 'fadeIn', 'pikoworks_core' )  => 'fadeiN',
                esc_html__( 'slideInDown', 'pikoworks_core' )  => 'slidedowN',
                esc_html__( 'bounceInUp', 'pikoworks_core' )  => 'bounceRighT',
                esc_html__( 'zoomIn', 'pikoworks_core' )  => 'zoomiN',
                esc_html__( 'zoomIn two', 'pikoworks_core' )  => 'zoomin_2',
                esc_html__( 'zoomInDown', 'pikoworks_core' )  => 'zoomInDowN',
                esc_html__( 'fadeInLeft', 'pikoworks_core' )  => 'fadeInLefT',
                esc_html__( 'fadeInUp', 'pikoworks_core' )  => 'fadeInuP',
                esc_html__( 'slideInUp', 'pikoworks_core' )  => 'slideInuP',
            ),            
            'heading'     => esc_html__( 'Select Animation', 'pikoworks_core' ),
            'param_name'  => 'sanam',
            'group'       => esc_html__( 'Multi Columns', 'pikoworks_core' ),
            'admin_label' => true,
            'dependency' => array(
                        'element'   => 'use_responsive',
                        'value'     => array( '0' ),
                    ),
	),
        array(
            "type"        => "pikoworks_number",
            "heading"     => esc_html__("The items on large Device (Screen resolution of device >= 1200px )", 'pikoworks_core'),
            "param_name"  => "items_large_device",
            "value"       => "4",
            "suffix"      => esc_html__("item", 'pikoworks_core'),
            "description" => esc_html__('The number of item on desktop', 'pikoworks_core'),
            'dependency' => array(
                        'element'   => 'use_responsive',
                        'value'     => array( '1' ),
                    ),
            'group'       => esc_html__( 'Multi Columns', 'pikoworks_core' ),
	  ),
        array(
            "type"        => "pikoworks_number",
            "heading"     => esc_html__("The items on desktop (Screen resolution of device >= 992px < 1199px )", 'pikoworks_core'),
            "param_name"  => "items_desktop",
            "value"       => "3",
            "suffix"      => esc_html__("item", 'pikoworks_core'),
            "description" => esc_html__('The number of item on desktop', 'pikoworks_core'),
            'dependency' => array(
                        'element'   => 'use_responsive',
                        'value'     => array( '1' ),
                    ),
            'group'       => esc_html__( 'Multi Columns', 'pikoworks_core' ),
	  ),
        array(
            "type"        => "pikoworks_number",
            "heading"     => esc_html__("The items on tablet (Screen resolution of device >=768px and < 992px )", 'pikoworks_core'),
            "param_name"  => "items_tablet",
            "value"       => "2",
            "suffix"      => esc_html__("item", 'pikoworks_core'),
            "description" => esc_html__('The number of item on desktop', 'pikoworks_core'),
            'dependency' => array(
                        'element'   => 'use_responsive',
                        'value'     => array( '1' ),
                    ),
            'group'       => esc_html__( 'Multi Columns', 'pikoworks_core' ),
	),
        array(
            "type"        => "pikoworks_number",
            "heading"     => esc_html__("The items on mobile (Screen resolution of device < 768px)", 'pikoworks_core'),
            "param_name"  => "items_mobile",
            "value"       => "1",
            "suffix"      => esc_html__("item", 'pikoworks_core'),
            "description" => esc_html__('The number of item on desktop', 'pikoworks_core'),
            'dependency' => array(
                        'element'   => 'use_responsive',
                        'value'     => array( '1' ),
                    ),
           'group'       => esc_html__( 'Multi Columns', 'pikoworks_core' ),            
	 ),       
        array(
            "type"        => "textfield",
            "heading"     => esc_html__( "Extra class name", 'pikoworks_core' ),
            "param_name"  => "el_class",
            "description" => esc_html__( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "js_composer" ),
        ),
         array(
            'type'           => 'css_editor',
            'heading'        => esc_html__( 'Css', 'pikoworks_core' ),
            'param_name'     => 'css',
            'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'pikoworks_core' ),
            'group'          => esc_html__( 'Design options', 'pikoworks_core' )
	)
    )
));
}
class WPBakeryShortCode_pikoworks_blog_post extends WPBakeryShortCode { 
    
    protected function content($atts, $content = null) {
        $atts = function_exists( 'vc_map_get_attributes' ) ? vc_map_get_attributes( 'pikoworks_blog_post', $atts ) : $atts;
        $atts = shortcode_atts( array( 
            'data_source' => '',            
            'category' => '',            
            'post_names' => '',            
            'type' => 'style1',            
            'number'        => 7,
            'size' => '3cols-image',
            'order' => 'DESC',
            'orderby' => 'date',
            'excerpt'   => '25',
            'show_read_more_btn'    =>  'no',
            'read_more_text'    =>  esc_html__('Read more', 'pikoworks_core'),
            
            'hover_zoom' => '',
            
            'el_class' => '',
            //Carousel            
            'autoplay'      => "false",
            'navigation'    => "false",
            'navigation_btn'    => "owl-nav-show-hover",
            'slidespeed'    => 250,
            'loop'          => "false",
            'dots'         => "false",
            'margin'        => 10,                 
            //Default
            'use_responsive' => 1,
            'sanam' => '',
            'items_large_device'   => 4,
            'items_desktop'   => 3,
            'items_tablet'   => 2,
            'items_mobile'   => 1,
            'css'           => '',
            
            
        ), $atts );
        extract($atts);
        $css_class = 'blog-post-slide posts-wrap '. $hover_zoom . ' ' . $type . ' ' . $el_class;
        if ( function_exists( 'vc_shortcode_custom_css_class' ) ):
            $css_class .= ' ' . apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), '', $atts );
        endif;
            
        $single_animation = '';
        if($use_responsive == '0'){
            $single_animation = $sanam;
        }
            
        $show_read_more_btn = trim( $show_read_more_btn ) == 'yes';
                 
                 
         $args = array(
                'post_type' => 'post',
                'post_status' => 'publish',
                'orderby' => esc_attr( $orderby ),
                'order'   => esc_attr( $order ),
                'posts_per_page' => esc_attr( $number ),
                'post__in' => explode(",", $post_names),
            );
         if ($data_source == '') {
            $args = array(
                    'post_type' => 'post',
                    'post_status' => 'publish',
                    'orderby' => esc_attr( $orderby ),
                    'order'   => esc_attr( $order ),
                    'posts_per_page' => esc_attr( $number ),
//                    'category__in' => explode(",", $category),
            );        
         }
         if ($category != '') {
                $args['tax_query'] = array(
                    array(
                        'taxonomy' => 'category',
                        'field' => 'slug',
                        'terms' => explode(',', $category),
                        'operator' => 'IN'
                    )
                );
            }
         
        
        $query = new WP_Query( $args );      

        
        ob_start();        
        
        if ( $query->have_posts() ) :
            $data_carousel = array(
                "autoplay"      => esc_html($autoplay),
                "navigation"    => esc_html($navigation),
                "margin"        => esc_html($margin),
                "smartSpeed"    => esc_html($slidespeed),
                "loop"          => esc_html($loop),
                "theme"         => 'style-navigation-bottom',
                "autoheight"    => "false",
                'nav'           => esc_html($navigation),
                'dots'          => esc_html($dots),
            );
            if( $use_responsive == '1' ) {
                $arr = array(
                    '0' => array(
                        "items" => esc_html($items_mobile)
                    ), 
                    '768' => array(
                        "items" => esc_html($items_tablet)
                    ), 
                    '992' => array(
                        "items" => esc_html($items_desktop)
                    ),
                    '1200' => array(
                        "items" => esc_html($items_large_device)
                    )
                );
                $data_responsive = json_encode($arr);
                $data_carousel["responsive"] = $data_responsive;
                
                if( ( $query->post_count <= 1  ) ){
                    $data_carousel['loop'] = 'false';
                }
                
            }else{
                $data_carousel['items'] =  1;
                
            }            
            ?>
           <div class="<?php echo esc_attr( $css_class ) ?>">
                <div class="owl-carousel <?php echo esc_attr( $single_animation . ' ' . $navigation_btn ) ?>" <?php echo  _data_carousel( $data_carousel ); ?>>
                    <?php                            
                        while ( $query->have_posts() ) : $query->the_post(); ?>
                           <article class="row">
                               <?php if($type == 'style1'): ?>
                                <div class="col-sm-12 col-md-6">
                                    
                                <?php $thumbnail = pikoworks_post_format($size);
                                if (!empty($thumbnail)) : ?>
                                    <figure class="entry-thumbnail-wrap">
                                        <?php echo wp_kses_post($thumbnail); ?>
                                    </figure>
                                <?php endif; ?>
                                    
                                    <?php pikoworks_grid_blog_meta(); ?>
                                </div>
                                <div class="col-sm-12 col-md-6">
                                    <?php the_title( sprintf( '<h4 class="entry-title"><a href="%s" data-rel="bookmark">', esc_url( get_permalink() ) ), '</a></h4>' ); ?>                        
                                    <?php 
                                      if ( ! has_excerpt() ) {
                                        echo '<p>';  
                                        echo wp_trim_words( get_the_content(), esc_attr($excerpt), ' ... ' );
                                        if($show_read_more_btn)echo pikoworks_read_more_link();
                                        echo '</p>';
                                        } else {
                                              echo '<p>';
                                              echo wp_trim_words( get_the_excerpt(), esc_attr($excerpt), ' ... ' );
                                              if($show_read_more_btn) echo pikoworks_read_more_link();
                                              echo '</p>';
                                        }                                      
                                      ?>                                    
                                </div>
                               <?php elseif($type == 'style2'): ?>
                               <div class="col-sm-12">
                                <?php $thumbnail = pikoworks_post_format($size);
                                if (!empty($thumbnail)) : ?>
                                    <figure class="entry-thumbnail-wrap">
                                        <?php pikoworks_blog_shortcode_meta(); ?>
                                        <?php echo wp_kses_post($thumbnail); ?>
                                    </figure>
                                <?php endif; ?>                                    
                                    <?php the_title( sprintf( '<h4 class="entry-title"><a href="%s" data-rel="bookmark">', esc_url( get_permalink() ) ), '</a></h4>' ); ?>                        
                                    <?php 
                                      if ( ! has_excerpt() ) {
                                              echo '<p>';
                                              echo wp_trim_words( get_the_content(), esc_attr($excerpt), ' ... ' );
                                              if($show_read_more_btn) echo pikoworks_read_more_link();
                                              echo '</p>';
                                              
                                              
                                        } else { 
                                            echo '<p>';
                                            echo wp_trim_words( get_the_excerpt(), esc_attr($excerpt), ' ... ' );
                                            if($show_read_more_btn) echo pikoworks_read_more_link();
                                            echo '</p>';
                                        }                                      
                                      ?>                                    
                                </div>
                               <?php else: ?>
                                <div class="col-sm-12">
                                    <?php   $thumbnail = pikoworks_post_format($size);
                                    if (!empty($thumbnail)) : ?>
                                        <figure class="entry-thumbnail-wrap">
                                            <?php echo wp_kses_post($thumbnail); ?>
                                        </figure>
                                    <?php endif; ?>
                                    <?php pikoworks_grid_blog_meta(); ?>
                                    <?php the_title( sprintf( '<h4 class="entry-title"><a href="%s" data-rel="bookmark">', esc_url( get_permalink() ) ), '</a></h4>' ); ?> 
                                    <?php if ( $show_read_more_btn ) : ?>
                                    <?php echo pikoworks_read_more_link(); ?>                   
                                    <?php endif; ?>
                                </div>
                               <?php endif; ?>
                            </article> 
                    <?php  endwhile; // end of the loop.
                    ?>
                </div>
            </div>
            <?php
            
        endif;     
       
        wp_reset_postdata();
        wp_reset_query();
        $result = ob_get_contents();
        ob_clean();
        return $result;
    }    
}