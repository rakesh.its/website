<?php
/**
 * @author  themepiko
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
add_action( 'vc_before_init', 'pikoworks_title' );
function pikoworks_title(){
 
// Setting shortcode lastest
vc_map( array(
    "name"        => esc_html__( "Main Title", 'pikoworks_core'),
    "base"        => "pikoworks_title",
    "category"    => esc_html__('Pikoworks', 'pikoworks_core' ),
    "icon" => get_template_directory_uri() . "/assets/images/logo/vc-icon.png",
    "description" => esc_html__( "different type Title", 'pikoworks_core'),
    "params"      => array(
        array(
            'type' => 'dropdown',
            'param_name' => 'title_style',
            'value' => array(
                esc_html__('Style1', 'pikoworks_core') => '1',
                esc_html__('Style2', 'pikoworks_core') => '2',
                esc_html__('Style3', 'pikoworks_core') => '3',
                esc_html__('Style4', 'pikoworks_core') => '0',
                esc_html__('Icon with title', 'pikoworks_core') => '5',
            ),
            'heading' => esc_html__('Set title Style', 'pikoworks_core'),
            "admin_label" => true,
        ),
        array(
            'type' => 'checkbox',
            'heading' => esc_html__('Sub Title', 'pikoworks_core'),
            'param_name' => 'sub_title',
            'value' => array(esc_html__('Yes, please', 'pikoworks_core') => 'yes'),
            'admin_label' => true,
            'dependency' => array(
                            'element'   => 'title_style',
                            'value'     => array( '1','2' ),
                        ),                                
        ),
        
        array(
            'type' => 'dropdown',
            'param_name' => 'title_align',
            'heading' => esc_html__('Title align', 'pikoworks_core'),
            'value' => array(
                esc_html__('Left', 'pikoworks_core') => 'left',
                esc_html__('Center', 'pikoworks_core') => 'center',
                esc_html__('Right', 'pikoworks_core') => 'right',
            ),
            'admin_label' => true,
            'dependency' => array('element'   => 'title_style', 'value'     => array( '1','2','0', '5' )),
        ),
        array(
            'type' => 'checkbox',
            'heading' => esc_html__('Text align center', 'pikoworks_core'),
            'param_name' => 'is_layout3_center',
            'value' => array(esc_html__('Yes', 'pikoworks_core') => 'center-bottom'),
            'dependency' => array('element'   => 'title_style', 'value'     => array( '3')),
        ),
        
        array(
            'type' => 'textfield',
            'param_name' => 'title_text',
            'heading' => esc_html__('Title', 'pikoworks_core'),
            'value' => 'Title text',
            "admin_label" => true,
        ),
        array(
            'type' => 'pikoworks_number',
            'param_name' => 'block_font_size',
            'heading' => esc_html__('Title font-size', 'pikoworks_core'),            
            "admin_label" => true,
            "description" => esc_html__( "If not set automatically call typhography font size. or Enter numeric value like as 12, 14  16... etc", "pikoworks_core" ),            
        ),
        array(
            'type' => 'textarea',
            'heading' => esc_html__('Block Description', 'pikoworks_core'),
            'param_name' => 'block_desc',
            'value'  => 'Block short desc Text',
            'dependency' => array( 'element'   => 'title_style', 'value'     => array( '5' )),
        ),
        array(
            'type'          => 'colorpicker',
            'heading'       => esc_html__( 'Text color', 'pikoworks_core' ),
            'description'   => esc_html__( 'If not set automatically call typhography color', 'pikoworks_core' ),
            'param_name'    => 'block_desc_color',            
            'admin_label' => false,
        ),
        array(
            'type' => 'iconpicker',
            'param_name' => 'block_icon',
            'heading' => esc_html__('heading Icon', 'pikoworks_core'),
            'settings' => array(
                    'emptyIcon' => true, // default true, display an "EMPTY" icon?                    
                    'iconsPerPage' => 100, // default 100, how many icons per/page to display
            ),
         'dependency' => array( 'element'   => 'title_style', 'value'     => array( '5' ),), 

        ),
        array(
            'type'          => 'colorpicker',
            'heading'       => esc_html__( 'Icon Color', 'pikoworks_core' ),
            'param_name'    => 'block_icon_color',
            'std'           => '#45bf55',
            'dependency' => array( 'element'   => 'title_style', 'value'     => array( '5' ),),
        ),      
        array(
            "type"        => "textfield",
            "heading"     => esc_html__( "Extra class name", 'pikoworks_core' ),
            "param_name"  => "el_class",
            "description" => esc_html__( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "pikoworks_core" ),
        ),
         array(
            'type'           => 'css_editor',
            'heading'        => esc_html__( 'Css', 'pikoworks_core' ),
            'param_name'     => 'css',
            'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'pikoworks_core' ),
            'group'          => esc_html__( 'Design options', 'pikoworks_core' )
	)
    )
));

}
class WPBakeryShortCode_pikoworks_title extends WPBakeryShortCode { 
    
    protected function content($atts, $content = null) {
        $atts = function_exists( 'vc_map_get_attributes' ) ? vc_map_get_attributes( 'pikoworks_title', $atts ) : $atts;
        $atts = shortcode_atts( array(            
            'title_style' => '1',
            'sub_title' => '',            
            'title_text' => '',      
            'title_align' => 'left', 
            'is_layout3_center' => '', 
            
            
            'block_font_size' => '',
            'block_icon' => '',
            'block_icon_color' => '#45bf55',
            'block_desc' => '',
            'block_desc_color' => '',
        
            'el_class'     =>  '',
            'css'           => '',
            
            
        ), $atts );
        extract($atts);
        
        $css_class = 'main-title style-layout-' . $title_style  . ' ' . $el_class;
        if ( function_exists( 'vc_shortcode_custom_css_class' ) ):
            $css_class .= ' ' . apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), '', $atts );
        endif;

       
            $block_icon_html = '';  
        if( trim($block_icon) != ''){    
            $block_icon_style = trim( $block_icon_color ) != '' ? 'color: ' . esc_attr( $block_icon_color ) . ';' : '';
            if ( trim( $block_icon_color ) != '' ) {
                    $block_icon_style = 'style="' . esc_html($block_icon_style) .  '"';
                }
            $block_icon_html = '<span class="' . esc_attr( $block_icon ) . '" ' . $block_icon_style . '  aria-hidden="true"></span>'; 
        }
        
        $block_font_size = trim( $block_font_size ) != '' ? 'font-size: ' . esc_attr( $block_font_size ) . 'px;' : '';
        $block_desc_color = trim( $block_desc_color ) != '' ? 'color: ' . esc_attr( $block_desc_color ) . ';' : '';
        
         $title_color_font_size= '';
        if ( trim( $block_desc_color ) != '' || $block_font_size != '' ) {
            $title_color_font_size = 'style="' . esc_html($block_desc_color) . ' ' . esc_html($block_font_size) . '"';
        }
        
        if ( trim( $block_desc_color ) != '' ) {
            $block_desc_color = 'style="' . esc_html($block_desc_color) .  '"';
        }
        
        
        ob_start();  ?>      
        
        <div class="<?php echo esc_attr( $css_class ) ?>">
            <div class="main-title-wrap <?php echo esc_attr($title_align . ' ' . $is_layout3_center); ?> <?php if($sub_title == 'yes'){ echo 'sub';} ?>">
                <?php if($title_style == 1 ): ?>
                    <?php if($sub_title !== 'yes'):?>
                        <h3 class="main-title" <?php echo balanceTags($title_color_font_size); ?>><span class="title-square2"><?php echo esc_attr( $title_text ); ?></span></h3>
                    <?php else: ?>
                        <h4 class="sub-title" <?php echo balanceTags($title_color_font_size); ?>><?php echo esc_attr( $title_text ); ?></h4>
                    <?php endif; ?>
                <?php elseif($title_style == 2 ): ?> 
                    <?php if($sub_title !== 'yes'):?>
                        <h3 class="main-title" <?php echo balanceTags($title_color_font_size); ?>><span class="title-square"><?php echo esc_attr( $title_text ); ?></span></h3>
                    <?php else: ?>
                        <h4 class="sub-title two" <?php echo balanceTags($title_color_font_size); ?>><?php echo esc_attr( $title_text ); ?></h4>
                    <?php endif; ?>
                <?php elseif($title_style == 3 ): ?> 
                        <h3 class="main-title" <?php echo balanceTags($title_color_font_size); ?>><span class="title-square2"><?php echo esc_attr( $title_text ); ?></span></h3>
                <?php elseif($title_style == 5 ): ?>
                        <span class="icon-title" <?php echo balanceTags($title_color_font_size); ?>><?php echo balanceTags($block_icon_html) . ' ' .esc_attr( $title_text ); ?></span>
                        <p <?php echo balanceTags($block_desc_color); ?>><?php echo sanitize_text_field( $block_desc ) ?></p>
                <?php else: ?> 
                        <h3 class="main-title line-double"><?php echo esc_attr( $title_text ); ?></h3>    
                <?php endif; //title_Style1 ?>

            </div>
        </div>

        <?php
        $result = ob_get_contents();
        ob_clean();
        return $result;
    }    
    
}