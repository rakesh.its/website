<?php
/**
 * @Background Slide
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
add_action( 'vc_before_init', 'pikoworks_background_slide' );
function pikoworks_background_slide(){

vc_map( array(
    "name"                    => esc_html__( "Background Slide", 'pikoworks-core'),
    "base"                    => "bg_slide",
    "category"                => esc_html__('Pikoworks', 'pikoworks-core' ),
    "icon" => get_template_directory_uri() . "/assets/images/logo/vc-icon.png",
    "description"             => esc_html__( "slide with content block", 'pikoworks-core'),
    "as_parent" => array('except ' => ''),
    "content_element"         => true,
    "show_settings_on_create" => true,
    "params"                  => array(
        array(
                "type" => "pikoworks_number",
                "heading" => esc_attr__("Slider Height", "pikoworks_core"),
                "param_name" => "slide_height",
                "admin_label" => true,
                "value" => "500"
        ),
        array(
            'type' => 'attach_images',
            'param_name' => 'images',
            'admin_label' => true,
            'heading' => esc_html__('Upload slider Image', 'pikoworks_core'),
        ),        
//        array(
//            "type"        => "dropdown",
//            "heading"     => esc_html__("Enable video slide", 'pikoworks-core'),
//            "param_name"  => "enable_video",
//            "admin_label" => true,
//            'value'       => array(
//        	esc_html__( 'Disable', 'pikoworks-core' ) => '0',
//                esc_html__( 'Enable', 'pikoworks-core' ) => '1',
//            ),
//        ),        
//        array(
//            'type' => 'textfield',
//            'param_name' => 'bg_video',
//            'heading' => esc_html__('Embaded video Link', 'pikoworks_core'),
//            'description'     => sprintf( wp_kses( __( 'Embaded link like as:  <a href="%s" target="__blank"> Click </a>', 'pikoworks_core' ), array( 'a' => array( 'href' => array(), 'target' => array() ) ) ), 'http://www.youtube.com/watch?v=Ur9gQDq7E3o' ),
//            'value' => '',
//            'dependency' => Array('element' => 'enable_video', 'value' => array('1'))
//        ),
//        array(
//            'type' => 'attach_image',
//            'param_name' => 'bg_video_falback',
//            'admin_label' => true,
//            'heading' => esc_html__('Upload video fallback Image', 'pikoworks_core'),
//            'description' => esc_html__('if video not loaded image will loaded', 'pikoworks_core'),
//            'dependency' => Array('element' => 'enable_video', 'value' => array('1'))
//        ),
//        array(
//            "type"        => "dropdown",
//            "heading"     => esc_html__("Video sound", 'pikoworks-core'),
//            "param_name"  => "video_sound",
//            "admin_label" => true,
//            'value'       => array(
//        	esc_html__( 'Mute', 'pikoworks-core' ) => 'true',
//                esc_html__( 'Yes', 'pikoworks-core' ) => 'false',
//            ),
//            'dependency' => Array('element' => 'enable_video', 'value' => array('1'))
//        ),
//        array(
//            "type"        => "dropdown",
//            "heading"     => esc_html__("Video Loop", 'pikoworks-core'),
//            "param_name"  => "video_loop",
//            "admin_label" => true,
//            'value'       => array(
//        	esc_html__( 'True', 'pikoworks-core' ) => 'true',
//                esc_html__( 'False', 'pikoworks-core' ) => 'false',
//            ),
//            'dependency' => Array('element' => 'enable_video', 'value' => array('1'))
//        ),
        array(
            "type"        => "dropdown",
            "heading"     => esc_html__("Slide Effect", 'pikoworks-core'),
            "param_name"  => "slide_effect",
            "admin_label" => true,
            'value'       => array(
        	esc_html__( 'Transition', 'pikoworks-core' ) => '1',
                esc_html__( 'Animation', 'pikoworks-core' ) => '2',
        	),
        ),
        array(
            "type"        => "dropdown",
            "heading"     => esc_html__("Slide Transition", 'pikoworks-core'),
            "param_name"  => "transition",
            'value'       => array(
        	esc_html__( 'Fade In', 'pikoworks-core' ) => 'fade',
                esc_html__( 'Slide in left', 'pikoworks-core' ) => 'slideLeft',
                esc_html__( 'Slide in right', 'pikoworks-core' ) => 'slideRight',
                esc_html__( 'Slide in right', 'pikoworks-core' ) => 'slideRight',
                esc_html__( 'Slide in up', 'pikoworks-core' ) => 'slideUp',
                esc_html__( 'Slide in up 2', 'pikoworks-core' ) => 'slideUp2',
                esc_html__( 'Slide in slideDown', 'pikoworks-core' ) => 'slideDown',
                esc_html__( 'Slide in slideDown2', 'pikoworks-core' ) => 'slideDown2',
                esc_html__( 'Slide in zoomIn', 'pikoworks-core' ) => 'zoomIn',
                esc_html__( 'Slide in zoomIn2', 'pikoworks-core' ) => 'zoomIn2',
                esc_html__( 'Slide in zoomOut', 'pikoworks-core' ) => 'zoomOut',
                esc_html__( 'Slide in zoomOut2', 'pikoworks-core' ) => 'zoomOut2',
                esc_html__( 'Slide in swirlLeft', 'pikoworks-core' ) => 'swirlLeft',
                esc_html__( 'Slide in swirlLeft2', 'pikoworks-core' ) => 'swirlLeft2',
                esc_html__( 'Slide in swirlRight', 'pikoworks-core' ) => 'swirlRight',
                esc_html__( 'Slide in burn', 'pikoworks-core' ) => 'burn',
                esc_html__( 'Slide in burn2', 'pikoworks-core' ) => 'burn2',
                esc_html__( 'Slide in blur', 'pikoworks-core' ) => 'blur',
                esc_html__( 'Slide in blur2', 'pikoworks-core' ) => 'blur2',
                esc_html__( 'Slide in flash', 'pikoworks-core' ) => 'flash',
                esc_html__( 'Slide in flash2', 'pikoworks-core' ) => 'flash2',
        	),
            'dependency' => Array('element' => 'slide_effect', 'value' => array('1'))
        ),
        array(
            "type"        => "dropdown",
            "heading"     => esc_html__("Slide Transition", 'pikoworks-core'),
            "param_name"  => "slide_animation",            
            'value'       => array(
        	esc_html__( 'Kenburns', 'pikoworks-core' ) => 'kenburns',
                esc_html__( 'Kenburns Up', 'pikoworks-core' ) => 'kenburnsUp',
                esc_html__( 'Kenburns Down', 'pikoworks-core' ) => 'kenburnsDown',
                esc_html__( 'Kenburns Right', 'pikoworks-core' ) => 'kenburnsRight',
                esc_html__( 'Kenburns Up Left', 'pikoworks-core' ) => 'kenburnsUpLeft',
                esc_html__( 'Kenburns Up Right', 'pikoworks-core' ) => 'kenburnsUpRight',
                esc_html__( 'Kenburns Down Left', 'pikoworks-core' ) => 'kenburnsDownLeft',
                esc_html__( 'Kenburns Down Right', 'pikoworks-core' ) => 'kenburnsDownRight',
                esc_html__( 'Random', 'pikoworks-core' ) => 'random',
        	),
            'dependency' => Array('element' => 'slide_effect', 'value' => array('2'))
        ),
        array(
            'type' => 'pikoworks_number',
            'param_name' => 'duration',
            'heading' => esc_html__('Transition duration in milliseconds', 'pikoworks_core'),
            "admin_label" => true,
            'description' => esc_html('Note:1sec = 1000ms', 'pikoworks_core'),
            'value' => '5000',
        ),
        array(
            'type' => 'dropdown',
            'heading' => 'Slide loop',
            'param_name' => 'img_loop',
            'value'       => array(
        	esc_html__( 'True', 'pikoworks-core' ) => 'true',
                esc_html__( 'False', 'pikoworks-core' ) => 'false',
            ),
        ),
        array(
            'type' => 'dropdown',
            'heading' => 'Slide Timer',
            'param_name' => 'timer',
            'value'       => array(
        	esc_html__( 'True', 'pikoworks-core' ) => 'true',
                esc_html__( 'False', 'pikoworks-core' ) => 'false',
            ),
        ),
        array(
            "type"        => "dropdown",
            "heading"     => esc_html__("Overlay dot pattern", 'pikoworks-core'),
            "param_name"  => "overlay",
            "admin_label" => true,
            'value'       => array(
        	esc_html__( 'No', 'pikoworks-core' ) => 'false',
                esc_html__( 'Yes', 'pikoworks-core' ) => 'true',
            ),
        ),
        array(
            'type'        => 'css_editor',
            'heading'     => esc_html__( 'Css', 'pikoworks-core' ),
            'param_name'  => 'css',
            'group'       => esc_html__( 'Design options', 'pikoworks-core' ),
            'admin_label' => false,
	),
        
    ),
    "js_view" => 'VcColumnView'
));
}
class WPBakeryShortCode_bg_slide extends WPBakeryShortCodesContainer {
    
    protected function content($atts, $content = null) {
        $atts = function_exists( 'vc_map_get_attributes' ) ? vc_map_get_attributes( 'bg_slide', $atts ) : $atts;
        extract( shortcode_atts( array(
            'slide_height'	=> '500',           
            'images'       => '',            
            'img_loop' => 'true',
            'enable_video' => '0',
            'bg_video_falback' => '',
            'bg_video' => '',
            'video_sound'       => 'true',
            'video_loop'       => 'true',
            'slide_effect'       => 1,
            'transition'       => 'fade',
            'slide_animation'  => 'kenburns',
            'duration'  => '50000',
            'timer'  => 'false',
            'overlay'  => 'false',
            'el_class'       => '',
            'css'           => '',
        ), $atts ) );
        
        $uniqId= uniqid();
        $css_class = 'bg-slide-' .$uniqId . ' '. $el_class;
        if ( function_exists( 'vc_shortcode_custom_css_class' ) ):
            $css_class .= ' ' . apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), '', $atts );
        endif;
        
        
        if ( '' === $images ) {
                $images = '-1,-2,-3';
        }
        $images = explode( ',', $images );
                                
            ob_start();?> 
            <div class="<?php echo esc_attr($css_class); ?>" >
              <div class="container" height="<?php echo esc_attr($slide_height); ?>px">
                <?php echo do_shortcode($content); ?>  
              </div>
            </div>

            <?php
            if($slide_effect == 1){
              $slide_effect =  "transition: '".esc_attr($transition)."'";
            }else{
                $slide_effect =  "animation: '".esc_attr($slide_animation)."'";
            }
            
//            $video_slide_src = '';
//            if($enable_video == '1'){
//                $video_slide_src = " {   src: '".wp_get_attachment_url( $bg_video_falback )."',
//                            video: {
//                                src: ['".esc_url($bg_video)."'],
//                                loop: ".esc_attr($video_loop).",
//                                mute: ".esc_attr($video_sound)."
//                            }}";
//                }
            
            
            ?>
            <script type="text/javascript">
                jQuery(document).ready(function ($) {
                    if($('.bg-slide-<?php echo esc_attr($uniqId); ?>').length > 0){
                        $('.bg-slide-<?php echo esc_attr($uniqId); ?>').vegas({
                            timer: <?php echo esc_attr($timer ); ?>,
                            transitionDuration: <?php echo esc_attr( $duration ); ?>,
                            slides: [ 
                                <?php                                
                                foreach($images as $image): ?>
                                {src: '<?php echo wp_get_attachment_url( $image ); ?>'},
                                <?php endforeach;?>  
                                <?php //echo $video_slide_src; ?>    

                            ],
                            <?php echo balanceTags($slide_effect); ?>,               
                            overlay: <?php echo esc_attr($overlay); ?>,
                            loop: <?php echo esc_attr($img_loop); ?>
                        });
                    }
                });
            </script> 

        <?php
            return ob_get_clean();
    }
}