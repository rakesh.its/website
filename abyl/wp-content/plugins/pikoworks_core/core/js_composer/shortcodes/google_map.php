<?php
/**
 * @author  themepiko
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
add_action( 'vc_before_init', 'pikoworks_google_map' );
function pikoworks_google_map(){
 
   global $pikoworks_vc_anim_effects_in; 
// Setting shortcode lastest
vc_map( array(
    "name"        => esc_html__( "Google Map", 'pikoworks_core'),
    "base"        => "pikoworks_google_map",
    "category"    => esc_html__('Pikoworks', 'pikoworks_core' ),
    "icon" => get_template_directory_uri() . "/assets/images/logo/vc-icon.png",
    "description" => esc_html__( "Show different type of product", 'pikoworks_core'),
    "params"      => array(
        
        array(
            'type'       => 'dropdown',
            'heading'    => esc_html__( 'Google Map style', 'pikoworks_core' ),
            'param_name' => 'map_style',
            'admin_label' => true,
            'value'      => array(
                esc_html__( 'Only Map', 'pikoworks_core' )    => 'map',
                esc_html__( 'Map with Contact', 'pikoworks_core' ) => 'map_contact',
            )
        ),
        array(
            'type' => 'textfield',
            'param_name' => 'title',
            'heading' => esc_html__('Contact from Title', 'pikoworks_core'),
            'value' => esc_html__('Drop a line', 'pikoworks_core'),
            'admin_label' => true,
            'dependency' => array(
                    'element'   => 'map_style',
                    'value' => 'map_contact',
                ),
        ),
        array(
            'type' => 'dropdown',
            'param_name' => 'form_position',
            'heading' => esc_html__('Contact from Title', 'pikoworks_core'),
            'value'      => array(
                esc_html__( 'Right', 'pikoworks_core' )    => 'right',
                esc_html__( 'Left', 'pikoworks_core' ) => 'left',
            ),
            'admin_label' => true,
            'dependency' => array(
                    'element'   => 'map_style',
                    'value' => 'map_contact',
                ),
        ),
        
        array(
            'type' => 'textfield',
            'param_name' => 'contact_form',
            'heading' => esc_html__('Contact form 7 or other shortcode', 'pikoworks_core'),
            'description' => esc_html__('NB: WP Admin -> Contact -> Contact from ->shortcode like as: [contact-form-7 id="947" title="Contact form 1"]', 'pikoworks_core'),
            'value' => '',
            'dependency' => array(
                    'element'   => 'map_style',
                    'value' => 'map_contact',
                ),
        ),
        array(
            'type' => 'textfield',
            'param_name' => 'height',
            'heading' => esc_html__('Map height', 'pikoworks_core'),
            'description' => esc_html__('Default height 500 in pixel, Type numeric value to change height', 'pikoworks_core'),
            'value' => '500',
            'admin_label' => true,
            'dependency' => array(
                    'element'   => 'map_style',
                    'value' => 'map',
                ),
        ),
        array(
            "type"        => "textfield",
            "heading"     => esc_html__( "Map Zooming", 'pikoworks_core' ),
            "param_name"  => "map_zoom",
            'value' => '12',
            'admin_label' => true,
            'description' => esc_html__('Default zoom 12, Type numeric value to change zoom label', 'pikoworks_core'),
            
        ),
        array(
            'type' => 'attach_image',
            'param_name' => 'image',
            'heading' => esc_html__('Map Marker', 'pikoworks_core'),
            'description' => esc_html__('The Image size 47x68px ratina Double Size', 'pikoworks_core')
        ),        
        array(
            'type' => 'textfield',
            'param_name' => 'latitude',
            'heading' => esc_html__('Latitude', 'pikoworks_core'),
            'description'     => sprintf( wp_kses( __( 'Enter your Latitude value. Visit  <a href="%s" target="__blank"> Click </a>  Search your location the copy the code and paste here', 'pikoworks_core' ), array( 'a' => array( 'href' => array(), 'target' => array() ) ) ), 'http://mondeca.com/index.php/en/any-place-en' ),
            'value' => '38.23205',
        ),
        array(
            'type' => 'textfield',
            'param_name' => 'longitude',
            'heading' => esc_html__('Longitude', 'pikoworks_core'),
            'description' => esc_html__('Enter your Longitude value', 'pikoworks_core'),
            'value' => '-85.80710',
        ),
         array(
                'type'          => 'dropdown',
                'heading'       => esc_html__( 'CSS Animation', 'pikoworks_core' ),
                'param_name'    => 'css_animation',
                'admin_label' => true,
                'value'         => $pikoworks_vc_anim_effects_in,
                'description'   => esc_html__( 'Choose your animation', 'pikoworks_core' )
            ),
        array(
                'type'          => 'textfield',
                'heading'       => esc_html__( 'Animation Delay', 'pikoworks_core' ),
                'param_name'    => 'animation_delay',
                'std'           => '0.5',
                'admin_label' => false,
                'description'   => esc_html__( 'Delay unit is second.', 'pikoworks_core' ),
                'dependency' => array(
                                'element'   => 'css_animation',
                                'not_empty' => true,
                            ),
            ), 
        array(
            "type"        => "textfield",
            "heading"     => esc_html__( "Extra class name", 'pikoworks_core' ),
            "param_name"  => "el_class",
            "description" => esc_html__( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "pikoworks_core" ),
        ),
         array(
            'type'           => 'css_editor',
            'heading'        => esc_html__( 'Css', 'pikoworks_core' ),
            'param_name'     => 'css',
            'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'pikoworks_core' ),
            'group'          => esc_html__( 'Design options', 'pikoworks_core' )
	)
    )
));
}
class WPBakeryShortCode_pikoworks_google_map extends WPBakeryShortCode { 
    
    protected function content($atts, $content = null) {
        $atts = function_exists( 'vc_map_get_attributes' ) ? vc_map_get_attributes( 'pikoworks_google_map', $atts ) : $atts;
        $atts = shortcode_atts( array(            
            'map_style' => 'map',
            'title' => esc_html('Drop a line', 'pikoworks_core'),
            'contact_form' => '',
            'form_position' => 'right',
            'height' => '500',
            'image'   => '',
            'map_zoom'   => '12',
            'latitude'   => '38.23205',
            'longitude'   => '-85.80710',
            'css_animation'     =>  '',
            'animation_delay'   =>  '0.5',   // In second
            'el_class'           => '',
            'css'           => '',
            
            
        ), $atts );
        extract($atts);
        
        $css_class = 'google-map wow  ' . $css_animation . ' ' . $el_class;
        if ( function_exists( 'vc_shortcode_custom_css_class' ) ):
            $css_class .= ' ' . apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), '', $atts );
        endif;

         if ( !is_numeric( $animation_delay ) ) {
                $animation_delay = 0;
            }    
            $animation_delay = $animation_delay . 's';           
      
       
            $image_attributes = wp_get_attachment_image_src( $image );
        
        ob_start();        
        ?>
		<?php if($map_style == 'map'): ?>
        <div class="<?php echo esc_attr( $css_class ); ?>" data-wow-delay="<?php echo esc_attr( $animation_delay ) ?>">
            <div id="map_contact" style="height:<?php echo esc_attr($height); ?>px;"></div>
        </div>
        <?php else: ?>
        <div class="map-contact-wrap">        
                <div id="map_contact"></div>
                <div class="contact">
                        <div class="container">
                                <div class="row">
                                        <?php if($form_position == 'right'){
                                            echo '<div class="col-md-7"></div>';
                                        } ?>
                                        <div class="col-md-5 contact-box <?php echo esc_attr( $css_class ); ?>" data-wow-delay="<?php echo esc_attr( $animation_delay ) ?>">
                                            <div class="main-title-wrap left ">
                                                <h1 class="main-title"><span class="title-square2"><?php echo esc_attr($title); ?></span></h1>
                                            </div>
                                            <?php echo do_shortcode( $contact_form ); ?>
                                        </div>
                                        <?php if($form_position == 'left'){
                                            echo '<div class="col-md-7"></div>';
                                        } ?>
                                </div>
                        </div>
                </div>
        </div>
        <?php endif;?>

        <script>
              function initMap() {

                    var map = new google.maps.Map(document.getElementById('map_contact'), {
                    center: {lat: <?php echo esc_attr($latitude); ?>, lng: <?php echo esc_attr($longitude); ?>},
                    zoom: <?php echo esc_attr($map_zoom); ?>,
                    scrollwheel: false,
                    navigationControl: true,
                    mapTypeControl: false,
                    scaleControl: false,
                    draggable: true,        

                    styles: [
                            {
                              featureType: 'all',
                              stylers: [
                                    { saturation: -80 }
                              ]
                            },{
                              featureType: 'road.arterial',
                              elementType: 'geometry',
                              stylers: [
                                    { hue: '#00ffee' },
                                    { saturation: 50 }
                              ]
                            },{
                              featureType: 'poi.business',
                              elementType: 'labels',
                              stylers: [
                                    { visibility: 'off' }
                              ]
                            }
                      ]
                    });

               var image = '<?php echo balanceTags($image_attributes[0], true ); ?>';
                    var beachMarker = new google.maps.Marker({
                            position: {lat: <?php echo esc_attr($latitude); ?>, lng: <?php echo esc_attr($longitude); ?>},
                            map: map,
                            icon: image
                    });        
              }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCIBzHIVgCtYA1ZsY5TTyqCJyxEgErjIXA&callback=initMap"  async defer></script>

        <?php        
        $result = ob_get_contents();
        ob_clean();
        return $result;
    }    
    
}