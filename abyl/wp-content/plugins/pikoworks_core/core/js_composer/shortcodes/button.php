<?php
/**
 * @author  themepiko
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
add_action( 'vc_before_init', 'pikoworks_button' );
function pikoworks_button(){ 
   global $pikoworks_vc_anim_effects_in; 
// Setting shortcode lastest
vc_map( array(
    "name"        => esc_html__( "Button", 'pikoworks_core'),
    "base"        => "pikoworks_button",
    "category"    => esc_html__('Pikoworks', 'pikoworks_core' ),
    "icon" => get_template_directory_uri() . "/assets/images/logo/vc-icon.png",
    "description" => esc_html__( "Squred, Round, Outline", 'pikoworks_core'),
    "params"      => array(
            array(
                    'type'          => 'dropdown',                    
                    'heading'       => esc_html__( 'Button Layout type', 'pikoworks_core' ),
                    'param_name'    => 'type_type',
                    'value' => array(                        
                        esc_html__('Advance', 'pikoworks_core') => 'advance',
                        esc_html__('Theme Default', 'pikoworks_core') => 'default',
                    ),                    
                    'admin_label' => true, 
            ),
            array(
                'type'          => 'dropdown',                    
                'heading'       => esc_html__( 'Button hover effect', 'pikoworks_core' ),
                'param_name'    => 'btn_effect',
                'value' => array(
                    esc_html__('left to right', 'pikoworks_core') => '0',
                    esc_html__('Swipe left', 'pikoworks_core') => '1',
                    esc_html__('Swipe bottom', 'pikoworks_core') => '2',
                    esc_html__('Swipe corner', 'pikoworks_core') => '3',
                    esc_html__('Swipe center', 'pikoworks_core') => '4',
                    esc_html__('Swipe corner 2', 'pikoworks_core') => '5',
                    esc_html__('Swipe top bottom', 'pikoworks_core') => '6',
                    esc_html__('Swipe corner 3', 'pikoworks_core') => '7',
                    esc_html__('Shuffle corner', 'pikoworks_core') => '8',
                    esc_html__('Shuffle top bottom', 'pikoworks_core') => '9',
                    esc_html__('Aware', 'pikoworks_core') => '10',
                ),                    
                'admin_label' => true,
                'dependency' => array('element'   => 'type_type', 'value' => 'default'),
            ),
            array(
                'type' => 'checkbox',
                'heading'       => '',
                'param_name' => 'btn_white',
                'value' => array(esc_html__('Yes', 'pikoworks_core') => 'white'),
                'description'   => esc_html__( 'border color white', 'pikoworks_core' ),
                'admin_label' => false,
                 'dependency' => array('element'   => 'type_type', 'value' => 'default'),
            ),
            array(
                    'type'          => 'dropdown',                    
                    'heading'       => esc_html__( 'Button Style', 'pikoworks_core' ),
                    'param_name'    => 'type',
                    'value' => array(
                        esc_html__('Squad', 'pikoworks_core') => 'squad',
                        esc_html__('Round', 'pikoworks_core') => 'round',
                        esc_html__('Outline', 'pikoworks_core') => 'outline',
                    ),                    
                    'admin_label' => true,
                   'dependency' => array('element'   => 'type_type', 'value' => 'advance'),
            ),
            array(
                    'type'          => 'dropdown',
                    'class'         => 'vc_admin_label admin_label_type',
                    'heading'       => esc_html__( 'Button Size', 'pikoworks_core' ),
                    'param_name'    => 'btn_size',
                    'value' => array(
                        esc_html__('medium', 'pikoworks_core') => 'medium',
                        esc_html__('small', 'pikoworks_core') => 'small',
                        esc_html__('large', 'pikoworks_core') => 'large',
                    ),                    
                    'admin_label' => true, 
            ),
            array(
                'type' => 'checkbox',
                'heading'       => esc_html__('Button Center','pikoworks_core'),
                'param_name' => 'btn_center',
                'value' => array(esc_html__('Yes', 'pikoworks_core') => 'yes'),
                'description'   => esc_html__( 'If the two or more button no need check it.', 'pikoworks_core' ),
                'admin_label' => false,                                             
            ),
            array(
                'type' => 'checkbox',
                'heading'       => '',
                'param_name' => 'btn_fullwidth',
                'value' => array(esc_html__('Yes', 'pikoworks_core') => 'btn-fullwidth'),
                'description'   => esc_html__( 'If check button fullwidth', 'pikoworks_core' ),
                'admin_label' => true,
                'dependency' => array('element'   => 'type_type', 'value' => 'advance'),
            ), 
            array(
                    'type'          => 'dropdown',
                    'heading'       => esc_html__( 'Border in px', 'pikoworks_core' ),
                    'param_name'    => 'border',
                    'value' => array( '1px' => 'onepx', '2px' => 'twopx', '3px' => 'threepx'),                       
                    'admin_label' => true,                     
            ),
            array(
                'type'          => 'colorpicker',                
                'heading'       => esc_html__( 'Border color', 'pikoworks_core' ),
                'param_name'    => 'border_color',
                'dependency' => array('element'   => 'type_type', 'value' => 'advance'),
            ),           
            array(
                'type'          => 'vc_link',
                'heading'       => esc_html__( 'Button title and Link ', 'pikoworks_core' ),
                'param_name'    => 'link',
                'std'           => esc_html__( 'Button link', 'pikoworks_core' ),
            ),
            array(
                'type'          => 'colorpicker',
                'heading'       => esc_html__( 'Text color', 'pikoworks_core' ),
                'param_name'    => 'text_color',
                'dependency' => array('element'   => 'type_type', 'value' => 'advance'),
            ),            
            array(
                'type'          => 'colorpicker',
                'heading'       => esc_html__( 'Backgrund color', 'pikoworks_core' ),
                'param_name'    => 'bg_color',
                'dependency' => array('element'   => 'type_type', 'value' => 'advance'),
            ),
            array(
                'type' => 'checkbox',
                'heading'       => '',
                'param_name' => 'btn_hover',
                'description'   => esc_html__( 'if use button different color Hover color theme Color please.', 'pikoworks_core' ),
                'value' => array(esc_html__('Yes', 'pikoworks_core') => 'layout-outline'),
                'dependency' => array('element'   => 'type_type', 'value' => 'advance'),
            ),          
        array(
                'type'          => 'dropdown',
                'heading'       => esc_html__( 'CSS Animation', 'pikoworks_core' ),
                'param_name'    => 'css_animation',
                'value'         => $pikoworks_vc_anim_effects_in,
                'admin_label' => true,
                'description'   => esc_html__( 'Choose your animation', 'pikoworks_core' ),
                'dependency' => array('element'   => 'type_type', 'value' => 'advance'),
            ),  
        array(
                'type'          => 'textfield',
                'heading'       => esc_html__( 'Animation Delay', 'pikoworks_core' ),
                'param_name'    => 'animation_delay',
                'std'           => '0.5',
                'description'   => esc_html__( 'Delay unit is second.', 'pikoworks_core' ),
                'dependency' => array(
                                'element'   => 'css_animation',
                                'not_empty' => true,
                            ),
            ),         
        array(
            "type"        => "textfield",
            "heading"     => esc_html__( "Extra class name", 'pikoworks_core' ),
            "param_name"  => "el_class",
        ),
         array(
            'type'           => 'css_editor',
            'heading'        => esc_html__( 'Css', 'pikoworks_core' ),
            'param_name'     => 'css',
            'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'pikoworks_core' ),
            'group'          => esc_html__( 'Design options', 'pikoworks_core' )
	)
    )
));
}
class WPBakeryShortCode_pikoworks_button extends WPBakeryShortCode { 
    
    protected function content($atts, $content = null) {
        $atts = function_exists( 'vc_map_get_attributes' ) ? vc_map_get_attributes( 'pikoworks_button', $atts ) : $atts;
        $atts = shortcode_atts( array(
            'type' => 'squad',                
            'btn_size' => '',                
            'link' => '',            
            'text_color' => '',
            'border' => '',
            'border_color' => '',
            'bg_color' => '',
            'btn_fullwidth' => '',
            'btn_hover' => '',
            'btn_center' => '',
            
            'type_type' => 'advance', //Since theme v1.2
            'btn_effect' => '4', 
            'btn_white' => '', 
            
            
            
            'css_animation'     =>  '',
            'animation_delay'   =>  '0.5',   // In second
            'el_class'           => '',
            'css'           => '',
            
            
        ), $atts );
        extract($atts);
        
         
         $_animation_delay = '';
        if ( trim( $css_animation ) != '' ) { 
             $css_animation = 'wow '. $css_animation;
            if ( !is_numeric( $animation_delay ) ) {
                $_animation_delay = 'data-wow-delay="0"';
            }else{
                $_animation_delay = 'data-wow-delay="' .  esc_attr($animation_delay) .  's"';
            }
        }
        
        $btn_center_class = '';
        if($btn_center == 'yes'){
           $btn_center_class = 'btn-center';
        }
        
        
        $css_class = 'btn-inline layout-'. $type. ' ' . $border . ' ' . $btn_size . '  ' . $btn_fullwidth . ' ' . $btn_hover . ' ' . $css_animation . ' ' . $el_class;
        if($type_type == 'default'){
            $css_class = 'btnwrap ' . $border . ' ' . $btn_size . '  ' . $btn_white . ' '  . $btn_center_class . ' '  . $el_class;
        }
        
        if ( function_exists( 'vc_shortcode_custom_css_class' ) ):
            $css_class .= ' ' . apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), '', $atts );
        endif;

        
                 
      
        
            $button_html = '';
            $button_style = '';
            $read_more_html = '';
            $btn_center_before = '';
            $btn_center_after = '';
            $link_default = array(
                    'url'       =>  '',
                    'title'     =>  '',
                    'target'    =>  '_self'
                );

                if ( function_exists( 'vc_build_link' ) ):
                    $link = vc_build_link( $link );
                else:
                    $link = $link_default;
                endif;
                
                
                
             
            $text_color = trim( $text_color ) != '' ? 'color: ' . esc_attr( $text_color ) . ';' : '';  
            $border_color = trim( $border_color ) != '' ? 'border-color: ' . esc_attr( $border_color ) . ';' : '';  
            $bg_color = trim( $bg_color ) != '' ? 'background-color: ' . esc_attr( $bg_color ) . ';' : '';  
            
            if ( trim( $text_color ) != '' || trim( $border_color )  != '' || trim( $bg_color )  != '' ) {
                    $button_style = 'style="' .  esc_attr($text_color) . ' ' .  esc_attr($border_color) . ' ' .  esc_attr($bg_color) .'"';
                }
                
            $btn10_after = $btn89_before = $btn89_after = '';
            if( $btn_effect == '8'|| $btn_effect == '9'){
                $btn89_before = '<i class="shuffle">';
                $btn89_after = '</i>';
            }    
            if($btn_effect == '10'){
                $btn10_after = '<div class="aware"></div>';
            }    
                
            if ( trim( $link['url'] ) != '' && $type_type == 'default' ) {
                
                $read_more_html = '<a href="' . esc_url( $link['url'] ) . '" target="' . esc_attr( $link['target'] ) . '" title="' . esc_attr( $link['title'] ) . '" class="piko-btn-'. $btn_effect.'" ><span>' .$btn89_before . esc_attr( $link['title'] ) .$btn89_after . '</span>' . $btn10_after . '</a>';            
                
            } elseif ( trim( $link['url'] ) != '' ) {
                
                $read_more_html = '<a href="' . esc_url( $link['url'] ) . '" target="' . esc_attr( $link['target'] ) . '" title="' . esc_attr( $link['title'] ) . '" '.$button_style .' >' . esc_attr( $link['title'] ) . '</a>';
            }
            if($btn_center == 'yes'){
                $btn_center_before = '<div class="text-center">';
                $btn_center_after = '</div>';
            }
            
            $button_html = $btn_center_before . '<div class="' . esc_attr( $css_class ) . '" ' . $_animation_delay . '><div class="btn-wrap">
                        ' . $read_more_html . '
                    </div></div>'. $btn_center_after; //end $css_class
            
            
            //Since theme v1.2
            
            if($type_type == 'default'){
                
                $button_html = '<div class="' . esc_attr( $css_class ) . '">' . $read_more_html . '</div>';
                
            }
 
        ob_start();
        
        echo balanceTags( $button_html ); 
        
        $result = ob_get_contents();
        ob_clean();
        return $result;
    }    
    
}