<?php
/**
 * @author  themepiko
 *  Simple Info Box
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

add_action( 'vc_before_init', 'pikoworks_simple_infobox' );
function pikoworks_simple_infobox(){
// Setting shortcode lastest
vc_map( array(
    "name"        => esc_html__( "Simple Infobox", 'pikoworks_core'),
    "base"        => "simple_infobox",
    "category"    => esc_html__('Pikoworks', 'pikoworks_core' ),
    "icon" => get_template_directory_uri() . "/assets/images/logo/vc-icon.png",
    "description" => esc_html__( "Content & possible bg image ", 'pikoworks_core'),
    "params"      => array(        
        array(
                "type" => "attach_image",
                "heading" => esc_attr__("Image", "pikoworks_core"),
                "param_name" => "image",
                "admin_label" => false,
                "value" => ""
        ),		
        array(
                "type" => "textfield",
                "heading" => esc_attr__("Caption", "pikoworks_core"),
                "param_name" => "caption",
                "admin_label" => true,
                "value" => esc_attr__("Caption Over Title", "pikoworks_core")
        ),				
        array(
                "type" => "textfield",
                "heading" => esc_attr__("Title", "pikoworks_core"),
                "param_name" => "title",
                "admin_label" => true,
                "value" => esc_attr__("Enter Title Here", "pikoworks_core")
        ),
        array(
                "type" => "textarea",
                "heading" => esc_attr__("Excerpt", "pikoworks_core"),
                "param_name" => "excerpt",
                "admin_label" => true,
                "value" => ""
        ),				
        array(
                "type" => "colorpicker",
                "heading" => esc_attr__("Foreground Color", "pikoworks_core"),
                "param_name" => "text_color",
                "admin_label" => false,
                "value" => "#ffffff"
        ),        
        array(
            'type' => 'vc_link',
            'param_name' => 'link',
            'heading' => esc_html__('Button link', 'pikoworks_core'),
        ),        
        array(
            "type"        => "textfield",
            "heading"     => esc_html__( "Extra class name", 'pikoworks_core' ),
            "param_name"  => "el_class",
            "description" => esc_html__( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "pikoworks_core" ),
        ),
         array(
            'type'           => 'css_editor',
            'heading'        => esc_html__( 'Css', 'pikoworks_core' ),
            'param_name'     => 'css',
            'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'pikoworks_core' ),
            'group'          => esc_html__( 'Design options', 'pikoworks_core' )
	)
    )
));
}
class WPBakeryShortCode_simple_infobox extends WPBakeryShortCode { 
    
    protected function content($atts, $content = null) {
        $atts = function_exists( 'vc_map_get_attributes' ) ? vc_map_get_attributes( 'simple_infobox', $atts ) : $atts;
        $atts = shortcode_atts( array(            
            'image'		=> '',
            'caption'		=> '',
            'title'		=> '',
            'excerpt'		=> '',
            'text_color'        => '#ffffff',
            'link'              => '',
            'el_class'           => '',
            'css'           => '',            
            
        ), $atts );
        extract($atts);
        
        $css_class = 'simlple-info-box wpb_text_column wpb_content_element ' . $el_class;
        if ( function_exists( 'vc_shortcode_custom_css_class' ) ):
            $css_class .= ' ' . apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), '', $atts );
        endif;
        
        $link_default = array(
            'url'       =>  '',
            'title'     =>  '',
            'target'    =>  '_self'
        );

        if ( $link != '' && function_exists( 'vc_build_link' ) ):
            $link = vc_build_link( $link );
        else:
            $link = $link_default;
        endif;
        
        $block_btn = $inine_style = $inine_style_border = '';
        
         $text_color = trim( $text_color ) != '' ? 'color: ' . esc_attr( $text_color ) . ';' : '';  
         $border_color = trim( $text_color ) != '' ? 'border-color: ' . esc_attr( $text_color ) . ';' : ''; 
            
        if ( trim( $text_color ) != '') {
                $inine_style = 'style="' .  esc_attr($text_color) .'"';
            }
        if ( trim( $text_color ) != '' ) {
                $inine_style_border = 'style="' .  esc_attr($text_color) . ' ' .  esc_attr($border_color) .'"';
        }
        
        if ( trim( $link['url'] ) != '' ) {
            $block_btn = '<a '.$inine_style_border.' href="' . esc_url( $link['url'] ) . '" target="' . esc_attr( $link['target'] ) . '" title="' . esc_attr( $link['title'] ) . '">' . esc_attr( $link['title'] ) . '</a>';
        }       
        
        
        ob_start();

        $output = '<div class="'.esc_attr($css_class).'">';

        $image = wp_get_attachment_image_src($image, 'full');
        if(isset($image[0])) {
                $output .= '<img src="'. esc_url($image[0]) .'" alt="' . esc_attr($caption) . '"/>';
        }
        
        $output .= '<section>';
        $output .= '<h5 '. $inine_style .'>'. esc_attr($caption) .'</h5>';
        $output .= '<h2 '. $inine_style .'>'. esc_attr($title) .'</h2>';
        if($excerpt) $output .= '<p '.$inine_style.'>'. $excerpt .'</p>';
        $output .=  $block_btn . PHP_EOL;
        $output .= '</section>';

        $output .= PHP_EOL .'</div>';
        
         echo balanceTags( $output ); 
        $result = ob_get_contents();
        ob_clean();
        return $result;
    }    
    
}