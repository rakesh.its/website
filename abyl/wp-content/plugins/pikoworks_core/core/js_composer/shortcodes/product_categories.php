<?php
/**
 * @author  themepiko
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
add_action( 'vc_before_init', 'pikoworks_loop_product_categories' );
function pikoworks_loop_product_categories(){ 

// Setting shortcode lastest
vc_map( array(
    "name"        => esc_html__( "Product Categories", 'pikoworks_core'),
    "base"        => "pikoworks_loop_product_categories",
    "category"    => esc_html__('Pikoworks', 'pikoworks_core' ),
    "icon" => get_template_directory_uri() . "/assets/images/logo/vc-icon.png",
    "description" => esc_html__( "Show product Categories", 'pikoworks_core'),
    "params"      => array(           
        array(
            "type"       => "dropdown",
            "heading"    => esc_html__("Order by", 'pikoworks_core'),
            "param_name" => "orderby",
            "value"      => array(
        	esc_html__('None', 'pikoworks_core')     => 'none',
                esc_html__('ID', 'pikoworks_core')       => 'ID',
                esc_html__('Author', 'pikoworks_core')   => 'author',
                esc_html__('Name', 'pikoworks_core')     => 'name',
                esc_html__('Date', 'pikoworks_core')     => 'date',
                esc_html__('Modified', 'pikoworks_core') => 'modified',
                esc_html__('Rand', 'pikoworks_core')     => 'rand',
        	),
            'std'         => 'ID',
            'admin_label' => true,
            "description" => esc_html__("Select how to sort retrieved posts.",'pikoworks_core')
        ),
        array(
            "type"       => "dropdown",
            "heading"    => esc_html__("Order", 'pikoworks_core'),
            "param_name" => "order",
            "value"      => array(
                esc_html__( 'Descending', 'pikoworks_core' ) => 'DESC',
                esc_html__( 'Ascending', 'pikoworks_core' )  => 'ASC'
        	),
            'std'         => 'DESC',
            'admin_label' => true,
            "description" => esc_html__("Designates the ascending or descending order.",'pikoworks_core')
        ),            
        
        // Carousel
        array(
            'type'  => 'dropdown',
            'value' => array(
                esc_html__( 'Yes', 'pikoworks_core' ) => 'true',
                esc_html__( 'No', 'pikoworks_core' )  => 'false'
            ),
            'std'         => 'false',
            'heading'     => esc_html__( 'AutoPlay', 'pikoworks_core' ),
            'param_name'  => 'autoplay',
            'group'       => esc_html__( 'Carousel settings', 'pikoworks_core' ),
            'admin_label' => true
		),
        array(
            'type'  => 'dropdown',
            'value' => array(
                esc_html__( 'Yes', 'pikoworks_core' ) => 'true',
                esc_html__( 'No', 'pikoworks_core' )  => 'false'
            ),
            'std'         => 'false',
            'heading'     => esc_html__( 'Navigation', 'pikoworks_core' ),
            'param_name'  => 'navigation',
            'description' => esc_html__( "Show buton 'next' and 'prev' buttons.", 'pikoworks_core' ),
            'group'       => esc_html__( 'Carousel settings', 'pikoworks_core' ),
            'admin_label' => true,
	),        
        array(
            'type'  => 'dropdown',
            'value' => array(
                esc_html__( 'Nav Center', 'pikoworks_core' ) => 'owl-nav-show-inner',
                esc_html__( 'Outside Center', 'pikoworks_core' )  => 'owl-nav-show',
                esc_html__( 'Hover Center', 'pikoworks_core' )  => 'owl-nav-show-hover'
            ),
            'std'         => 'owl-nav-show-inner',
            'heading'     => esc_html__( 'Next/Prev Button', 'pikoworks_core' ),
            'param_name'  => 'navigation_btn',
            'group'       => esc_html__( 'Carousel settings', 'pikoworks_core' ),
            'dependency' => array(
                        'element'   => 'navigation',
                        'value'     => array( 'true' ),
                    ),
        ),
        array(
            'type'  => 'dropdown',
            'value' => array(
                esc_html__( 'Yes', 'pikoworks_core' ) => 'true',
                esc_html__( 'No', 'pikoworks_core' )  => 'false'
            ),
            'std'         => 'false',
            'heading'     => esc_html__( 'Bullets', 'pikoworks_core' ),
            'param_name'  => 'dots',
            'description' => esc_html__( "Show Carousel bullets bottom", 'pikoworks_core' ),
            'group'       => esc_html__( 'Carousel settings', 'pikoworks_core' ),
            'admin_label' => true,
	),
        array(
            'type'  => 'dropdown',
            'value' => array(
                esc_html__( 'Yes', 'pikoworks_core' ) => 'true',
                esc_html__( 'No', 'pikoworks_core' )  => 'false'
            ),
            'std'         => 'false',
            'heading'     => esc_html__( 'Loop', 'pikoworks_core' ),
            'param_name'  => 'loop',
            'description' => esc_html__( "Inifnity loop. Duplicate last and first items to get loop illusion.", 'pikoworks_core' ),
            'group'       => esc_html__( 'Carousel settings', 'pikoworks_core' ),
            'admin_label' => true,
            ),
        array(
            "type"        => "pikoworks_number",
            "heading"     => esc_html__("Slide Speed", 'pikoworks_core'),
            "param_name"  => "slidespeed",
            "value"       => "200",
            "suffix"      => esc_html__("milliseconds", 'pikoworks_core'),
            "description" => esc_html__('Slide speed in milliseconds', 'pikoworks_core'),
            'group'       => esc_html__( 'Carousel settings', 'pikoworks_core' ),
	  	),
        array(
            "type"        => "pikoworks_number",
            "heading"     => esc_html__("Margin", 'pikoworks_core'),
            "param_name"  => "margin",
            "value"       => "30",
            "suffix"      => esc_html__("px", 'pikoworks_core'),
            "description" => esc_html__('Distance( or space) between 2 item', 'pikoworks_core'),
            'group'       => esc_html__( 'Carousel settings', 'pikoworks_core' )
	  	),
        
        array(
            'type'  => 'dropdown',
            'value' => array(
                esc_html__( 'Yes', 'pikoworks_core' ) => 1,
                esc_html__( 'No', 'pikoworks_core' )  => 0
            ),
            'std'         => 1,
            'heading'     => esc_html__( 'Single/Multiple Carousel', 'pikoworks_core' ),
            'param_name'  => 'use_responsive',
            'description' => esc_html__( "Yes is Multiple or No is Single Item carosuel", 'pikoworks_core' ),
            'group'       => esc_html__( 'Multi Columns', 'pikoworks_core' ),
            'admin_label' => true,            
	),
        array(
            'type'  => 'dropdown',
            'value' => array(
                esc_html__( '--- No Animation ---', 'pikoworks_core' ) => '',
                esc_html__( 'fadeIn', 'pikoworks_core' )  => 'fadeiN',
                esc_html__( 'slideInDown', 'pikoworks_core' )  => 'slidedowN',
                esc_html__( 'bounceInUp', 'pikoworks_core' )  => 'bounceRighT',
                esc_html__( 'zoomIn', 'pikoworks_core' )  => 'zoomiN',
                esc_html__( 'zoomIn two', 'pikoworks_core' )  => 'zoomin_2',
                esc_html__( 'zoomInDown', 'pikoworks_core' )  => 'zoomInDowN',
                esc_html__( 'fadeInLeft', 'pikoworks_core' )  => 'fadeInLefT',
                esc_html__( 'fadeInUp', 'pikoworks_core' )  => 'fadeInuP',
                esc_html__( 'slideInUp', 'pikoworks_core' )  => 'slideInuP',
            ),            
            'heading'     => esc_html__( 'Select Animation', 'pikoworks_core' ),
            'param_name'  => 'sanam',
            'group'       => esc_html__( 'Multi Columns', 'pikoworks_core' ),
            'dependency' => array(
                        'element'   => 'use_responsive',
                        'value'     => array( '0' ),
                    ),
	),
        array(
            "type"        => "pikoworks_number",
            "heading"     => esc_html__("The items on large Device (Screen resolution of device >= 1200px )", 'pikoworks_core'),
            "param_name"  => "items_large_device",
            "value"       => "4",
            "suffix"      => esc_html__("item", 'pikoworks_core'),
            "description" => esc_html__('The number of item on desktop', 'pikoworks_core'),
            'dependency' => array(
                        'element'   => 'use_responsive',
                        'value'     => array( '1' ),
                    ),
            'group'       => esc_html__( 'Multi Columns', 'pikoworks_core' ),
	  ),
        array(
            "type"        => "pikoworks_number",
            "heading"     => esc_html__("The items on desktop (Screen resolution of device >= 992px < 1199px )", 'pikoworks_core'),
            "param_name"  => "items_desktop",
            "value"       => "3",
            "suffix"      => esc_html__("item", 'pikoworks_core'),
            "description" => esc_html__('The number of item on desktop', 'pikoworks_core'),
            'dependency' => array(
                        'element'   => 'use_responsive',
                        'value'     => array( '1' ),
                    ),
            'group'       => esc_html__( 'Multi Columns', 'pikoworks_core' ),
	  ),
        array(
            "type"        => "pikoworks_number",
            "heading"     => esc_html__("The items on tablet (Screen resolution of device >=768px and < 992px )", 'pikoworks_core'),
            "param_name"  => "items_tablet",
            "value"       => "2",
            "suffix"      => esc_html__("item", 'pikoworks_core'),
            "description" => esc_html__('The number of item on desktop', 'pikoworks_core'),
            'dependency' => array(
                        'element'   => 'use_responsive',
                        'value'     => array( '1' ),
                    ),
            'group'       => esc_html__( 'Multi Columns', 'pikoworks_core' ),
	  	),
        array(
            "type"        => "pikoworks_number",
            "heading"     => esc_html__("The items on mobile (Screen resolution of device < 768px)", 'pikoworks_core'),
            "param_name"  => "items_mobile",
            "value"       => "1",
            "suffix"      => esc_html__("item", 'pikoworks_core'),
            "description" => esc_html__('The number of item on desktop', 'pikoworks_core'),
            'dependency' => array(
                        'element'   => 'use_responsive',
                        'value'     => array( '1' ),
                    ),
           'group'       => esc_html__( 'Multi Columns', 'pikoworks_core' ),
	 ),        
        array(
            "type"        => "textfield",
            "heading"     => esc_html__( "Extra class name", 'pikoworks_core' ),
            "param_name"  => "el_class",
            "description" => esc_html__( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "js_composer" ),
        ),
         array(
            'type'           => 'css_editor',
            'heading'        => esc_html__( 'Css', 'pikoworks_core' ),
            'param_name'     => 'css',
            'group'          => esc_html__( 'Design options', 'pikoworks_core' )
	)
    )
));
}
class WPBakeryShortCode_pikoworks_loop_product_categories extends WPBakeryShortCode { 
    
    protected function content($atts, $content = null) {
        $atts = function_exists( 'vc_map_get_attributes' ) ? vc_map_get_attributes( 'pikoworks_loop_product_categories', $atts ) : $atts;
        $atts = shortcode_atts( array(            
            'order' => 'DESC',
            'orderby' => 'ID',
            'el_class' => '',
            //Carousel            
            'autoplay'      => "false",
            'navigation'    => "false",
            'navigation_btn' => 'owl-nav-show-inner',
            'slidespeed'    => 250,
            'loop'          => "false",
            'dots'         => "false",
            'margin'        => 10,                 
            //Default
            'use_responsive' => 1,
            'sanam' => '',            
            'items_large_device'   => 4,
            'items_desktop'   => 3,
            'items_tablet'   => 2,
            'items_mobile'   => 1,
            'css'           => '',
            
            
        ), $atts );
        extract($atts);
        
        $css_class = 'style2' . $el_class . ' ' . $navigation_btn;
        if ( function_exists( 'vc_shortcode_custom_css_class' ) ):
            $css_class .= ' ' . apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), '', $atts );
        endif;
                   
        //single product animation    
        $single_animation = '';
        if($use_responsive == '0'){
            $single_animation = $sanam;
        }
        
        ob_start();        
        
        if ( class_exists('WooCommerce') ) :
            $data_carousel = array(
                "autoplay"      => esc_html($autoplay),
                "navigation"    => esc_html($navigation),
                "margin"        => esc_html($margin),
                "smartSpeed"    => esc_html($slidespeed),
                "loop"          => esc_html($loop),
                "theme"         => 'style-navigation-bottom',
                "autoheight"    => "false",
                'nav'           => esc_html($navigation),
                'dots'          => esc_html($dots),
            );
            if( $use_responsive == '1' ) {
                $arr = array(
                    '0' => array(
                        "items" => esc_html($items_mobile)
                    ), 
                    '768' => array(
                        "items" => esc_html($items_tablet)
                    ), 
                    '992' => array(
                        "items" => esc_html($items_desktop)
                    ),
                    '1200' => array(
                        "items" => esc_html($items_large_device)
                    )
                );
                $data_responsive = json_encode($arr);
                $data_carousel["responsive"] = $data_responsive;               
                
                
            }else{
                $data_carousel['items'] =  1;
                
            }
            
            if ( isset( $atts[ 'ids' ] ) ) {
              $ids = explode( ',', $atts[ 'ids' ] );
              $ids = array_map( 'trim', $ids );
            } else {
              $ids = array();
            }
            // get terms and workaround WP bug with parents/pad counts
              $args = array(  
                'orderby'    => esc_attr($orderby),
                'order'      => esc_attr($order),
                'hide_empty' => 0,
                'pad_counts'  => 1,
                'parent'      => 0,
                'include'    => $ids,
                'pad_counts' => true,
                'child_of'   => 0,
            );

              $product_categories = get_terms( 'product_cat', $args );
              $product_categories = array_slice( $product_categories, 0 );         
           
            ?>
            <div class="<?php echo esc_attr( $css_class ); ?>"> 
                <div class="owl-carousel <?php echo esc_attr( $single_animation ) ?>" <?php echo  _data_carousel( $data_carousel ); ?>>
                    <?php                            
                      if ( $product_categories ) {
                            foreach ( $product_categories as $category ) {
                              woocommerce_get_template( 'content-product_cat_slide.php', array(
                                'category' => $category,                
                              ) );
                            }
                          }
                          woocommerce_reset_loop();
                    ?>
                </div>
            </div>
            <?php
            
        endif;     

        $result = ob_get_contents();
        ob_clean();
        return $result;
    }
}