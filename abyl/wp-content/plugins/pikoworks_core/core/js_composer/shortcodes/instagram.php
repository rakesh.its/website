<?php
/**
 * @author  themepiko
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
add_action( 'vc_before_init', 'pikoworks_instagram' );
function pikoworks_instagram(){
 
   global $pikoworks_vc_anim_effects_in; 
// Setting shortcode lastest
vc_map( array(
    "name"        => esc_html__( "Instagram", 'pikoworks_core'),
    "base"        => "pikoworks_instagram",
    "category"    => esc_html__('Pikoworks', 'pikoworks_core' ),
    "icon" => get_template_directory_uri() . "/assets/images/logo/vc-icon.png",
    "description" => esc_html__( "Instagram Photo strem", 'pikoworks_core'),
    "params"      => array(
        
        array(
              "type" => "textfield",
              "heading" => esc_html__("Title", 'pikoworks_core'),
              "param_name" => "title",
            ),
            array(
              "type" => "textfield",
              "heading" => esc_html__("Username", 'pikoworks_core'),
              "param_name" => "username",
            ),
            array(
              "type" => "textfield",
              "heading" => esc_html__("Numer of photos", 'pikoworks_core'),
              "param_name" => "number",
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__( 'Photo size', 'pikoworks_core' ),
                'param_name' => 'size',
                'value' => array(                                
                        esc_html__( '320x320', 'pikoworks_core' ) => 'small',
                        esc_html__( '150x150', 'pikoworks_core' ) => 'thumbnail',
                        esc_html__( '640x640', 'pikoworks_core' ) => 'large',
                        esc_html__( '1040x1040', 'pikoworks_core' ) => 'original',
                ),
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__('Carousel Style', 'pikoworks_core'),
                'param_name' => 'is_slider',
                'value' => array(
                    esc_html__('Yes, please', 'pikoworks_core') => 'yes',
                    esc_html__('No', 'pikoworks_core') => 'no',
                    ),
                'std'         => 'no',
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__( 'Columns', 'pikoworks_core' ),
                'param_name' => 'columns',
                'value' => array(
                        2 => 2,
                        3 => 3,
                        4 => 4,
                        6 => 6,
                ),
                'dependency' => array(
                        'element'   => 'is_slider',
                        'value'     => array( 'no' ),
                    ),
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__( 'Open links in', 'pikoworks_core' ),
                'param_name' => 'target',
                'value' => array(
                        esc_html__( 'Current window (_self)', 'pikoworks_core' ) => '_self',
                        esc_html__( 'New window (_blank)', 'pikoworks_core' ) => '_blank',
                ),
            ),
            array(
              "type" => "textfield",
              "heading" => esc_html__("Link text", 'pikoworks_core'),
              "param_name" => "link",
            ),           
            // Carousel
            array(
                'type'  => 'dropdown',
                'value' => array(
                    esc_html__( 'Yes', 'pikoworks_core' ) => 'true',
                    esc_html__( 'No', 'pikoworks_core' )  => 'false'
                ),
                'std'         => 'false',
                'heading'     => esc_html__( 'AutoPlay', 'pikoworks_core' ),
                'param_name'  => 'autoplay',
                'group'       => esc_html__( 'Carousel settings', 'pikoworks_core' ),
                'dependency' => array(
                                'element'   => 'is_slider',
                                'value'     => array( 'yes' ),
                            ),
                'admin_label' => false
                    ),
            array(
                'type'  => 'dropdown',
                'value' => array(
                    esc_html__( 'Yes', 'pikoworks_core' ) => 'true',
                    esc_html__( 'No', 'pikoworks_core' )  => 'false'
                ),
                'std'         => 'false',
                'heading'     => esc_html__( 'Navigation', 'pikoworks_core' ),
                'param_name'  => 'navigation',
                'description' => esc_html__( "Show buton 'next' and 'prev' buttons.", 'pikoworks_core' ),
                'group'       => esc_html__( 'Carousel settings', 'pikoworks_core' ),
                'dependency' => array(
                                'element'   => 'is_slider',
                                'value'     => array( 'yes' ),
                            ),
                'admin_label' => false,
            ),                           
            array(
                'type'  => 'dropdown',
                'value' => array(
                    esc_html__( 'Nav Center', 'pikoworks_core' ) => 'owl-nav-show-inner',
                    esc_html__( 'Outside Center', 'pikoworks_core' )  => 'owl-nav-show',
                    esc_html__( 'Hover Center', 'pikoworks_core' )  => 'owl-nav-show-hover'
                ),
                'std'         => 'owl-nav-show-inner',
                'heading'     => esc_html__( 'Next/Prev Button', 'pikoworks_core' ),
                'param_name'  => 'navigation_btn',
                'group'       => esc_html__( 'Carousel settings', 'pikoworks_core' ),
                'dependency' => array(
                            'element'   => 'navigation',
                            'value'     => array( 'true' ),
                        ),
            ),
            array(
                'type'  => 'dropdown',
                'value' => array(
                    esc_html__( 'Yes', 'pikoworks_core' ) => 'true',
                    esc_html__( 'No', 'pikoworks_core' )  => 'false'
                ),
                'std'         => 'false',
                'heading'     => esc_html__( 'Bullets', 'pikoworks_core' ),
                'param_name'  => 'dots',
                'description' => esc_html__( "Show Carousel bullets bottom", 'pikoworks_core' ),
                'group'       => esc_html__( 'Carousel settings', 'pikoworks_core' ),
                'dependency' => array(
                                'element'   => 'is_slider',
                                'value'     => array( 'yes' ),
                            ),
                'admin_label' => false,
            ),
            array(
                'type'  => 'dropdown',
                'value' => array(
                    esc_html__( 'Yes', 'pikoworks_core' ) => 'true',
                    esc_html__( 'No', 'pikoworks_core' )  => 'false'
                ),
                'std'         => 'false',
                'heading'     => esc_html__( 'Loop', 'pikoworks_core' ),
                'param_name'  => 'loop',
                'description' => esc_html__( "Inifnity loop. Duplicate last and first items to get loop illusion.", 'pikoworks_core' ),
                'group'       => esc_html__( 'Carousel settings', 'pikoworks_core' ),
                'dependency' => array(
                                'element'   => 'is_slider',
                                'value'     => array( 'yes' ),
                            ),
                'admin_label' => false,
                ),
            array(
                "type"        => "pikoworks_number",
                "heading"     => esc_html__("Slide Speed", 'pikoworks_core'),
                "param_name"  => "slidespeed",
                "value"       => "200",
                "suffix"      => esc_html__("milliseconds", 'pikoworks_core'),
                "description" => esc_html__('Slide speed in milliseconds', 'pikoworks_core'),
                'group'       => esc_html__( 'Carousel settings', 'pikoworks_core' ),
                'dependency' => array(
                                'element'   => 'is_slider',
                                'value'     => array( 'yes' ),
                            ),
                'admin_label' => false,
                    ),
            array(
                "type"        => "pikoworks_number",
                "heading"     => esc_html__("Margin", 'pikoworks_core'),
                "param_name"  => "margin",
                "value"       => "30",
                "suffix"      => esc_html__("px", 'pikoworks_core'),
                "description" => esc_html__('Distance( or space) between 2 item', 'pikoworks_core'),
                'dependency' => array(
                                'element'   => 'is_slider',
                                'value'     => array( 'yes' ),
                            ),
                'group'       => esc_html__( 'Carousel settings', 'pikoworks_core' )
                    ),

            array(
                'type'  => 'dropdown',
                'value' => array(
                    esc_html__( 'Yes', 'pikoworks_core' ) => 1,
                    esc_html__( 'No', 'pikoworks_core' )  => 0
                ),
                'std'         => 1,
                'heading'     => esc_html__( 'Single/Multiple Carousel', 'pikoworks_core' ),
                'param_name'  => 'use_responsive',
                'description' => esc_html__( "Yes is Multiple or No is Single Item carosuel", 'pikoworks_core' ),
                'group'       => esc_html__( 'Multi Columns', 'pikoworks_core' ),
                'dependency' => array(
                                'element'   => 'is_slider',
                                'value'     => array( 'yes' ),
                            ),
                'admin_label' => false,            
            ),
            array(
                'type'  => 'dropdown',
                'value' => array(
                    esc_html__( '--- No Animation ---', 'pikoworks_core' ) => '',
                    esc_html__( 'fadeIn', 'pikoworks_core' )  => 'fadeiN',
                    esc_html__( 'slideInDown', 'pikoworks_core' )  => 'slidedowN',
                    esc_html__( 'bounceInUp', 'pikoworks_core' )  => 'bounceRighT',
                    esc_html__( 'zoomIn', 'pikoworks_core' )  => 'zoomiN',
                    esc_html__( 'zoomIn two', 'pikoworks_core' )  => 'zoomin_2',
                    esc_html__( 'zoomInDown', 'pikoworks_core' )  => 'zoomInDowN',
                    esc_html__( 'fadeInLeft', 'pikoworks_core' )  => 'fadeInLefT',
                    esc_html__( 'fadeInUp', 'pikoworks_core' )  => 'fadeInuP',
                    esc_html__( 'slideInUp', 'pikoworks_core' )  => 'slideInuP',
                ),            
                'heading'     => esc_html__( 'Select Animation', 'pikoworks_core' ),
                'param_name'  => 'sanam',
                'group'       => esc_html__( 'Multi Columns', 'pikoworks_core' ),
                'dependency' => array(
                            'element'   => 'use_responsive',
                            'value'     => array( '0' ),
                        ),
            ),
            array(
                "type"        => "pikoworks_number",
                "heading"     => esc_html__("The items on large Device (Screen resolution of device >= 1200px )", 'pikoworks_core'),
                "param_name"  => "items_large_device",
                "value"       => "4",
                "suffix"      => esc_html__("item", 'pikoworks_core'),
                "description" => esc_html__('The number of item on desktop', 'pikoworks_core'),
                'dependency' => array(
                            'element'   => 'use_responsive',
                            'value'     => array( '1' ),
                        ),
                'group'       => esc_html__( 'Multi Columns', 'pikoworks_core' ),
              ),
            array(
                "type"        => "pikoworks_number",
                "heading"     => esc_html__("The items on desktop (Screen resolution of device >= 992px < 1199px )", 'pikoworks_core'),
                "param_name"  => "items_desktop",
                "value"       => "3",
                "suffix"      => esc_html__("item", 'pikoworks_core'),
                "description" => esc_html__('The number of item on desktop', 'pikoworks_core'),
                'dependency' => array(
                            'element'   => 'use_responsive',
                            'value'     => array( '1' ),
                        ),
                'group'       => esc_html__( 'Multi Columns', 'pikoworks_core' ),
              ),
            array(
                "type"        => "pikoworks_number",
                "heading"     => esc_html__("The items on tablet (Screen resolution of device >=768px and < 992px )", 'pikoworks_core'),
                "param_name"  => "items_tablet",
                "value"       => "2",
                "suffix"      => esc_html__("item", 'pikoworks_core'),
                "description" => esc_html__('The number of item on desktop', 'pikoworks_core'),
                'dependency' => array(
                            'element'   => 'use_responsive',
                            'value'     => array( '1' ),
                        ),
                'group'       => esc_html__( 'Multi Columns', 'pikoworks_core' ),
                    ),
            array(
                "type"        => "pikoworks_number",
                "heading"     => esc_html__("The items on mobile (Screen resolution of device < 768px)", 'pikoworks_core'),
                "param_name"  => "items_mobile",
                "value"       => "1",
                "suffix"      => esc_html__("item", 'pikoworks_core'),
                "description" => esc_html__('The number of item on desktop', 'pikoworks_core'),
                'dependency' => array(
                            'element'   => 'use_responsive',
                            'value'     => array( '1' ),
                        ),
               'group'       => esc_html__( 'Multi Columns', 'pikoworks_core' ),
             ),
             array(
                'type'          => 'dropdown',
                'heading'       => esc_html__( 'CSS Animation', 'pikoworks_core' ),
                'param_name'    => 'css_animation',
                'value'         => $pikoworks_vc_anim_effects_in,
                'description'   => esc_html__( 'Choose your animation', 'pikoworks_core' )
            ),
            array(
                    'type'          => 'textfield',
                    'heading'       => esc_html__( 'Animation Delay', 'pikoworks_core' ),
                    'param_name'    => 'animation_delay',
                    'std'           => '0.5',
                    'description'   => esc_html__( 'Delay unit is second.', 'pikoworks_core' ),
                    'dependency' => array(
                                    'element'   => 'css_animation',
                                    'not_empty' => true,
                                ),
            ),
            array(
                "type"        => "textfield",
                "heading"     => esc_html__( "Extra class name", 'pikoworks_core' ),
                "param_name"  => "el_class",
                "description" => esc_html__( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "pikoworks_core" ),
            ),

             array(
                'type'           => 'css_editor',
                'heading'        => esc_html__( 'Css', 'pikoworks_core' ),
                'param_name'     => 'css',
                'group'          => esc_html__( 'Design options', 'pikoworks_core' )
            )
    )
));

}
class WPBakeryShortCode_pikoworks_instagram extends WPBakeryShortCode { 
    
    protected function content($atts, $content = null) {
        $args = function_exists( 'vc_map_get_attributes' ) ? vc_map_get_attributes( 'pikoworks_instagram', $atts ) : $atts;
        $args = shortcode_atts( array(            
            'title'  => '',
            'username'  => '',
            'number'  => 9,
            'columns'  => 4,
            'size'  => 'small',
            'target'  => '',
            'link'  => '',            
            
            'use_responsive' => '1',
            'sanam' => '',
            'is_slider' => '',
            //Carousel            
            'autoplay'      => "false",
            'navigation'    => "false",
            'navigation_btn' => 'owl-nav-show-inner',
            'slidespeed'    => 250,
            'loop'          => "false",
            'dots'         => "false",
            'margin'        => 10,                 
            //Default
            'items_large_device'   => 4,
            'items_desktop'   => 3,
            'items_tablet'   => 2,
            'items_mobile'   => 1,
        
            'el_class'     =>  '',
            'css_animation'     =>  '',
            'animation_delay'   =>  '0.5',   // In second
            'css'           => '',
            
            
        ), $atts );
        extract($args);
        
        $css_class = 'instagram-wrap wow ' . $css_animation . ' ' . $el_class;
        if ( function_exists( 'vc_shortcode_custom_css_class' ) ):
            $css_class .= ' ' . apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), '', $args );
        endif;

         if ( !is_numeric( $animation_delay ) ) {
                $animation_delay = 0;
            }    
            $animation_delay = $animation_delay . 's';
        
        ob_start();  ?>        
        <div class="<?php echo esc_attr( $css_class ) ?>" data-wow-delay="<?php echo esc_attr( $animation_delay ) ?>" >
            <?php the_widget( 'pikoworks_Instagram_Widget', $args ); ?>
        </div>

        <?php
        $result = ob_get_contents();
        ob_clean();
        return $result;
    }
}