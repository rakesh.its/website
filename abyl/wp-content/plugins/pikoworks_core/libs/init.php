<?php
// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}
 

 /**
 * Load Redux Framework 
 */
if ( !class_exists( 'ReduxFramework' ) && file_exists( PIKOWORKSCORE_LIBS . 'admin/ReduxCore/framework.php' ) ) {
    require_once( PIKOWORKSCORE_LIBS . 'admin/ReduxCore/framework.php' );
}
if( file_exists(PIKOWORKSCORE_LIBS . 'admin/redux-extensions/extensions-init.php') ){
 	require_once(PIKOWORKSCORE_LIBS . 'admin/redux-extensions/extensions-init.php' );
 } 
 /**
 * Load Metaboxes
 */
 if ( ! class_exists( 'RW_Meta_Box' ) ){
    require_once PIKOWORKSCORE_LIBS.'/admin/meta-box/meta-box.php';
}

 //Check Mobile tablet device
if( ! class_exists( 'Mobile_Detect' ) ){
    require_once PIKOWORKSCORE_LIBS.'classes/Mobile_Detect.php';
}
$detect = new Mobile_Detect;
if( ! function_exists( 'pikoworks_is_mobile' ) ){
    function pikoworks_is_mobile(){
        global $detect;
        return $detect->isMobile();
    }
}
if( ! function_exists( 'pikoworks_is_tablet' ) ){
    function pikoworks_is_tablet(){
        global $detect;
        return $detect->isTablet();
    }
}
 /**
 * Load ajax mailchime 
 */
require_once PIKOWORKSCORE_LIBS.'news_letter.php';