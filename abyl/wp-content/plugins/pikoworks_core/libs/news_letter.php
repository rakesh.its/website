<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

function pikoworks_submit_mailchimp_via_ajax() {
    
    if ( !class_exists( 'MCAPI' ) ) {
        require_once( PIKOWORKSCORE_LIBS. 'classes/MCAPI.class.php' );
    }
    
    $response = array(
        'html'      =>  '',
        'message'   =>  '',
        'success'   =>  'no'
    );
    
    $api_key = isset( $_POST['api_key'] ) ? $_POST['api_key'] : '';
    $list_id = isset( $_POST['list_id'] ) ? $_POST['list_id'] : '';
    $success_message = isset( $_POST['success_message'] ) ? $_POST['success_message'] : '';
    $email = isset( $_POST['email'] ) ? $_POST['email'] : '';
    
    $response['message'] = esc_html__( 'Failed', 'construct-core' );
    
    $merge_vars = array();
    
    if ( class_exists( 'MCAPI' ) ) {
        $api = new MCAPI( $api_key );
        if(  $api->listSubscribe( $list_id, $email, $merge_vars ) === true ) {
            $response['message'] = sanitize_text_field( $success_message );
            $response['success'] = 'yes';
    	} else {
            // Sending failed message
            $response['message'] = $api->errorMessage;
    	}
    }
    
    wp_send_json( $response );
    die();
}
add_action( 'wp_ajax_pikoworks_submit_mailchimp_via_ajax', 'pikoworks_submit_mailchimp_via_ajax' );
add_action( 'wp_ajax_nopriv_pikoworks_submit_mailchimp_via_ajax', 'pikoworks_submit_mailchimp_via_ajax' );


if ( isset( $_POST['news_letter'] ) ) {
    
    if ( !class_exists( 'MCAPI' ) ) {
        require_once( PIKOWORKSCORE_LIBS. 'classes/MCAPI.class.php' );
    }
    
    $api_key          = isset( $_POST['api_key'] ) ? $_POST['api_key'] : '';
    $list_id          = isset( $_POST['list_id'] ) ? $_POST['list_id'] : '';
    $success_message = isset( $_POST['success_message'] ) ? $_POST['success_message'] : esc_html__( 'Failed', 'pikoworks_core' );	
    $email = isset( $_POST['email'] ) ? sanitize_email( $_POST['email'] ) : '';
    //$merge_vars      = array( 'FIRSTNAME' => $fname, 'LASTNAME' => $lname );
    $merge_vars = array();
    
    if ( trim( $api_key ) != '' && trim( $list_id ) != '' && is_email( $email ) && class_exists( 'MCAPI' ) )  {
        $api = new MCAPI( $api_key );
        if(  $api->listSubscribe( $list_id, $email, $merge_vars ) === true ) {
    	   //echo  '<div class="mailchip-success">' . $success_message . '</div>';
    	} else {
    	   //echo  '<div class="mailchip-success">' . $api->errorMessage . '</div>';
    	}
    }
}