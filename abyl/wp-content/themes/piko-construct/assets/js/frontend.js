/*!
*Template Name: construct
*Author: ThemePiko
*url: http://themepiko.com
*Version: 1.0
*/
;window.pikoworks = {};
function get_ajax_loading(){
    return jQuery('.pikoworks-ajax-loading');
}
function get_message_box(){
    return jQuery('.pikoworks-global-message');
}
function get_overlay(){
    return jQuery('.pikoworks-overlay');
}
function pikoworks_get_container_width(){
    var container_width = jQuery('#page_wrapper > .main > .row').innerWidth() - 30;
    if(jQuery('body').is('.header-layout-3') || jQuery('body').is('.header-layout-4')){
        if(jQuery(window).width() > 992){
            container_width = jQuery(window).width();
        }
    }
    return container_width;
}
function pikoworks_generate_rand(){
    return Math.round(new Date().getTime() + (Math.random() * 1000));
}
function addStyleSheet( css ) {
    var head, styleElement;
    head = document.getElementsByTagName('head')[0];
    styleElement = document.createElement('style');
    styleElement.setAttribute('type', 'text/css');
    if (styleElement.styleSheet) {
        styleElement.styleSheet.cssText = css;
    } else {
        styleElement.appendChild(document.createTextNode(css));
    }
    head.appendChild(styleElement);
    return styleElement;
}

// jQuery fn extend
(function(pikoworks, $) {

    pikoworks = pikoworks || {};

    $.extend( pikoworks, {
        options : {
            debug : true,
            show_sticky_header : pikoworks_global_message.enable_sticky_header == '1' ? true : false,
            default_timer : 20,
            show_ajax_overlay : true,
            infiniteConfig : {
                navSelector  : "div.pagination",
                nextSelector : "div.pagination a.next",
                loading      : {
                    finished: function(){
                        $('.pikoworks-infinite-loading').hide();
                    },
                    finishedMsg: "xx",
                    msg: $("<div class='pikoworks-infinite-loading'><div></div></div>")
                }
            }
        },
        helpers : {
            is_email : function(email){
                var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                return re.test(email);
            },
            is_cookie_enabled : function(){
                if (navigator.cookieEnabled) return true;
                // set and read cookie
                document.cookie = "cookietest=1";
                var ret = document.cookie.indexOf("cookietest=") != -1;
                // delete cookie
                document.cookie = "cookietest=1; expires=Thu, 01-Jan-1970 00:00:01 GMT";
                return ret;
            },
            is_touch_device : function(){
                return !!('ontouchstart' in window) // works on most browsers
                    || !!('onmsgesturechange' in window); // works on ie10
            }
        }
    });

}).apply(this, [window.pikoworks, jQuery]);


// jQuery fn extend
(function(pikoworks, $) {
    "use strict";

    pikoworks = pikoworks || {};

    // Default Extend
      // Post Comment From
    $.extend(pikoworks, {
        PostComment : {
             initialize : function(){
                 var self = this;
                 self.events();
                 return self;
             },
            
             events : function(){
                 var self = this;
                 /**
                  * Comment Form
                  */
                 try {
                     $('#commentform').on('submit',function(){
                         if($('#commentform #author').length > 0 &&  $('#commentform #author').val() == ''){
                             alert(pikoworks_global_message.global.comment_author);
                             $('#commentform #author').focus();
                             return false;
                         }
                         if($('#commentform #email').length > 0 && !pikoworks.helpers.is_email($('#commentform #email').val())){
                             alert(pikoworks_global_message.global.comment_email);
                             $('#commentform #email').focus();
                             return false;
                         }
                         if($('#commentform #rating').length > 0 && $('#commentform #rating').val() == ''){
                             alert(pikoworks_global_message.global.comment_rating);
                             return false;
                         }
                         if($('#commentform #comment').length > 0 && $('#commentform #comment').val() == ''){
                             alert(pikoworks_global_message.global.comment_content);
                             $('#commentform #comment').focus();
                             return false;
                         }
                     });
                 } catch (ex) { log_js(ex); }                 
                 return self;
             }
         }        
    });

    // Mega Menu
    $.extend(pikoworks, {
        MegaMenu: {
            defaults: {
                menu: $('.mega-menu'),
                hoverIntentConfig : {
                    sensitivity: 2,
                    interval: 0,
                    timeout: 0
                },
                rtl:false
            },

            initialize: function(options) {

                this.$setting = $.extend(this.defaults,options);

                this.$menu = this.$setting.menu;

                this.build()
                    .events();

                return this;
            },

            IsSidebarMenu : function( $menu ){
                return $menu.closest('.mega-menu-sidebar').length;
            },
            IsRightMenu : function( $menu ){
                return false;
            },
            popupWidth : function(){
                var winWidth = $(window).width();

                if (winWidth >= pikoworks_get_container_width())
                    return pikoworks_get_container_width();
                if (winWidth >= 992)
                    return 940;
                if (winWidth >= 768)
                    return 720;

                return $(window).width() - 30;
            },
            build: function() {
                var self = this;

                self.$menu.each( function() {
                    var $menu = $(this);
                    var is_sidebar_menu = self.IsSidebarMenu( $menu );
                    var is_right_menu = self.IsRightMenu( $menu );

                    if ( is_sidebar_menu ){
                        self._side_menu( self, $menu );
                    }else{
                        self._normal_menu( self, $menu );
                    }
                });

                return self;
            },
            _normal_menu : function( self, $menu ) {
                var $menu_container = $menu.closest('.columns');
                var container_width = self.popupWidth();
                var offset = 0;


                if($(window).width() >= $menu_container.width()){
                    container_width = $menu_container.width();
                }

                if ($menu_container.length) {
                    if (self.$setting.rtl) {
                        offset = ($menu_container.offset().left + $menu_container.width()) - ($menu.offset().left + $menu.width()) + parseInt($menu_container.css('padding-right'));
                    } else {
                        offset = $menu.offset().left - $menu_container.offset().left - parseInt($menu_container.css('padding-left'));
                    }
                    offset = (offset == 1) ? 0 : offset;
                }

                var $menu_items = $menu.find('> li');

                $menu_items.each( function() {
                    var $menu_item = $(this);
                    var $popup = $menu_item.find('> .popup');
                    if ($popup.length > 0) {
                        $popup.css('display', 'block');
                        if ($menu_item.hasClass('wide')) {
                            $popup.css('left', 0);
                            var padding = parseInt($popup.css('padding-left')) + parseInt($popup.css('padding-right')) +
                                parseInt($popup.find('> .inner').css('padding-left')) + parseInt($popup.find('> .inner').css('padding-right'));

                            var row_number = 4;

                            if ($menu_item.hasClass('col-2')) row_number = 2;
                            if ($menu_item.hasClass('col-3')) row_number = 3;
                            if ($menu_item.hasClass('col-4')) row_number = 4;
                            if ($menu_item.hasClass('col-5')) row_number = 5;
                            if ($menu_item.hasClass('col-6')) row_number = 6;

                            if ($(window).width() < 992)
                                row_number = 1;

                            var col_length = 0;
                            $popup.find('> .inner > ul > li').each(function() {
                                var cols = parseInt($(this).attr('data-cols'));
                                if (cols < 1)
                                    cols = 1;

                                if (cols > row_number)
                                    cols = row_number;

                                col_length += cols;
                            });

                            if (col_length > row_number) col_length = row_number;

                            var popup_max_width = $popup.find('.inner').css('max-width');

                            var col_width = container_width / row_number;

                            if (popup_max_width != 'none' && parseInt(popup_max_width) < container_width) {

                                col_width = parseInt(popup_max_width) / row_number;
                            }

                            $popup.find('> .inner > ul > li').each(function() {
                                var cols = parseFloat($(this).attr('data-cols'));
                                if (cols < 1)
                                    cols = 1;

                                if (cols > row_number)
                                    cols = row_number;

                                if ($menu_item.hasClass('pos-center') || $menu_item.hasClass('pos-left') || $menu_item.hasClass('pos-right'))
                                    $(this).css('width', (100 / col_length * cols) + '%');
                                else
                                    $(this).css('width', (100 / row_number * cols) + '%');
                            });

                            if ($menu_item.hasClass('pos-center')) { // position center
                                $popup.find('> .inner > ul').width(col_width * col_length - padding);
                                var left_position = $popup.offset().left - ($(window).width() - col_width * col_length) / 2;
                                $popup.css({
                                    'left': -left_position
                                });
                            } else if ($menu_item.hasClass('pos-left')) { // position left
                                $popup.find('> .inner > ul').width(col_width * col_length - padding);
                                $popup.css({
                                    'left': 0
                                });
                            } else if ($menu_item.hasClass('pos-right')) { // position right
                                $popup.find('> .inner > ul').width(col_width * col_length - padding);
                                $popup.css({
                                    'left': 'auto',
                                    'right': 0
                                });
                            } else { // position justify
                                $popup.find('> .inner > ul').width(container_width - padding);
                                if (self.$setting.rtl) {
                                    $popup.css({
                                        'right': 0,
                                        'left': 'auto'
                                    });
                                    var right_position = ($popup.offset().left + $popup.width()) - ($menu.offset().left + $menu.width()) - offset;
                                    $popup.css({
                                        'right': right_position,
                                        'left': 'auto'
                                    });
                                } else {
                                    $popup.css({
                                        'left': 0,
                                        'right': 'auto'
                                    });
                                    var left_position = $popup.offset().left - $menu.offset().left + offset;
                                    $popup.css({
                                        'left': -left_position,
                                        'right': 'auto'
                                    });
                                }
                            }
                        }
                        if (!($menu.hasClass('effect-down')))
                            $popup.css('display', 'none');

                        $menu_item.hoverIntent(
                            $.extend({}, self.$setting.hoverIntentConfig, {
                                over: function(){
                                    if (!($menu.hasClass('effect-down')))
                                        $menu_items.find('.popup').hide();
                                    $popup.show();
                                },
                                out: function(){
                                    if (!($menu.hasClass('effect-down')))
                                        $popup.hide();
                                }
                            })
                        );
                    }
                });
            },
            _side_menu : function( self, $menu ) {
                var $menu_container = $menu.closest('.container');
                var container_width;
                if ($(window).width() < 992 ){
                    container_width = self.popupWidth();
                }
                else{
                    container_width = self.popupWidth() - 45;
                    if( $menu.closest('body').hasClass('header-layout-3') || $menu.closest('body').hasClass('header-layout-4') ){
                        container_width = container_width - $menu.width() - 40;
                    }
                }

                var is_right_sidebar = self.IsRightMenu($menu);

                var $menu_items = $menu.find('> li');

                $menu_items.each( function() {
                    var $menu_item = $(this);
                    var $popup = $menu_item.find('> .popup');
                    if ($popup.length > 0) {
                        $popup.css('display', 'block');
                        if ($menu_item.hasClass('wide')) {
                            $popup.css('left', 0);
                            var padding = parseInt($popup.css('padding-left')) + parseInt($popup.css('padding-right')) +
                                parseInt($popup.find('> .inner').css('padding-left')) + parseInt($popup.find('> .inner').css('padding-right'));

                            var row_number = 4;

                            if ($menu_item.hasClass('col-2')) row_number = 2;
                            if ($menu_item.hasClass('col-3')) row_number = 3;
                            if ($menu_item.hasClass('col-4')) row_number = 4;
                            if ($menu_item.hasClass('col-5')) row_number = 5;
                            if ($menu_item.hasClass('col-6')) row_number = 6;

                            if ($(window).width() < 992)
                                row_number = 1;

                            var col_length = 0;
                            $popup.find('> .inner > ul > li').each(function() {
                                var cols = parseInt($(this).attr('data-cols'));
                                if (cols < 1)
                                    cols = 1;

                                if (cols > row_number)
                                    cols = row_number;

                                col_length += cols;
                            });

                            if (col_length > row_number) col_length = row_number;

                            var popup_max_width = $popup.find('.inner').css('max-width');
                            var col_width = container_width / row_number;
                            if ('none' !== popup_max_width && popup_max_width < container_width) {
                                col_width = parseInt(popup_max_width) / row_number;
                            }

                            $popup.find('> .inner > ul > li').each(function() {
                                var cols = parseFloat($(this).attr('data-cols'));
                                if (cols < 1)
                                    cols = 1;

                                if (cols > row_number)
                                    cols = row_number;

                                if ($menu_item.hasClass('pos-center') || $menu_item.hasClass('pos-left') || $menu_item.hasClass('pos-right'))
                                    $(this).css('width', (100 / col_length * cols) + '%');
                                else
                                    $(this).css('width', (100 / row_number * cols) + '%');
                            });

                            $popup.find('> .inner > ul').width(col_width * col_length + 1);
                            if (is_right_sidebar) {
                                $popup.css({
                                    'left': 'auto',
                                    'right': $(this).width()
                                });
                            } else {
                                $popup.css({
                                    'left': $(this).width(),
                                    'right': 'auto'
                                });
                            }
                        }
                        if (!($menu.hasClass('subeffect-down')))
                            $popup.css('display', 'none');

                        $menu_item.hoverIntent(
                            $.extend({}, self.$setting.hoverIntentConfig, {
                                over: function(){
                                    if (!($menu.hasClass('subeffect-down')))
                                        $menu_items.find('.popup').hide();
                                    $popup.show();
                                    $popup.parent().addClass('open');
                                },
                                out: function(){
                                    if (!($menu.hasClass('subeffect-down')))
                                        $popup.hide();
                                    $popup.parent().removeClass('open');
                                }
                            })
                        );
                    }
                });
            },
            events: function() {
                var self = this;

                $('.header-toogle-menu-button').on('click',function(){
                    if($(this).hasClass('active')){
                        $('.header-toogle-menu-button').removeClass('active');
                    }else{
                        $('.header-toogle-menu-button').addClass('active');
                    }
                    $('.header-wrapper .mega-menu-sidebar').toggleClass('open-menu');
                });


                $(window).on('resize', function() {
                    self.build();
                });

                setTimeout(function() {
                    self.build();
                }, 400);

                return self;
            }
        }
    });


    // Accordion Menu
    $.extend(pikoworks, {

        AccordionMenu: {

            defaults: {
                menu: $('.accordion-menu')
            },

            initialize: function($menu) {
                this.$menu = ($menu || this.defaults.menu);

                this.events()
                    .build();

                return this;
            },

            build: function() {
                var self = this;

                self.$menu.find('li.menu-item.active').each(function() {
                    if ($(this).find('> .arrow').length)
                        $(this).find('> .arrow').trigger('click');
                });

                return self;
            },

            events: function() {
                var self = this;

                self.$menu.find('.arrow').on('click',function() {
                    var $parent = $(this).parent();
                    $(this).next().stop().slideToggle();
                    if ($parent.hasClass('open')) {
                        $parent.removeClass('open');
                    } else {
                        $parent.addClass('open');
                    }
                });

                $(document).on('click', '.toggle-menu-mobile-button, #mobile_menu_wrapper_overlay', function(e){                   
                    e.preventDefault();
                    $('body').toggleClass('open-mobile-menu');
                });
                $(window).resize(function(){
                    if($(window).width() > 992){
                        $('body').removeClass('open-mobile-menu');
                    }
                })
                return self;
            }
        }

    });
     // StickyHeader
    $.extend(pikoworks, {
        StickyHeader: {

            defaults: {
                header: $('#header')
            },

            initialize: function($header) {
                this.$header = ($header || this.defaults.header);
                this.sticky_height = 0;
                this.sticky_offset = 0;
                this.sticky_pos = 0;

                this.$header = ($header || this.defaults.header);

                if (!pikoworks.options.show_sticky_header || !this.$header.length)
                    return this;

                var self = this;

                self.reset()
                    .build()
                    .events();
                return self;
            },

            build: function() {
                var self = this;

                var scroll_top = $(window).scrollTop(),
                    $this_header_sticky = self.$header;

                if(self.$header.css('position') == 'fixed'){
                    $this_header_sticky = self.$header;
                }
                if(self.$header.find('.header-main').css('position') == 'fixed'){
                    $this_header_sticky = self.$header.find('.header-main');
                }
                if(self.$header.find('.main-menu-wrap').css('position') == 'fixed'){
                    $this_header_sticky = self.$header.find('.main-menu-wrap');
                }              

                if (scroll_top > self.sticky_pos) {
                    self.$header.addClass('active-sticky');
                    self.$header.find('#main-menu').addClass('container');
                    $this_header_sticky.css({
                        'top' : self.adminbar_height
                    });
                }else{
                    self.$header.removeClass('active-sticky');
                    self.$header.find('#main-menu').removeClass('container');
                    $this_header_sticky.removeAttr('style');
                }

                return self;
            },

            reset: function() {
                var self = this;

                var $admin_bar = $('#wpadminbar');
                var height = 0;
                if ($admin_bar.length) {
                    height = $('#wpadminbar').css('position') == 'fixed' ? $('#wpadminbar').height() : 0;
                }
                self.adminbar_height = height;

                if( self.$header.closest('body').is('.header-layout-3')){
                    self.sticky_pos = self.$header.find('.header-main').height() + self.adminbar_height;
                }else{
                    self.sticky_pos = self.$header.height() + self.adminbar_height;
                }

                self.$header.removeAttr('style');

                return self;
            },

            events: function() {
                var self = this;

                $(window).on('resize', function() {
                    self.reset()
                        .build();
                });

                $(window).on('scroll', function() {
                    self.build();
                });

                return self;
            }
        }
    });

}).apply(this, [window.pikoworks, jQuery]);

(function(pikoworks, $) {
    "use strict";

    $(document).foundation({
        reveal : {
            animation : 'fade'
        },
        dropdown: {
            opened : function () {
                var $this = jQuery(this);
                $this.removeClass('animated fadeOutDown').addClass('animated fadeInUp');
            },
            closed : function () {
                var $this = jQuery(this);
                $this.removeClass('animated fadeInUp').addClass('animated fadeOutDown');
                setTimeout(function(){
                    $this.removeClass('animated fadeOutDown');
                },200);
            }
        }
    });

    function pikoworks_init(){

        if (typeof pikoworks.DefaultExtend !== 'undefined') {
            pikoworks.DefaultExtend.initialize();
        }
        // Post Comment
        if (typeof pikoworks.PostComment !== 'undefined') {
            pikoworks.PostComment.initialize();
        }

        // Mega Menu
        if (typeof pikoworks.MegaMenu !== 'undefined') {
            pikoworks.MegaMenu.initialize();
        }

        // Mega Menu
        if (typeof pikoworks.AccordionMenu !== 'undefined') {
            pikoworks.AccordionMenu.initialize();
        }

        // Sticky Header
        if (typeof pikoworks.StickyHeader !== 'undefined') {
            pikoworks.StickyHeader.initialize();
        }

        $('.slick-slider').trigger('resize');
        setTimeout(function(){
            $('.slick-slider').trigger('resize');
        },200);

    }
    $(document).ready(function() {
        pikoworks_init();
        $(window).on('vc_reload', function() {
            pikoworks_init();
        });
    });

}).apply(this, [window.pikoworks, jQuery]);

(function($) {
    'use strict';

    $(document).ready(function(){
        function fixMenuHeader4(){
            if($('body').is('.header-layout-4')){
                var $menu = $('.header-wrapper .mega-menu-sidebar .main-menu-wrap');
                if($(window).height() > $menu.innerHeight()){
                    $menu.css({
                        'margin-top' : parseInt( ($(window).height() - $menu.innerHeight()) / 2 )
                    });
                }else{
                    $menu.css({
                        'margin-top': 0
                    });
                }
            }
        }
        fixMenuHeader4();

        $(window).scroll(function(){
            if($('body').is('.header-layout-4')){
                var scroll_top = $(window).scrollTop(),
                    $menu = $('.header-wrapper .mega-menu-sidebar .main-menu-wrap');
                if($('#header').is('.active-sticky')){
                    $('.header-wrapper').css('position','static');
                    $menu.css({
                        'margin-top' : 0,
                        'top': scroll_top + parseInt( ($(window).height() - $menu.innerHeight()) / 4 )
                    })
                }else{
                    $('.header-wrapper').removeAttr('style');
                    $menu.css({
                        'margin-top' : parseInt( ($(window).height() - $menu.innerHeight()) / 3 ),
                        'top': 0
                    })
                }
            }
        });

        function fixBannerTitle(){
            var body_font_size = parseInt($('body').css('font-size').match(/\d+/gi));
            $('.ult-new-ib-title,.ult-new-ib-content').each(function(){
                var random_class = 'ult-new-ib-title-' + pikoworks_generate_rand(),
                    prefix = $(this).css('font-size').replace(/\d+/gi,''),
                    this_font_size = parseInt($(this).css('font-size').match(/\d+/gi)),
                    md_size = (this_font_size / 992) * 800,
                    xs_size = (this_font_size / 992) * 500,
                    style_html = '';
                $(this).addClass(random_class);

                style_html += '@media only screen and ( max-width:992px ) and ( min-width:768px ){';
                style_html += '.' + random_class + '{';
                style_html += 'font-size:' + (md_size > body_font_size ? md_size : parseInt(body_font_size + 2) ) + prefix +' !important;';
                style_html += '}';
                style_html += '}';
                style_html += '@media only screen and ( max-width:768px ){';
                style_html += '.' + random_class + '{';
                style_html += 'font-size:' + (xs_size > body_font_size ? xs_size : parseInt(body_font_size + 2) ) + prefix + ' !important;';
                style_html += '}';
                style_html += '}';
                addStyleSheet(style_html);
            });
        }
        fixBannerTitle();


        if(pikoworks.helpers.is_touch_device()){
            var elements = '.pikoworks-block-banner a';
            $(document).on('click',elements,function(e){
                if(!$(this).hasClass('click-go-go')){
                    e.preventDefault();
                    $(this).addClass('click-go-go');
                }
            });
            $('.products .product_link').each(function(){
                $(this).closest('.product_images_wrapper').addClass('is_touch_devices');
            })
        }
    })
})(jQuery);


//main function
(function ($) {
	"use strict";
        var rtl = jQuery( 'body' ).hasClass( 'rtl' );
	var Pikoworks = {
		initialised: false,
		mobile: false,
		container : $('#portfolio-item-container'),
		gridMasonry: $('.column-wrap'),
		init: function () {

			if(!this.initialised) {
				this.initialised = true;
			} else {
				return;
			}

			// Call Pikoworks Functions
			this.checkMobile();
			this.preLoader();
			this.onepageNav();
			this.counterVal();
			this.hashChange();
			
			
			this.progressBars();
			
			
			this.categoriesAccordion();
			this.countDowntime();
                        this.shareGroup();
			this.socialShear();
			this.toggleLogin();
			this.backtoTop();
			this.buttonEffect();
			this.fix_slide_after_menu();
			
                        
                        /* Call function if plugin is included */
                        if ($.fn.owlCarousel) {
                                this.owlsettingCarousel();
				this.owlCarouselsInit();
			}
			if ( typeof Swiper === 'function' ) {
				this.swiperCarousels();
                                this.productsinglePage();
			}
                        if (typeof WOW === 'function') { 
                          new WOW().init();
                        }
			

			var self = this;
			/* Imagesloaded plugin included in isotope.pkgd.min.js */
			/* Portfolio isotope + Blog masonry with images loaded plugin */
			if ( typeof imagesLoaded === 'function' ) {
				imagesLoaded(self.container, function() {
//					self.isotopeActivate();
					// recall for plugin support
//					self.isotopeFilter();
				});				

				// Porudcts Masonry
				imagesLoaded(self.gridMasonry, function() {
					self.gridMasonryCol();
				});
			}
                        
                        
                        this.mailChimp();
                        this.prettyPhotoInit();
                        if ($.fn.fancySelect) {
                            //this.fancySelect(); //[not works]
                        }

		},
                preLoader : function() {
                    /* preloader*/
                    setTimeout(function() {
                        $('#site-loading').fadeOut(300);
                    }, 300);
                },
                
		checkMobile: function () {
			/* Mobile Detect*/
			if ( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test( navigator.userAgent ) ) {
				this.mobile = true;
			} else {
				this.mobile = false;
			}
		},
                hashChange: function(){
                    /**
                     * Makes "skip to content" link work correctly in IE9, Chrome, and Opera
                     * for better accessibility.
                     *
                     * @link http://www.nczonline.net/blog/2013/01/15/fixing-skip-to-content-links/
                     */
                        var isWebkit = navigator.userAgent.toLowerCase().indexOf( 'webkit' ) > -1,
                                isOpera  = navigator.userAgent.toLowerCase().indexOf( 'opera' )  > -1,
                                isIE     = navigator.userAgent.toLowerCase().indexOf( 'msie' )   > -1;

                        if ( ( isWebkit || isOpera || isIE ) && document.getElementById && window.addEventListener ) {
                                window.addEventListener( 'hashchange', function() {
                                        var id = location.hash.substring( 1 ),
                                                element;

                                        if ( ! ( /^[A-z0-9_-]+$/.test( id ) ) ) {
                                                return;
                                        }

                                        element = document.getElementById( id );

                                        if ( element ) {
                                                if ( ! ( /^(?:a|select|input|button|textarea)$/i.test( element.tagName ) ) ) {
                                                        element.tabIndex = -1;
                                                }

                                                element.focus();

                                                // Repositions the window on jump-to-anchor to account for admin bar and border height.
                                                window.scrollBy( 0, -53 );
                                        }
                                }, false );
                        }
                },
                onepageNav: function(){ //one page nav
                     $('a.page-scroll').bind('click', function (event) {
                        var $anchor = $(this);
                        $('html, body').stop().animate({
                            scrollTop: $($anchor.attr('href')).offset().top
                        }, 1500, 'easeInOutExpo');
                        event.preventDefault();
                    });
                },
                
                countDowntime:function (){ //product veriabal timer/coming soon
                   if($('.countdown-lastest, .count-down-time').length >0){
                      var labels = ['Years', 'Months', 'Weeks', 'Days', 'Hrs', 'Mins', 'Secs'];
                      var layout = '<span class="countdown-row"><span class="countdown-section"><span class="countdown-amount">{dnn}</span><span class="countdown-period">'+pikoworks_global_message.global.days+'</span></span><span class="countdown-section"><span class="countdown-amount">{hnn}</span><span class="countdown-period">'+pikoworks_global_message.global.hours+'</span></span><span class="countdown-section"><span class="countdown-amount">{mnn}</span><span class="countdown-period">'+pikoworks_global_message.global.minutes+'</span></span><span class="countdown-section"><span class="countdown-amount">{snn}</span><span class="countdown-period">'+pikoworks_global_message.global.seconds+'</span></span></span>';

                      $('.countdown-lastest, .count-down-time').each(function() {
                          var austDay = new Date($(this).data('y'),$(this).data('m') - 1,$(this).data('d'),$(this).data('h'),$(this).data('i'),$(this).data('s'));
                          $(this).countdown({
                              until: austDay,
                              labels: labels, 
                              layout: layout
                          });
                      });
                  }
                },
                owlsettingCarousel: function(){
                    function settingCarousel($this, $selector){
                        var config = $this.data();
                        config.navText = ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'];
                        if( config.smartspeed != 'undefined'){
                          config.smartSpeed = config.smartspeed;
                        }
                //        if( config.autoplayHoverPause != 'undefined'){
                //          config.autoplayHoverPause = config.autoplayHoverPause;
                //        }
                        if( config.allitems != "undefined"){
                           if(config.allitems <=1){
                              config.loop = false;
                           }
                        }
                        config.autoplayHoverPause = true;
                        config.lazyLoad = true;
                        if( $this.hasClass('fadeiN') ){
                            config.animateOut="fadeOut";
                            config.animateIn="fadeIn";    
                        }else if( $this.hasClass('slidedowN') ){
                            config.animateOut="slideOutDown";
                            config.animateIn="slideInDown"; 
                        }else if( $this.hasClass('bounceRighT') ){
                            config.animateOut="bounceOutRight";
                            config.animateIn="bounceInUp"; 
                        }else if( $this.hasClass('zoomiN') ){
                            config.animateOut="zoomOut";
                            config.animateIn="zoomIn"; 
                        }else if( $this.hasClass('zoomin_2') ){
                            config.animateOut="slideOutDown";
                            config.animateIn="zoomIn"; 
                        }else if( $this.hasClass('zoomInDowN') ){
                            config.animateOut="zoomOutDown";
                            config.animateIn="zoomInDown"; 
                        }else if( $this.hasClass('fadeInLefT') ){
                            config.animateOut="fadeOutLeft";
                            config.animateIn="fadeInLeft"; 
                        }else if( $this.hasClass('fadeInuP') ){
                            config.animateOut="zoomOut";
                            config.animateIn="fadeInUp"; 
                        }else if( $this.hasClass('slideInuP') ){
                            config.animateOut="zoomOutDown";
                            config.animateIn="slideInUp"; 
                        }
                        config.rtl = rtl;
                        config.onInitialized = function( event ){
                            var $item_active = $this.find( '.tab-panel.active .owl-item.active' );
                            $item_active.each( function ( $i ) {
                                var $item = jQuery(this);
                                var $style = $item.attr("style");
                                $style    = ( $style == undefined ) ? '' : $style;
                                var delay = $i * 300;
                                $item.attr("style", $style +
                                          ";-webkit-animation-delay:" + delay + "ms;"
                                        + "-moz-animation-delay:" + delay + "ms;"
                                        + "-o-animation-delay:" + delay + "ms;"
                                        + "animation-delay:" + delay + "ms;"
                                ).addClass('slideInTop animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
                                    $item.removeClass('slideInTop animated');
                                    if ( $style )
                                        $item.attr("style", $style);
                                }); 
                            });
                        }
                        if( typeof( $selector ) == 'undefined' ){
                            $this.owlCarousel(config);
                        }else{
                            $selector.owlCarousel(config);
                        }

                    }
                    
                    $( ".owl-carousel").each(function(index, el) { //init function setting
                        var $this = $(this);
                        settingCarousel($this);
                    });
                    
                },
                
                owlCarouselsInit: function () {                   
                    var navIcon = ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'];
                    $('.owl-blog-gallery').each(function(){ 
                        $('.owl-blog-gallery').addClass('owl-carousel');
                        $('.owl-blog-gallery').owlCarousel({
                            margin:30,
                            autoplay:true,
                            dots:false,
                            loop:false,
                            items:1,
                            nav:true,
                            smartSpeed:1000,
                            navText:navIcon
                        });        
                    });
                    
                    /**---------- related,upsales product--------------- **/
                    $('.related.products .row, .upsells.products .row, .cross-sells .row').each(function(){ 
                        $('.related.products .row').addClass('owl-carousel owl-nav-show-hover');
                        $('.upsells.products .row, .cross-sells .row').addClass('owl-carousel owl-nav-show-hover').removeClass('row');                        
                        $('.related.products .row, .upsells.products .owl-carousel, .cross-sells .owl-carousel').owlCarousel({
                            margin:30,
                            dots:false,
                            nav:true,                    
                            navText: navIcon,
                            responsive : {
                              0 : {
                                  items : 1
                              },
                              768 : {
                                  items : 2
                              },
                              992 : {
                                  items : 3
                              },
                              1200 : {
                                  items : 3
                              }
                          },
                          rtl: rtl
                        });
                    });  
                    /**---------- cross product--------------- **/
//                    $('.cross-sells .row').each(function(){ 
//                        $('.cross-sells .row').addClass('owl-carousel owl-nav-show-hover').removeClass('row');          
//                        $(this).owlCarousel({
//                            margin:30,
//                            dots:false,
//                            nav:true,
//                            items:1,
//                            navText:navIcon,                    
//                            rtl: rtl
//                            }
//                        );                    
//                    });  


		

		},
                categoriesAccordion: function() {                  
                    // ! Categories Accordion
                    $.fn.etAccordionMenu = function ( options ) {                        
                        var $this = $(this);

                        var plusIcon = '+';
                        var minusIcon = '&ndash;';
                       
                        $this.addClass('with-accordion');
                        var openerHTML = '<div class="open-this">'+plusIcon+'</div>';

                        $this.find('li').has('.children, .nav-sublist-dropdown').has('li').addClass('parent-level0').prepend(openerHTML);
                        
                        $this.find('.open-this').click(function() {
                            if($(this).parent().hasClass('opened')) {
                                $(this).html(plusIcon).parent().removeClass('opened').find('> ul, > div.nav-sublist-dropdown').slideUp(200);
                            }else {
                                $(this).html(minusIcon).parent().addClass('opened').find('> ul, > div.nav-sublist-dropdown').slideDown(200);
                            }
                        });

                        return this;
                    }
                    
                    $('.product-categories').etAccordionMenu();                    
                },               
		swiperCarousels: function () {
		    // Index6 Product slider
			new Swiper('.product-inside-slider', {
		        nextButton: '.swiper-button-next',
		        prevButton: '.swiper-button-prev',
		        spaceBetween: 0,
		        speed: 600,
		        loop: true
		    });
		},
                
                counterVal: function (){
                    $('.count').counterUp({
                        delay: 10,
                        time: 4000
                    });
                },
                
		
		progressBars: function () {
			var self = this;
			// Calculate and Animate Progress 
			$('.progress-animate').waypoint( function (direction) {
				var $this =  $(this.element),
					progressVal = $this.data('width');

				$this.css({ 'width' : progressVal + '%'}, 400);

				setTimeout(function() {
					$this.removeClass('progress-animate');
					$this.find('.progress-text').fadeIn(400);
				}, 400);
			}, {
				offset: '80%',
				triggerOnce: true 
			});
		},
		
		gridMasonryCol: function() {
			// Trigger for isotope plugin
			if ( $.fn.isotope ) {
				var gridMasonry = this.gridMasonry,
                                    layoutMode = gridMasonry.data('layoutmode');
				gridMasonry.isotope({
                                    itemSelector: '.column',
                                    layoutMode: (layoutMode) ? layoutMode : 'masonry'
                            });
			}
		},
		isotopeReinit: function (container) {
			// Recall for isotope plugin
			if ( $.fn.isotope ) {
				this.gridMasonry.isotope('destroy');
				this.gridMasonryCol();
			}
		},
                socialShear: function(){
                    $('.print-button button').on('click', function() {
                      $(this).parent().toggleClass('start-animation');
                    });                   
                },
		shareGroup: function () {
			// Product.html share group -- toggle class to show all social icons
			$('.social-icon.share').on('mouseover', function() {
				$(this).closest('.social-icons').addClass('opened');
			});

			$('.social-icons').on('mouseleave', function() {
				$(this).removeClass('opened');
			});
		},
                toggleLogin: function(){
                       //TOGOLE LOGIN & REGISTER
                    $('.piko-my-account .piko-togoleform').on("click",function(e){
                        var formId = $(this).attr('href');
                        $(this).closest('.piko-my-account-form').removeClass('show slide');
                        $(formId).addClass('show slide');
                        e.preventDefault();
                    });
                },
                productsinglePage: function(){ //single porduct zooming 
                    var productGalleryCarousel = new Swiper('.product-gallery-carousel', {
                        pagination: '',
                        nextButton: '.swiper-button-next',
                        prevButton: '.swiper-button-prev',
                        slidesPerView: 4,
                        spaceBetween: 20,
                        breakpoints: {
                            1200: {
                                spaceBetween: 14
                            },
                            992: {
                                slidesPerView: 3,
                                spaceBetween: 14
                            },
                            580: {
                                slidesPerView: 3,
                                spaceBetween: 14
                            },
                            320: {
                                slidesPerView: 2,
                                spaceBetween: 14
                            }
                        }
                    });

                    if ($.fn.elevateZoom) {
                        $('#product-zoom').elevateZoom({
                            responsive: true, // simple solution for zoom plugin down here // it can cause bugs at resize
                            zoomType: 'inner', // you can use 'inner' or 'window' // change inner and go to product.html page and see zoom plugin differances
                            cursor: "crosshair",
                            borderSize: 1,
                            borderColour: '#e7e7e7',
                            lensSize : 180,
                            lensBorder: 4,
                            lensOpacity: 1,
                            lensColour: 'rgba(255, 255, 255, 0.18)'
                        });

                        /* swap images for zoom on click event */
                        $('.product-gallery-carousel').find('a').on('click', function (e) {
                            var ez = $('#product-zoom').data('elevateZoom'),
                                smallImg = $(this).data('image'),
                                bigImg = $(this).data('zoom-image');
                                $('#product-zoom').removeAttr( "srcset" );

                                ez.swaptheimage(smallImg, bigImg);
                            e.preventDefault();
                        });
                    }
                },
               mailChimp: function(){ // Submit Mailchimp via ajax
           
                        $(document).on('submit', 'form[name="news_letter"]', function(e){
                            var $this = $(this);
                            var thisWrap = $this.closest('.newsletter-form-wrap');

                            if ( $this.hasClass('processing') ) {
                                return false;
                            }
                            var api_key = $this.find('input[name="api_key"]').val();
                            var list_id = $this.find('input[name="list_id"]').val();
                            var success_message = $this.find('input[name="success_message"]').val();
                            var email = $this.find('input[name="email"]').val();

                            var data = {
                                action: 'pikoworks_submit_mailchimp_via_ajax',
                                api_key: api_key,
                                list_id: list_id,
                                success_message: success_message,
                                email: email
                            }

                            $this.addClass('processing');
                            thisWrap.find('.return-message').remove();

                            $.post(ajaxurl, data, function(response){

                                if ( $.trim(response['success']) == 'yes' ) {
                                    $this.after('<p class="return-message bg-success">' + response['message'] + '</p>');
                                    $this.find('input[name="email"]').val('');
                                }
                                else{
                                    $this.after('<p class="return-message bg-danger">' + response['message'] + '</p>');
                                }

                                console.log(response);
                                $this.removeClass('processing');

                            }); 

                            e.preventDefault();
                            return false;

                        });
                },
                
                backtoTop: function(){
                     var scroll_top_duration = 700;    
                    //smooth scroll to top
                    $('.backtotop, .back-top').on('click', function (event) {
                        event.preventDefault();
                        $('body,html').animate({
                            scrollTop: 0
                        }, scroll_top_duration
                                );
                    });
                    
                    var offset = 300,
                    offset_opacity = 1200,
                    $back_to_top = $('.back-top');
            
                    //hide or show the "back to top" link
                    $(window).scroll(function () {
                        ($(this).scrollTop() > offset) ? $back_to_top.addClass('top-is-visible') : $back_to_top.removeClass('top-is-visible top-fade-out');
                        if ($(this).scrollTop() > offset_opacity) {
                            $back_to_top.addClass('top-fade-out');
                        }
                        //footer parallux
                        if ($(window).scrollTop() > 150) {
                            $("body").addClass("fp_show");            
                        } else {
                            $("body").removeClass("fp_show");
                        }
                    });  
                },
                buttonEffect: function(){
                     $('.piko-btn-10').on('mouseenter', function(e) {
                                        var parentOffset = $(this).offset(),
                                relX = e.pageX - parentOffset.left,
                                relY = e.pageY - parentOffset.top;
                                        $(this).find('.aware').css({top:relY, left:relX});
                    })
                    .on('mouseout', function(e) {
                            var parentOffset = $(this).offset(),
                            relX = e.pageX - parentOffset.left,
                            relY = e.pageY - parentOffset.top;
                            $(this).find('aware').css({top:relY, left:relX});
                    });                   
                },
                fix_slide_after_menu: function(){                    
                    $(window).on("scroll", function() {
                        "use strict";
                        var c = jQuery(window).height();
                        jQuery('.slide-after-menu').find('#main-menu').removeClass('container')
                        if (jQuery(window).scrollTop() > c) {
                            jQuery(".slide-after-menu .site-header").addClass("active-sticky"); {
                                jQuery(".site-header").outerHeight();
                            }
                        } else {
                            jQuery(".slide-after-menu .site-header").removeClass("active-sticky"); {
                                jQuery(".site-header").outerHeight();
                            }
                        }
                    }); 
                },
                prettyPhotoInit:  function(){ //prettyphoto init and fix rel to data-del
                    $('[href][title="single"]').attr('data-rel','prettyPhoto');
                    $('[href][title="single"]').removeAttr('title');
                    $('[href][title="gallary"]').attr('data-rel','prettyPhoto[blog]');
                    $('[href][title="gallary"]').removeAttr('title');

                     $("a[data-rel^='prettyPhoto']").prettyPhoto({
                        hook: 'data-rel',
                        social_tools: '',
                        animation_speed: 'normal',
                        theme: 'light_square'
                    });
                },                
                fancySelect: function(){
                    $(".woocommerce-toolbar select").fancySelect();                    
                }
	};
        
	// Ready Event
	$(document).ready(function () {
		// Init our app
		Pikoworks.init();

		// Reinit Product Masonry/Grid on Tab change
		$('.products-tab, .products-tab-container').find('a[data-toggle=tab]').on('shown.bs.tab', function () {
			Pikoworks.isotopeReinit();

			 // Index6 Product slider - Reinit
			if ( $('.product-inside-slider').length ) {
				new Swiper('.product-inside-slider', {
			        nextButton: '.swiper-button-next',
			        prevButton: '.swiper-button-prev',
			        spaceBetween: 0,
			        speed: 600,
			        loop: true
			    });
			}
			
		});
                                
                jQuery( '.tab-content.mobile-carousel' ).each(function(index, el) { //not check
                    var $this = $(this);
                    var $tab_panel = $this.find( '.tab-pane.active .on-mobile-carousel' );
                    settingCarousel($this, $tab_panel);
                });
                
                
                // product category archive layout
        $('.products-grid .column-wrap, .archive.woocommerce .column-wrap').prev('.product-category.product').addClass('cat-last').after('<div class="clearfix"></div>');
         // fix quick sticky menu view
        $( document ).ajaxComplete(function() { 
           if($('#yith-quick-view-modal').hasClass('open')){
                $('#yith-quick-view-modal.open').prevAll('#page').addClass('quick-product-zoom');
           }           
        });
        $(document).on('click','.yith-quick-view-overlay, #yith-quick-view-close',function(e){
            $('#yith-quick-view-modal').prevAll('#page').removeClass('quick-product-zoom');
        });
        
	});


})(jQuery);

/**
 * Created ajax action
 */
"use strict";
var PortfolioAjaxAction = {
    htmlTag:{
        load_more :'.load-more',
        portfolio_container: '#portfolio-'
    },
    vars:{
        ajax_url: '',
        tab_category_action: 'filter'
    },

    processFilter:function(elm, isLoadmore){
        var $this = jQuery(elm);
        var l = Ladda.create(elm);
        l.start();
        var $filterType = $this.attr('data-load-type');
        var $overlay_style = $this.attr('data-overlay-style');
        var $section_id = $this.attr('data-section-id');
        var $data_source = $this.attr('data-source');
        var $data_portfolioIds = $this.attr('data-portfolio-ids');
        var $data_show_paging = $this.attr('data-show-paging');
        var $current_page =  $this.attr('data-current-page');
        var $category  = $this.attr('data-category');
        var $offset = 0;
        var $post_per_page = $this.attr('data-post-per-page');
        var $column = $this.attr('data-column');
        var $padding = '';
        var $order =  $this.attr('data-order');
        var $layout_type = $this.attr('data-layout-type');

        if($filterType=='ajax'){
            jQuery('a.active', jQuery(elm).parent().parent()).removeClass('active');
            jQuery('li.active', jQuery(elm).parent().parent()).removeClass('active');
            jQuery($this).parent().addClass('active');
            jQuery($this).addClass('active');
        }else{
            $category = jQuery('a.active', jQuery(elm).parent().parent()).attr('data-category');
        }

        jQuery.ajax({
            url: PortfolioAjaxAction.vars.ajax_url,
            data: ({action : 'pikoworks_portfolio_load_by_category', postsPerPage: $post_per_page, current_page: $current_page,
                layoutType: $layout_type,category : $category,
                columns: $column, colPadding: $padding, offset: 0, order: $order,
                data_source  : $data_source, portfolioIds: $data_portfolioIds, data_show_paging: $data_show_paging,
                overlay_style: $overlay_style, data_section_id: $section_id
            }),
            success: function(data) {
                l.stop();
                if($filterType=='ajax')
                    PortfolioAjaxAction.registerFilterByCategory($section_id);

                if($data_show_paging=='1'){
                    jQuery('#load-more-' + $section_id).empty();
                    if(jQuery('.paging',data).length>0){
                        var $loadButton = jQuery('.paging a',data);
                        $loadButton.attr('data-section-id',$section_id);
                        jQuery('#load-more-' + $section_id).append($loadButton);
                        PortfolioAjaxAction.registerLoadmore();
                    }
                }
                var $container = jQuery('#portfolio-container-' + $section_id);

                var $item = jQuery('.portfolio-item',data);


                if(isLoadmore == null || !isLoadmore){
                    $container.isotope();
                    jQuery('.portfolio-item',$container).each(function(){
                        $container.isotope( 'remove', jQuery(this) );
                    })
                    $container.fadeOut();
                    $item.css('transition','all 0.3s');
                    $item.css('-webkit-transition','all 0.3s');
                    $item.css('-moz-transition','all 0.3s');
                    $item.css('-ms-transition','all 0.3s');
                    $item.css('-o-transition','all 0.3s');
                    $item.css('opacity',0);
                }else{
                    $item.fadeOut();
                }
                if(isLoadmore !=null && isLoadmore && $filterType=='ajax'){
                    $container.append( $item );
                }else{

                    $container.append( $item ).isotope( 'appended', $item);
                    var $containerIsotope = jQuery('div[data-section-id="' + $section_id + '"]');
                    $containerIsotope.imagesLoaded( function() {
                        jQuery('.portfolio-item > div').hoverdir('destroy');
                        jQuery('.portfolio-item > div').hoverdir('rebuild');
                        $container.isotope({ filter: '*' });
                    });

                }

                PortfolioAjaxAction.registerPrettyPhoto();

                var owl = jQuery($container).data('owlCarousel');
                if(owl!=null && $item.length > 0 ){
                    owl.destroy();
                    jQuery($container).owlCarousel({
                        items : $column,
                        pagination: false,
                        navigation: true,
                        navigationText: ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>']
                    });
                }
                jQuery('.portfolio-item > div.entry-thumbnail').hoverdir();
                if($filterType=='ajax'){
                    $container.fadeIn(10,function(){
                        $item.css('opacity',1);
                    });
                }else{
                    $item.fadeIn();
                }



                PortfolioAjaxAction.registerLoadmore($section_id);

            },
            error:function(){
                if($filterType=='ajax')
                    PortfolioAjaxAction.registerFilterByCategory($section_id);
            }
        });
    },

    registerLoadmore:function(sectionId){
        jQuery('a','#load-more-' + sectionId).off();
        jQuery('a','#load-more-' + sectionId).click(function(){
            PortfolioAjaxAction.processFilter(this, true);
        });
    },

    registerPrettyPhoto:function(){
        jQuery("a[data-rel^='prettyPhoto']").prettyPhoto(
            {
                hook: 'data-rel',
                theme: 'light_square',
                slideshow: 5000,
                deeplinking: false,
                social_tools: false
            });
    },

    registerFilterByCategory:function(sectionId){
        var $container = jQuery('#portfolio-' + sectionId);
        jQuery('.portfolio-tabs li',$container).each(function(){
            jQuery('a',jQuery(this)).off();
            jQuery('a',jQuery(this)).click(function(){
                PortfolioAjaxAction.processFilter(this, false);
            });
        });
    },

    wrapperContentResize:function(){
        jQuery('#wrapper-content').bind('resize', function(){
            var $container = jQuery('.portfolio-wrapper');
            var owl = jQuery('.portfolio-wrapper').data('owlCarousel');
            if(owl==null ){
                 $container.isotope({
                 itemSelector: '.portfolio-item'
                 }).isotope('layout');

            }
        });
    },

    init:function(ajax_url, tab_category_action, dataSectionId){
        PortfolioAjaxAction.vars.ajax_url = ajax_url;
        PortfolioAjaxAction.vars.tab_category_action = tab_category_action;
        PortfolioAjaxAction.registerLoadmore(dataSectionId);
        PortfolioAjaxAction.registerPrettyPhoto();
        if(tab_category_action=='ajax'){
            PortfolioAjaxAction.registerFilterByCategory(dataSectionId);
        }
        PortfolioAjaxAction.wrapperContentResize();
    }
}