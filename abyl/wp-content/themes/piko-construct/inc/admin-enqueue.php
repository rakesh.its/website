<?php
/**
 * Enqueues scripts and styles admin.
 *
 */

if(!function_exists('pikoworks_admin_scripts')){
    function pikoworks_admin_scripts(){
       wp_enqueue_style('streamline-large', pikoworks_plugin.'/streamline-large/styles.css', false, pikoworks_theme_version, 'all');        
       wp_enqueue_style('linearicons', pikoworks_plugin.'/linearicons/style.css', false, pikoworks_theme_version, 'all'); 
       
        wp_enqueue_script('pikoworks-admin', pikoworks_js.'/admin.js', array('jquery'), pikoworks_theme_version, true);      
    }
    add_action( 'admin_enqueue_scripts', 'pikoworks_admin_scripts' );
}