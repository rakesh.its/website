<?php
/*
 * @author themepiko
 * header | footer content function | body class | slider top
 */

//menu style
if(!function_exists('pikoworks_headers_style')){
    function pikoworks_headers_style(){
        $prefix = 'pikoworks_';
        global $pikoworks;
        
        $menu_style =  get_post_meta(get_the_ID(), $prefix . 'menu_style',true);
        if (!isset($menu_style) || $menu_style == '-1' || $menu_style == '') {
            $menu_style = pikoworks_get_option_data('menu_style', '1');
        }
        $menu_before_slider_enable = pikoworks_get_option_data('menu_before_slider_enable', 0);
        
        
        $menu_before_slider_meta =  get_post_meta(get_the_ID(), $prefix . 'slide_after_menu_enable',true);
        
        $menu_before_slider =  get_post_meta(get_the_ID(), $prefix . 'menu_before_slider',true);
        if (!isset($menu_before_slider) || $menu_before_slider == '') {
            $menu_before_slider = pikoworks_get_option_data('menu_before_slider','');
        }
        
        if($menu_before_slider_enable == '1' && $menu_style == '2' && is_front_page() || $menu_before_slider_meta == '1' && $menu_before_slider != '' ){
           echo do_shortcode( $menu_before_slider );
        }
        
        switch ($menu_style) {               
                case '6':
                {
                    pikoworks_get_template('headers/header', '6');
                    break;
                }
                case '5':
                {
                    pikoworks_get_template('headers/header', '5');
                    break;
                }
                case '4':
                {
                    pikoworks_get_template('headers/header', '4');
                    break;
                }
                case '3':
                {
                    pikoworks_get_template('headers/header', '3');
                    break;
                }  
                case '2':
                {
                    pikoworks_get_template('headers/header', '2');
                    break;
                }
                default:
                {
                    pikoworks_get_template('headers/header', '1'); 
                }
            }
        pikoworks_breadcrumbs(); //breadcrumbs 
    }
}

if ( !function_exists( 'pikoworks_get_top_bar_menu' ) ){
	function pikoworks_get_top_bar_menu(){
		$arg_default = array(
			'fallback_cb'     => false,
			'container'       => false,
		);
		$args = array_merge($arg_default , apply_filters( 'pikoworks_get_top_bar_menu_location' , array(
			'theme_location'  => 'top_menu',
		)) );

		wp_nav_menu($args);
	}
}

if( !function_exists('pikoworks_main_menu_fallback') ){
	function pikoworks_main_menu_fallback(){
		$output = '<ul class="main-menu mega-menu show-arrow effect-fadein-up subeffect-fadein-left">';
		$menu_fallback = wp_list_pages('number=5&depth=2&echo=0&title_li=');
		$menu_fallback = str_replace('page_item','page_item menu-item',$menu_fallback);
		$menu_fallback = str_replace("<ul class='children'>","<ul class='sub-menu'>",$menu_fallback);
		$output .= $menu_fallback;
		$output .= '</ul>';
		echo do_shortcode($output);
	}
}

if( !function_exists('pikoworks_main_mobile_menu_fallback') ){
	function pikoworks_main_mobile_menu_fallback(){
		$output = '<ul class="mobile-main-menu accordion-menu">';
		$menu_fallback = wp_list_pages('echo=0&title_li=');
		$menu_fallback = str_replace('page_item','page_item menu-item',$menu_fallback);
		$menu_fallback = str_replace("<ul class='children'>","<span class='arrow'></span><ul class='sub-menu'>",$menu_fallback);
		$output .= $menu_fallback;
		$output .= '</ul>';
		echo do_shortcode($output);
	}
}

if ( !function_exists( 'pikoworks_get_main_menu' ) ){
	function pikoworks_get_main_menu(){
                $menu_anm = pikoworks_get_option_data('main_menu_animation','effect-down');
                $submenu_anm = pikoworks_get_option_data('sub_menu_animation','subeffect-down');
		if(class_exists('Pikoworks_Walker_Top_Nav_Menu')) {
			$arg_default = array(
				'container' => false,
				'menu_class' => 'main-menu mega-menu '.esc_attr($menu_anm . ' ' . $submenu_anm).' show-arrow',
				'fallback_cb' => 'pikoworks_main_menu_fallback',
				'walker' => new Pikoworks_Walker_Top_Nav_Menu
			);
			$args = array_merge($arg_default , apply_filters( 'pikoworks_filter_main_menu_location' , array(
				'theme_location' => 'primary',
			)) );
			wp_nav_menu($args);
		}
	}
}
if ( !function_exists( 'pikoworks_get_secondary_menu' ) ){
	function pikoworks_get_secondary_menu(){
            $menu_anm = pikoworks_get_option_data('main_menu_animation','effect-down');
            $submenu_anm = pikoworks_get_option_data('sub_menu_animation','subeffect-down'); 
		if(class_exists('Pikoworks_Walker_Top_Nav_Menu')) {
			$arg_default = array(
				'container' => false,
				'menu_class' => 'main-menu mega-menu '.esc_attr($menu_anm . ' ' . $submenu_anm).' show-arrow',
				'fallback_cb' => 'pikoworks_main_menu_fallback',
				'walker' => new Pikoworks_Walker_Top_Nav_Menu
			);
			$args = array_merge($arg_default , apply_filters( 'pikoworks_filter_main_menu_location' , array(
				'theme_location' => 'secondary',
			)) );
			wp_nav_menu($args);
		}
	}
}

if ( !function_exists( 'pikoworks_get_mobile_main_menu' ) ){
	function pikoworks_get_mobile_main_menu(){
		if(class_exists('Pikoworks_Walker_Accordion_Nav_Menu')) {
			$arg_default = array(
				'container' => false,
				'menu_class' => 'mobile-main-menu accordion-menu',
				'fallback_cb' => 'pikoworks_main_mobile_menu_fallback',
				'walker' => new Pikoworks_Walker_Accordion_Nav_Menu
			);
			$args = array_merge($arg_default , apply_filters( 'pikoworks_filter_main_menu_location' , array(
				'theme_location' => 'primary',
			)) );
			wp_nav_menu($args);
		}
                
                $prefix = 'pikoworks_';
                global $pikoworks;

                $menu_style =  get_post_meta(get_the_ID(), $prefix . 'menu_style',true);
                if (!isset($menu_style) || $menu_style == '-1' || $menu_style == '') {
                    $menu_style = isset( $pikoworks['menu_style'] ) ? $pikoworks['menu_style'] : '1';
                }
                
		if(class_exists('Pikoworks_Walker_Accordion_Nav_Menu') && $menu_style == '6') {
			$arg_default_secondary = array(
				'container' => false,
				'menu_class' => 'mobile-main-menu accordion-menu',
				'fallback_cb' => 'pikoworks_main_mobile_menu_fallback',
				'walker' => new Pikoworks_Walker_Accordion_Nav_Menu
			);
			$args_secondary = array_merge($arg_default_secondary , apply_filters( 'pikoworks_filter_main_menu_location' , array(
				'theme_location' => 'secondary',
			)) );
			wp_nav_menu($args_secondary);
		}
	}
}

if ( !function_exists( 'pikoworks_get_topbar_left' ) ){
    function pikoworks_get_topbar_left(){
        global $pikoworks;
        $allow_html = array(
            'a' => array('href' => array(), 'title' => array(), 'target' => array() ),
            'em' => array(), 'strong' => array(), 'i' => array('class' => array()), 'ul' => array('class' => array()), 'li' => array(), 
        );
        $top_bar_left = isset( $pikoworks['top_bar_left'] ) ? $pikoworks['top_bar_left'] : 'custom';
        $top_bar_infotext = isset( $pikoworks['top_bar_infotext'] ) ? $pikoworks['top_bar_infotext'] : '';
       ?>
       <div class="site-top-bar-text">
            <?php 
            if($top_bar_left == 'custom'):
                echo wp_kses( $top_bar_infotext , $allow_html); 
            elseif($top_bar_left == 'social_icon'):
                if( $GLOBALS['pikoworks']['top_bar_use_social']){
                    echo '<ul>';    
                       foreach ($GLOBALS['pikoworks']['top_bar_use_social'] as $key => $val){
                           if(! empty($GLOBALS['pikoworks'][$key]) && $val == 1 ){
                               echo "<li><a target='_blank' href='" .esc_url($GLOBALS['pikoworks'][$key]) . "' data-placement='bottom' data-toggle='tooltip' data-original-title='" .esc_attr($key) . "'><i class='fa fa-" .esc_attr($key). "'  aria-hidden='true'></i></a></li>";
                           }
                       }
                    echo '</ul>';  
                };
            else:
               echo wp_kses( $top_bar_infotext , $allow_html);
                if( $GLOBALS['pikoworks']['top_bar_use_social']){
                        echo '<ul>';    
                           foreach ($GLOBALS['pikoworks']['top_bar_use_social'] as $key => $val){
                               if(! empty($GLOBALS['pikoworks'][$key]) && $val == 1 ){
                                   echo "<li><a target='_blank' href='" .esc_url($GLOBALS['pikoworks'][$key]) . "' data-placement='bottom' data-toggle='tooltip' data-original-title='" .esc_attr($key) . "'><i class='fa fa-" .esc_attr($key). "'  aria-hidden='true'></i></a></li>";
                               }
                           }
                        echo '</ul>';  
                }
            endif;
            ?>
       </div>
    <?php        
    }
}

if ( !function_exists( 'pikoworks_get_topbar_right' ) ){
    function pikoworks_get_topbar_right(){
        global $pikoworks;
        $top_bar_right_wpml = isset( $pikoworks['top_bar_right_wpml'] ) ? $pikoworks['top_bar_right_wpml'] : 0;
        $allow_html = array(
            'a' => array('href' => array(), 'title' => array(), 'target' => array() ),
            'img' => array('src' => array(), 'alt' => array(), 'height' => array(),'width' => array() ),
            'em' => array(), 'strong' => array(), 'i' => array('class' => array()), 'ul' => array('class' => array()), 'li' => array(), 
        );        
        $top_bar_right_option = pikoworks_get_option_data('top_bar_right_option', 0);        
        $top_bar_infotext_right = pikoworks_get_option_data('top_bar_infotext_right','');        
        
       ?>
       <div id="site-navigation-top-bar" class="top-bar-navigation">
           <?php
           if($top_bar_right_option == 0){
                pikoworks_get_top_bar_menu();
                
                if( $top_bar_right_wpml == 1 ){ 
                    echo '<ul class="menu">';
                    pikoworks_lang_switcher();
                    echo '</ul>';
                }
                
               
           }else{
              echo wp_kses( $top_bar_infotext_right , $allow_html);  
           }
           
           ?>       
           
        </div>
    <?php        
    }
}

if ( !function_exists( 'pikoworks_get_topbar' ) ){
    function pikoworks_get_topbar(){ 
        $prefix = 'pikoworks_';
        $menu_width =  get_post_meta(get_the_ID(), $prefix . 'manu_width',true);
        if (!isset($menu_width) || $menu_width == '-1' || $menu_width == '') {
            $menu_width = isset( $GLOBALS['pikoworks']['full_width_menu'] ) ? $GLOBALS['pikoworks']['full_width_menu'] : 'container';
        }        
        ?>
    <div class="header-top">
        <div class="<?php echo esc_attr($menu_width); ?>">
            <div class="row">
                    <div class="col-md-6 header-left">
                        <?php pikoworks_get_topbar_left(); ?>
                    </div>
                    <div class="col-md-6 header-right">
                        <?php pikoworks_get_topbar_right(); ?>
                    </div>
            </div>
        </div>
    </div>
    <?php       
    }
}

if ( !function_exists( 'pikoworks_get_brand_logo' ) ){
    function pikoworks_get_brand_logo(){
     $prefix = 'pikoworks_';   
    $default_logo_id =  get_post_meta(get_the_ID(), $prefix . 'logo_upload', true);
    $logo_default['url'] =  wp_get_attachment_image_url($default_logo_id, '') ? wp_get_attachment_image_url($default_logo_id, '') : '';
    if (!isset($logo_default['url']) ||  $logo_default['url'] == '') {    
        $logo_default = isset( $GLOBALS['pikoworks']['logo_upload'] ) ? $GLOBALS['pikoworks']['logo_upload'] : array( 'url' => get_template_directory_uri() . '/assets/images/logo/logo.png' );
    }
    $site_name = get_bloginfo('name');
    $site_description = get_bloginfo('description');
    $logo_type = pikoworks_get_option_data('enable_text_logo', 0);
    $brand_name = pikoworks_get_option_data('brand_logo_name', '');
    ?>
        <?php echo (is_front_page() && is_home()) ? '<h1 class="site-logo">' : '<div class="site-logo">' ;?>
        <a href="<?php echo esc_url(home_url("/"))?>">
            <?php if($logo_type == 1): ?>
            <?php echo esc_attr($brand_name);?>
            <?php else: ?>
                <img src="<?php echo esc_url($logo_default['url']);?>" alt="<?php echo esc_attr($site_name);?>" title="<?php echo esc_attr($site_description);?>" class="site-logo-image"/>
            <?php endif; ?>    
        </a>
        <?php echo (is_front_page() && is_home()) ? '</h1>' : '</div>' ;?>   
    <?php
        
    }
}

if ( !function_exists( 'pikoworks_get_sticky_logo' ) ){
    function pikoworks_get_sticky_logo(){
        $prefix = 'pikoworks_';  
        $mobile_logo_id =  get_post_meta(get_the_ID(), $prefix . 'logo_upload_mobile', true);
        $logo_mobile_src['url'] =  wp_get_attachment_image_url($mobile_logo_id, '') ? wp_get_attachment_image_url($mobile_logo_id, '') : '';
        if (!isset($logo_mobile_src['url']) || $logo_mobile_src['url'] == '') { 
            $logo_mobile_src = isset( $GLOBALS['pikoworks']['logo_upload_mobile'] ) ? $GLOBALS['pikoworks']['logo_upload_mobile'] : array( 'url' => get_template_directory_uri() . '/assets/images/logo/logo-inverse.png' );
        }
        $site_name = get_bloginfo('name');
        $site_description = get_bloginfo('description');
        $logo_type = pikoworks_get_option_data('enable_text_logo', 0);
        $brand_name = pikoworks_get_option_data('brand_logo_name', '');
        ?>
        <div class="sticky-logo">
            <a href="<?php echo esc_url(home_url("/"))?>">
                <?php if($logo_type == 1): ?>
            <?php echo esc_attr($brand_name);?>
            <?php else: ?>
                <img src="<?php echo esc_url($logo_mobile_src['url']);?>" alt="<?php echo esc_attr($site_name);?>" title="<?php echo esc_attr($site_description);?>" class="site-logo-image"/>
            <?php endif; ?>   
            </a>
        </div>
        <?php
    }
}


if ( !function_exists( 'pikoworks_get_header_action' ) ){
    function pikoworks_get_header_action(){
        global $woocommerce, $pikoworks;
        $show_search = isset( $pikoworks['menu_search'] ) ? $pikoworks['menu_search'] == 1 : 1;
        $show_cart = isset( $pikoworks['show_cart_iocn'] ) ? $pikoworks['show_cart_iocn'] == 1 : 1;
        
        ?>
        <div class="header-actions header">
            <ul>
                    <li class="toggle-menu-mobile hidden-md hidden-lg">
                            <a href="#" class="toggle-menu-mobile-button tools_button">
                                    <span class="tools_button_icon"><span class="lnr lnr-menu" aria-hidden="true"></span></span>
                            </a>
                    </li>                   
                    <?php if(function_exists('WC') && $show_cart == 1):?>
                            <li class="shopping-bag-button dropdown header-dropdown cart-dropdown">                                
                                <a href="javascript:void(0)" class="tools_button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span class="tools_button_icon"><span class="lnr lnr-cart" aria-hidden="true"></span></span>
                                    <span class="cart-items">0</span>
                                </a>                                    
                                <div id="header-mini-cart" class="dropdown-menu" data-dropdown-content>                                            
                                    <div class="widget_shopping_cart">
                                        <div class="widget_shopping_cart_content">                                                        
                                                <div class="cart-loading"></div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                    <?php endif;?>
                    <?php if($show_search):?>
                            <li class="search-button dropdown header-dropdown">
                                    <a class="tools_button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="javascript:void(0)">
                                        <span class="tools_button_icon"><span class="lnr lnr-magnifier" aria-hidden="true"></span></span>
                                    </a>
                                <div id="header-search-form" class="dropdown-menu" data-dropdown-content><?php get_search_form();?></div>
                            </li>
                    <?php endif;?>
            </ul>           
        </div>
<?php
        
    }
}

/*
 * ------------------------------------
 *          configure footer
 * ----------------------------------- 
 */
if(!function_exists('pikoworks_footer_style')){
    function pikoworks_footer_style(){
        $prefix = 'pikoworks_';
        global $pikoworks;
        
        $footer_layout =  get_post_meta(get_the_ID(), $prefix . 'footer_layout',true);
        if (!isset($footer_layout) || $footer_layout == '-1' || $footer_layout == '') {
            $footer_layout = isset( $pikoworks['footer_layout'] ) ? $pikoworks['footer_layout'] : 'layout3';
        } 
        
        
        switch ($footer_layout) {                
                case 'layout3':
                {
                    get_template_part('template-parts/footer/footer-layout', 'three');
                    break;
                }  
                case 'layout2':
                {
                    get_template_part('template-parts/footer/footer-layout', 'two');
                    break;
                }
                default:
                {
                    get_template_part('template-parts/footer/footer-layout', 'one'); 
                }
            }
    }
}


if(!function_exists('pikoworks_footer_sidebar_one')){
    function pikoworks_footer_sidebar_one(){
        $prefix = 'pikoworks_';
        global $pikoworks;
        
        $footer_widgets =  get_post_meta(get_the_ID(), $prefix . 'widgets_area',true);
        if (!isset($footer_widgets) || $footer_widgets == '0' || $footer_widgets == '') {
            $footer_widgets = isset( $pikoworks['footer_widgets'] ) ? $pikoworks['footer_widgets'] : true;
        } 
        $footer_columns =  get_post_meta(get_the_ID(), $prefix . 'footer_cloumn',true);
        if (!isset($footer_columns) || $footer_columns == '-1' || $footer_columns == '') {
            $footer_columns = isset( $pikoworks['footer_columns'] ) ? $pikoworks['footer_columns'] : 4;
        } 
        $footer_sidebar_one =  get_post_meta(get_the_ID(), $prefix . 'footer_sidebar_one',true);
        if (!isset($footer_sidebar_one) || $footer_sidebar_one == '-1' || $footer_sidebar_one == '') {
            $footer_sidebar_one = isset( $pikoworks['optn_footer_Widgets_one'] ) ? $pikoworks['optn_footer_Widgets_one'] : 'sidebar-3';
        } 
        
        if ( is_active_sidebar( $footer_sidebar_one ) && $footer_widgets == true ) {
            $widget_areas = $footer_columns;
		if( ! $widget_areas ){
			$widget_areas = 4;
		}
                ?>
                <div class="sub-footer <?php echo 'cols_' . esc_attr( $widget_areas ); ?>">
                    <?php dynamic_sidebar( $footer_sidebar_one ); ?>
                </div>	
            <?php
        }
        
    }
}
if(!function_exists('pikoworks_footer_sidebar_two')){
    function pikoworks_footer_sidebar_two(){
        $prefix = 'pikoworks_';
        global $pikoworks;
        
        $footer_widgets_two =  get_post_meta(get_the_ID(), $prefix . 'widgets_area_two',true);
        if (!isset($footer_widgets_two) || $footer_widgets_two == '0' || $footer_widgets_two == '') {
            $footer_widgets_two = isset( $pikoworks['optn_footer_widgets_two'] ) ? $pikoworks['optn_footer_widgets_two'] : true;
        }
        
        $footer_columns_two =  get_post_meta(get_the_ID(), $prefix . 'footer_cloumn_two',true);
        if (!isset($footer_columns_two) || $footer_columns_two == '-1' || $footer_columns_two == '') {
            $footer_columns_two = isset( $pikoworks['optn_footer_columns_two'] ) ? $pikoworks['optn_footer_columns_two'] : 4;
        }
        
        $footer_sidebar_two =  get_post_meta(get_the_ID(), $prefix . 'footer_sidebar_two',true);
        if (!isset($footer_sidebar_two) || $footer_sidebar_two == '-1' || $footer_sidebar_two == '') {
            $footer_sidebar_two = isset( $pikoworks['optn_footer_Widgets_two'] ) ? $pikoworks['optn_footer_Widgets_two'] : 'sidebar-5';
        }
        
        
        if ( is_active_sidebar( $footer_sidebar_two ) && $footer_widgets_two == true ){
            $widget_areas_two = $footer_columns_two;
		if( ! $widget_areas_two ){
			$widget_areas_two = 4;
		}
                ?>
                <div class="sub-footer <?php echo 'cols_' . esc_attr( $widget_areas_two ); ?>">
                       <?php dynamic_sidebar( $footer_sidebar_two ); ?>
                </div>	
            <?php
        }
    }
}


/*---------------------------------------
 * header body class configure
 * ------------------------------------ */ 
 if(!function_exists('pikoworks_body_class')){
    function pikoworks_body_class( $classes ) {
        $prefix = 'pikoworks_';
        global $pikoworks, $post;        
        
        $page_class_extra =  get_post_meta(get_the_ID(), $prefix . 'page_class_extra',true);
        $theme_default =  get_post_meta(get_the_ID(), $prefix . 'layout_style',true);
        if (!isset($theme_default) || $theme_default == '-1' || $theme_default == '') {
            $theme_default = pikoworks_get_option_data('main_width_floatled', '');
        }
        $coming_soon_body_class = pikoworks_get_option_data('enable_coming_soon_mode', 0);
        $footer_parallax = pikoworks_get_option_data('footer_parallax', '');
        
        $menu_style =  get_post_meta(get_the_ID(), $prefix . 'menu_style',true);
        if (!isset($menu_style) || $menu_style == '-1' || $menu_style == '') {
            $menu_style = pikoworks_get_option_data('menu_style', '1');
        } 
        
         $footer_layout =  get_post_meta(get_the_ID(), $prefix . 'slide_after_menu_sticky',true);
         $footer_layout =  get_post_meta(get_the_ID(), $prefix . 'footer_layout',true);
        if (!isset($footer_layout) || $footer_layout == '-1' || $footer_layout == '') {
            $footer_layout = pikoworks_get_option_data('footer_layout', 'layout3');
        }
        $transparency_meta =  get_post_meta(get_the_ID(), $prefix . 'header_transparency',true);
        $transparency = pikoworks_get_option_data('header_transparency',false);
        $transparency_option = pikoworks_get_option_data('header_transparency_option','fontpage');
        
        $slide_after_menu_meta =  get_post_meta(get_the_ID(), $prefix . 'slide_after_menu_sticky',true);
        
           $boxed_background_image_type = pikoworks_get_option_data('boxed_background_image_type', '');
            
            switch ($theme_default){
                case 'theme_float':
                    {
                    $classes[] = 'theme_float_full';
                    $classes[] = 'theme_float';
                    break;    
                    }
                case 'theme_float_boxed':
                    {                    
                    $classes[] = 'theme_float';
                    $classes[] = 'home-box-wapper';
                    break;    
                    }
                case 'theme_boxed':
                    {
                    $classes[] = 'home-box-wapper';
                    break;    
                    }
            }  
            
            if( $boxed_background_image_type){
                    $classes[] = esc_attr($boxed_background_image_type);
            }           
            
            if( $coming_soon_body_class == 1 ){
                    $classes[] = 'coming-soon';
            }                  
            if ( $footer_parallax == 1 && $footer_layout == 'layout2'  ) {	
                $classes[] = 'has-footer-parallax';
            }
            if(pikoworks_get_option_data('sticky_header_hide_logo',false)){
                $classes[] = 'sticky-header-hide-logo';
            }
            if(pikoworks_get_option_data('sticky_header_hide_search',false)){
                $classes[] = 'sticky-header-hide-search';
            }            
            if(pikoworks_get_option_data('sticky_header_hide_cart',false)){
                $classes[] = 'sticky-header-hide-cart';
            }
            if(pikoworks_get_option_data('slide_after_menu_sticky',false) && is_front_page() || $slide_after_menu_meta == '1'){
                $classes[] = 'slide-after-menu';
            }            
            if($transparency && $transparency_option == 'fontpage' && is_front_page()|| $transparency_meta){
                $classes[] = 'header-transparency';
            }
            if($transparency && $transparency_option == 'allpage'){
                $classes[] = 'header-transparency';
            }
            // Adds a class of custom-background-image to sites with a custom background image.
            if ( get_background_image() ) {
                    $classes[] = 'custom-background-image';
            }
            // Adds a class of group-blog to sites with more than 1 published author.
            if ( is_multi_author() ) {
                    $classes[] = 'group-blog';
            }
            // Adds a class of hfeed to non-singular pages.
            if ( ! is_singular() ) {
                    $classes[] = 'hfeed';
            }            
            if (is_single() ) {
                foreach((wp_get_post_terms( $post->ID)) as $term) {
                    // add category slug to the $classes array
                    $classes[] = esc_attr( $term->slug );
                }
                foreach((wp_get_post_categories( $post->ID, array('fields' => 'slugs'))) as $category) {
                    // add category slug to the $classes array
                    $classes[] = esc_attr( $category );
                }
            }           
            
            $classes[] = 'page-parent header-layout-' . esc_attr($menu_style);           
            
            $classes[] = esc_attr($page_class_extra);

            return $classes;
    }

    add_filter( 'body_class', 'pikoworks_body_class', 1000 );
}