<?php
/**
 * sidebar congifure function css class
 * @author themepiko
 */
if ( !function_exists( 'pikoworks_primary_page_sidebar_class' ) ) {    
    /**
     * Add class to #primary
     * @return string 
     **/
    function pikoworks_primary_page_sidebar_class( $class = '' ) {
        global $pikoworks;
        
        $prefix = 'pikoworks_';
             
         $sidebar_position =  get_post_meta(get_the_ID(), $prefix . 'page_sidebar',true);
         $sidebar_width =  get_post_meta(get_the_ID(), $prefix . 'sidebar_width',true);
             
       
        if (($sidebar_position === '') || ($sidebar_position == '-1')) {            
            $sidebar_position = isset( $pikoworks['optn_page_sidebar_pos'] ) ? $pikoworks['optn_page_sidebar_pos'] : 'fullwidth';
        }

        if (($sidebar_width === '') || ($sidebar_width == '-1')) {
                $sidebar_width = $pikoworks['optn_page_sidebar_width'];
        } 
        
        //calculating cloumn
        if ($sidebar_position == 'left' || $sidebar_position == 'right') {
                if ($sidebar_width == 'large') {
                        $content_col = 'col-md-8 ';
                } else {
                        $content_col = 'col-md-9 ';
                }
        }
        if ($sidebar_position == 'both' ) {
                if ($sidebar_width == 'large') {
                        $content_col = 'col-md-4 ';
                } else {
                        $content_col = 'col-md-6 ';
                }
        }
        
        if ( $sidebar_position == 'fullwidth' ) {
            $class .= ' col-xs-12';
        }elseif($sidebar_position == 'both'){
            $class .= ' col-xs-12 col-sm-6 '. esc_attr($content_col);
        }else{
            $class .= ' col-xs-12 col-sm-8 '. esc_attr($content_col) . ' has-sidebar-' . esc_attr( $sidebar_position );
        }
        
        return esc_attr( $class );        
    }
    
}
if ( !function_exists( 'pikoworks_secondary_page_sidebar_class' ) ) {    
    /**
     * Add class to #secondary
     * @return string 
     **/
    function pikoworks_secondary_page_sidebar_class( $class = '' ) {
        global $pikoworks;
        
        $prefix = 'pikoworks_';        
            
            $sidebar_position =  get_post_meta(get_the_ID(), $prefix . 'page_sidebar',true);
            $sidebar_width =  get_post_meta(get_the_ID(), $prefix . 'sidebar_width',true);
            
        if (($sidebar_position === '') || ($sidebar_position == '-1')) {
            $sidebar_position = $pikoworks['optn_page_sidebar_pos'];
        }        
        
        if (($sidebar_width === '') || ($sidebar_width == '-1')) {
            $sidebar_width = $pikoworks['optn_page_sidebar_width'];
        }
        
        $sidebar_col = 'col-md-3 ';        
        if ($sidebar_width == 'large') {
            $sidebar_col = 'col-md-4 ';
        }
        
        if ( $sidebar_position == 'fullwidth' ) {
            $class .= ' col-xs-12 content-area-fullwidth';
        }elseif($sidebar_position == 'both'){
            $class .= ' col-xs-12 col-sm-3 ' . esc_attr($sidebar_col) ;
        }
        else{
            $class .= ' col-xs-12 col-sm-4 ' . esc_attr($sidebar_col) . 'sidebar sidebar-' . esc_attr( $sidebar_position );
        }
        
        return esc_attr( $class );        
    }    
}
/*----------------------------blog -----------------------------------------*/
if ( !function_exists( 'pikoworks_primary_blog_class' ) ) {    
    /**
     * Add class to #primary
     * @return string 
     **/
    function pikoworks_primary_blog_class( $class = '' ) {
        global $pikoworks;        
          
        $sidebar_position = isset( $pikoworks['optn_blog_sidebar_pos'] ) ? trim( $pikoworks['optn_blog_sidebar_pos'] ) : 'right'; 
        $sidebar_width = isset( $pikoworks['optn_blog_sidebar_width'] ) ? trim( $pikoworks['optn_blog_sidebar_width'] ) : 'small';
       
        
        //calculating cloumn
        if ($sidebar_position == 'left' || $sidebar_position == 'right') {
                if ($sidebar_width == 'large') {
                        $content_col = 'col-md-8 ';
                } else {
                        $content_col = 'col-md-9 ';
                }
        }
        if ($sidebar_position == 'both' ) {
                if ($sidebar_width == 'large') {
                        $content_col = 'col-md-4 ';
                } else {
                        $content_col = 'col-md-6 ';
                }
        }
        
        if ( $sidebar_position == 'fullwidth' ) {
            $class .= ' col-xs-12';
        }elseif($sidebar_position == 'both'){
            $class .= ' col-xs-12 col-sm-6 '. esc_attr($content_col);
        }else{
            $class .= ' col-xs-12 col-sm-8 '. esc_attr($content_col) . ' has-sidebar-' . esc_attr( $sidebar_position );
        }
        
        return esc_attr( $class );        
    }
    
}
if ( !function_exists( 'pikoworks_secondary_blog_class' ) ) {    
    /**
     * Add class to #secondary
     * @return string 
     **/
    function pikoworks_secondary_blog_class( $class = '' ) {
        global $pikoworks;        
        
        $sidebar_position = isset( $pikoworks['optn_blog_sidebar_pos'] ) ? trim( $pikoworks['optn_blog_sidebar_pos'] ) : 'right'; 
        $sidebar_width = isset( $pikoworks['optn_blog_sidebar_width'] ) ? trim( $pikoworks['optn_blog_sidebar_width'] ) : 'small';
        
        $sidebar_col = 'col-md-3 ';        
        if ($sidebar_width == 'large') {
            $sidebar_col = 'col-md-4 ';
        }
        
        if ( $sidebar_position == 'fullwidth' ) {
            $class .= ' col-xs-12 content-area-fullwidth';
        }elseif($sidebar_position == 'both'){
            $class .= ' col-xs-12 col-sm-3 ' . esc_attr($sidebar_col) ;
        }
        else{
            $class .= ' col-xs-12 col-sm-4 ' . esc_attr($sidebar_col) . 'sidebar sidebar-' . esc_attr( $sidebar_position );
        }
        
        return esc_attr( $class );        
    }    
}

/*-------------------------------blog single--------------------------------------*/
if ( !function_exists( 'pikoworks_primary_blog_single_sidebar_class' ) ) {    
    /**
     * Add class to #primary
     * @return string 
     **/
    function pikoworks_primary_blog_single_sidebar_class( $class = '' ) {
        global $pikoworks;
        
        $prefix = 'pikoworks_';       
        
        $sidebar_position =  get_post_meta(get_the_ID(), $prefix . 'page_sidebar',true);
        $sidebar_width =  get_post_meta(get_the_ID(), $prefix . 'sidebar_width',true);        
        
        if (($sidebar_position === '') || ($sidebar_position == '-1')) { 
            $sidebar_position = isset( $pikoworks['optn_blog_single_sidebar_pos'] ) ? $pikoworks['optn_blog_single_sidebar_pos'] : 'fullwidth';
        }

        if (($sidebar_width === '') || ($sidebar_width == '-1')) {
                $sidebar_width = $pikoworks['optn_blog_single_sidebar_width'];
        } 
        
        //calculating cloumn
        if ($sidebar_position == 'left' || $sidebar_position == 'right') {
                if ($sidebar_width == 'large') {
                        $content_col = 'col-md-8 ';
                } else {
                        $content_col = 'col-md-9 ';
                }
        }
        if ($sidebar_position == 'both' ) {
                if ($sidebar_width == 'large') {
                        $content_col = 'col-md-4 ';
                } else {
                        $content_col = 'col-md-6 ';
                }
        }
        
        if ( $sidebar_position == 'fullwidth' ) {
            $class .= ' col-xs-12';
        }elseif($sidebar_position == 'both'){
            $class .= ' col-xs-12 col-sm-6 '. esc_attr($content_col);
        }else{
            $class .= ' col-xs-12 col-sm-8 '. esc_attr($content_col) . ' has-sidebar-' . esc_attr( $sidebar_position );
        }
        
        return esc_attr( $class );        
    }
    
}
if ( !function_exists( 'pikoworks_secondary_blog_single_sidebar_class' ) ) {    
    /**
     * Add class to #secondary
     * @return string 
     **/
    function pikoworks_secondary_blog_single_sidebar_class( $class = '' ) {
        global $pikoworks;
        
        $prefix = 'pikoworks_';       
        
        $sidebar_position =  get_post_meta(get_the_ID(), $prefix . 'page_sidebar',true);
        $sidebar_width =  get_post_meta(get_the_ID(), $prefix . 'sidebar_width',true);
        
        if (($sidebar_position === '') || ($sidebar_position == '-1')) {
            $sidebar_position = $pikoworks['optn_blog_single_sidebar_pos'];
        }        
        
        if (($sidebar_width === '') || ($sidebar_width == '-1')) {
            $sidebar_width = $pikoworks['optn_blog_single_sidebar_width'];
        }
        
        $sidebar_col = 'col-md-3 ';        
        if ($sidebar_width == 'large') {
            $sidebar_col = 'col-md-4 ';
        }
        
        if ( $sidebar_position == 'fullwidth' ) {
            $class .= ' col-xs-12 content-area-fullwidth';
        }elseif($sidebar_position == 'both'){
            $class .= ' col-xs-12 col-sm-3 ' . esc_attr($sidebar_col) ;
        }
        else{
            $class .= ' col-xs-12 col-sm-4 ' . esc_attr($sidebar_col) . 'sidebar sidebar-' . esc_attr( $sidebar_position );
        }
        
        return esc_attr( $class );        
    }    
}
/*--------------------------------search-------------------------------------*/
if ( !function_exists( 'pikoworks_primary_search_class' ) ) {    
    /**
     * Add class to #primary
     * @return string 
     **/
    function pikoworks_primary_search_class( $class = '' ) {
        global $pikoworks;        
          
        $sidebar_position = isset( $pikoworks['optn_search_sidebar_pos'] ) ? trim( $pikoworks['optn_search_sidebar_pos'] ) : 'right'; 
        $sidebar_width = isset( $pikoworks['optn_search_sidebar_width'] ) ? trim( $pikoworks['optn_search_sidebar_width'] ) : 'small';
       
        
        //calculating cloumn
        if ($sidebar_position == 'left' || $sidebar_position == 'right') {
                if ($sidebar_width == 'large') {
                        $content_col = 'col-md-8 ';
                } else {
                        $content_col = 'col-md-9 ';
                }
        }
        if ($sidebar_position == 'both' ) {
                if ($sidebar_width == 'large') {
                        $content_col = 'col-md-4 ';
                } else {
                        $content_col = 'col-md-6 ';
                }
        }
        
        if ( $sidebar_position == 'fullwidth' ) {
            $class .= ' col-xs-12';
        }elseif($sidebar_position == 'both'){
            $class .= ' col-xs-12 col-sm-6 '. esc_attr($content_col);
        }else{
            $class .= ' col-xs-12 col-sm-8 '. esc_attr($content_col) . ' has-sidebar-' . esc_attr( $sidebar_position );
        }
        
        return esc_attr( $class );        
    }
    
}
if ( !function_exists( 'pikoworks_secondary_search_class' ) ) {    
    /**
     * Add class to #secondary
     * @return string 
     **/
    function pikoworks_secondary_search_class( $class = '' ) {
        global $pikoworks;
        
        $sidebar_position = isset( $pikoworks['optn_search_sidebar_pos'] ) ? trim( $pikoworks['optn_search_sidebar_pos'] ) : 'right'; 
        $sidebar_width = isset( $pikoworks['optn_search_sidebar_width'] ) ? trim( $pikoworks['optn_search_sidebar_width'] ) : 'small';
        
        $sidebar_col = 'col-md-3 ';        
        if ($sidebar_width == 'large') {
            $sidebar_col = 'col-md-4 ';
        }
        
        if ( $sidebar_position == 'fullwidth' ) {
            $class .= ' col-xs-12 content-area-fullwidth';
        }elseif($sidebar_position == 'both'){
            $class .= ' col-xs-12 col-sm-3 ' . esc_attr($sidebar_col) ;
        }
        else{
            $class .= ' col-xs-12 col-sm-4 ' . esc_attr($sidebar_col) . 'sidebar sidebar-' . esc_attr( $sidebar_position );
        }
        
        return esc_attr( $class );        
    }    
}
/*----------------------------service -----------------------------------------*/
if ( !function_exists( 'pikoworks_primary_service_class' ) ) {    
    /**
     * Add class to #primary
     * @return string 
     **/
    function pikoworks_primary_service_class( $class = '' ) {
        global $pikoworks;        
          
        $sidebar_position = isset( $pikoworks['optn_archive_service_sidebar_pos'] ) ? trim( $pikoworks['optn_archive_service_sidebar_pos'] ) : 'right'; 
        $sidebar_width = isset( $pikoworks['optn_archive_service_sidebar_width'] ) ? trim( $pikoworks['optn_archive_service_sidebar_width'] ) : 'small';
       
        
        //calculating cloumn
        if ($sidebar_position == 'left' || $sidebar_position == 'right') {
                if ($sidebar_width == 'large') {
                        $content_col = 'col-md-8 ';
                } else {
                        $content_col = 'col-md-9 ';
                }
        }
        if ($sidebar_position == 'both' ) {
                if ($sidebar_width == 'large') {
                        $content_col = 'col-md-4 ';
                } else {
                        $content_col = 'col-md-6 ';
                }
        }
        
        if ( $sidebar_position == 'fullwidth' ) {
            $class .= ' col-xs-12';
        }elseif($sidebar_position == 'both'){
            $class .= ' col-xs-12 col-sm-6 '. esc_attr($content_col);
        }else{
            $class .= ' col-xs-12 col-sm-8 '. esc_attr($content_col) . ' has-sidebar-' . esc_attr( $sidebar_position );
        }
        
        return esc_attr( $class );        
    }
    
}
if ( !function_exists( 'pikoworks_secondary_service_class' ) ) {    
    /**
     * Add class to #secondary
     * @return string 
     **/
    function pikoworks_secondary_service_class( $class = '' ) {
        global $pikoworks;        
        
        $sidebar_position = isset( $pikoworks['optn_archive_service_sidebar_pos'] ) ? trim( $pikoworks['optn_archive_service_sidebar_pos'] ) : 'right'; 
        $sidebar_width = isset( $pikoworks['optn_archive_service_sidebar_width'] ) ? trim( $pikoworks['optn_archive_service_sidebar_width'] ) : 'small';
        
        $sidebar_col = 'col-md-3 ';        
        if ($sidebar_width == 'large') {
            $sidebar_col = 'col-md-4 ';
        }
        
        if ( $sidebar_position == 'fullwidth' ) {
            $class .= ' col-xs-12 content-area-fullwidth';
        }elseif($sidebar_position == 'both'){
            $class .= ' col-xs-12 col-sm-3 ' . esc_attr($sidebar_col) ;
        }
        else{
            $class .= ' col-xs-12 col-sm-4 ' . esc_attr($sidebar_col) . 'sidebar sidebar-' . esc_attr( $sidebar_position );
        }
        
        return esc_attr( $class );        
    }    
}
/*----------------------------Archive product -----------------------------------------*/
if ( !function_exists( 'pikoworks_primary_product_class' ) ) {    
    /**
     * Add class to #primary
     * @return string 
     **/
    function pikoworks_primary_product_class( $class = '' ) {
        global $pikoworks;        
          
        $sidebar_position = isset( $pikoworks['optn_product_sidebar_pos'] ) ? trim( $pikoworks['optn_product_sidebar_pos'] ) : 'fullwidth'; 
        $sidebar_width = isset( $pikoworks['optn_product_sidebar_width'] ) ? trim( $pikoworks['optn_product_sidebar_width'] ) : 'small';
       
        
        //calculating cloumn
        if ($sidebar_position == 'left' || $sidebar_position == 'right') {
                if ($sidebar_width == 'large') {
                        $content_col = 'col-md-8 ';
                } else {
                        $content_col = 'col-md-9 ';
                }
        }
        if ($sidebar_position == 'both' ) {
                if ($sidebar_width == 'large') {
                        $content_col = 'col-md-4 ';
                } else {
                        $content_col = 'col-md-6 ';
                }
        }
        
        if ( $sidebar_position == 'fullwidth' ) {
            $class .= ' col-xs-12';
        }elseif($sidebar_position == 'both'){
            $class .= ' col-xs-12 col-sm-6 '. esc_attr($content_col);
        }else{
            $class .= ' col-xs-12 col-sm-8 '. esc_attr($content_col) . ' has-sidebar-' . esc_attr( $sidebar_position );
        }
        
        return esc_attr( $class );        
    }
    
}
if ( !function_exists( 'pikoworks_secondary_product_class' ) ) {    
    /**
     * Add class to #secondary
     * @return string 
     **/
    function pikoworks_secondary_product_class( $class = '' ) {
        global $pikoworks;        
        
        $sidebar_position = isset( $pikoworks['optn_product_sidebar_pos'] ) ? trim( $pikoworks['optn_product_sidebar_pos'] ) : 'fullwidth'; 
        $sidebar_width = isset( $pikoworks['optn_product_sidebar_width'] ) ? trim( $pikoworks['optn_product_sidebar_width'] ) : 'small';
        
        $sidebar_col = 'col-md-3 ';        
        if ($sidebar_width == 'large') {
            $sidebar_col = 'col-md-4 ';
        }
        
        if ( $sidebar_position == 'fullwidth' ) {
            $class .= ' col-xs-12 content-area-fullwidth';
        }elseif($sidebar_position == 'both'){
            $class .= ' col-xs-12 col-sm-3 ' . esc_attr($sidebar_col) ;
        }
        else{
            $class .= ' col-xs-12 col-sm-4 ' . esc_attr($sidebar_col) . 'sidebar sidebar-' . esc_attr( $sidebar_position );
        }
        
        return esc_attr( $class );        
    }    
}
/*-------------------------------blog single--------------------------------------*/
if ( !function_exists( 'pikoworks_primary_product_single_sidebar_class' ) ) {    
    /**
     * Add class to #primary
     * @return string 
     **/
    function pikoworks_primary_product_single_sidebar_class( $class = '' ) {
        global $pikoworks;
        
        $prefix = 'pikoworks_';                
        $sidebar_position =  get_post_meta(get_the_ID(), $prefix . 'page_sidebar',true);
         $sidebar_width =  get_post_meta(get_the_ID(), $prefix . 'sidebar_width',true);             
       
        if (($sidebar_position === '') || ($sidebar_position == '-1')) {            
            $sidebar_position = isset( $pikoworks['optn_product_single_sidebar_pos'] ) ? $pikoworks['optn_product_single_sidebar_pos'] : 'fullwidth';
        }        

        if (($sidebar_width === '') || ($sidebar_width == '-1')) {
                $sidebar_width = $pikoworks['optn_product_single_sidebar_width'];
        } 
        
        //calculating cloumn
        if ($sidebar_position == 'left' || $sidebar_position == 'right') {
                if ($sidebar_width == 'large') {
                        $content_col = 'col-md-8 ';
                } else {
                        $content_col = 'col-md-9 ';
                }
        }
        if ($sidebar_position == 'both' ) {
                if ($sidebar_width == 'large') {
                        $content_col = 'col-md-4 ';
                } else {
                        $content_col = 'col-md-6 ';
                }
        }
        
        if ( $sidebar_position == 'fullwidth' ) {
            $class .= ' col-xs-12';
        }elseif($sidebar_position == 'both'){
            $class .= ' col-xs-12 col-sm-6 '. esc_attr($content_col);
        }else{
            $class .= ' col-xs-12 col-sm-8 '. esc_attr($content_col) . ' has-sidebar-' . esc_attr( $sidebar_position );
        }
        
        return esc_attr( $class );        
    }
    
}
if ( !function_exists( 'pikoworks_secondary_product_single_sidebar_class' ) ) {    
    /**
     * Add class to #secondary
     * @return string 
     **/
    function pikoworks_secondary_product_single_sidebar_class( $class = '' ) {
        global $pikoworks;
        
        $prefix = 'pikoworks_';               
        $sidebar_position =  get_post_meta(get_the_ID(), $prefix . 'page_sidebar',true);
        $sidebar_width =  get_post_meta(get_the_ID(), $prefix . 'sidebar_width',true); 
        
        if (($sidebar_position === '') || ($sidebar_position == '-1')) {
            $sidebar_position = $pikoworks['optn_product_single_sidebar_pos'];
        }        
        
        if (($sidebar_width === '') || ($sidebar_width == '-1')) {
            $sidebar_width = $pikoworks['optn_product_single_sidebar_width'];
        }
        
        $sidebar_col = 'col-md-3 ';        
        if ($sidebar_width == 'large') {
            $sidebar_col = 'col-md-4 ';
        }
        
        if ( $sidebar_position == 'fullwidth' ) {
            $class .= ' col-xs-12 content-area-fullwidth';
        }elseif($sidebar_position == 'both'){
            $class .= ' col-xs-12 col-sm-3 ' . esc_attr($sidebar_col) ;
        }
        else{
            $class .= ' col-xs-12 col-sm-4 ' . esc_attr($sidebar_col) . 'sidebar sidebar-' . esc_attr( $sidebar_position );
        }
        
        return esc_attr( $class );        
    }    
}

/*-----------------------service single -------------------------*/
if ( !function_exists( 'pikoworks_primary_single_service_class' ) ) {    
    /**
     * Add class to #primary
     * 
     * @return string 
     **/
    function pikoworks_primary_single_service_class( $class = '' ) {
        global $pikoworks;        
        $sidebar_position = isset( $pikoworks['service_single_sidebar_pos'] ) ? trim( $pikoworks['service_single_sidebar_pos'] ) : 'right';        
        if ( $sidebar_position == 'fullwidth' ) {
            $class .= ' col-xs-12';
        }
        else{
            $class .= ' col-xs-12 col-sm-8 col-md-9 has-sidebar-' . esc_attr( $sidebar_position );
        }        
        return esc_attr( $class );        
    }    
}
if ( !function_exists( 'pikoworks_secondary_single_service_class' ) ) {    
    /**
     * Add class to #secondary
     * 
     * @return string 
     **/
    function pikoworks_secondary_single_service_class( $class = '' ) {
        global $pikoworks;        
        $sidebar_position = isset( $pikoworks['service_single_sidebar_pos'] ) ? trim( $pikoworks['service_single_sidebar_pos'] ) : 'right';        
        if ( $sidebar_position == 'fullwidth' ) {
            $class .= ' col-xs-12 content-area-fullwidth';
        }
        else{
            $class .= ' col-xs-12 col-sm-4 col-md-3 sidebar sidebar-' . esc_attr( $sidebar_position );
        }        
        return esc_attr( $class );        
    }    
}
/*-----------------------portfolio single -------------------------*/
if ( !function_exists( 'pikoworks_primary_portfolio_class' ) ) {    
    /**
     * Add class to #primary
     * 
     * @return string 
     **/
    function pikoworks_primary_portfolio_class( $class = '' ) {
        global $pikoworks;
        
        $sidebar_position = isset( $pikoworks['portfolio_single_sidebar_pos'] ) ? trim( $pikoworks['portfolio_single_sidebar_pos'] ) : 'right';
        
        if ( $sidebar_position == 'fullwidth' ) {
            $class .= ' col-xs-12';
        }
        else{
            $class .= ' col-xs-12 col-sm-8 col-md-9 has-sidebar-' . esc_attr( $sidebar_position );
        }
        
        return esc_attr( $class );
        
    }
    
}
if ( !function_exists( 'pikoworks_secondary_portfolio_class' ) ) {
    
    /**
     * Add class to #secondary
     * 
     * @return string 
     **/
    function pikoworks_secondary_portfolio_class( $class = '' ) {
        global $pikoworks;
        
        $sidebar_position = isset( $pikoworks['portfolio_single_sidebar_pos'] ) ? trim( $pikoworks['portfolio_single_sidebar_pos'] ) : 'right';
        
        if ( $sidebar_position == 'fullwidth' ) {
            $class .= ' col-xs-12 content-area-fullwidth';
        }
        else{
            $class .= ' col-xs-12 col-sm-4 col-md-3 sidebar sidebar-' . esc_attr( $sidebar_position );
        }
        
        return esc_attr( $class );
        
    }
    
}
/*-----------------------team single -------------------------*/

if ( !function_exists( 'pikoworks_primary_team_class' ) ) {    
    /**
     * Add class to #primary
     * 
     * @return string 
     **/
    function pikoworks_primary_team_class( $class = '' ) {
        global $pikoworks;
        
        $sidebar_position = isset( $pikoworks['team_single_sidebar_pos'] ) ? trim( $pikoworks['team_single_sidebar_pos'] ) : 'right';
        
        if ( $sidebar_position == 'fullwidth' ) {
            $class .= ' col-xs-12';
        }
        else{
            $class .= ' col-xs-12 col-sm-8 col-md-9 has-sidebar-' . esc_attr( $sidebar_position );
        }
        
        return esc_attr( $class );
        
    }
    
}
if ( !function_exists( 'pikoworks_secondary_team_class' ) ) {
    
    /**
     * Add class to #secondary
     * 
     * @return string 
     **/
    function pikoworks_secondary_team_class( $class = '' ) {
        global $pikoworks;
        
        $sidebar_position = isset( $pikoworks['team_single_sidebar_pos'] ) ? trim( $pikoworks['team_single_sidebar_pos'] ) : 'right';
        
        if ( $sidebar_position == 'fullwidth' ) {
            $class .= ' col-xs-12 content-area-fullwidth';
        }
        else{
            $class .= ' col-xs-12 col-sm-4 col-md-3 sidebar sidebar-' . esc_attr( $sidebar_position );
        }
        
        return esc_attr( $class );
        
    }
    
}