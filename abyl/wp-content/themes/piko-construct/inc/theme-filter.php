<?php

function pikoworks_widget_tag_cloud_args( $args ) {
	$args['largest'] = 1;
	$args['smallest'] = 1;
	$args['unit'] = 'em';
	return $args;
}
add_filter( 'widget_tag_cloud_args', 'pikoworks_widget_tag_cloud_args' );

/*remove all redux notice */
if ( ! class_exists( 'reduxNewsflash' ) ){
    class reduxNewsflash {
        public function __construct( $parent, $params ) {}
    }
}
add_filter( 'redux/pikoworks/aURL_filter', '__return_empty_string' );
/*remove update notices */
if(class_exists('RevSliderBaseAdmin') || class_exists( 'VC_Manager' ) ){
    function pikoworks_filter_plugin_updates( $value ) {
        
        if( isset($value) && is_object($value)){
            unset( $value->response['js_composer/js_composer.php'] );
            unset( $value->response['revslider/revslider.php'] ); 
        }

        return $value;
    }
  add_filter( 'site_transient_update_plugins', 'pikoworks_filter_plugin_updates', 10, 1 );
}

/**
* To enable font upload, adding file mime types
*/
function pikoworks_custom_upload_mimes ( $existing_mimes=array() ) {
	// add your extension to the array
	$existing_mimes['eot'] 	= 'application/vnd.ms-fontobject';
	$existing_mimes['ttf'] 	= 'application/octet-stream';
	$existing_mimes['woff'] = 'application/x-woff';
	$existing_mimes['svg'] 	= 'image/svg+xml';
	
	return $existing_mimes;
}
add_filter('upload_mimes', 'pikoworks_custom_upload_mimes');


function pikoworks_slug_post_classes( $classes, $class, $post_id ) {
    $wp_default = pikoworks_get_option_data('optn_archive_display_type','default');
    $archive_blog_columns = pikoworks_get_option_data('optn_archive_display_columns','2');
    
    if ( get_post_type( get_the_ID() ) == 'post' && !is_singular() ) {
        switch ($wp_default) {    
        case 'list':               
                $classes[] = 'blog-list';              
                break;
        case 'grid': 
              switch ($archive_blog_columns) {    
                case '2':
                    $classes[] = 'col-md-6 col-sm-6 columns-'. esc_attr($archive_blog_columns);
                    break;
                case '3':
                    $classes[] = 'col-md-4 col-sm-4 columns-'. esc_attr($archive_blog_columns);
                    break;
                case '4':
                    $classes[] = 'col-md-3 col-sm-3 columns-'. esc_attr($archive_blog_columns);
                    break;
                default :
                    $classes[] = 'col-xs-12 columns-'. esc_attr($archive_blog_columns);
                } 
                     
            break;
        default :
            if(!is_search()){
              $classes[] = 'col-xs-12';   
            }                       
        }
        
    }    
    if ( get_post_type( get_the_ID() ) == 'post' && is_singular() ) {
       $classes[] = 'posts-wrap';
    }
    
    $archive_service_columns = pikoworks_get_option_data('optn_archive_service_display_columns','2');
    if ( get_post_type( get_the_ID() ) == 'service' && !is_singular() ) {
        switch ($archive_service_columns) {    
        case '2':
            $classes[] = 'col-md-6 col-sm-6 columns-'. esc_attr($archive_service_columns);
            break;
        case '3':
            $classes[] = 'col-md-4 col-sm-4 columns-'. esc_attr($archive_service_columns);
            break;
        case '4':
            $classes[] = 'col-md-3 col-sm-3 columns-'. esc_attr($archive_service_columns);
            break;
        default :
            $classes[] = 'col-xs-12 columns-'. esc_attr($archive_service_columns);
            
        }       
    }
    if ( get_post_type( get_the_ID() ) == 'service' && is_singular() ) {
         $classes[] = 'service-single '. esc_attr(pikoworks_primary_single_service_class());    
    }
    
    if ( get_post_type( get_the_ID() ) == 'portfolio' && is_singular() ) {
         $classes[] = 'post-single ';    
    }
    if ( get_post_type( get_the_ID() ) == 'ourteam' && is_singular() ) {
         $classes[] = 'ourteam-wrap';   
    }
    if ( get_post_type( get_the_ID() ) == 'product' && !is_singular() ) {
         $classes[] = 'column';   
    }
 
    return $classes;
}
add_filter( 'post_class', 'pikoworks_slug_post_classes', 10, 3 );

/*
 * 
 * added since 1.3.0
 */


if ( !function_exists( 'pikoworks_get' ) ){
	function pikoworks_get($var){
		return isset($_GET[$var]) ? $_GET[$var] : (isset($_REQUEST[$var]) ? $_REQUEST[$var] : '');
	}
}
//preset layout
add_filter( 'pikoworks_filter_option_data', 'pikoworks_add_filter_theme_options_presets');
if(!function_exists('pikoworks_add_filter_theme_options_presets')){
	function pikoworks_add_filter_theme_options_presets($options){
            if ( ! class_exists( 'Redux' ) ) {                
                return;
            }            
		if($_preset = pikoworks_get('set')){                       
			$_file = get_template_directory() . '/inc/presets/'.$_preset.'.json';
			if ( file_exists( $_file )) {
				require_once ABSPATH . '/wp-admin/includes/class-wp-filesystem-base.php';
				require_once ABSPATH . '/wp-admin/includes/class-wp-filesystem-direct.php';
				$piko_fs = new WP_Filesystem_Direct(false);
				if(!is_wp_error($piko_fs)){
					$_content = $piko_fs->get_contents($_file);
					$_options = json_decode( $_content, true );
					$options = array_merge( $options, $_options );
				}
			}
		}
		return $options;
	}
}