<?php
/**
 * Enqueues scripts and styles.
 *
 */

/**
 * Enqueue css, js files
 */

if ( !function_exists( 'pikoworks_add_action_wp_enqueue_scripts' ) ){
	add_action( 'wp_enqueue_scripts', 'pikoworks_add_action_wp_enqueue_scripts', 9999 );
        function pikoworks_add_action_wp_enqueue_scripts(){
            global $pikoworks, $pikoworks_woocommerce_global_message_js;
            $min_suffix = (isset($pikoworks['enable_minifile']) && $pikoworks['enable_minifile'] == 1) ? '.min' : '';            
            $enable_min = pikoworks_get_option_data('enable_minifile', 0);      
            $color_skin = isset( $pikoworks['color_skin'] ) ? $pikoworks['color_skin'] : 'default';
            $font_adobe_typekit = isset( $pikoworks['adobe_typekit'] ) ? $pikoworks['adobe_typekit'] : '1';
            
            $handles_style_remove = array(
                'yith-woocompare-widget',
                'woocommerce_prettyPhoto_css',
                'yith-wcwl-font-awesome',
                'woocomposer-front-slick',
                'jquery-colorbox',
                'font-awesome',
            );
            $handles_script_remove = array(
                'woocomposer-slick',
                'prettyPhoto',
                'prettyPhoto-init'
            );
            foreach ($handles_style_remove as $style) {
                if ( wp_style_is( $style, $list = 'registered' ) ) {
                        wp_deregister_style( $style );
                }
            }
            foreach ($handles_script_remove as $script) {
                if ( wp_script_is( $script, $list = 'enqueued' ) ) {
                        wp_dequeue_script( $script );
                }
            }
            
            /*
             * Stylesheet
             */
            if(wp_style_is('js_composer_front','registered')){
                wp_enqueue_style('js_composer_front');
            }               
            wp_enqueue_style( 'pikoworks-style', get_stylesheet_uri() ); // Theme stylesheet.    
            
            if(function_exists( 'WC' )){                
                wp_enqueue_style('pikoworks-main-style', pikoworks_css. '/style'.$min_suffix.'.css', false, pikoworks_theme_version, 'all');  
            }else{
                wp_enqueue_style('pikoworks-main-style', pikoworks_css. '/no-style'.$min_suffix.'.css', false, pikoworks_theme_version, 'all'); 
            }
            
            /*
             * inline style
             */
            $custom_add_css = pikoworks_get_option_data('custom_css', '');
            $skin_custom = pikoworks_get_option_data('color_skin', 'default');
            $primary_color_scheme =  pikoworks_get_option_data('main_color', '');
            $primary_font_color_scheme =  pikoworks_get_option_data('main_bg_font_color', '');
            $secondary_color =  pikoworks_get_option_data('secondary_color', '');

            $custom_main_css = '';
            if($skin_custom == 'skin_custom'){
                $custom_main_css = '.btn-wrap a:hover{color:'.esc_attr($secondary_color).' !important;}.layout-outline .btn-wrap a:hover { background-color: '.esc_attr($primary_color_scheme).' !important;}.layout-outline .btn-wrap a:hover {border-color: '.esc_attr($primary_color_scheme).' !important;}.layout-outline .btn-wrap a:hover {color: '.esc_attr($primary_font_color_scheme).' !important;}';
            }                 
            $custom_css = $custom_add_css . $custom_main_css ;
            wp_add_inline_style( 'pikoworks-main-style', $custom_css );
            
            /*
             * Scripts
             */
            if ( $font_adobe_typekit == '2' && !empty($pikoworks['font_typekit_kit_id']) ) {
                wp_enqueue_script('pikoworks-font-typekit', esc_url('//use.typekit.net/' . $pikoworks['font_typekit_kit_id'] .'.js'), array(), NULL, FALSE );
                wp_enqueue_script('pikoworks-font-typekit-exec', pikoworks_js. 'typekit.js', array(), NULL, FALSE );
            }
           
            if(function_exists( 'WC' )){
                if($enable_min == 0){
                    wp_enqueue_script('pikoworks-jquery-plugins', pikoworks_js.'/plugins.min.js', array('jquery'), pikoworks_theme_version, true);  
                }
                wp_enqueue_script('pikoworks-frontend', pikoworks_js.'/frontend'.$min_suffix.'.js', array('jquery'), pikoworks_theme_version, true);
            }else{
                if($enable_min == 0){
                    wp_enqueue_script('pikoworks-jquery-plugins', pikoworks_js.'/no-plugins.min.js', array('jquery'), pikoworks_theme_version, true);  
                }
                wp_enqueue_script('pikoworks-frontend', pikoworks_js.'/no-frontend'.$min_suffix.'.js', array('jquery'), pikoworks_theme_version, true);
            }
            /*
             * inline custome Scripts
             */
            $custom_add_script = pikoworks_get_option_data('custom_js', '');
            $enable_coming_soon = pikoworks_get_option_data('enable_coming_soon_mode', false);
            $enable_coming_soon_bg = pikoworks_get_option_data('enable_coming_soon_bg_img', false);
            $coming_script = '';
            if($enable_coming_soon == 1 && $enable_coming_soon_bg == 1){
                $coming_soon_photo_ids = explode( ',', pikoworks_get_option_data('coming_soon_bg_img'));
                $img_source = array();
                $i=0;
                foreach($coming_soon_photo_ids as $coming_soon_photo_id):                
                    $i++;
                    $img_source[$i] = "{src: '" . wp_get_attachment_url( $coming_soon_photo_id )."'}";                
                endforeach;                
                $coming_script = "jQuery(document).ready(function ($) {
                    if($('.coming-soon').length > 0){
                        $('.coming-soon').vegas({
                            timer: " . esc_attr( $GLOBALS['pikoworks']['coming_soon_slide_timer'] ) . ",
                            transitionDuration: " . esc_attr( pikoworks_get_option_data('coming_soon_bg_img_transit_duration') ). ",
                            slides: [". join(', ', $img_source) ."],
                            transition: '" .esc_attr( pikoworks_get_option_data('coming_soon_bg_img_transit') )."',               
                            overlay: true
                        });
                    }
                });";
            }            
            $custom_script = $custom_add_script . $coming_script;
            wp_add_inline_script( 'pikoworks-frontend', $custom_script);            
            
            wp_localize_script( 'pikoworks-frontend', 'pikoworks_global_message', apply_filters( 'pikoworks_filter_global_message_js', array(
			
			'global' => array(
				'error' => esc_attr__('An error occurred ,Please try again !','piko-construct'),
				'comment_author'    => esc_attr__('Please enter Name !','piko-construct'),
				'comment_email'     => esc_attr__('Please enter Email Address !','piko-construct'),
				'comment_rating'    => esc_attr__('Please select a rating !','piko-construct'),
				'comment_content'   => esc_attr__('Please enter Comment !','piko-construct'),
				'days'   => esc_attr__('Days','piko-construct'),
				'hours'   => esc_attr__('Hours','piko-construct'),
				'minutes'   => esc_attr__('Minutes','piko-construct'),
				'seconds'   => esc_attr__('Seconds','piko-construct')
			),
			'enable_sticky_header' => pikoworks_get_option_data('sticky_header',false)
		) ) );
            
            if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
                wp_enqueue_script( 'comment-reply' );
            }
        }
}