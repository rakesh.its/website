<?php
/**
 * @author themepiko
 *
 */
if(!function_exists('pikoworks_breadcrumbs')){
    function pikoworks_breadcrumbs() {
      //page title style
      global $pikoworks;
      // Get header layout style
      
    if( function_exists( 'is_woocommerce' ) && ( is_shop() || is_product_category() || is_product_tag() || is_product() )){
       $header_img = isset( $pikoworks['optn_archive_header_img'] ) ? $pikoworks['optn_archive_header_img'] : array( 'url' => get_template_directory_uri() . '/assets/images/bg/page-title.jpg' );
        $header_title_text_align =  isset( $pikoworks['woo_header_title_text_align'] ) ? $pikoworks['woo_header_title_text_align'] : 'left'; 
        $page_header_title =  isset( $pikoworks['woo_page_header_title'] ) ? $pikoworks['woo_page_header_title'] : 1;
        
        
        $breadcrumb_layout =  get_post_meta(get_the_ID(),'pikoworks_breadcrumb_layout',true);
        if (!isset($breadcrumb_layout) || $breadcrumb_layout == 'global' || $breadcrumb_layout == '') {
            $breadcrumb_layout =  isset( $pikoworks['woo_breadcrumb_layout'] ) ? $pikoworks['woo_breadcrumb_layout'] : 'one_cols';
        }
        
        if(is_product()){
            $page_header_title =  get_post_meta(get_the_ID(),'pikoworks_single_header_title_section',true);
            if (!isset($page_header_title) || $page_header_title == 'global' || $page_header_title == '') { 
               $page_header_title =  isset( $pikoworks['woo_single_header_title'] ) ? $pikoworks['woo_single_header_title'] : 1;
            }
            $breadcrumb_layout =  get_post_meta(get_the_ID(),'pikoworks_breadcrumb_layout',true);
            if (!isset($breadcrumb_layout) || $breadcrumb_layout == 'global' || $breadcrumb_layout == '') {   
               $breadcrumb_layout =  isset( $pikoworks['woo_single_breadcrumb_layout'] ) ? $pikoworks['woo_single_breadcrumb_layout'] : 'one_cols';
            }           
           $header_title_text_align =  isset( $pikoworks['woo_single_header_title_text_align'] ) ? $pikoworks['woo_single_header_title_text_align'] : 'left'; 
        }
            $layout_2column =  get_post_meta(get_the_ID(),'pikoworks_breadcrumb_layout_title',true);
            if (!isset($layout_2column) || $layout_2column == '-1' || $layout_2column == '') {
            $layout_2column =  isset( $pikoworks['woo_breadcrumb_layout_title'] ) ? $pikoworks['woo_breadcrumb_layout_title'] : 'title-left';
            }

    }else{
      $header_img = isset( $pikoworks['optn_header_img'] ) ? $pikoworks['optn_header_img'] : array( 'url' => get_template_directory_uri() . '/assets/images/bg/page-title.jpg' );  
      $header_title_text_align =  isset( $pikoworks['optn_header_title_text_align'] ) ? $pikoworks['optn_header_title_text_align'] : 'center'; 
        $page_header_title =  get_post_meta(get_the_ID(),'pikoworks_single_header_title_section',true);
        if (!isset($page_header_title) || $page_header_title == 'global' || $page_header_title == '') { 
        $page_header_title =  isset( $pikoworks['page_header_title'] ) ? $pikoworks['page_header_title'] : 1;
        }
        $breadcrumb_layout =  get_post_meta(get_the_ID(),'pikoworks_breadcrumb_layout',true);
        if (!isset($breadcrumb_layout) || $breadcrumb_layout == 'global' || $breadcrumb_layout == '') { 
            $breadcrumb_layout =  isset( $pikoworks['breadcrumb_layout'] ) ? $pikoworks['breadcrumb_layout'] : 'one_cols';
        }     
        $layout_2column =  get_post_meta(get_the_ID(),'pikoworks_breadcrumb_layout_title',true);
        if (!isset($layout_2column) || $layout_2column == '-1' || $layout_2column == '') {
            $layout_2column =  isset( $pikoworks['breadcrumb_layout_title'] ) ? $pikoworks['breadcrumb_layout_title'] : 'title-left';
        }
      
    }  
    
    $breadcrubm_layout =  get_post_meta(get_the_ID(),'pikoworks_disable_breadcrubm_layout',true);
    if (!isset($breadcrubm_layout) || $breadcrubm_layout == 'global' || $breadcrubm_layout == '') {
        $breadcrubm_layout =  isset( $pikoworks['optn_breadcrubm_layout'] ) ? $pikoworks['optn_breadcrubm_layout'] : 1;        
    }
    
    $woo_disable =  isset( $pikoworks['woo_breadcrumbs_disable'] ) ? $pikoworks['woo_breadcrumbs_disable'] : 1;
    $woo_single_disable =  isset( $pikoworks['woo_single_breadcrumbs_disable'] ) ? $pikoworks['woo_single_breadcrumbs_disable'] : 1;
    
    $breadcrubm_width =  isset( $pikoworks['optn_breadcrubm_width'] ) ? $pikoworks['optn_breadcrubm_width'] : 'container';
    
    $breadcrumb_disable =  isset( $pikoworks['breadcrumbs_disable'] ) ? $pikoworks['breadcrumbs_disable'] : 1;
    
    
    $woo_breadcrumb_disable =  isset( $pikoworks['woo_disable_breadcrubm'] ) ? $pikoworks['woo_disable_breadcrubm'] : 1;
    $enable_slider =  isset( $pikoworks['woo_archive_slide_enable'] ) ? $pikoworks['woo_archive_slide_enable'] : 0;
    $archive_slider =  isset( $pikoworks['woo_archive_side_shortcode'] ) ? $pikoworks['woo_archive_side_shortcode'] : '';
    
    
    
    
    $breadcrubm_name =  isset( $pikoworks['optn_breadcrumb_name'] ) ? $pikoworks['optn_breadcrumb_name'] : esc_html__('Home', 'piko-construct');
    $breadcrubm_delimiter =  isset( $pikoworks['optn_breadcrumb_delimiter'] ) ? $pikoworks['optn_breadcrumb_delimiter'] : 'fa-angle-right';
    $header_img_repeat =  isset( $pikoworks['optn_header_img_repeat'] ) ? $pikoworks['optn_header_img_repeat'] : 'repeat';
    $breadcrumbs_prefix =  isset( $pikoworks['breadcrumbs_prefix'] ) ? $pikoworks['breadcrumbs_prefix'] : '';  
    $page_sub_title = strip_tags(term_description());    
    
    $sub_title_html = '';
    $breadcrumbs_prefix_html ='';
   if ($page_sub_title != '') :
        $sub_title_html = '<span class="banner-subtitle"> ' . esc_attr($page_sub_title) .'</span>';
     endif;    
   if ($breadcrumbs_prefix != '') :
        $breadcrumbs_prefix_html = '<span class="prefix"> ' . esc_attr($breadcrumbs_prefix) .'</span>';
     endif;

$showOnHome = 1; // 1 - show breadcrumbs on the homepage, 0 - don't show
$delimiter = '<i class="fa '. esc_attr($breadcrubm_delimiter) .'" aria-hidden="true"></i> '; // delimiter between crumbs
$home = $breadcrubm_name; // text for the 'Home' link
$showCurrent = 1; // 1 - show current post/page title in breadcrumbs, 0 - don't show
$before = '<span class="current">'; // tag before the current crumb
$after = '</span>'; // tag after the current crumb
    
$top_banner_class = '';
$top_banner_style = '';
if ( trim( $header_img['url'] ) != '' ) {
    $top_banner_class .= ' has-bg-img';
    $top_banner_style = 'style="background: ' . esc_attr(isset($pikoworks['optn_header_img_bg_color']) ? $pikoworks['optn_header_img_bg_color'] : '#f5f5f5' ) .' url(' . esc_url( $header_img['url'] ) . ') ' . esc_attr( $header_img_repeat ) . ' center center;"';
}
else{
    $top_banner_class .= ' no-bg-img';
}


$title = '';

if ( is_front_page() && is_home() ) {
    // Default homepage
    
} elseif ( is_front_page() ) {
    // static homepage
    
} elseif ( is_home() ) {
    // blog page
    $post_id = get_option( 'page_for_posts' );
    $title = pikoworks_single_title( $post_id );
    $top_banner_style = pikoworks_single_header_bg_style( $post_id );
    $header_title_text_align = pikoworks_single_title_align( $post_id );
    
    if ( trim( $top_banner_style ) != '' ) {
        $top_banner_class = 'has-bg-img';
    }
    else{
        $top_banner_class = 'no-bg-img';
    }
    
} else {
    //everything else
    
    // Is a singular
    if ( is_singular() ) {
        $show_single_title_section = pikoworks_is_single_show_header_title_section();
        if ( !$show_single_title_section ) {
            echo '<div class="just-wraper"></div>';
            return;   
        }        
        $title = pikoworks_single_title();
        $top_banner_style = pikoworks_single_header_bg_style();
        $header_title_text_align = pikoworks_single_title_align();
        if ( function_exists( 'is_woocommerce' ) && is_product() ) {
           $header_title_text_align = pikoworks_single_title_align(); 
        }
    }
    else{
        // Is archive or taxonomy
        if ( is_archive() ) {
            
        // $title = post_type_archive_title( '', true );
            
            if( is_post_type_archive() ){
                 $title = post_type_archive_title( '', false );
                  
                if ( function_exists( 'is_woocommerce' ) ) { 
                 if ( is_woocommerce() ) {
                      if ( apply_filters( 'woocommerce_show_page_title', false ) ) {
                        $title = woocommerce_page_title( true ); 
                        }
                      }
                }
                      
            }else{
                $title = get_the_archive_title();
            }
            
            // Checking for shop archive
            if ( function_exists( 'is_woocommerce' ) ) { // Products archive, products category, products search page...               
                
                   if ( !is_woocommerce() && is_shop() ) {
                    if ( apply_filters( 'woocommerce_show_page_title', true ) ) {
                        $post_id = get_option( 'woocommerce_shop_page_id' );
                        $use_custom_title = get_post_meta( $post_id, 'pikoworks_use_custom_title', true ) == 'yes';
                        
                        if ( $use_custom_title ) {
                            $title = pikoworks_single_title( $post_id );
                        }
                        else{
                            $title = woocommerce_page_title( false );    
                        }
                        
                        $top_banner_style = pikoworks_single_header_bg_style( $post_id );
                        $header_title_text_align = pikoworks_single_title_align( $post_id );
                         
                    }
                }
                 
            } 
            
        }
        else{           
            if ( is_404() ) {
                $title = isset( $pikoworks['optn_404_breadcrumb'] ) ? esc_attr($pikoworks['optn_404_breadcrumb']) : esc_html__( 'Oops 404 !', 'piko-construct' );
            }
            else{ 
                if ( is_search() ) {
                    $title = sprintf( esc_html__( 'Search results for: %s', 'piko-construct' ), get_search_query() );
                }
                else{
                    // is category, is tag, is tax
                    $title = single_cat_title( '', false );   
                } 
            }
        }        
    }
}

if ( trim( $top_banner_style ) != '' ) {
    $top_banner_class = 'has-bg-img';
}
else{
    $top_banner_class = 'no-bg-img';
}
$layout_two = '';
if($breadcrumb_layout == 'two_cols'){
    $layout_two = $layout_2column;
}

$top_banner_text_align = '';
$top_banner_text_align .= ' text-' .$header_title_text_align . ' ' .$layout_two ;

$title_html ='';
if($page_header_title == 1){
    $title_html = '<h1>' . sanitize_text_field($title) . '</h1>' . do_shortcode($sub_title_html);
}


    // custom breadcrumbs
      global $post;
      $homeLink = home_url( '/' );
      $woo_padding = '';
      if( function_exists( 'is_woocommerce' ) && ( is_shop() || is_product_category() || is_product_tag()) ){
        $woo_padding .= 'woo-breadcrumb';  
      }else{
         $woo_padding .= 'woo-single';   
      }
      

      if (is_front_page()) {

        if ($showOnHome == 1)  echo '<div class="just-wraper"></div>';

      } elseif(function_exists( 'is_woocommerce' ) && ( is_shop() || is_product_category() || is_product_tag() || is_product() )){
            if($woo_disable == 0 && ( is_shop() || is_product_category() || is_product_tag() ) || $woo_single_disable == 0 && is_product() ){
                echo '<div class="just-wraper"></div>';
                return;
            }          
          
             echo '<section class="page-header '. esc_attr($top_banner_class . ' ' . $woo_padding . ' ' . $top_banner_text_align).'" '.  do_shortcode($top_banner_style) .'>
                <div class="' . esc_attr($breadcrubm_width) . '">  ' . do_shortcode($title_html);
             
                    if(function_exists('pikoworks_wc_product_cats_list')){ echo pikoworks_wc_product_cats_list(); }                          
                     echo  do_shortcode($sub_title_html); //product category list
             
            if($woo_breadcrumb_disable == 1){                
              echo '<div class="breadcrumb">';  
                woocommerce_breadcrumb();
              echo '</div></div>';                
            }             
            echo '</section>';
            if($enable_slider == 1 && !is_product()){
               echo '<div class="woo-archive-slider">' . do_shortcode( $archive_slider ) . '</div>';
            }
            
      } else {
          if($breadcrumb_disable == 0){
                echo '<div class="just-wraper"></div>';
                return;
            }
           echo '<section class="page-header '. esc_attr($top_banner_class . ' ' . $top_banner_text_align).'" '.  do_shortcode($top_banner_style) .'>
                <div class="' . esc_attr($breadcrubm_width) . '">  ' . do_shortcode($title_html);
           
           if($breadcrubm_layout == 1 && !is_home()){
              echo '<div class="breadcrumb">' . do_shortcode($breadcrumbs_prefix_html);
              echo  '<a href="' . esc_url($homeLink) . '">' . esc_html($home) . '</a> ' . do_shortcode($delimiter) . ' ';
            }else{
                echo '</div></section>';
                return; //stop the next code when not show breadcrubm
            }          

        if ( is_category() ) {
          $thisCat = get_category(get_query_var('cat'), false);
          if ($thisCat->parent != 0) echo get_category_parents($thisCat->parent, TRUE, ' ' . $delimiter . ' ');
          echo wp_kses_post($before) . esc_html__('Category', 'piko-construct'). '"' . single_cat_title('', false) . '"' . wp_kses_post($after);

        }elseif ( is_home() ) {            
          echo wp_kses_post($before) . get_the_title( get_the_ID() ) . wp_kses_post($after);
        } elseif ( is_search() ) {
          echo wp_kses_post($before) . esc_html__('Search', 'piko-construct').'"' . get_search_query() . '"' . wp_kses_post($after);

        } elseif ( is_day() ) {
          echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
          echo '<a href="' . get_month_link(get_the_time('Y'),get_the_time('m')) . '">' . get_the_time('F') . '</a> ' . $delimiter . ' ';
          echo wp_kses_post($before) . get_the_time('d') . wp_kses_post($after);

        } elseif ( is_month() ) {
          echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
          echo wp_kses_post($before) . get_the_time('F') . wp_kses_post($after);

        } elseif ( is_year() ) {
          echo wp_kses_post($before) . get_the_time('Y') . wp_kses_post($after);

        } elseif ( is_single() && !is_attachment() ) {
          if ( get_post_type() != 'post' ) {
            $post_type = get_post_type_object(get_post_type());
            $slug = $post_type->rewrite;
            echo '<a href="' . esc_url($homeLink) . $slug['slug'] . '/">' . $post_type->labels->singular_name . '</a>';
            if ($showCurrent == 1) echo ' ' . $delimiter . ' ' . wp_kses_post($before) . get_the_title() . wp_kses_post($after);
          } else {
            $cat = get_the_category(); $cat = $cat[0];
            $cats = get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
            if ($showCurrent == 0) $cats = preg_replace("#^(.+)\s$delimiter\s$#", "$1", $cats);
            echo do_shortcode($cats);
            if ($showCurrent == 1) echo wp_kses_post($before) . get_the_title() . wp_kses_post($after);
          }

        } elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ) {
          $post_type = get_post_type_object(get_post_type());
          echo wp_kses_post($before) . $post_type->labels->singular_name . wp_kses_post($after);

        } elseif ( is_attachment() ) {
          $parent = get_post($post->post_parent);
          $cat = get_the_category($parent->ID); $cat = $cat[0];
          echo get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
          echo '<a href="' . get_permalink($parent) . '">' . $parent->post_title . '</a>';
          if ($showCurrent == 1) echo ' ' . $delimiter . ' ' . wp_kses_post($before) . get_the_title() . wp_kses_post($after);

        } elseif ( is_page() && !$post->post_parent ) {
          if ($showCurrent == 1) echo wp_kses_post($before) . get_the_title() . wp_kses_post($after);

        } elseif ( is_page() && $post->post_parent ) {
          $parent_id  = $post->post_parent;
          $breadcrumbs = array();
          while ($parent_id) {
            $page = get_page($parent_id);
            $breadcrumbs[] = '<a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a>';
            $parent_id  = $page->post_parent;
          }
          $breadcrumbs = array_reverse($breadcrumbs);
          for ($i = 0; $i < count($breadcrumbs); $i++) {
            echo do_shortcode($breadcrumbs[$i]);
            if ($i != count($breadcrumbs)-1) echo ' ' . $delimiter . ' ';
          }
          if ($showCurrent == 1) echo ' ' . $delimiter . ' ' . wp_kses_post($before) . get_the_title() . wp_kses_post($after);

        } elseif ( is_tag() ) {
          echo wp_kses_post($before) . esc_html__('Posts tagged', 'piko-construct'). '"' . single_tag_title('', false) . '"' . wp_kses_post($after);

        } elseif ( is_author() ) {
           global $author;
          $userdata = get_userdata($author);
          echo wp_kses_post($before) . esc_html__('Articles posted by: ', 'piko-construct') . $userdata->display_name . wp_kses_post($after);

        } elseif ( is_404() ) {
          echo wp_kses_post($before) . esc_html__('Error 404 ', 'piko-construct') . wp_kses_post($after);
        }

        if ( get_query_var('paged') ) {
          if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ' (';
          //echo esc_html__('Page', 'piko-construct') . ' ' . get_query_var('paged');
          if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ')';
        }

        if(!is_home()){
            echo'</div> </div>';
        }    
        echo '</section>';

      }
    } 
} // end pikoworks_breadcrumbs()


if ( !function_exists( 'pikoworks_single_title' ) ) {
    function pikoworks_single_title( $post_id = 0 ) {
        //for global meta
        global $pikoworks;
        
        $post_id = max( 0, intval( $post_id ) );
        $title = '';
        
        if ( $post_id == 0 && is_singular() ) {
            $post_id = get_the_ID();
        }
        
        if ( $post_id > 0 ) {
            
            $show_single_title_section_setting = get_post_meta( $post_id, 'pikoworks_single_header_title_section', true );
            $use_custom_title = get_post_meta( $post_id, 'pikoworks_use_custom_title', true ) == 'yes';
            
            // if is single post, check options title type 
            if ( get_post_type( $post_id ) == 'post' ) {
                // check using single post title or blgo title for header title section
                $title_type = isset( $pikoworks['opt_single_post_title_type'] ) ? trim( $pikoworks['opt_single_post_title_type'] ) : 'single'; // single, blog
                if ( $title_type == 'blog' ) {
                    // if using global setting or show title but not use custom title
                    if ( $show_single_title_section_setting == 'global' || $show_single_title_section_setting == 'show' && !$use_custom_title ) {
                        $post_id = get_option( 'page_for_posts' );
                        $use_custom_title = get_post_meta( $post_id, 'pikoworks_use_custom_title', true ) == 'yes';
                    }
                    
                }
            }
            
            $title = get_the_title( $post_id );
            
            if ( $use_custom_title ) {
                $title = get_post_meta( $post_id, 'pikoworks_custom_header_title', true );
            } 
            else{
                
            }
            
        }
        
        return $title;
        
    }
}

if ( !function_exists( 'pikoworks_single_header_bg_style' ) ) {
    function pikoworks_single_header_bg_style( $post_id = 0 ) {
        global $pikoworks;
        $post_id = max( 0, intval( $post_id ) );
        $top_banner_style = '';
        
        if ( $post_id == 0 && is_singular() ) {
            $post_id = get_the_ID();
        }
        
        $header_img = array(
            'url' => get_template_directory_uri() . '/assets/images/bg/page-title.jpg'
        );
        $header_img_repeat = 'repeat';
        
        if ( $post_id > 0 ) {
            $header_bg_type = get_post_meta( $post_id, 'pikoworks_header_bg_type', true );
            if ( trim( $header_bg_type ) == '' ) {
                $header_bg_type = 'global';
            }            
            switch ( $header_bg_type ){
                case 'global':
                    
                    if( function_exists( 'is_woocommerce' ) && ( is_shop() || is_product_category() || is_product_tag() || is_product() )){
                       $header_img = isset( $pikoworks['optn_archive_header_img'] ) ? $pikoworks['optn_archive_header_img'] : array( 'url' => get_template_directory_uri() . '/assets/images/bg/page-title.jpg' );
                    }else{
                        $header_img = isset( $pikoworks['optn_header_img'] ) ? $pikoworks['optn_header_img'] : $header_img;
                    } 
                    
                    $header_img_repeat =  isset( $pikoworks['optn_header_img_repeat'] ) ? $pikoworks['optn_header_img_repeat'] : $header_img_repeat;
                    break;
                case 'image':
                    $header_img_id = get_post_meta( $post_id, 'pikoworks_header_bg_src', true );
                    $header_img['url'] = wp_get_attachment_image_url($header_img_id, '') ? wp_get_attachment_image_url($header_img_id, '') : $header_img['url'];
                    $header_img_repeat = trim( get_post_meta( $post_id, 'pikoworks_header_bg_repeat', true ) ) != '' ? trim( get_post_meta( $post_id, 'pikoworks_header_bg_repeat', true ) ) : $header_img_repeat;
                    break;
                case 'no_image':
                    $header_img['url'] = '';
                    break;
            }
        }       
        if ( trim( $header_img['url'] ) != '' ) {
            if ( $header_img_repeat == 'no-repeat' ) {                
             $top_banner_style = 'style="background: '. esc_attr(isset($pikoworks['optn_header_img_bg_color']) ? $pikoworks['optn_header_img_bg_color'] : '#f5f5f5' ) .' url(' . esc_url( $header_img['url'] ) . ') ' . esc_attr( $header_img_repeat ) . ' center center; background-size: cover !important;"';   
            }
            elseif ( $header_img_repeat == 'fixed' ) {                
             $top_banner_style = 'style="background: '. esc_attr(isset($pikoworks['optn_header_img_bg_color']) ? $pikoworks['optn_header_img_bg_color'] : '#f5f5f5' ) .' url(' . esc_url( $header_img['url'] ) . ') ' . esc_attr( $header_img_repeat ) . ' center center; background-size: cover !important; background-attachment: fixed; background-position: 50% 50%"';   
            }
            else{
                $top_banner_style = 'style="background: '. esc_attr(isset($pikoworks['optn_header_img_bg_color']) ? $pikoworks['optn_header_img_bg_color'] : '#f5f5f5' ) .' url(' . esc_url( $header_img['url'] ) . ') ' . esc_attr( $header_img_repeat ) . ' center center;"';
            }
        }        
        return $top_banner_style;
        
    }
}

if ( !function_exists( 'pikoworks_single_title_align' ) ) {
    function pikoworks_single_title_align( $post_id = 0 ) {
        global $pikoworks;
         $header_title_text_align =  isset( $pikoworks['optn_header_title_text_align'] ) ? $pikoworks['optn_header_title_text_align'] : 'center';
        
        if(function_exists( 'is_woocommerce' ) && is_product()){
           $header_title_text_align =  isset( $pikoworks['woo_single_header_title_text_align'] ) ? $pikoworks['woo_single_header_title_text_align'] : 'left'; 
        }
        
        $post_id = max( 0, intval( $post_id ) );
        
        if ( $post_id == 0 ) {
            $post_id = get_the_ID();
        }
        
        if ( $post_id > 0 ) {
            
            $post_title_align = get_post_meta( $post_id, 'pikoworks_header_title_text_align', true );
            
            if ( $post_title_align != 'global' ) {
                $header_title_text_align = $post_title_align;
            }
            
        }
        
        return $header_title_text_align;
        
    }
}


if ( !function_exists( 'pikoworks_is_single_show_header_title_section' ) ) {
    function pikoworks_is_single_show_header_title_section( $post_id = 0 ) {
        global $pikoworks;
        
        $show_single_title_section_setting = 'show';
        $show_single_title_section = true;
        
        $post_id = max( 0, intval( $post_id ) );
        $title = '';
        
        if ( $post_id == 0 ) {
            $post_id = get_the_ID();
        }
        
        if ( get_post_type( $post_id ) == 'product' ) {
            $show_single_title_section = isset( $pikoworks['optn_single_product_header_title_section'] ) ? $pikoworks['optn_single_product_header_title_section'] == 1 : true;  
        }
        if ( get_post_type( $post_id ) == 'post' ) {
            $show_single_title_section = isset( $pikoworks['optn_single_post_header_title_section'] ) ? $pikoworks['optn_single_post_header_title_section'] == 1 : true;  
        }
        
        $show_single_title_section_setting = get_post_meta( $post_id, 'pikoworks_single_header_title_section', true );
        if ( $show_single_title_section_setting == 'show' ) {
            $show_single_title_section = true;
        }
        if ( $show_single_title_section_setting == 'dont_show' ) {
            $show_single_title_section = false;
        }
        
        return $show_single_title_section;
    }
}