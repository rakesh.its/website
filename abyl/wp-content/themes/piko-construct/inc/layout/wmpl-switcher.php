<?php 
/* 
 * language wpml
 */

if( ! function_exists( 'pikoworks_lang_switcher' ) ){
    function pikoworks_lang_switcher() {
        if( function_exists( 'icl_get_languages' ) ){
                $languages = icl_get_languages( 'skip_missing=0&orderby=code' );
                $output = '';
                if ( ! empty( $languages ) ) {
                        $output .= '<li class="menu-item-has-children"> <a href="#"> '. ICL_LANGUAGE_NAME_EN .'</a>';
                        $output .= '<ul class="sub-menu">';
                        foreach ( $languages as $l ) {
                                if ( ! $l['active'] ) {
                                        $output .= '<li>';
                                        $output .= '<a href="' . $l['url'] . '"> <img src="'.$l['country_flag_url'].'" height="12" alt="'.$l['language_code'].'" width="18" />';
                                        $output .= icl_disp_language( $l['native_name'] );
                                        $output .= '</a>';
                                        $output .= '</li>';
                                }
                        }
                        $output .= '</ul>';
                        $output .= '</li>';
                        echo do_shortcode( $output );
                }
        }
    }
}
