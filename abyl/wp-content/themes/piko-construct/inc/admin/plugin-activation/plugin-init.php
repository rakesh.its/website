<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

add_action( 'tgmpa_register', 'pikoworks_register_required_plugins' );
function pikoworks_register_required_plugins() {
    
    $Ultimate_VC_Addons = $wishlist_plugin = $compare_plugin = $quickview_plugin = '';
    if ( in_array( 'js_composer/js_composer.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) { 
            $Ultimate_VC_Addons = array(
                        'name'                     => 'Ultimate Addons for Visual Composer',
                        'slug'                     => 'Ultimate_VC_Addons',
                        'source'                   => 'http://themepiko.com/resources/plugins/Ultimate_VC_Addons.zip',
                        'required'                 => false,
                        'version'                  => '3.16.7',
                    );
    }
    
    if(function_exists( 'WC' )){
        $wishlist_plugin = array(
                            'name'      => 'YITH WooCommerce Wishlist',
                            'slug'      => 'yith-woocommerce-wishlist',
                            'required'  => false,
                    );

        $compare_plugin = array(
                            'name'      => 'YITH WooCommerce Compare',
                            'slug'      => 'yith-woocommerce-compare',
                            'required'  => false,
                    );
        $quickview_plugin = array(
                            'name'      => 'YITH WooCommerce Quick View',
                            'slug'      => 'yith-woocommerce-quick-view',
                            'required'  => false,
                    );
    }
    
    
    
	$plugins = array(
                // This is an example of how to include a plugin bundled with a theme.
                array(
                    'name'               => 'Pikoworks Core', // The plugin name.
                    'slug'               => 'pikoworks_core', // The plugin slug (typically the folder name).
                    'source'             => 'http://themepiko.com/resources/plugins/piko-construct/pikoworks_core.zip', // The plugin source.
                    'required'           => true, // If false, the plugin is only 'recommended' instead of required.
                    'version'            => '1.0', // E.g. 1.0.0. If set, the active plugin must be this version or higher. If the plugin version is higher than the plugin version installed, the user will be notified to update the plugin.			'force_activation'   => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
                    'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
                    'external_url'       => '', // If set, overrides default API URL and points to an external URL.
                    'is_callable'        => '', // If set, this callable will be be checked for availability to determine if a plugin is active.
		),
                // This is an example of how to include a plugin from an arbitrary external source in your theme.
		array(
                    'name'         => 'WPBakery Page Builder',
                    'slug'         => 'js_composer',
                    'source'       => 'http://themepiko.com/resources/plugins/js_composer.zip',
                    'required'     => true,
                    'version'      => '6.0.5', 
                    'external_url' => '',
		),
                // This is an example of how to include a plugin from the WordPress Plugin Repository.
		array(
                    'name'      => 'Contact Form 7',
                    'slug'      => 'contact-form-7',
                    'required'  => false,
		),                
                // This is an example of how to include a plugin from an arbitrary external source in your theme.
		array(
			'name'         => 'Slider Revolution', // The plugin name.
			'slug'         => 'revslider', // The plugin slug (typically the folder name).
			'source'       => 'http://themepiko.com/resources/plugins/revslider.zip', // The plugin source.
			'required'     => false, // If false, the plugin is only 'recommended' instead of required.
                        'version'      => '6.1', // E.g. 1.0.0. If set, the active plugin must be this version or higher. If the plugin version is higher than the plugin version installed, the user will be notified to update the plugin.
			'external_url' => '', // If set, overrides default API URL and points to an external URL.
		), 
                // This is an example of how to include a plugin from the WordPress Plugin Repository.
		array(
                    'name'      => 'WooCommerce',
                    'slug'      => 'woocommerce',
                    'required'  => false,
                    'version'   => ''
		),
                array(
                    'name'      => 'Demo Import',
                    'slug'      => 'one-click-demo-import',
                    'required'  => false,
		),
                $Ultimate_VC_Addons,
                $wishlist_plugin,
                $compare_plugin,
                $quickview_plugin,
	);	
	$config = array(
		'id'           => 'tgmpa',                 // Unique ID for hashing notices for multiple instances of TGMPA.
		'default_path' => '',                      // Default absolute path to bundled plugins.
		'menu'         => 'tgmpa-install-plugins', // Menu slug.
		'parent_slug'  => 'themes.php',            // Parent menu slug.
		'capability'   => 'edit_theme_options',    // Capability needed to view plugin install page, should be a capability associated with the parent menu used.
		'has_notices'  => true,                    // Show admin notices or not.
		'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
		'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
		'is_automatic' => false,                   // Automatically activate plugins after installation or not.
		'message'      => '',                      // Message to output right before the plugins table.
		
	);

	tgmpa( $plugins, $config );
}
