<?php
    /**
     * ReduxFramework Sample Config File
     * For full documentation, please visit: http://docs.reduxframework.com/
     */

    if ( ! class_exists( 'Redux' ) ) {
        return;
    }


    // This is your option name where all the Redux data is stored.
    $opt_name = "pikoworks";
    $theme = wp_get_theme();

    $args = array(
        'opt_name'             => $opt_name,
        'display_name'         => $theme->get( 'Name' ),
        'display_version'      => $theme->get( 'Version' ),
        'menu_type'            => 'menu',
        'allow_sub_menu'       => true,
        'menu_title'           => esc_html__( 'Theme Options', 'piko-construct' ),
        'page_title'           => esc_html__( 'Theme Options', 'piko-construct' ),
        'google_api_key'       => '',
        'google_update_weekly' => false,
        'async_typography'     => true,
        //'disable_google_fonts_link' => true,
        'admin_bar'            => false,
        'global_variable'      => '',
        'dev_mode'             => false,
        'update_notice'        => false,
        'customizer'           => true,
        'page_priority'        => 58,
        'page_parent'          => 'themes.php',
        'page_permissions'     => 'manage_options',
        'menu_icon'            => 'dashicons-admin-generic',
        'last_tab'             => '',
        'page_icon'            => 'icon-themes',
        'page_slug'            => 'theme_options',
        'save_defaults'        => true,
        'default_show'         => false,
        'default_mark'         => '',
        'show_import_export'   => true,
        'transient_time'       => 60 * MINUTE_IN_SECONDS,
        'output'               => true,
        'output_tag'           => true,
        'database'             => '',
        'use_cdn'              => true,
        // HINTS
        'hints'                => array(
            'icon'          => 'el el-question-sign',
            'icon_position' => 'right',
            'icon_color'    => 'lightgray',
            'icon_size'     => 'normal',
            'tip_style'     => array(
                'color'   => 'red',
                'shadow'  => true,
                'rounded' => false,
                'style'   => '',
            ),
            'tip_position'  => array(
                'my' => 'top left',
                'at' => 'bottom right',
            ),
            'tip_effect'    => array(
                'show' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'mouseover',
                ),
                'hide' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'click mouseleave',
                ),
            ),
        ),
        'output'                => true,
	'output_tag'            => true,
	'compiler'              => true,
	'page_permissions'      => 'manage_options',
	'save_defaults'         => true,
	'database'              => 'options',
	'transient_time'        => '3600',
	'show_import_export'    => false,
	'network_sites'         => true 
    );
    
    Redux::setArgs( $opt_name, $args );
    //  START general option Fields
    Redux::setSection( $opt_name, array(
        'title'            => esc_html__( 'General', 'piko-construct' ),
        'id'               => 'basic',        
        'customizer_width' => '400px',
        'icon'             => 'el el-home',
        'fields'           => array(
            array(
                    'id'      => 'main-width-content',
                    'type'    => 'button_set',
                    'title'   => esc_html__( 'Main body Content width', 'piko-construct' ),                    
                    'options' => array(
                        'container' => 'Container', 
                        'container-fluid' => 'Container Fluid',
                        ), 
                    'default' => 'container'  
            ),
            array(
                'id'       => 'boxed_site',
                'type'     => 'switch',
                'title'    => esc_html__('Boxed Version', 'piko-construct'),
                'default' => '0',
                'on' => esc_html__( 'Enable', 'piko-construct' ),
                'off' => esc_html__( 'Disabled', 'piko-construct' ),
            ),
            array(
                    'id'      => 'main_width_floatled',
                    'type'    => 'button_set',
                    'title'   => esc_html__( 'Boxed Version', 'piko-construct' ),
                    'subtitle'      => esc_html__( 'Default full width', 'piko-construct' ),
                    'desc'      => esc_html__( 'All boxes vresion choose footer container fluid', 'piko-construct' ),
                    'options' => array(
                        'theme_default' => esc_html__('Default', 'piko-construct'),
                        'theme_float' => esc_html__('Float Full', 'piko-construct'),
                        'theme_float_boxed' => esc_html__('Float Boxed', 'piko-construct'),
                        'theme_boxed' => esc_html__('Boxed', 'piko-construct'),
                        ), 
                    'default' => 'theme_default',
                    'required' => array( 'boxed_site', '=', 1),

            ),
            array(
                'id'       => 'boxed_background_image_type',
                'type'     => 'button_set',
                'title'    => esc_html__( 'Background Image Type', 'piko-construct' ),
                'options'  => array(
                        'boxed_bg_image_default'     => esc_html__( 'Default', 'piko-construct' ),
                        'boxed_bg_image_pattern' => esc_html__( 'Pattern', 'piko-construct' ),
                        'boxed_bg_image_custom'        => esc_html__( 'Custom', 'piko-construct' )
                ),
                'default'  => 'boxed_bg_image_default',
                'required' => array( 'main_width_floatled', '=', array('theme_float', 'theme_float_boxed', 'theme_boxed' )),
            ),
            array(
                'id'       => 'boxed_background_image',
                'type'     => 'image_select',
                'title'    => esc_html__('Background Image', 'piko-construct'),
                'tiles'    => true,
                'options'  => array(
                        get_template_directory_uri() . '/assets/images/bg/bg-body1.jpg'      => array(
                                'img'   => get_template_directory_uri() . '/assets/images/bg/bg-body1.jpg'
                        ),
                        get_template_directory_uri() . '/assets/images/bg/bg-body2.jpg'      => array(
                                'img'   => get_template_directory_uri() . '/assets/images/bg/bg-body2.jpg'
                        ),                        
                ),
                'default'  => get_template_directory_uri() . '/assets/images/bg/bg-body1.jpg',
                'required' => array(
                        array( 'boxed_background_image_type', '=', 'boxed_bg_image_default' ),
                        array( 'boxed_site', '=', 1 ),
                ),
                'output'   => array(
                        'background-image' => 'body'
                )
            ),
            array(
                'id'       => 'boxed_background_pattern',
                'type'     => 'image_select',
                'title'    => esc_html__('Background Pattern', 'piko-construct'),
                'tiles'    => true,
                'options'  => array(
                        get_template_directory_uri() . '/assets/images/bg/pattern1.png'      => array('img'   => get_template_directory_uri() . '/assets/images/bg/pattern1.png'),
                        get_template_directory_uri() . '/assets/images/bg/pattern2.png'      => array('img'   => get_template_directory_uri() . '/assets/images/bg/pattern2.png'),
                        get_template_directory_uri() . '/assets/images/bg/pattern3.png'      => array('img'   => get_template_directory_uri() . '/assets/images/bg/pattern3.png'),
                        get_template_directory_uri() . '/assets/images/bg/pattern4.png'      => array('img'   => get_template_directory_uri() . '/assets/images/bg/pattern4.png'),
                        get_template_directory_uri() . '/assets/images/bg/pattern5.png'      => array('img'   => get_template_directory_uri() . '/assets/images/bg/pattern5.png'),
                        get_template_directory_uri() . '/assets/images/bg/pattern6.png'      => array('img'   => get_template_directory_uri() . '/assets/images/bg/pattern6.png'),
                        get_template_directory_uri() . '/assets/images/bg/pattern7.png'      => array('img'   => get_template_directory_uri() . '/assets/images/bg/pattern7.png'),
                        get_template_directory_uri() . '/assets/images/bg/pattern8.png'      => array('img'   => get_template_directory_uri() . '/assets/images/bg/pattern8.png'),
                        get_template_directory_uri() . '/assets/images/bg/pattern9.png'      => array('img'   => get_template_directory_uri() . '/assets/images/bg/pattern9.png'),
                        get_template_directory_uri() . '/assets/images/bg/pattern10.png'      => array('img'   => get_template_directory_uri() . '/assets/images/bg/pattern10.png'),
                        get_template_directory_uri() . '/assets/images/bg/pattern11.png'      => array('img'   => get_template_directory_uri() . '/assets/images/bg/pattern11.png'),
                        get_template_directory_uri() . '/assets/images/bg/pattern12.png'      => array('img'   => get_template_directory_uri() . '/assets/images/bg/pattern12.png'),                    
                ),
                'default'  => get_template_directory_uri() . '/assets/images/bg/pattern12.png',
                'required' => array(
                        array( 'boxed_background_image_type', '=', 'boxed_bg_image_pattern', ),
                        array( 'boxed_site', '=', 1 ),
                ),
                'output'   => array(
                        'background-image' => 'body'
                )
            ),
            array(
                'id'       => 'boxed_background_custom_image',
                'type'     => 'background',
                'title'    => esc_html__('Custom Background', 'piko-construct'),
                'required' => array(
                        array( 'boxed_background_image_type', '=', 'boxed_bg_image_custom', ),
                        array( 'boxed_site', '=', 1 ),
                ),
                'output'   => array(
                        'background-image' => 'body'
                )
            ),
            array(
                'id'      => 'optn_enable_loader',
                'type'    => 'switch',
                'title'   => esc_html__( 'Enable Page Loader', 'piko-construct' ),
                'default' => '0',
                'on' => esc_html__( 'Enable', 'piko-construct' ),
                'off' => esc_html__( 'Disabled', 'piko-construct' ),
            ),
            array(
                'id'       => 'home_preloader',
                'type'     => 'select',
                'title'    => esc_html__( 'Preloader style', 'piko-construct' ),                
                'options' => array(
                    'square-1'	=> 'Square 01',
                    'square-2'	=> 'Square 02',
                    'square-5'	=> 'Square 03',
                    'square-7'	=> 'Square 04',
                    'square-8'	=> 'Square 05',
                    'round-1'	=> 'Round 01',
                    'round-5'	=> 'Round 02',
                    'round-7'	=> 'Round 03',
                    'various-4'	=> 'Various 01',
                    'various-7'	=> 'Various 02',
                    'various-8'	=> 'Various 03',
                    'various-9'	=> 'Various 04',
                    'various-10' => 'Various 05',
                ),                
                'default'  => 'square-1',
                'required' => array( 'optn_enable_loader', '=', true, ),
            ),
            array(
                'id'       => 'home_preloader_bg_color',
                'type'     => 'color_rgba',
                'title'    => esc_html__( 'Preloader background color', 'piko-construct' ),
                'subtitle' => esc_html__( 'Set Preloader background color.', 'piko-construct' ),
                'default'  => array(),
                'required' => array( 'optn_enable_loader', '=', true, ),
            ),

            array(
                'id'       => 'home_preloader_spinner_color',
                'type'     => 'color',
                'title'    => esc_html__('Preloader spinner color', 'piko-construct'),
                'subtitle' => esc_html__('Pick a preloader spinner color for the Top Bar', 'piko-construct'),
                'default'  => '',
                'validate' => 'color',
                'required' => array( 'optn_enable_loader', '=', true, ),
            ),         
        )
        
    ) );
    // coming soon    
    Redux::setSection( $opt_name, array(
        'title'   => esc_html__( 'Maintanence Mode', 'piko-construct' ),
        'subsection' => true,
        'icon'    => 'el el-icon-time',        
        'fields' => array(
            array(
                'id'            => 'enable_coming_soon_mode',
                'type'          => 'switch',
                'title'         => esc_html__( 'Coming soon mode', 'piko-construct' ),
                'subtitle'      => esc_html__( 'Turn coming soon mode on/off', 'piko-construct' ),
                'desc'          => esc_html__( 'If turn on, All member login then see the site', 'piko-construct' ),
                'default'       => 0,
                'on'            => esc_html__( 'On', 'piko-construct' ),
                'off'           => esc_html__( 'Off', 'piko-construct' ),
            ),
            array(
                'id'            => 'coming_soon_date',
                'type'          => 'date',
                'title'         => esc_html__('Coming soon date', 'piko-construct'),
                'subtitle'      => esc_html__( 'Date is important current date to next date', 'piko-construct' ),
                'required'      => array( 'enable_coming_soon_mode', '=', 1 ),
            ),
            array(
                'id' => 'coming_soon_logo',
                'type' => 'media',
                'url' => true,
                'title' => esc_html__('Coming Soon logo uploade', 'piko-construct'),
                'compiler' => 'true',                
                'desc' => esc_html__('Upload image for coming soon page', 'piko-construct'),
                'default'  => array(
                   'url' => get_template_directory_uri(). '/assets/images/logo/logo@2x.png'
                ),
                'required'      => array( 'enable_coming_soon_mode', '=', 1 ),
            ),
            array(
                'id' => 'coming_soon_site_title',
                'type' => 'text',
                'title' => esc_html__('Coming Soon Site Title', 'piko-construct'),
                'default' => esc_html__( 'Launching Soon', 'piko-construct' ),
                'required' => array( 'enable_coming_soon_mode', '=', 1 ),
                'validate' => 'no_html'
            ), 
            array(
                'id'            => 'coming_soon_text',
                'type'          => 'editor',
                'title'         => esc_html__('Coming soon text', 'piko-construct'),
                'default'       => wp_kses( __('Our website is under construction. Stay tuned for something amazing!!! if needs urgent to communicate with us Drop a line. <a href="help@example.com">help@example.com</a>', 'piko-construct'), array( 'br', 'a' => array( 'href' => array() ), 'b' ) ),
                'required'      => array( 'enable_coming_soon_mode', '=', 1 ),
            ),
            array(
                'id'            => 'enable_coming_soon_newsletter',
                'type'          => 'switch',
                'title'         => esc_html__( 'Coming soon news letter', 'piko-construct' ),
                'desc'          => esc_html__( 'If turn on, news latter form will show on coming soon page', 'piko-construct' ),
                'default'       => 1,
                'on'            => esc_html__( 'On', 'piko-construct' ),
                'off'           => esc_html__( 'Off', 'piko-construct' ),
                'required'      => array( 'enable_coming_soon_mode', '=', 1 ),
            ),
            array(
                'id'            => 'disable_coming_soon_when_date_small',
                'type'          => 'switch',
                'title'         => esc_html__( 'Coming soon disable when count down date expired', 'piko-construct' ),
                'default'       => 1,
                'on'            => esc_html__( 'Disable', 'piko-construct' ),
                'off'           => esc_html__( 'Don\'t disable', 'piko-construct' ),
                'required'      => array( 'enable_coming_soon_mode', '=', 1 ),
            ),
            array(
                'id'            => 'enable_coming_soon_bg_img',
                'type'          => 'switch',
                'title'         => esc_html__( 'Background Image/slide/pattern', 'piko-construct' ),
                'subtitle'      => esc_html__( 'Turn background image slide on/off', 'piko-construct' ),
                'required'      => array( 'enable_coming_soon_mode', '=', 1 ),
                'default'       => 0,
                'on'            => esc_html__( 'On', 'piko-construct' ),
                'off'           => esc_html__( 'Off', 'piko-construct' ),
            ),
            array(
                'id'        => 'coming_soon_bg_img',
                'type'      => 'gallery',
                'title'    => esc_html__( 'Upload backgournd image', 'piko-construct' ),
                'subtitle' => esc_html__( 'Recomended size 1920x1050px', 'piko-construct' ),
                'desc'     => esc_html__( 'Gallery Image sliding the background nb: if need front color change. just go to Typography and change color', 'piko-construct' ),
                'required' => array( 'enable_coming_soon_bg_img', '=', 1 ),
                'compiler' => true,
            ),
            array(
                    'id'       => 'coming_soon_bg_overlay',
                    'type'     => 'image_select',
                    'title'    => esc_html__('Overlay Pattern', 'piko-construct'),
                    'desc'     => esc_html__( 'if want to use only pattern dont upload gallery image', 'piko-construct'),
                    'tiles'    => true,
                    'options'  => array(                        
                        get_template_directory_uri() . '/assets/images/bg/pattern1.png'      => array('img'   => get_template_directory_uri() . '/assets/images/bg/pattern1.png'),
                        get_template_directory_uri() . '/assets/images/bg/pattern2.png'      => array('img'   => get_template_directory_uri() . '/assets/images/bg/pattern2.png'),
                        get_template_directory_uri() . '/assets/images/bg/pattern3.png'      => array('img'   => get_template_directory_uri() . '/assets/images/bg/pattern3.png'),
                        get_template_directory_uri() . '/assets/images/bg/pattern4.png'      => array('img'   => get_template_directory_uri() . '/assets/images/bg/pattern4.png'),
                        get_template_directory_uri() . '/assets/images/bg/pattern5.png'      => array('img'   => get_template_directory_uri() . '/assets/images/bg/pattern5.png'),
                        get_template_directory_uri() . '/assets/images/bg/pattern6.png'      => array('img'   => get_template_directory_uri() . '/assets/images/bg/pattern6.png'),
                        get_template_directory_uri() . '/assets/images/bg/pattern7.png'      => array('img'   => get_template_directory_uri() . '/assets/images/bg/pattern7.png'),
                        get_template_directory_uri() . '/assets/images/bg/pattern8.png'      => array('img'   => get_template_directory_uri() . '/assets/images/bg/pattern8.png'),
                        get_template_directory_uri() . '/assets/images/bg/pattern9.png'      => array('img'   => get_template_directory_uri() . '/assets/images/bg/pattern9.png'),
                        get_template_directory_uri() . '/assets/images/bg/pattern10.png'      => array('img'   => get_template_directory_uri() . '/assets/images/bg/pattern10.png'),
                        get_template_directory_uri() . '/assets/images/bg/pattern11.png'      => array('img'   => get_template_directory_uri() . '/assets/images/bg/pattern11.png'),
                        get_template_directory_uri() . '/assets/images/bg/pattern12.png'      => array('img'   => get_template_directory_uri() . '/assets/images/bg/pattern12.png'),                    
                ),
                'default'  => get_template_directory_uri() . '/assets/images/bg/pattern12.png',
                    'required' => array( 'enable_coming_soon_bg_img', '=', 1 ),
                    'output'   => array(
                        'background-image' => '.vegas-overlay'
                    )
                    
                ),
            array(
                    'id'      => 'coming_soon_bg_img_transit',
                    'type'    => 'select',
                    'title'   => esc_html__( 'Available Transition', 'piko-construct' ),
                    'required' => array( 'enable_coming_soon_bg_img', '=', 1 ),
                    'default' => 'fade',
                    'options' => array(
                            'fade' => esc_html__( 'fade', 'piko-construct' ),
                            'zoomIn' => esc_html__( 'zoomIn', 'piko-construct' ), 
                            'zoomOut' => esc_html__( 'zoomOut', 'piko-construct' ), 
                            'swirlLeft' => esc_html__( 'swirlLeft', 'piko-construct' ),                        
                            'swirlRight' => esc_html__( 'swirlRight', 'piko-construct' ),                        
                            'blur' => esc_html__( 'blur', 'piko-construct' ),                       
                    ),                   
            ),            
            array(
                'id'      => 'coming_soon_bg_img_transit_duration',
                'type'    => 'text',
                'title'   => esc_html__( ' Transition duration in milliseconds', 'piko-construct' ),
                'subtitle' => esc_html__( 'Note:1sec = 1000ms ', 'piko-construct' ),
                'required' => array( 'enable_coming_soon_bg_img', '=', 1 ),
                'default' => '5000',                                      
            ),            
            array(
                    'id'      => 'coming_soon_slide_timer',
                    'type'    => 'button_set',
                    'title'   => esc_html__( ' Slide Timer On/off', 'piko-construct' ),
                    'required' => array( 'enable_coming_soon_bg_img', '=', 1 ),
                    'default' => 'true', 
                    'options' => array(
                        'true' => esc_html__( 'On', 'piko-construct' ),
                        'false' => esc_html__( 'Off', 'piko-construct' ),                                               
                    ),  
            ), 
            
            array(
                'id'            => 'enable_coming_soon_social',
                'type'          => 'switch',
                'title'         => esc_html__( 'Social Icon', 'piko-construct' ),
                'subtitle'      => esc_html__( 'Social icon on/off', 'piko-construct' ),
                'required'      => array( 'enable_coming_soon_mode', '=', 1 ),
                'default'       => 0,
                'on'            => esc_html__( 'On', 'piko-construct' ),
                'off'           => esc_html__( 'Off', 'piko-construct' ),
            ),
            array(
                'id'       => 'coming_soon_social',
                'type'     => 'checkbox',
                'title'    => esc_html__( 'Footer Social Media Icons to display', 'piko-construct' ),
                'subtitle' => esc_html__( 'The Social urls taken from Social Media settings tab please enter first the social Urls.', 'piko-construct' ),
                'required'      => array( 'enable_coming_soon_social', '=', 1 ),
                'default' => array(
                    'facebook' => '1', 
                    'twitter' => '1',
                    'instagram' => '1',
                    'flickr' => '1'
                ),
                'options'  => array(
                        'facebook'   => 'Facebook',
                        'twitter'    => 'Twitter',
                        'flickr'     => 'Flickr',
                        'instagram'  => 'Instagram',
                        'behance'    => 'Behance',
                        'dribbble'   => 'Dribbble',                        
                        'git'        => 'Git',
                        'linkedin'   => 'Linkedin',
                        'pinterest'  => 'Pinterest',
                        'yahoo'      => 'Yahoo',
                        'delicious'  => 'Delicious',
                        'dropbox'    => 'Dropbox',
                        'reddit'     => 'Reddit',
                        'soundcloud' => 'Soundcloud',
                        'google'     => 'Google',
                        'google-plus' => 'Google Plus',
                        'skype'      => 'Skype',
                        'youtube'    => 'Youtube',
                        'vimeo'      => 'Vimeo',
                        'tumblr'     => 'Tumblr',
                        'whatsapp'   => 'Whatsapp',
                ),
            ),
            array (
                    'title' => esc_html__('Coming Page Primary Color', 'piko-construct'),
                    'subtitle' => esc_html__('Default use: #45bf55', 'piko-construct'),
                    'id' => 'comting_soon_primary_color',
                    'type' => 'color',
                    'default' => '',
                    'transparent' => false,
                    'required'      => array( 'enable_coming_soon_social', '=', 1 ),
                    'output'   => array(
                        'background-color'    => '.maintenance button:hover,.maintenance button:focus, .maintenance .social-page a:hover, .maintenance .social-page a:focus',
                        'color'    => '.coming-soon .maintenance a:hover, .coming-soon .maintenance .countdown-amount',
                        'border-color'    => '.maintenance .social-page a:hover, .maintenance .social-page a:focus',
                    )
            ),
            array (
                    'title' => esc_html__('Coming Page Title Color', 'piko-construct'),
                    'subtitle' => esc_html__('Default use: #555555', 'piko-construct'),
                    'id' => 'comting_soon_title_color',
                    'type' => 'color',
                    'default' => '',
                    'transparent' => false,
                    'required'      => array( 'enable_coming_soon_social', '=', 1 ),
                    'output'   => array(
                        'background-color'    => '.maintenance button',
                        'color'    => '.coming-soon .maintenance h1',
                    )
            ),
            array (
                    'title' => esc_html__('Coming body text Color', 'piko-construct'),
                    'subtitle' => esc_html__('Default use: #777777', 'piko-construct'),
                    'id' => 'comting_soon_body_color',
                    'type' => 'color',
                    'default' => '',
                    'transparent' => false,
                    'required'      => array( 'enable_coming_soon_social', '=', 1 ),
                    'output'   => array(
                        'color'    => '.coming-soon .maintenance p, .coming-soon .maintenance a, .coming-soon .maintenance .countdown-period',
                    )
            ),
            array(
                'id' => 'coming_page_layout',                
                'type'    => 'switch',
                'title'   => esc_html__( 'coming Page Content Align Center', 'piko-construct' ),
                'default' => '0',
                'on' => esc_html__( 'Yes', 'piko-construct' ),
                'off' => esc_html__( 'No', 'piko-construct' ),
                'required'      => array( 'enable_coming_soon_social', '=', 1 ),
            ),
            array(
                'id'             => 'coming_page_padding_top',
                'type'           => 'spacing',
                'mode'           => 'padding',
                'units'          => '%',
                'units_extended' => 'false',
                'title'          => esc_html__('Coming Page padding Top', 'piko-construct'),
                'subtitle'       => esc_html__('This must be numeric (no %). Leave for default use 5.', 'piko-construct'),
                'left'          => false,
                'right'          => false,
                'bottom'          => false,
                'output'        => array('.coming-soon .maintenance, .coming-soon .maintenance.enable-news'),
                'default'            => array(
                    'padding-top'     => '',
                    'units'          => '%',
                ),
                'required'      => array( 'enable_coming_soon_social', '=', 1 ),
            ),
        ),
    )); 
    Redux::setSection( $opt_name, array(
        'title'  => esc_html__( 'Performance', 'piko-construct' ),
        'icon'   => 'el el-dashboard',
        'subsection' => true,
        'fields' => array(
            array(
                'id' => 'enable_minifile',                
                'type'    => 'switch',
                'title'   => esc_html__( 'Enable Mini File CSS, JS', 'piko-construct' ),
                'default' => '1',
                'on' => esc_html__( 'Enable', 'piko-construct' ),
                'off' => esc_html__( 'Disabled', 'piko-construct' ),
            ),
        )
    ));
    //404 style    
    Redux::setSection( $opt_name, array(
        'title'   => esc_html__( 'Page 404', 'piko-construct' ),
        'icon'    => 'el el-remove-sign',
        'subsection' => true,
        'fields'  => array(                                           
                array(
                    'id'      => 'optn_404_style1',
                    'type'    => 'text',
                    'title'   => esc_html__( 'Text Style', 'piko-construct' ),
                    'default'  => sprintf( wp_kses( __( '4<span>0</span>4', 'piko-construct' ), array( 'span' => array( 'class' => array(),  ) ) ) ),
                ),
                array(
                    'id' => 'optn_404_img',
                    'type' => 'media',
                    'url' => true,
                    'title' => esc_html__('Uploaded 404 image', 'piko-construct'),
                    'compiler' => 'true',
                    'desc'     => esc_html__( 'Select an image or insert an image url to use for the 404page. the image use 276px*400px', 'piko-construct' ),                        
                    'default'  => array(
                                'url' => get_template_directory_uri(). '/assets/images/404.png'
                    ),                        
                ),
                array (
                    'title' => esc_html__('404 Background Color', 'piko-construct'),
                    'subtitle' => esc_html__('Default background color use #e5ffb9', 'piko-construct'),
                    'id' => 'e404_background_color',
                    'type' => 'color',
                    'default' => '',
                    'transparent' => true,
                    'output'    => array('background-color' => '.error404 .site-inner'),                   
                ),
                array(
                    'id'      => 'optn_404_heading',
                    'type'    => 'text',
                    'title'   => esc_html__( 'Heading text', 'piko-construct' ),                    
                    'default' => 'That page does not exist.',
                                      
                ),
                array(
                    'id'       => 'optn_404_content',
                    'type'     => 'editor',
                    'title'    => esc_html__( 'Content body Text', 'piko-construct' ),
                    'args'   => array(
                    'teeny'  => true,
                    ),
                    'default'  => 'The link you clicked might be corrupted or the page may have been removed.. Maybe try a search?',

                ),
                array(
                    'id'      => 'optn_404_btn',
                    'type'    => 'text',
                    'title'   => esc_html__( 'Button text', 'piko-construct' ),                    
                    'default' => 'go to home',
                                      
                ),                           
                array(
                    'id'      => 'optn_404_breadcrumb',
                    'type'    => 'text',
                    'title'   => esc_html__( 'Top bar title text', 'piko-construct' ),
                    'desc' => esc_html__( 'Effect this text breadcrumb', 'piko-construct' ),                    
                    'default' => 'Oops 404 !',
                                      
                ),                           
            )
        ));     
    //  header option
    Redux::setSection( $opt_name, array(
        'title'            => esc_html__( 'Header', 'piko-construct' ),        
        'desc'             => esc_html__( 'These are really basic fields!', 'piko-construct' ),
        'customizer_width' => '400px',
        'icon'             => 'dashicons dashicons-archive'
    ) );
    
    //header logo, menu
    Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Logo upload', 'piko-construct' ),
        'subsection' => true,
        'fields'     => array(
            array(
                    'id'      => 'enable_text_logo',
                    'type'    => 'switch',
                    'title'   => esc_html__( 'Enable text logo', 'piko-construct' ),
                    'default' => '0',
                    'on' => esc_html__( 'Enable', 'piko-construct' ),
                    'off' => esc_html__( 'Disabled', 'piko-construct' )
            ),
             array(
                'id'       => 'brand_logo_name',
                'type'     => 'text',
                'title'    => esc_html__( 'Name your site', 'piko-construct' ),
                'default'  => esc_html__('CONSTRUCT','piko-construct'),
                'required' => array( 'enable_text_logo', '=', 1 ),
            ),
            array(
                'id'             => 'brand_name_font_size',
                'type'           => 'typography',
                'title'          => esc_html__( 'Brand Name', 'piko-construct' ),
                'subtitle' => esc_html__( 'Font size and color', 'piko-construct' ),
                'compiler'       => true,
                'google'         => false,
                'font-backup'    => false,
                'all_styles'     => true,
                'font-weight'    => true,
                'font-family'    => false,
                'text-align'     => false,
                'font-style'     => false,
                'subsets'        => false,
                'font-size'      => true,
                'line-height'    => false,
                'word-spacing'   => false,
                'letter-spacing' => false,
                'color'          => true,
                'preview'        => true,
                'output'         => array( '.site-header .site-logo a,.site-header .site-logo a:hover,.site-header .site-logo a:focus' ),
                'units'          => 'px',
                'default'        => array(
                        'font-size' => '',
                        'color' => '',
                ),
                'required' => array( 'enable_text_logo', '=', 1 ),
            ),
            array(
                'id'             => 'brand_name_sticky_font_size',
                'type'           => 'typography',
                'title'          => esc_html__( 'Active stikcy Brand', 'piko-construct' ),
                'subtitle' => esc_html__( 'Font size and color', 'piko-construct' ),
                'compiler'       => true,
                'google'         => false,
                'font-backup'    => false,
                'all_styles'     => true,
                'font-weight'    => true,
                'font-family'    => false,
                'text-align'     => false,
                'font-style'     => false,
                'subsets'        => false,
                'font-size'      => true,
                'line-height'    => false,
                'word-spacing'   => false,
                'letter-spacing' => false,
                'color'          => true,
                'preview'        => true,
                'output'         => array( '.site-header .sticky-logo a,.site-header .sticky-logo a:hover,.site-header .sticky-logo a:focus' ),
                'units'          => 'px',
                'default'        => array(
                        'font-size' => '',
                        'color' => '',
                ),
                'required' => array( 'enable_text_logo', '=', 1 ),
            ),            
            array(
                'id'       => 'logo_upload',
                'type'     => 'media',
                'url'      => true,
                'title'    => esc_html__( 'Logo upload', 'piko-construct' ),
                'subtitle' => esc_html__( 'Normal logo', 'piko-construct' ),
                'desc'     => esc_html__( 'The logo size default width: 180px, height: 40px.', 'piko-construct' ),
                'compiler' => true,
                'required' => array( 'enable_text_logo', '=', 0 ),
                'default'  => array(
                    'url' => get_template_directory_uri(). '/assets/images/logo/logo.png'
                )
            ),
            array(
                'id'       => 'logo_upload_mobile',
                'type'     => 'media',
                'url'      => true,
                'title'    => esc_html__( 'Logo upload mobile', 'piko-construct' ),
                'subtitle' => esc_html__( 'its also work inverse menu', 'piko-construct' ),
                'desc'     => esc_html__( 'The logo size default width: 180px, height: 40px.', 'piko-construct' ),
                'compiler' => true,
                'required' => array( 'enable_text_logo', '=', 0 ),
                'default'  => array(
                    'url' => get_template_directory_uri(). '/assets/images/logo/logo-inverse.png'
                )
            ),
            array(
                'id'             => 'brand_logo_padding',
                'type'           => 'spacing',
                'output'         => array('.header-layout-1 .site-header .site-logo,.header-layout-2 .site-header .header-main .header-left .site-logo,.header-layout-4 .site-header .site-logo,.header-layout-5 .site-header .site-logo,.header-layout-6 .site-header .site-logo'),
                'mode'           => 'padding',
                'left'           => false,
                'right'           => false,
                'units'          => array('px'),
                'units_extended' => 'false',
                'title'          => esc_html__('Brand Adjustment', 'piko-construct'),
                'subtitle'       => esc_html__('Padding top and padding bottom', 'piko-construct'),
                'default'            => array(
                    'padding-top'     => '',
                    'padding-bottom'  => '', 
                    'units'          => 'px', 
                )
            ),
            array(
                'id'             => 'brand_logo_active_padding',
                'type'           => 'spacing',
                'output'         => array('.header-layout-1 .site-header.active-sticky .sticky-logo,.header-layout-2 .site-header.active-sticky .sticky-logo,.header-layout-4 .site-header.active-sticky .sticky-logo,.header-layout-6 .site-header.active-sticky .site-logo'),
                'mode'           => 'padding',
                'left'           => false,
                'right'           => false,
                'units'          => array('px'),
                'units_extended' => 'false',
                'title'          => esc_html__('Brand Active sticky Adjustment', 'piko-construct'),
                'subtitle'       => esc_html__('Padding top and padding bottom', 'piko-construct'),
                'default'            => array(
                    'padding-top'     => '',
                    'padding-bottom'  => '', 
                    'units'          => 'px', 
                )
            ),
            array(
                'id'             => 'brand_logo_mobile_padding',
                'type'           => 'spacing',
                'output'         => array('.site-header .sticky-logo'),
                'mode'           => 'padding',
                'left'           => false,
                'right'           => false,
                'units'          => array('px'),
                'units_extended' => 'false',
                'title'          => esc_html__('Brand Mobile Adjustment', 'piko-construct'),
                'subtitle'       => esc_html__('Padding top and padding bottom', 'piko-construct'),
                'default'            => array(
                    'padding-top'     => '',
                    'padding-bottom'  => '', 
                    'units'          => 'px', 
                )
            ),
        )
    ) );
     Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Custom Favicon', 'piko-construct' ),
        'subsection' => true,
        'fields'     => array(            
            array(
                'id' => 'optn_favicon',
                'type' => 'media',
                'url'=> true,
                'title' => esc_html__('Custom favicon', 'piko-construct'),
                'subtitle' => esc_html__('Upload a 16px x 16px Png/Gif/ico image that will represent your website favicon', 'piko-construct'),
                'compiler' => true,
                'default'  => array(
                    'url' => get_template_directory_uri(). '/assets/images/logo/favicon.ico'
                )
            ),
            array(
                'id' => 'custom_ios_title',
                'type' => 'text',
                'title' => esc_html__('Custom iOS Bookmark Title', 'piko-construct'),
                'subtitle' => esc_html__('Enter a custom title for your site for when it is added as an iOS bookmark.', 'piko-construct'),
                'default' => ''
            ),
            array(
                'id' => 'custom_ios_icon57',
                'type' => 'media',
                'url'=> true,
                'title' => esc_html__('Custom iOS 57x57', 'piko-construct'),
                'subtitle' => esc_html__('Upload a 57px x 57px Png image that will be your website bookmark on non-retina iOS devices.', 'piko-construct'),
            ),
            array(
                'id' => 'custom_ios_icon72',
                'type' => 'media',
                'url'=> true,
                'title' => esc_html__('Custom iOS 72x72', 'piko-construct'),
                'subtitle' => esc_html__('Upload a 72px x 72px Png image that will be your website bookmark on non-retina iOS devices.', 'piko-construct'),
            ),
            array(
                'id' => 'custom_ios_icon114',
                'type' => 'media',
                'url'=> true,
                'title' => esc_html__('Custom iOS 114x114', 'piko-construct'),
                'subtitle' => esc_html__('Upload a 114px x 114px Png image that will be your website bookmark on retina iOS devices.', 'piko-construct'),
            ),
            array(
                'id' => 'custom_ios_icon144',
                'type' => 'media',
                'url'=> true,
                'title' => esc_html__('Custom iOS 144x144', 'piko-construct'),
                'subtitle' => esc_html__('Upload a 144px x 144px Png image that will be your website bookmark on retina iOS devices.', 'piko-construct'),                
            ),
           
        )
    ) ); 
    
    //header main menu 
    Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Menu Layout', 'piko-construct' ),
        'desc'       => esc_html__( 'Configure your Main Menu Bellow the Option Available', 'piko-construct' ),
        'subsection' => true,
        'fields'     => array(
            array(
                    'id'      => 'full_width_menu',
                    'type'    => 'button_set',
                    'title'   => esc_html__( 'Manu width', 'piko-construct' ),                    
                    'options' => array(
                        'container' => 'Container', 
                        'container-fluid' => 'Container Fluid',
                        ), 
                    'default' => 'container'  
            ),   
            array(
            'id'       => 'menu_style',
            'type'     => 'image_select',
            'title'    => esc_html__('Manu style', 'piko-construct'),
            'subtitle' => esc_html__('Select a header layout option from the examples.', 'piko-construct'),    
            'options' => array(
                '1' => array('alt'   => 'style 1', 'img'   => get_template_directory_uri() . '/assets/images/theme-options/header-1.jpg'),
                '2' => array('alt'   => 'style 2', 'img'   => get_template_directory_uri() . '/assets/images/theme-options/header-2.jpg'),
                '3' => array('alt'   => 'style 3', 'img'   => get_template_directory_uri() . '/assets/images/theme-options/header-3.jpg'),
                '4' => array('alt'   => 'style 4', 'img'   => get_template_directory_uri() . '/assets/images/theme-options/header-4.jpg'),
                '5' => array('alt'   => 'style 5', 'img'   => get_template_directory_uri() . '/assets/images/theme-options/header-5.jpg'),
                '6' => array('alt'   => 'style 6', 'img'   => get_template_directory_uri() . '/assets/images/theme-options/header-6.jpg'),
                ), 
                'default' => '1'
            ),
            array(
                'id'      => 'menu_before_slider_enable',
                'type'    => 'switch',
                'title'   => esc_html__( 'Slider before menu', 'piko-construct' ),
                'subtitle'  => esc_html__('Tab Transparency Should be disable', 'piko-construct'),                
                'default' => 0,
                'on' => esc_html__( 'Enable', 'piko-construct' ),
                'off' => esc_html__( 'Disabled', 'piko-construct' ),
                'required'  => array('menu_style', '=', 2)
            ),
            array(
                "type"        => "text",
                "title" => esc_html__('Put the slider shortcode', 'piko-construct'),
                'subtitle'  => esc_html__('Enter Slider Revolution shortcode', 'piko-construct'),
                'desc'  => esc_html__('Like as: [rev_slider alias="home1"]  Here any shortcode works', 'piko-construct'),                
                "id"  => "menu_before_slider",
                'required'  => array('menu_before_slider_enable', '=', 1)
            ),
            array(
                "type"        => "switch",
                "title" => esc_html__('Enable sticky menu', 'piko-construct'),
                'subtitle'  => esc_html__('Please read the text important', 'piko-construct'),
                'desc'  => esc_html__('!IMPORTANT: slide after menu sticky mode if enable sticky mode perfectly works. if need additional change: Tab Sticky Menu -> Sticky Header option enable then configure', 'piko-construct'),
                "id"  => "slide_after_menu_sticky",
                'default' => 0,
                'on' => esc_html__( 'Enable', 'piko-construct' ),
                'off' => esc_html__( 'Disabled', 'piko-construct' ),
                'required'  => array('menu_before_slider_enable', '=', 1)
            ),
            array(
                'id' => 'optn_verticle_menu_sidebar',
                'type' => 'select',
                'title' => esc_html__('Vertical menu bottom Sidebar', 'piko-construct'),
                'subtitle' => "Choose Your sidebar",
                'data'      => 'sidebars',
                'default' => '',
                'required' => array( 'menu_style', '=', array('3')), 
            ),
            array(
                'id'       => 'menu_custom_text',
                'type'     => 'textarea',
                'title'    => esc_html__( 'Custom Text', 'piko-construct' ),
                'subtitle' => esc_html__( 'NB: add Your custom text flowing html', 'piko-construct' ),
                'required' => array( 'menu_style', '=', array('5')),               
                'default'  => wp_kses( __( '<ul>
                                <li>
                                   <i class="fa fa-truck"></i>
                                   <span>Free Delivery</span>
                                   <p>on all orders</p>
                                </li>
                                <li>
                                   <i class="fa fa-clock-o"></i>
                                   <span class="text-custom">08:00 - 17:00</span>
                                   <p>monday - saturday</p>
                                </li>
                                <li>
                                   <i class="fa fa-phone"></i>
                                   <span class="text-custom">0203-980-14-79</span>
                                   <p>call us now</p>
                                </li>
                            </ul>', 'piko-construct' ), array( 'div' => array( 'class' => array(),  ), 'ul' => array( 'class' => array(),  ), 'li' => array(), 'i' => array( 'class' => array()), 'span' => array(), 'p' => array(),  'a' => array( 'href' => array(), 'target' => array()) ) ),
                
                
            ),
            array (
                    'title' => esc_html__('Logo Background Color', 'piko-construct'),
                    'subtitle' => esc_html__('Default color use: #45bf55.', 'piko-construct'),
                    'id' => 'header_logo_background_color',
                    'type' => 'color',
                    'default' => '',
                    'transparent' => true,
                    'output'    => array('background-color' => '.header-layout-6 .site-header .site-logo'),
                    'required' => array( 'menu_style', '=', array('6')),
            ),           
        )
    ) );
    
    //header top menu 
    Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Top Menu', 'piko-construct' ),
        'desc'       => esc_html__( 'Configure your Top menu', 'piko-construct' ),
        'subsection' => true,
        'fields'     => array(
             array(
                    'id'      => 'enable_top_bar',
                    'type'    => 'switch',
                    'title'   => esc_html__( 'Enable Top Bar', 'piko-construct' ),
                    'default' => '0',
                    'on' => esc_html__( 'Enable', 'piko-construct' ),
                    'off' => esc_html__( 'Disabled', 'piko-construct' ),
                    'required' => array( 'menu_style', '=', array('1', '2', '4', '5',  '6') ),
            ),                       
            array(
                'id'       => 'top_bar_left',
                'type'     => 'button_set',
                'title'    => esc_html__('Top Bar Left', 'piko-construct'),
                'options' => array(
                    'custom' => 'Custom', 
                    'social_icon' => 'Social Icon',
                    'both' => 'Combine'
                 ), 
                'default' => 'custom',
                'required' => array( 'enable_top_bar', '=', 1),
            ),            
            array (
                'title' => esc_html__('Top Bar Custom Text Left', 'piko-construct'),
                'subtitle' => esc_html__('Type in your Top Bar info here.', 'piko-construct'),
                'id' => 'top_bar_infotext',
                'type' => 'textarea',
                'required' => array( 'top_bar_left', '=', array('custom', 'both'), ),
                'default' => '<ul>
                        <li><i class="fa fa-phone"></i><span>(123) 4567 890</span></li>
                        <li><i class="fa fa-clock-o"></i><span>MON - SAT: 08 am - 17 pm</span></li>
                        <li><i class="fa fa-envelope-o"></i><a href="mailto:support@example.com">Support@example.com</a></li>
                </ul>'
		),            
            array(
		'id'       => 'top_bar_use_social',
		'type'     => 'checkbox',
		'title'    => esc_html__( 'Select Social Media Icons to display', 'piko-construct' ),
		'subtitle' => esc_html__( 'The Social urls taken from Social Media settings tab please enter first the social Urls.', 'piko-construct' ),		
                'required' => array( 'top_bar_left', '=', array('social_icon', 'both'), ),
                'default' => array(
                    'facebook' => '1', 
                    'twitter' => '1',
                    'instagram' => '1',
                    'flickr' => '1'
                ),
		'options'  => array(
			'facebook'   => 'Facebook',
                        'twitter'    => 'Twitter',
                        'flickr'     => 'Flickr',
                        'instagram'  => 'Instagram',
                        'behance'    => 'Behance',
                        'dribbble'   => 'Dribbble',                        
                        'git'        => 'Git',
                        'linkedin'   => 'Linkedin',
                        'pinterest'  => 'Pinterest',
                        'yahoo'      => 'Yahoo',
                        'delicious'  => 'Delicious',
                        'dropbox'    => 'Dropbox',
                        'reddit'     => 'Reddit',
                        'soundcloud' => 'Soundcloud',
                        'google'     => 'Google',
                        'google-plus' => 'Google Plus',
                        'skype'      => 'Skype',
                        'youtube'    => 'Youtube',
                        'vimeo'      => 'Vimeo',
                        'tumblr'     => 'Tumblr',
                        'whatsapp'   => 'Whatsapp',
		),
            ),
            array(
                    'id'      => 'top_bar_right_option',
                    'type'    => 'switch',
                    'title'   => esc_html__( 'Enable Custom text Right', 'piko-construct' ),
                    'subtitle' => esc_html__('Default show top menu', 'piko-construct'),
                    'default' => '0',
                    'on' => esc_html__( 'Enable', 'piko-construct' ),
                    'off' => esc_html__( 'Disabled', 'piko-construct' ), 
                    'required' => array( 'enable_top_bar', '=', 1),
            ),
            array(
                    'id'      => 'top_bar_right_wpml',
                    'type'    => 'switch',
                    'title'   => esc_html__( 'Top Bar Right WPML Language Switcher', 'piko-construct' ),
                    'default' => '0',
                    'on' => esc_html__( 'Enable', 'piko-construct' ),
                    'off' => esc_html__( 'Disabled', 'piko-construct' ), 
                    'required' => array( 'top_bar_right_option', '=', 0),
            ),
            array (
                'title' => esc_html__('Top Bar Custom Text Right', 'piko-construct'),
                'subtitle' => esc_html__('Type in your Top Bar Right info here.', 'piko-construct'),
                'id' => 'top_bar_infotext_right',
                'type' => 'textarea',
                'required' => array( 'top_bar_right_option', '=', 1),
                'default' => '<ul>
                        <li><a target="_blank" href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                        <li><a target="_blank" href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                        <li><a target="_blank" href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                </ul>'
            ), 
            array (
                    'id' => 'header_topbar_color_options',
                    'icon' => true,
                    'type' => 'info',
                    'raw' => '<h3>Header Topbar color configure</h3>',
                    'required' => array( 'enable_top_bar', '=', 1),
            ),
            array (
                    'title' => esc_html__('Topbar Background Color', 'piko-construct'),
                    'subtitle' => esc_html__('Default color use: #333.', 'piko-construct'),
                    'id' => 'header_topbar_background_color',
                    'type' => 'color',
                    'default' => '',
                    'transparent' => true,
                    'output'    => array('background-color' => '.header-top,.header-layout-5 .site-header .header-top'),
                    'required' => array( 'enable_top_bar', '=', 1),
            ),
            array (
                    'title' => esc_html__('Topbar Text-color', 'piko-construct'),
                    'subtitle' => esc_html__('Default color use: #fff.', 'piko-construct'),
                    'id' => 'header_topbar_text_color',
                    'type' => 'color',
                    'default' => '',
                    'transparent' => true,
                    'output'    => array('color' => '.header-top, .header-top a,.header-layout-5 .site-header .header-top, .header-layout-5 .site-header .header-top a'),
                    'required' => array( 'enable_top_bar', '=', 1),
            ),
            array (
                    'title' => esc_html__('Topbar Icon color', 'piko-construct'),
                    'subtitle' => esc_html__('Default color use: #45bf55.', 'piko-construct'),
                    'id' => 'header_topbar_icon_color',
                    'type' => 'color',
                    'default' => '',
                    'transparent' => true,
                    'output'    => array('color' => '.header-top .fa, .header-top i'),
                    'required' => array( 'enable_top_bar', '=', 1),
            ),
            array (
                    'title' => esc_html__('Topbar link hover color', 'piko-construct'),
                    'subtitle' => esc_html__('Default color use: #fff.', 'piko-construct'),
                    'id' => 'header_topbar_link_hover_color',
                    'type' => 'color',
                    'default' => '',
                    'transparent' => true,
                    'output'    => array('color' => '.header-layout-6.header-transparency .header-top .site-top-bar-text a:hover,.header-layout-5.header-transparency .header-top .site-top-bar-text a:hover,.header-layout-4.header-transparency .header-top .site-top-bar-text a:hover,.header-layout-2.header-transparency .header-top .site-top-bar-text a:hover,.header-layout-1.header-transparency .header-top .site-top-bar-text a:hover,.header-layout-6.header-transparency .header-top .top-bar-navigation .menu > li > a:hover,.header-layout-5.header-transparency .header-top .top-bar-navigation .menu > li > a:hover,.header-layout-4.header-transparency .header-top .top-bar-navigation .menu > li > a:hover,.header-layout-2.header-transparency .header-top .top-bar-navigation .menu > li > a:hover,.header-layout-1.header-transparency .header-top .top-bar-navigation .menu > li > a:hover,header-top li a:hover .fa, .header-top li a:hover i,.header-top a:hover,.header-layout-5 .site-header .header-top a:hover, .header-layout-5 .site-header .header-top a:focus'),
                    'required' => array( 'enable_top_bar', '=', 1),
            ),            
            array (
                    'title' => esc_html__('Topbar Dropdown Menu color', 'piko-construct'),
                    'subtitle' => esc_html__('Default color use: #333.', 'piko-construct'),
                    'id' => 'header_topbar_dropdown_color',
                    'type' => 'color',
                    'default' => '',
                    'transparent' => true,
                    'output'    => array('color' => '.header-top .sub-menu a'),
                    'required' => array( 'enable_top_bar', '=', 1),
            ),
            array (
                    'title' => esc_html__('Topbar Dropdown Menu Hover color', 'piko-construct'),
                    'subtitle' => esc_html__('Default color use: #45bf55.', 'piko-construct'),
                    'id' => 'header_topbar_dropdown_hover_color',
                    'type' => 'color',
                    'default' => '',
                    'transparent' => true,
                    'output'    => array('color' => '.header-top .sub-menu li:hover a'),
                    'required' => array( 'enable_top_bar', '=', 1),
            ),
        )
    ) );    
    //header main menu configure
    Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Main Menu', 'piko-construct' ),
        'desc'       => esc_html__( 'Configure your Main Menu Bellow the Option Available', 'piko-construct' ),
        'subsection' => true,
        'fields'     => array(                     
            array(
                'id' => "main_menu_animation",
                'type' => 'select',
                'title' => esc_html__('Menu Dropdown Effect', 'piko-construct'),
                'options' => array(
                    'effect-down' => esc_html__('Drop Down', 'piko-construct'),
                    'effect-fadein-up' => esc_html__('Fade In Up', 'piko-construct'),
                    'effect-fadein-down' => esc_html__('Fade In Down', 'piko-construct'),
                    'effect-fadein' => esc_html__('Fade In', 'piko-construct'),
                ),
                'default' => 'effect-down'
            ),
            array(
                'id' => "sub_menu_animation",
                'type' => 'select',
                'title' => esc_html__('Dropdown Sub menu Effect', 'piko-construct'),
                'options' => array(
                    'subeffect-down' => esc_html__('Drop Down', 'piko-construct'),
                    'subeffect-fadein-left' => esc_html__('Fade In Left', 'piko-construct'),
                    'subeffect-fadein-right' => esc_html__('Fade In Right', 'piko-construct'),
                    'subeffect-fadein-up' => esc_html__('Fade In Up', 'piko-construct'),
                    'subeffect-fadein-down' => esc_html__('Fade In Down', 'piko-construct'),
                    'subeffect-fadein' => esc_html__('Fade In', 'piko-construct'),
                ),
                'default' => 'subeffect-fadein-left'
            ),
            array(
                    'id'      => 'menu_search',
                    'type'    => 'switch',
                    'title'   => esc_html__( 'Disable the Search the icon  Menu Bar', 'piko-construct' ),
                    'title'   => sprintf( wp_kses( __( '<i class="fa fa-search" ></i> Disable the Search the icon', 'piko-construct' ), array( 'i' => array( 'class' => array(),  ) ) ) ),
                    'default' => '1',
                    'on' => esc_html__( 'Enable', 'piko-construct' ),
                    'off' => esc_html__( 'Disabled', 'piko-construct' ),   
            ),
            array(
                    'id'      => 'show_cart_iocn',
                    'type'    => 'switch',
                    'title'   => esc_html__( 'Disable Cart Icon', 'piko-construct' ),
                    'title'   => sprintf( wp_kses( __( '<i class="fa fa-shopping-cart" ></i> Disable the cart Icon Menu Bar', 'piko-construct' ), array( 'i' => array( 'class' => array(),  ) ) ) ),
                    'default' => '1',
                    'on' => esc_html__( 'Enable', 'piko-construct' ),
                    'off' => esc_html__( 'Disabled', 'piko-construct' ),   
            ),
            array (
                    'id' => 'header_bg_options',
                    'icon' => true,
                    'type' => 'info',
                    'raw' => '<h3>Header Main color configure</h3>',
            ),            
            array(
                'id'        => 'header_background',
                'type'      => 'color_rgba',
                'title'     => 'Header Background Color',
                'default'   => array(
                    'color'     => '',
                    'alpha'     => 1
                ),
                'output'    => array( 
                        'background-color' => '.header-layout-1 .site-header .header-main .header-right,.header-layout-2 .site-header .header-main,.header-layout-4 .header-wrapper .mega-menu-sidebar,.header-layout-3 .header-wrapper.header-side-nav #header,.header-layout-5 .site-header .header-main .header-right,.header-layout-6 .site-header .header-main',
                        'border-bottom-color' => '.header-layout-1 .site-header .header-main,.header-layout-6 .site-header .header-main,.header-layout-5 .site-header .header-main .header-left',                    
                    ),               
            ),
            array (
                    'title' => esc_html__('Header Link Color', 'piko-construct'),
                    'subtitle' => esc_html__('Default color use: #555555.', 'piko-construct'),
                    'id' => 'header_link_color',
                    'type' => 'color',
                    'default' => '',
                    'transparent' => false,
                    'output'    => array('color' => '.mega-menu > li.menu-item > a,.site-header .header-actions .tools_button .cart-items, .mega-menu-sidebar .main-menu.mega-menu > li.menu-item > a, .mega-menu-sidebar .main-menu.mega-menu > li.menu-item > h5,.header-layout-5 .site-header .main-menu > .menu-item > a'),                  
                    
            ),
            array (
                    'title' => esc_html__('Header Link Background Color', 'piko-construct'),
                    'subtitle' => esc_html__('Default color no.', 'piko-construct'),
                    'id' => 'header_link_background_color',
                    'type' => 'color',
                    'default' => '',
                    'transparent' => true,
                    'output'    => array('background-color' => '.mega-menu > li.menu-item > a'),
                   
            ),
            array (
                    'title' => esc_html__('Header Link Hover Color', 'piko-construct'),
                    'subtitle' => esc_html__('Default color use: #45bf55.', 'piko-construct'),
                    'id' => 'header_link_hover_color',
                    'type' => 'color',
                    'default' => '',
                    'transparent' => false,
                    'output'    => array(
                        'color' => '.header-layout-6.header-transparency .site-header:not(.active-sticky) .header-actions .tools_button:hover,.header-layout-6.header-transparency .site-header:not(.active-sticky) .mega-menu > li.menu-item > a:hover,.header-layout-6.header-transparency .site-header:not(.active-sticky) .mega-menu > li.menu-item > h5:hover,.header-layout-5.header-transparency .site-header:not(.active-sticky) .header-actions .tools_button:hover,.header-layout-5.header-transparency .site-header:not(.active-sticky) .mega-menu > li.menu-item > a:hover,.header-layout-5.header-transparency .site-header:not(.active-sticky) .mega-menu > li.menu-item > h5:hover,.header-layout-4.header-transparency .site-header:not(.active-sticky) .header-actions .tools_button:hover,.header-layout-4.header-transparency .site-header:not(.active-sticky) .mega-menu > li.menu-item > a:hover,.header-layout-4.header-transparency .site-header:not(.active-sticky) .mega-menu > li.menu-item > h5:hover,.header-layout-2.header-transparency .site-header:not(.active-sticky) .header-actions .tools_button:hover,.header-layout-2.header-transparency .site-header:not(.active-sticky) .mega-menu > li.menu-item > a:hover,.header-layout-2.header-transparency .site-header:not(.active-sticky) .mega-menu > li.menu-item > h5:hover,.header-layout-1.header-transparency .site-header:not(.active-sticky) .header-actions .tools_button:hover,.header-layout-1.header-transparency .site-header:not(.active-sticky) .mega-menu > li.menu-item > a:hover,.header-layout-1.header-transparency .site-header:not(.active-sticky) .mega-menu > li.menu-item > h5:hover,.mega-menu > li.menu-item:hover > a,.mega-menu > li.menu-item.active > a,.mega-menu > li.menu-item.active > h5,.mega-menu-sidebar .main-menu.mega-menu > li.menu-item.active > a, .mega-menu-sidebar .main-menu.mega-menu > li.menu-item.active > h5, .mega-menu-sidebar .main-menu.mega-menu > li.menu-item:hover > a, .mega-menu-sidebar .main-menu.mega-menu > li.menu-item:hover > h5,.header-layout-5 .mega-menu > li.menu-item:hover > a',
                        ),
                    
            ),
            array (
                    'title' => esc_html__('Header Link Hover Background Color', 'piko-construct'),
                    'subtitle' => esc_html__('Default color use: #45bf55.', 'piko-construct'),
                    'id' => 'header_link_hover_background_color',
                    'type' => 'color',
                    'default' => '',
                    'transparent' => true,
                    'output'    => array('background-color' => '.mega-menu > li:hover > a, .mega-menu-sidebar .main-menu.mega-menu > li.menu-item.active > a, .mega-menu-sidebar .main-menu.mega-menu > li.menu-item.active > h5, .mega-menu-sidebar .main-menu.mega-menu > li.menu-item:hover > a, .mega-menu-sidebar .main-menu.mega-menu > li.menu-item:hover > h5'),
                    
            ),
            array (
                    'title' => esc_html__('Header Icon color', 'piko-construct'),
                    'subtitle' => esc_html__('cart, search icon Default color use: #333333.', 'piko-construct'),
                    'id' => 'header_action_icon_color',
                    'type' => 'color',
                    'default' => '',
                    'transparent' => false,
                    'output'    => array('color' => '.header-layout-1 .site-header .header-actions > ul a,.header-layout-2 .site-header .header-main .header-right .header-actions > ul a,.header-layout-3 .site-header .header-actions a.tools_button,.header-layout-4 .site-header .header-actions .tools_button,.header-layout-5 .site-header .header-actions .tools_button,.header-layout-6 .site-header .header-actions > ul a'),                  
                    
            ),
            array (
                    'title' => esc_html__('Header Icon hover color', 'piko-construct'),
                    'subtitle' => esc_html__('cart, search icon Default color use: #45bf55.', 'piko-construct'),
                    'id' => 'header_action_icon_hover_color',
                    'type' => 'color',
                    'default' => '',
                    'transparent' => true,
                    'output'    => array('color' => '.header-layout-1 .site-header .header-actions > ul a:hover,.header-layout-2 .site-header .header-main .header-right .header-actions > ul a:hover,.header-layout-3 .site-header .header-actions a.tools_button[aria-expanded="true"]:hover, .header-layout-3 .site-header .header-actions a.tools_button:hover,.header-layout-4 .site-header .header-actions .tools_button:hover,.header-layout-5 .site-header .header-actions .tools_button:hover,.header-layout-6 .site-header .header-actions > ul a:hover'),
                   
            ),
            array (
                    'title' => esc_html__('Cart Counter Background Color', 'piko-construct'),
                    'subtitle' => esc_html__('Default color use: #45bf55.', 'piko-construct'),
                    'id' => 'header_link_cart_bg_color',
                    'type' => 'color',
                    'default' => '',
                    'transparent' => false,
                    'output'    => array(
                        'background-color' => '.site-header .header-actions .tools_button .cart-items'
                        ),
                    
            ),
            array (
                    'id' => 'header_dropdown_menu_options',
                    'icon' => true,
                    'type' => 'info',
                    'raw' => '<h3>Header Dropdown menu Color configure</h3>',
            ),
            array (
                    'title' => esc_html__('Header dropdown Link Color', 'piko-construct'),
                    'subtitle' => esc_html__('Default color use: #666666.', 'piko-construct'),
                    'id' => 'header_dropdown_link_color',
                    'type' => 'color',
                    'default' => '',
                    'transparent' => false,
                    'output'    => array('color' => '.mega-menu .narrow .popup > .inner > ul.sub-menu > li > a,.main-menu-wrap .header-dropdown.lang ul li > a,.main-menu-wrap .header-dropdown.lang ul li a'),
                    
            ),
            array (
                    'title' => esc_html__('Header dropdown Link Hover Color', 'piko-construct'),
                    'subtitle' => esc_html__('Default color use: #ffffff.', 'piko-construct'),
                    'id' => 'header_dropdown_link_hover_color',
                    'type' => 'color',
                    'default' => '',
                    'transparent' => false,
                    'output'    => array('color' => '.mega-menu .narrow .popup > .inner > ul.sub-menu > li:hover > a, .mega-menu .narrow .popup > .inner > ul.sub-menu > li.current-menu-item a,.main-menu-wrap .header-dropdown.lang ul li:hover > a,.main-menu-wrap .header-dropdown.lang ul li a:hover'),
                    
            ),
            array (
                    'title' => esc_html__('Header dropdown Link Hover Background Color', 'piko-construct'),
                    'subtitle' => esc_html__('Default color use: #45bf55.', 'piko-construct'),
                    'id' => 'header_dropdown_link_hover_background_color',
                    'type' => 'color',
                    'default' => '',
                    'transparent' => true,
                    'output'    => array(
                        'background-color' => '.mega-menu .narrow .popup > .inner > ul.sub-menu > li:hover > a, .mega-menu .narrow .popup > .inner > ul.sub-menu > li.current-menu-item a,.main-menu-wrap .header-dropdown.lang ul li:hover > a,.main-menu-wrap .header-dropdown.lang ul li a:hover',
                        'border-color' => '.main-menu-wrap .header-dropdown.lang ul li:hover > a, .main-menu-wrap .header-dropdown.lang ul li a:hover',
                        ),
                    
            ),
            array (
                    'id' => 'header_sub_menu_options',
                    'icon' => true,
                    'type' => 'info',
                    'raw' => '<h3>Header Sub menu Color configure</h3>',
            ),
            array (
                    'title' => esc_html__('Header sub Link Color', 'piko-construct'),
                    'subtitle' => esc_html__('Default color use: #666666.', 'piko-construct'),
                    'id' => 'header_sub_link_color',
                    'type' => 'color',
                    'default' => '',
                    'transparent' => false,
                    'output'    => array('color' => '.mega-menu .narrow .popup > .inner > ul.sub-menu ul.sub-menu li a'),                    
            ),
            array (
                    'title' => esc_html__('Header sub Link Hover Color', 'piko-construct'),
                    'subtitle' => esc_html__('Default color use: #ffffff.', 'piko-construct'),
                    'id' => 'header_sub_link_hover_color',
                    'type' => 'color',
                    'default' => '',
                    'transparent' => false,
                    'output'    => array('color' => '.mega-menu .narrow .popup > .inner > ul.sub-menu ul.sub-menu li:hover a, .mega-menu .narrow .popup > .inner > ul.sub-menu ul.sub-menu li.current-menu-item a'),                    
            ),
            array (
                    'title' => esc_html__('Header sub Link Hover Background Color', 'piko-construct'),
                    'subtitle' => esc_html__('Default color use: #45bf55.', 'piko-construct'),
                    'id' => 'header_dropdown_sub_hover_background_color',
                    'type' => 'color',
                    'default' => '',
                    'transparent' => true,
                    'output'    => array('background-color' => '.mega-menu .narrow .popup > .inner > ul.sub-menu ul.sub-menu li:hover a, .mega-menu .narrow .popup > .inner > ul.sub-menu ul.sub-menu  li.current-menu-item a'),                   
            ),
            array (
                    'id' => 'header_mega_menu_options',
                    'icon' => true,
                    'type' => 'info',
                    'raw' => '<h3>Header Mega menu Color configure</h3>',
            ),
            array (
                    'title' => esc_html__('Header mega Link Color', 'piko-construct'),
                    'subtitle' => esc_html__('Default color use: #666666.', 'piko-construct'),
                    'id' => 'header_mega_link_color',
                    'type' => 'color',
                    'default' => '',
                    'transparent' => false,
                    'output'    => array('color' => '.mega-menu .wide .popup > .inner > ul.sub-menu > li.menu-item li.menu-item > a'),                    
            ),
            array (
                    'title' => esc_html__('Header mega Link Hover Color', 'piko-construct'),
                    'subtitle' => esc_html__('Default color use: #45bf55.', 'piko-construct'),
                    'id' => 'header_mega_link_hover_color',
                    'type' => 'color',
                    'default' => '',
                    'transparent' => false,
                    'output'    => array('color' => '.mega-menu .wide .popup > .inner > ul.sub-menu > li.menu-item li.menu-item > a:hover, .mega-menu .wide .popup > .inner > ul.sub-menu > li.menu-item li.menu-item > a:focus, .mega-menu .wide .popup > .inner > ul.sub-menu > li.menu-item li.menu-item.current_page_item > a'),                    
            ),
            array (
                    'title' => esc_html__('Header Mega menu Background Color', 'piko-construct'),
                    'subtitle' => esc_html__('Default color use: #f9f9f9.', 'piko-construct'),
                    'id' => 'header_mega_menu_background_color',
                    'type' => 'color',
                    'default' => '',
                    'transparent' => true,
                    'output'    => array(
                        'background-color' => '.mega-menu .wide .popup > .inner',
                        'border-color' => '.mega-menu .wide .popup > .inner',
                    ),                   
            ),
        )
    ) );    
    //header active sticky configure
    Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Sticky Menu', 'piko-construct' ),
        'desc'       => esc_html__( 'Configure your Main Menu Bellow the Option Available', 'piko-construct' ),
        'subsection' => true,
        'fields'     => array(
            array (
                    'title' => esc_html__('Sticky Header', 'piko-construct'),
                    'subtitle' => esc_html__('Enable / Disable the Sticky Header.', 'piko-construct'),
                    'id' => 'sticky_header',
                    'on' => esc_html__('Enable', 'piko-construct'),
                    'off' => esc_html__('Disabled', 'piko-construct'),
                    'type' => 'switch',
                    'default' => 1,
            ),
            array (
                    'title' => esc_html__('Hide Logo In Header Sticky', 'piko-construct'),
                    'id' => 'sticky_header_hide_logo',
                    'on' => esc_html__('Yes', 'piko-construct'),
                    'off' => esc_html__('No', 'piko-construct'),
                    'type' => 'switch',
                    'default' => 0,
                    'required' => array('sticky_header','=','1')
            ),
            array (
                    'title' => esc_html__('Hide Search Form In Header Sticky', 'piko-construct'),
                    'id' => 'sticky_header_hide_search',
                    'on' => esc_html__('Yes', 'piko-construct'),
                    'off' => esc_html__('No', 'piko-construct'),
                    'type' => 'switch',
                    'default' => 0,
                    'required' => array('sticky_header','=','1')
            ),
            array (
                    'title' => esc_html__('Hide Cart Box Header Sticky', 'piko-construct'),
                    'id' => 'sticky_header_hide_cart',
                    'on' => esc_html__('Yes', 'piko-construct'),
                    'off' => esc_html__('No', 'piko-construct'),
                    'type' => 'switch',
                    'default' => 0,
                    'required' => array('sticky_header','=','1')
            ),
            array (
                    'id' => 'header_sticky_options',
                    'icon' => true,
                    'type' => 'info',
                    'required' => array( 'sticky_header', '=', '1' ),
                    'raw' => '<h3>Header Sticky color configure</h3>',
            ),
            array(
                'id'        => 'header_sticky_background',
                'type'      => 'color_rgba',
                'title'     => 'Sticky Header Background Color',
                'required' => array( 'sticky_header', '=', '1' ),
                'default'   => array(
                    'color'     => '',
                    'alpha'     => 1
                ),
                'output'    => array('background-color' => '.header-layout-1 .site-header.active-sticky.sticky-menu-header .main-menu-wrap,.header-layout-2 .site-header.active-sticky.sticky-menu-header .header-main,.header-layout-5 .site-header.active-sticky.sticky-menu-header .main-menu-wrap, .header-layout-6 .site-header.active-sticky.sticky-menu-header .main-menu-wrap'),               
            ),
            array (
                    'title' => esc_html__('Sticky Logo Background Color', 'piko-construct'),
                    'subtitle' => esc_html__('Default color use: #45bf55.', 'piko-construct'),
                    'id' => 'header_logo_sticky_background_color',
                    'type' => 'color',
                    'default' => '',
                    'transparent' => true,
                    'output'    => array('background-color' => '.header-layout-6 .site-header.active-sticky .site-logo'),
                    'required' => array( 'menu_style', '=', array('6')),
            ),
            array (
                    'title' => esc_html__('Sticky Header Link Color', 'piko-construct'),
                    'subtitle' => esc_html__('Default use color: #555555.', 'piko-construct'),
                    'id' => 'header_sticky_link_color',
                    'type' => 'color',
                    'default' => '',
                    'transparent' => false,
                    'output'    => array('color' => '.active-sticky .mega-menu > li.menu-item > a,.active-sticky.site-header .header-actions .tools_button .cart-items'),                   
                    'required' => array('sticky_header','=','1')
            ),
            array (
                    'title' => esc_html__('Sticky Header Link Background Color', 'piko-construct'),
                    'subtitle' => esc_html__('Default color no.', 'piko-construct'),
                    'id' => 'header_sticky_link_background_color',
                    'type' => 'color',
                    'default' => '',
                    'transparent' => true,
                    'output'    => array('background-color' => '.active-sticky .mega-menu > li.menu-item > a'),
                    'required' => array('sticky_header','=','1')
            ),
            array (
                    'title' => esc_html__('Sticky Header Link Hover Color', 'piko-construct'),
                    'subtitle' => esc_html__('Default color use: #ffffff.', 'piko-construct'),
                    'id' => 'header_sticky_link_hover_color',
                    'type' => 'color',
                    'default' => '',
                    'transparent' => false,
                    'output'    => array(
                        'color' => '.active-sticky .mega-menu > li.menu-item:hover > a,.active-sticky .mega-menu > li.menu-item.active > a,.active-sticky .mega-menu > li.menu-item.active > h5',
                        'background-color' => '.active-sticky.site-header .header-actions .tools_button .cart-items'
                        ),
                    'required' => array('sticky_header','=','1')
            ),
            array (
                    'title' => esc_html__('Sticky Header Link Hover Background Color', 'piko-construct'),
                    'subtitle' => esc_html__('Default color use: #45bf55.', 'piko-construct'),
                    'id' => 'header_sticky_link_hover_background_color',
                    'type' => 'color',
                    'default' => '',
                    'transparent' => true,
                    'output'    => array('background-color' => '.active-sticky .mega-menu > li:hover > a'),
                    'required' => array('sticky_header','=','1')
            ),
            array (
                    'title' => esc_html__('Sticky Header Icon color', 'piko-construct'),
                    'subtitle' => esc_html__('cart, search icon Default color use: #333333.', 'piko-construct'),
                    'id' => 'header_action_sticky_icon_color',
                    'type' => 'color',
                    'default' => '',
                    'transparent' => false,
                    'output'    => array('color' => '.header-layout-1 .active-sticky.site-header .header-actions > ul a'),
                    'required' => array('sticky_header','=','1')
                    
            ),
            array (
                    'title' => esc_html__('Sticky Header Icon hover color', 'piko-construct'),
                    'subtitle' => esc_html__('cart, search icon Default color use: #45bf55.', 'piko-construct'),
                    'id' => 'header_action__sticky_icon_hover_color',
                    'type' => 'color',
                    'default' => '',
                    'transparent' => true,
                    'output'    => array('color' => '.header-layout-1 .active-sticky.site-header .header-actions > ul a:hover'),
                    'required' => array('sticky_header','=','1')
                   
            ),
        )
    ) );
    //header transparency
    Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Transparency', 'piko-construct' ),
        'desc'       => esc_html__( 'Configure your Main Menu Header Transparency Bellow the Option Available', 'piko-construct' ),
        'subsection' => true,
        'fields'     => array(
            array (
                    'title' => esc_html__('Transparency', 'piko-construct'),
                    'id' => 'header_transparency',
                    'on' => esc_html__('Enable', 'piko-construct'),
                    'off' => esc_html__('Disabled', 'piko-construct'),
                    'type' => 'switch',
                    'default' => 0,
            ),
            array(
                'id'      => 'header_transparency_option',
                'type'    => 'button_set',
                'title'   => esc_html__( 'Transparency Action', 'piko-construct' ),                    
                'options' => array(
                    'fontpage' => esc_html__('Font Page', 'piko-construct'), 
                    'allpage' => esc_html__('All Page', 'piko-construct'),
                    ), 
                'default' => 'fontpage',
                'required' => array( 'header_transparency', '=', 1 ),
            ),            
            array(
                'type'      => 'color_rgba',
                'title' => esc_html__('Border color', 'piko-construct'),
                'subtitle' => esc_html__('Default color use: #fff.', 'piko-construct'),
                'id' => 'header_transparency_border',             
                'output'    => array('border-bottom-color' => '.header-layout-6 .site-header .header-main,.header-layout-5.header-transparency .site-header .header-main .header-left, .header-layout-5.header-transparency .site-header .header-top,.header-layout-2 .site-header .header-main,.header-layout-1 .site-header .header-main'),
                'default'   => array(
                    'color'     => '',
                    'alpha'     => 1
                ),
                'required' => array('header_transparency','=','1')
            ),
        )
    ) );  
    
    //Breadcrumb
    Redux::setSection( $opt_name, array(
        'title'   => esc_html__( 'Breadcrumbs', 'piko-construct' ),
        'icon'    => 'el el-credit-card',        
        'fields'  => array(
            array (
                    'title' => esc_html__('Disable Breadcrumbs', 'piko-construct'),
                    'id' => 'breadcrumbs_disable',
                    'on' => esc_html__('Enabled', 'piko-construct'),
                    'off' => esc_html__('Disabled', 'piko-construct'),
                    'type' => 'switch',
                    'default' => 1,
            ),
            array(
                'id'      => 'optn_breadcrubm_width',
                'type'    => 'button_set',
                'title'   => esc_html__( 'Breadcrumb width', 'piko-construct' ),                    
                'options' => array(
                    'container' => 'Container', 
                    'container-fluid' => 'Container Fluid',
                    ), 
                'default' => 'container',
                'required' => array( 'breadcrumbs_disable', '=', 1 ),
            ),
            array(
                'id'       => 'breadcrumb_layout',
                'type'     => 'image_select',
                'title'    => esc_html__('Breadcrumb Layout', 'piko-construct'),
                'subtitle' => esc_html__( 'One Cloumn or Two Cloumn', 'piko-construct' ),
                'required' => array( 'breadcrumbs_disable', '=', 1 ),
                'options'  => array(
                    'one_cols'      => array(
                        'alt'   => 'One Cloumn', 
                        'img'   => ReduxFramework::$_url.'assets/img/1col.png'
                    ),
                    'two_cols'      => array(
                        'alt'   => 'Two Cloumn', 
                        'img'   => get_template_directory_uri() . '/assets/images/theme-options/2columns.png'
                    ),                    
                ),
                'default' => 'one_cols'
            ),
             array(
                'id'      => 'breadcrumb_layout_title',
                'type'    => 'button_set',
                'title'   => esc_html__( 'Title Align', 'piko-construct' ),                    
                'options' => array(
                    'title-left' => 'Left', 
                    'title-right' => 'Right',
                    ), 
                'default' => 'title-left',
                'required' => array( 'breadcrumb_layout', '=', 'two_cols' ),
            ),
              array(
                    'id'       => 'breadcrumbs_prefix',
                    'type'     => 'text',
                    'title'    => esc_html__( 'Breadcrumb prefix', 'piko-construct' ),
                    'subtitle' => esc_html__( 'if empty nothing', 'piko-construct' ),
                  'required' => array( 'breadcrumbs_disable', '=', 1 ),
                ),             
              array(
                    'id'       => 'optn_breadcrumb_name',
                    'type'     => 'text',
                    'compiler' => true,
                    'title'    => esc_html__( 'Breadcrumb Home Link', 'piko-construct' ),
                    'subtitle' => esc_html__( 'Default use Home', 'piko-construct' ),
                    'default'  => 'Home',
                  'required' => array( 'breadcrumbs_disable', '=', 1 ),
                ),             
              array(
                    'id'       => 'optn_breadcrumb_delimiter',
                    'type'     => 'text',
                    'compiler' => true,
                    'title'    => esc_html__( 'Breadcrumb delimiter', 'piko-construct' ),
                    'subtitle' => esc_html__( 'Delimiter use font Awesome class', 'piko-construct' ),
                    'desc'     => sprintf( wp_kses( __( 'Just use class like as: fa-angle-right  <a href="%s" target="__blank">Click font awesome</a>', 'piko-construct' ), array( 'a' => array( 'href' => array(), 'target' => array() ) ) ), 'http://fontawesome.io/icons/' ),
                    'default'  => 'fa-long-arrow-right',
                  'required' => array( 'breadcrumbs_disable', '=', 1 ),
                ),
                array (
                    'title' => esc_html__('Disable title', 'piko-construct'),
                    'id' => 'page_header_title',
                    'on' => esc_html__('Enabled', 'piko-construct'),
                    'off' => esc_html__('Disabled', 'piko-construct'),
                    'type' => 'switch',
                    'default' => 1,
                    'required' => array( 'breadcrumbs_disable', '=', 1 ),
                ),
                array(
                        'id'             => 'page-header_font_size',
                        'type'           => 'typography',
                        'title'          => esc_html__( 'Page title', 'piko-construct' ),
                        'subtitle' => esc_html__( 'Font size and color', 'piko-construct' ),
                        'compiler'       => true,
                        'google'         => false,
                        'font-backup'    => false,
                        'all_styles'     => true,
                        'font-weight'    => true,
                        'font-family'    => true,
                        'text-align'     => false,
                        'font-style'     => false,
                        'subsets'        => false,
                        'font-size'      => true,
                        'line-height'    => true,
                        'word-spacing'   => false,
                        'letter-spacing' => false,
                        'color'          => true,
                        'preview'        => true,
                        'output'         => array( '.page-header h1' ),
                        'units'          => 'px',
                        'default'        => array(
                                'font-size' => '45px',
                                'color' => '',
                        ),
                        'required' => array( 'page_header_title', '=', 1 ),
                ),
                array (
                    'title' => esc_html__('Disable Breadcrumb', 'piko-construct'),
                    'id' => 'optn_breadcrubm_layout',
                    'on' => esc_html__('Enabled', 'piko-construct'),
                    'off' => esc_html__('Disabled', 'piko-construct'),
                    'type' => 'switch',
                    'default' => 1,
                    'required' => array( 'breadcrumbs_disable', '=', 1 ),
                ),
            
                array(
                    'id'       => 'page_link_color',
                    'type'     => 'color',
                    'compiler' => true,
                    'title'    => esc_html__( 'Breadcrumb Link Color', 'piko-construct' ),
                    'subtitle' => esc_html__( 'Default white color #333333', 'piko-construct' ),
                    'default'  => '',
                    'required' => array( 'optn_breadcrubm_layout', '=', 1 ),
                    'output'   => array(
                            'color'    => '.page-header h1,.breadcrumb a,.breadcrumb i'
                        )
                ),
                array(
                    'id'       => 'page_link_hover_color',
                    'type'     => 'color',
                    'compiler' => true,
                    'title'    => esc_html__( 'Breadcrumb Link hover Color', 'piko-construct' ),
                    'subtitle' => esc_html__( 'Default white color #45bf55', 'piko-construct' ),
                    'default'  => '', 
                    'required' => array( 'optn_breadcrubm_layout', '=', 1 ),
                    'output'   => array(
                            'color'    => '.breadcrumb a:hover, .breadcrumb a:focus'
                        )
                ),
                array(
                    'id'       => 'page_link_active',
                    'type'     => 'color',
                    'compiler' => true,
                    'title'    => esc_html__( 'Active & Prefix Color', 'piko-construct' ),
                    'subtitle' => esc_html__( 'Default white color #747474', 'piko-construct' ),
                    'default'  => '',
                    'required' => array( 'optn_breadcrubm_layout', '=', 1 ),
                    'output'   => array(
                            'color'    => '.breadcrumb > .current, .breadcrumb .prefix, .woocommerce-breadcrumb'
                        )
                ),
                array(
                    'id'             => 'optn_page_title_padding',
                    'type'           => 'spacing',
                    'mode'           => 'padding',
                    'units'          => 'px',
                    'units_extended' => 'false',
                    'title'          => esc_html__('Breadcrumb padding', 'piko-construct'),
                    'subtitle'       => esc_html__('This must be numeric (no px). Leave for default.', 'piko-construct'),
                    'left'          => false,
                    'right'          => false,
                    'output'        => array('.page-header'),
                    'default'            => array(
                        'padding-top'     => '40px',
                        'padding-bottom'  => '41px',
                        'units'          => 'px',
                    ),
                    'required' => array( 'breadcrumbs_disable', '=', 1 ),
                ),
                array(
                        'id' => 'optn_header_img',
                        'type' => 'media',
                        'url' => true,
                        'title' => esc_html__('Header Image', 'piko-construct'),
                        'compiler' => 'true',                        
                        'subtitle' => esc_html__('Upload header image', 'piko-construct'),
                        'default'  => array(
                                    'url' => get_template_directory_uri(). '/assets/images/bg/page-title.jpg'
                        ),
                    'required' => array( 'breadcrumbs_disable', '=', 1 ),
                    ),
                array(
                    'id'       => 'optn_header_img_bg_color',
                    'type'     => 'color',
                    'compiler' => true,
                    'title'    => esc_html__( 'Bankground Color', 'piko-construct' ),
                    'output'    => array('background-color' => '.page-header'),
                    'default'  => '#f5f5f5',
                    'required' => array( 'breadcrumbs_disable', '=', 1 ),
                    
                ),
            array(
                'id'       => 'optn_header_img_repeat',
                'type'     => 'button_set',
                'multi'    => false,
                'title'    => esc_html__('Header Image Repeat', 'piko-construct'),
                'options'  => array(  
                    'repeat'   => esc_html__( 'Repeat', 'piko-construct' ),
                    'no-repeat'  => esc_html__( 'No repeat', 'piko-construct' ),
                    'fixed'  => esc_html__( 'Fixed', 'piko-construct' ),
                    ),
                'default'   => 'repeat',
                'subtitle' => esc_html__('Header background image repeat', 'piko-construct'),
                'required' => array( 'breadcrumbs_disable', '=', 1 ),
            ),
            array(
                'id'       => 'optn_header_title_text_align',
                'type'     => 'button_set',
                'multi'    => false,
                'title'    => esc_html__('Breadcrubm Text Align', 'piko-construct'),
                'options'  => array(
                    'left'   => esc_html__( 'Left', 'piko-construct' ),
                    'center'  => esc_html__( 'Center', 'piko-construct' ),
                    'right'  => esc_html__( 'Right', 'piko-construct' ),                            
                ),
                'default'   => 'center',
                'required' => array( 'breadcrumbs_disable', '=', 1 ),
            ),
        )
    ));
    //page setup
        Redux::setSection( $opt_name, array(
        'title' => esc_html__( 'Page Setting', 'piko-construct' ),
        'icon'  => 'el el-list-alt',
        'fields'     => array(                      
            array(
                'id'       => 'optn_page_sidebar_pos',
                'type'     => 'image_select',
                'title'    => esc_html__('Page Layout', 'piko-construct'),
                'subtitle' => esc_html__( 'Select Page layout No Sidebar, Left, Right and both', 'piko-construct' ),
                'options'  => array(
                    'fullwidth'      => array(
                        'alt'   => 'Full Width', 
                        'img'   => ReduxFramework::$_url.'assets/img/1col.png'
                    ),
                    'left'      => array(
                        'alt'   => 'Left sidebar', 
                        'img'   => ReduxFramework::$_url.'assets/img/2cl.png'
                    ),
                    'right'      => array(
                        'alt'   => 'Right sidebar', 
                        'img'  => ReduxFramework::$_url.'assets/img/2cr.png'
                    ),
                    'both'      => array(
                        'alt'   => 'Both sidebar', 
                        'img'  => ReduxFramework::$_url.'assets/img/3cm.png'
                    ),
                ),
                'default' => 'fullwidth'
            ),
            array(
                'id' => 'optn_page_sidebar',
                'type' => 'select',
                'title' => esc_html__('Page Sidebar', 'piko-construct'),
                'subtitle' => "Choose Your sidebar",
                'data'      => 'sidebars',
                'default' => 'sidebar-2',
                'required' => array( 'optn_page_sidebar_pos', '=', array('left', 'right', 'both'), ),
            ),
            array(
                'id' => 'optn_page_sidebar_left',
                'type' => 'select',
                'title' => esc_html__('Page sidebar left', 'piko-construct'),
                'subtitle' => "Choose Your sidebar",
                'data'      => 'sidebars',
                'required' => array( 'optn_page_sidebar_pos', '=', 'both', ),
            ),
            array(
                'id'       => 'optn_page_sidebar_width',
                'type'     => 'button_set',
                'title'    => esc_html__( 'Sidebar Width', 'piko-construct' ),              
                'default'  => 'small',
                'options'  => array(
                        'large' => esc_html__( 'Large(1/4)', 'piko-construct' ),
                        'small' => esc_html__( 'Small(1/3)', 'piko-construct' ),
                ),
                'required' => array( 'optn_page_sidebar_pos', '=', array('left', 'right', 'both'), ),
            ),            
        )
    ));
  
    
    //footer option
    Redux::setSection( $opt_name, array(
        'title'      => esc_html__( 'Footer', 'piko-construct' ), 
        'icon'             => 'el el-photo',
        'fields'     => array(
            array(
                    'id'      => 'footer-width-content',
                    'type'    => 'button_set',
                    'title'   => esc_html__( 'Footer width', 'piko-construct' ),                    
                    'options' => array(
                        'container' => 'Container', 
                        'container-fluid' => 'Container Fluid',
                        ), 
                    'default' => 'container'  
            ),            
            array(
                'id'       => 'footer_layout',
                'type'     => 'image_select',
                'title'    => esc_html__('Footer Style', 'piko-construct'),
                'subtitle' => esc_html__( 'Select Layout design style', 'piko-construct' ),
                'options'  => array(
                    'layout1'      => array(
                        'alt'   => 'Layout 1', 
                        'img'   => get_template_directory_uri() . '/assets/images/theme-options/footer-layout-1.jpg'
                    ),
                    'layout2'      => array(
                        'alt'   => 'Layout 2', 
                        'img'   => get_template_directory_uri() . '/assets/images/theme-options/footer-layout-2.jpg'
                    ),
                    'layout3'      => array(
                        'alt'   => 'Layout 3', 
                        'img'  => get_template_directory_uri() . '/assets/images/theme-options/footer-layout-3.jpg'
                    ),                    
                ),
                'default' => 'layout3'
            ),
            array(
                'id'      => 'footer_parallax',
                'type'    => 'switch',
                'title'   => esc_html__( 'Enable footer Parallax.', 'piko-construct' ),                 
                'default' => 0,
                'on' => esc_html__( 'Enable', 'piko-construct' ),
                'off' => esc_html__( 'Disabled', 'piko-construct' ),
                'required' => array( 'footer_layout', '=', 'layout2'),
            ),
            array(
                'id'             => 'footer_parallax_padding',
                'type'           => 'spacing',
                'mode'           => 'margin',
                'units'          => 'px',
                'title'          => esc_html__('Set margin according to your footer height', 'piko-construct'),
                'subtitle'       => esc_html__('This must be numeric. or Leave for default.', 'piko-construct'),
                'left'          => false,
                'right'          => false,
                'top'           => false,
                'output'        => array('.has-footer-parallax .site-inner'),
                'required' => array( 'footer_parallax', '=', 1),
                'default'            => array(
                    'margin-bottom'  => '450px',
                    'units'          => 'px',
                )
            ),
            
            array(
                'id'       => 'footer_background_image_type',
                'type'     => 'button_set',
                'title'    => esc_html__( 'Background Image Type', 'piko-construct' ),
                'options'  => array(
                        'footer_bg_default'     => esc_html__( 'Default', 'piko-construct' ),
                        'footer_bg_custom'        => esc_html__( 'Custom', 'piko-construct' )
                ),
                'default'  => 'footer_bg_default',
                'required' => array( 'footer_parallax', '=', 0),
            ),
            array(
                'id'       => 'footer_background_image',
                'type'     => 'image_select',
                'title'    => esc_html__('Background Image', 'piko-construct'),
                'tiles'    => true,
                'options'  => array(
                        get_template_directory_uri() . '/assets/images/bg/footer-bg.jpg'      => array(
                                'img'   => get_template_directory_uri() . '/assets/images/bg/footer-bg.jpg'
                        ),
                        get_template_directory_uri() . '/assets/images/bg/footer-bg2.jpg'      => array(
                                'img'   => get_template_directory_uri() . '/assets/images/bg/footer-bg2.jpg'
                        ),
                        get_template_directory_uri() . '/assets/images/bg/footer-bg3.jpg'      => array(
                                'img'   => get_template_directory_uri() . '/assets/images/bg/footer-bg3.jpg'
                        )
                ),
                'default'  => get_template_directory_uri() . '/assets/images/bg/footer-bg.jpg',
                'required' => array(
                        array( 'footer_background_image_type', '=', 'footer_bg_default' ),
                ),
                'output'   => array(
                        'background-image' => 'footer.site-footer.layout2'
                )
            ),            
            array(
                'id'       => 'footer_background_custom_image',
                'type'     => 'background',
                'title'    => esc_html__('Custom Background', 'piko-construct'),
                'required' => array(
                        array( 'footer_background_image_type', '=', 'footer_bg_custom' ),
                ),
                'output'   => array(
                        'background-image' => 'footer.site-footer.layout2'
                )
            ),
            array(
                'id'        => 'footer_opacity_bg_color',
                'type'      => 'color_rgba',
                'title'     => esc_html__('Background Color', 'piko-construct'),
                'subtitle'  => esc_html__('Rgba color/ opacity', 'piko-construct'),               
                'output'    => array('background-color' => 'footer.site-footer.layout2:before'),
                //'compiler'  => array('color' => '.site-header, .site-footer', 'background-color' => '.nav-bar'),
                'default'   => array(
                    'color'     => '#fff',
                    'alpha'     => 1
                ),
                'required' => array( 'footer_background_image_type', '=', 'footer_bg_default' ),
            ), 
            array(
                'id'      => 'footer_widgets',
                'type'    => 'switch',
                'title'   => esc_html__( 'Enable footer widgets area.', 'piko-construct' ),                 
                'default' => 0,
                'on' => esc_html__( 'Enable', 'piko-construct' ),
                'off' => esc_html__( 'Disabled', 'piko-construct' ),
            ),
            array(
                'id'       => 'footer_columns',
                'type'     => 'button_set',
                'title'    => esc_html__( 'Footer Columns', 'piko-construct' ),
                'desc'     => esc_html__( 'Select the number of columns appear to Display in the footer.', 'piko-construct' ),                
                'default'  => '4',
                'required' => array( 'footer_widgets', '=', true, ),
                'options'  => array(
                        '1' => esc_html__( '1 Column', 'piko-construct' ),
                        '2' => esc_html__( '2 Columns', 'piko-construct' ),
                        '3' => esc_html__( '3 Columns', 'piko-construct' ),
                        '4' => esc_html__( '4 Columns', 'piko-construct' ),
                ),
            ),
            array(
                'id' => 'optn_footer_Widgets_one',
                'type' => 'select',
                'title' => esc_html__('Footer Sidebar one', 'piko-construct'),
                'subtitle' => "Choose Your sidebar",
                'data'      => 'sidebars',
                'required' => array( 'footer_widgets', '=', true, ),
                'default'  => 'sidebar-3',
            ),
            array(
                'id'      => 'optn_footer_widgets_two',
                'type'    => 'switch',
                'title'   => esc_html__( 'Enable footer widgets area Two.', 'piko-construct' ),                 
                'default' => 1,
                'on' => esc_html__( 'Enable', 'piko-construct' ),
                'off' => esc_html__( 'Disabled', 'piko-construct' ),
            ),
            array(
                'id'       => 'optn_footer_columns_two',
                'type'     => 'button_set',
                'title'    => esc_html__( 'Footer Columns Two', 'piko-construct' ),
                'desc'     => esc_html__( 'Select the number of columns appear to Display in the footer two.', 'piko-construct' ),                
                'default'  => '4',
                'required' => array( 'optn_footer_widgets_two', '=', true, ),
                'options'  => array(
                        '1' => esc_html__( '1 Column', 'piko-construct' ),
                        '2' => esc_html__( '2 Columns', 'piko-construct' ),
                        '3' => esc_html__( '3 Columns', 'piko-construct' ),
                        '4' => esc_html__( '4 Columns', 'piko-construct' ),
                ),
            ),
            array(
                'id' => 'optn_footer_Widgets_two',
                'type' => 'select',
                'title' => esc_html__('Footer Sidebar two', 'piko-construct'),
                'subtitle' => "Choose Your sidebar",
                'data'      => 'sidebars',
                'required' => array( 'optn_footer_widgets_two', '=', true, ),
                'default'  => 'sidebar-5',
            ),            
        )
    ) );
     //footer inner color configure   
    Redux::setSection( $opt_name, array(
        'title' => esc_html__( 'Footer Color', 'piko-construct' ),
        'subsection' => true,
        'fields'     => array(                       
            array(
                'id'        => 'footer_opacity_bg_two_color',
                'type'      => 'color_rgba',
                'title'     => esc_html__('Background Color', 'piko-construct'),
                'subtitle'  => esc_html__('Background color', 'piko-construct'),               
                'output'    => array(
                    'background-color' => '.site-footer .footer-layout3-wrap .sidebar-two'),
                'default'   => array(
                    'color'     => '',
                    'alpha'     => 1
                ),                                    
            ),
             array(
                    'id'       => 'footer_sidebar3_title_color',
                    'type'     => 'color',
                    'compiler' => true,
                    'transparent' => false,
                    'title'    => esc_html__( 'Widgets Title color', 'piko-construct' ),
                    'default'  => '', 
                    'subtitle'  => esc_html__('Default use:#ebeaea ', 'piko-construct'), 
                    'output'   => array(
                            'color'    => '.sub-footer .widget .widget-title,.sidebar-two .sub-footer .widget .widget-title'
                        )
                ),
            
            array(
                'id'        => 'footer_sidebar_two_color',
                'type'      => 'color',
                'transparent' => false,
                'title'     => esc_html__('Font Color', 'piko-construct'),
                'subtitle'  => esc_html__('Default use:#bfbfbf ', 'piko-construct'),
                'output'    => array(
                    'color' => '.sub-footer .widget p,.sub-footer .widget,.sidebar-two .sub-footer .widget p, .sidebar-two .sub-footer .widget address'
                    ),
                'default'   => '',                                 
            ),
            array(
                'id'        => 'footer_sidebar_link_color',
                'type'      => 'color',
                'transparent' => false,
                'title'     => esc_html__('Link Color', 'piko-construct'),
                'subtitle'  => esc_html__('Default use:#bfbfbf ', 'piko-construct'),
                'output'    => array(
                    'color' => '.sub-footer .widget a,.sidebar-two .sub-footer .widget a, .sub-footer .widget ul li a, .sub-footer .widget .comment-author-link a,.sidebar-two .sub-footer .widget .tagcloud a, .sidebar-two .sub-footer .widget ul li a, .sidebar-two .sub-footer .widget a, .sidebar-two .sub-footer .widget abbr'
                    ),
                'default'   => '',                                 
            ),
            array(
                'id'       => 'sub_footer_link_hover',
                'type'     => 'color',
                'compiler' => true,
                'transparent' => false,
                'title'    => esc_html__( 'Link Hover color', 'piko-construct' ),
                'default'  => '',
                'subtitle'  => esc_html__('Default use:#45bf55 ', 'piko-construct'), 
                'output'   => array(
                    'color' => '.sub-footer .widget a:hover,.sub-footer .widget a:focus,.sidebar-two .sub-footer .widget a:hover, .sub-footer .widget ul li a:hover, .sub-footer .widget .comment-author-link a:hover'
                    )
            ),
        )
    ));
    
    //footer bootom setup   
    Redux::setSection( $opt_name, array(
        'title' => esc_html__( 'Footer Bottom', 'piko-construct' ),
        'subsection' => true,
        'fields'     => array(
            array(
                'id'      => 'optn_footer_style',
                'type'    => 'image_select',
                'title'   => esc_html__( 'Footer Bottom Design.', 'piko-construct' ),
                'default' =>'style1',
                'options'  => array(
                    'style1'      => array(
                        'alt'   => 'style 1', 
                        'img'   => get_template_directory_uri() . '/assets/images/theme-options/footer-layout-5.jpg'
                    ),
                    'style2'      => array(
                        'alt'   => 'style 2', 
                        'img'   => get_template_directory_uri() . '/assets/images/theme-options/footer-layout-4.jpg'
                    ),                                        
                ),
            ),                        
            array(
                'id'       => 'sub_footer_text',
                'type'     => 'editor',
                'title'    => esc_html__( 'Copyright text', 'piko-construct' ),
                'subtitle' => esc_html__( 'Add copyright text here', 'piko-construct' ),
                'args'   => array(
                    'teeny'            => true,
                ),
                'default'  => wp_kses( __( '&copy; All Right Reserved', 'piko-construct' ), array( 'a' => array( 'href' => array() ) ) ),
                
            ),
            array(
                'id'       => 'optn_footer_right',
                'type'     => 'button_set',
                'title'    => esc_html__( 'Footer Right Side Option', 'piko-construct' ),
                'desc'     => esc_html__( 'Select the option appear to Display in the bottom footer right side and left side copyright text.', 'piko-construct' ),                
                'default'  => '1',
                'required' => array( 'optn_footer_style', '=', 'style2', ),
                'options'  => array(                        
                        '1' => esc_html__( 'Social Icon', 'piko-construct' ),
                        '2' => esc_html__( 'Payment Logo', 'piko-construct' ),
                ),
            ),
            array(
                'id'        => 'optn_payment_logo_upload',
                'type'      => 'gallery',
                'title'    => esc_html__( 'Payment logo upload', 'piko-construct' ),
                'subtitle' => esc_html__( 'Add your payment logo', 'piko-construct' ),
                'desc'     => esc_html__( 'The image size width: 38px and height: 24px', 'piko-construct' ),
                'required' => array( 'optn_footer_right', '=', 2, ),
                'compiler' => true,
            ),
            array(
                'id'       => 'footer_social',
                'type'     => 'checkbox',
                'title'    => esc_html__( 'Footer Social Media Icons to display', 'piko-construct' ),
                'subtitle' => esc_html__( 'The Social urls taken from Social Media settings tab please enter first the social Urls.', 'piko-construct' ),
                'required' => array(
                        array( 'optn_footer_right', '=', 1, )
                ),
                'default' => array(
                    'facebook' => '1', 
                    'twitter' => '1',
                    'instagram' => '1',
                    'flickr' => '1'
                ),
                'options'  => array(
                        'facebook'   => 'Facebook',
                        'twitter'    => 'Twitter',
                        'flickr'     => 'Flickr',
                        'instagram'  => 'Instagram',
                        'behance'    => 'Behance',
                        'dribbble'   => 'Dribbble',                        
                        'git'        => 'Git',
                        'linkedin'   => 'Linkedin',
                        'pinterest'  => 'Pinterest',
                        'yahoo'      => 'Yahoo',
                        'delicious'  => 'Delicious',
                        'dropbox'    => 'Dropbox',
                        'reddit'     => 'Reddit',
                        'soundcloud' => 'Soundcloud',
                        'google'     => 'Google',
                        'google-plus' => 'Google Plus',
                        'skype'      => 'Skype',
                        'youtube'    => 'Youtube',
                        'vimeo'      => 'Vimeo',
                        'tumblr'     => 'Tumblr',
                        'whatsapp'   => 'Whatsapp',
                ),
            ),
            array(
                'id'        => 'footer_main_bg_color',
                'type'      => 'color_rgba',
                'title'     =>  esc_html__('Footer Background color', 'piko-construct'),
                'subtitle'  =>  esc_html__('Default use: #232526', 'piko-construct'),                
                'output'    => array('background-color' => '.site-footer .main-footer,.site-footer .footer-layout3-wrap .main-footer', 'border-color' => '.site-footer .main-footer', ),                
                
                'default'   => array(
                    'color'     => '',
                    'alpha'     => 1
                ),                                    
            ),
            array(
                'id'       => 'footer_main_font_color',
                'type'     => 'color',
                'compiler' => true,
                'title'    => esc_html__( 'Footer font color', 'piko-construct' ),
                'default'  => '',
                'transparent' => false,
                'subtitle'  => esc_html__('Default use: #8c8989', 'piko-construct'), 
                'output'   => array(
                        'color'    => '.site-info'
                    )
            ),            
            array(
                'id'       => 'footer_main_link_color',
                'type'     => 'color',
                'compiler' => true,
                'title'    => esc_html__( 'Footer Link color', 'piko-construct' ),
                'default'  => '',
                'transparent' => false,
                'subtitle'  => esc_html__('Default use: #232526', 'piko-construct'), 
                'output'   => array(
                        'color'    => '.site-footer.layout2 .site-info a,.info-center-wrap .site-info a,.main-footer .site-info a,.footer-layout3-wrap .main-footer .social-icon a,.main-footer .social-icon a, .footer-layout3-wrap .main-footer .footer-links-menu li a,.main-footer .footer-links-menu li a '
                    )
            ),            
            array(
                'id'       => 'footer_main_link_hover_color',
                'type'     => 'color',
                'compiler' => true,
                'title'    => esc_html__( 'Footer Link hover color', 'piko-construct' ),
                'default'  => '',
                'transparent' => false,
                'subtitle'  => esc_html__('Default use: #45bf55', 'piko-construct'), 
                'output'   => array(
                        'color'    => '.info-center-wrap .site-info a:hover, .info-center-wrap .site-info a:focus,.main-footer .site-info a:hover,.footer-layout3-wrap .main-footer .social-icon a:hover,.main-footer .social-icon a:hover, .footer-layout3-wrap .main-footer .footer-links-menu li a:hover,.main-footer .footer-links-menu li a:hover'
                    )
            ),
            array(
                'id'       => 'footer_main_top_border_color',
                'type'     => 'color',
                'compiler' => true,
                'title'    => esc_html__( 'Footer border color', 'piko-construct' ),
                'default'  => '',
                'transparent' => false,
                'subtitle'  => esc_html__('Default use: #ebeaea', 'piko-construct'), 
                'output'   => array(
                        'border-color'    => '.site-footer.layout2 .footer-perallx-wrap .info-center-wrap, .site-footer.layout2 .footer-perallx-wrap .main-footer',
                        'border-bottom-color' => '.site-footer .footer-layout3-wrap .sidebar-two,.site-footer .footer-layout3-wrap .sidebar-one'
                    )
            ),
        )
    ));
    //Blog setup   
    Redux::setSection( $opt_name, array(
        'title' => esc_html__( 'Archive Blog Setting', 'piko-construct' ),
        'icon'  => 'el el-pencil-alt',
        'fields'     => array(
            array(
                'id'       => 'optn_blog_sidebar_pos',
                'type'     => 'image_select',
                'title'    => esc_html__('Blog Layout', 'piko-construct'),
                'subtitle' => esc_html__( 'Select Page layout No Sidebar, Left, Right and both', 'piko-construct' ),
                'options'  => array(
                    'fullwidth'      => array(
                        'alt'   => 'Full Width', 
                        'img'   => ReduxFramework::$_url.'assets/img/1col.png'
                    ),
                    'left'      => array(
                        'alt'   => 'Left sidebar', 
                        'img'   => ReduxFramework::$_url.'assets/img/2cl.png'
                    ),
                    'right'      => array(
                        'alt'   => 'Right sidebar', 
                        'img'  => ReduxFramework::$_url.'assets/img/2cr.png'
                    ),
                    'both'      => array(
                        'alt'   => 'Both sidebar', 
                        'img'  => ReduxFramework::$_url.'assets/img/3cm.png'
                    ),
                ),
                'default' => 'fullwidth'                
            ),
            array(
                'id' => 'optn_blog_sidebar',
                'type' => 'select',
                'title' => esc_html__('Blog Sidebar', 'piko-construct'),
                'subtitle' => "Choose Your sidebar",
                'data'      => 'sidebars',
                'default' => 'sidebar-1',
                'required' => array( 'optn_blog_sidebar_pos', '=', array('left', 'right', 'both'), ),
            ),
            array(
                'id' => 'optn_blog_sidebar_left',
                'type' => 'select',
                'title' => esc_html__('Page sidebar left', 'piko-construct'),
                'subtitle' => "Choose Your sidebar",
                'data'      => 'sidebars',                
                'required' => array( 'optn_blog_sidebar_pos', '=', 'both', ),
            ),
            array(
                'id'       => 'optn_blog_sidebar_width',
                'type'     => 'button_set',
                'title'    => esc_html__( 'Sidebar Width', 'piko-construct' ),              
                'default'  => 'small',
                'options'  => array(
                        'large' => esc_html__( 'Large(1/4)', 'piko-construct' ),
                        'small' => esc_html__( 'Small(1/3)', 'piko-construct' ),
                ),
                'required' => array( 'optn_blog_sidebar_pos', '=', array('left', 'right', 'both'), ),
            ),
           
            array(
                'id' => 'optn_archive_display_type',
                'type' => 'button_set',
                'title' => esc_html__('Archive Display Type', 'piko-construct'),
                'subtitle' => esc_html__('Select archive display type', 'piko-construct'),
                'default'  => 'grid',                
                'options'  => array(
                        'default' => esc_html__( 'Default', 'piko-construct' ),
                        'grid' => esc_html__( 'Grid', 'piko-construct' ),
                        'list' => esc_html__( 'List', 'piko-construct' ),
                ),
            ),	        
             array(
                'id' => 'optn_archive_display_columns',
                'type' => 'select',
                'title' => esc_html__('Archive Display Columns', 'piko-construct'),
                'subtitle' => esc_html__('Choose the number of columns to display on archive pages.','piko-construct'),
                'options' => array(
                        '1'		=> '1',
                        '2'		=> '2',
                        '3'		=> '3',
                        '4'		=> '4',
                ),
                'default' => '2',
                'required' => array( 'optn_archive_display_type', '=', array('grid'), ),
            ),
            array(
                'id' => 'optn_archive_title_position',
                'type' => 'button_set',
                'title' => esc_html__('Archive Title Position', 'piko-construct'),
                'desc' => '',
                'options' => array('image-bottom' => 'Image bottom', 'image-top' => 'Image Top'),
                'default' => 'image-bottom',
                'required' => array( 'optn_archive_display_type', '=', array('default','grid'), ),
            ),
            array(
                'id'       => 'optn_blog_post_metatag',
                'type'     => 'select',
                'multi'    => true,
                'title'    => esc_html__('Choose Meta option', 'piko-construct'),
                'subtitle' => esc_html__('Configure archive page and single page no indicator show both (min 1 value need)', 'piko-construct'),
                'required' => array( 'optn_blog_single_social_shear', '=', true, ),
                'options'  => array(
                    'data'  => esc_html__('Post Date', 'piko-construct'),
                    'author'     => esc_html__('Author', 'piko-construct'),
                    'category'   => esc_html__('Category Archive', 'piko-construct'),
                    'comment' => esc_html__('Comment', 'piko-construct'),
                    'format' => esc_html__('Post Format Archive', 'piko-construct'),
                    'format_single' => esc_html__('Post Format Single page', 'piko-construct'),
                    'category_single' => esc_html__('Post Category Single page', 'piko-construct'),
                    'tags_single' => esc_html__('Post Tags Single page', 'piko-construct'),
                ),                
                'default'  => array( 'data', 'author', 'category', 'comment', 'format_single', 'category_single', 'tags_single' ),
            ),
            array(
                'id' => 'opt_blog_overlay_style',
                'type' => 'button_set',
                'title' => esc_html__('Blog Thumbnail overlay effect', 'piko-construct'),
                'options' => array(
                        ''=> esc_html__('Fade', 'piko-construct'),
                        'overlay-style-2' => esc_html__('Zoom', 'piko-construct'),
                ),
                'default' => '',
            ),            
            array(
                'id' => 'opt_blog_btn_style',
                'type' => 'button_set',
                'title' => esc_html__('Button hover effect', 'piko-construct'),
                'options' => array(
                        'advance'=> esc_html__('Default', 'piko-construct'),
                        'default' => esc_html__('Advance', 'piko-construct'),
                ),
                'default' => 'advance',
            ),
            array(
                'id' => 'opt_blog_btn_hover_effect',
                'type' => 'select',
                'title' => esc_html__('Button hover effect', 'piko-construct'),
                'options' => array(
                    '0'		=> esc_html__('left to right', 'piko-construct'),
                    '1'		=> esc_html__('Swipe left', 'piko-construct'),
                    '2'		=> esc_html__('Swipe bottom', 'piko-construct'),
                    '3'		=> esc_html__('Swipe corner', 'piko-construct'),
                    '4'		=> esc_html__('Swipe center', 'piko-construct'),
                    '5'		=> esc_html__('Swipe corner 2', 'piko-construct'),
                    '6'		=> esc_html__('Swipe top bottom', 'piko-construct'),
                    '7'		=> esc_html__('Swipe corner 3', 'piko-construct'),
                    '8'		=> esc_html__('Shuffle corner', 'piko-construct'),
                    '9'		=> esc_html__('Shuffle top bottom', 'piko-construct'),
                    '10'	=> esc_html__('Aware', 'piko-construct')
                ),
                'default' => '4',
                'required' => array( 'opt_blog_btn_style', '=', array('default' ) ), 
            ),
            array(
                'id' => 'opt_blog_btn_size',
                'type' => 'button_set',
                'title' => esc_html__('Button size', 'piko-construct'),
                'options' => array(
                        'small'=> esc_html__('Small', 'piko-construct'),
                        'medium' => esc_html__('Medium', 'piko-construct'),
                ),
                'default' => 'medium',
                'required' => array( 'opt_blog_btn_style', '=', array('default' ) ), 
            ),
            array(
                'id' => 'opt_blog_btn_color',
                'type'     => 'select',
                'multi'    => true,
                'title' => esc_html__('Button more', 'piko-construct'),
                'options' => array(
                        'white'=> esc_html__('Border White', 'piko-construct'),
                        'btn-center' => esc_html__('Button center', 'piko-construct'),
                ),
                'default' => '',
                'required' => array( 'opt_blog_btn_style', '=', array('default' ) ), 
            ),
            
            
            array(
                'id' => 'opt_blog_continue_reading',
                'type' => 'text',
                'title' => esc_html__('Read More Button Text', 'piko-construct'),
                'default' =>  esc_html__('Read More', 'piko-construct'),
                'required' => array( 'optn_archive_display_type', '=', array('list','grid')),
            ),
            array(
                'id' => 'optn_archive_except_word',
                'type' => 'text',
                'title' => esc_html__('Excerpt Word', 'piko-construct'),
                'default' => '55',
                'required' => array( 'optn_archive_display_type', '=', array('list','grid')),
            ), 
        )
    ));
     Redux::setSection( $opt_name, array(
        'title' => esc_html__( 'Blog Single Page', 'piko-construct' ),
        'subsection' => true,
        'fields'     => array(                      
            array(
                'id'       => 'optn_blog_single_sidebar_pos',
                'type'     => 'image_select',
                'title'    => esc_html__('Blog single Layout', 'piko-construct'),
                'subtitle' => esc_html__( 'Select Page layout No Sidebar, Left, Right and both', 'piko-construct' ),
                'options'  => array(
                    'fullwidth'      => array(
                        'alt'   => 'Full Width', 
                        'img'   => ReduxFramework::$_url.'assets/img/1col.png'
                    ),
                    'left'      => array(
                        'alt'   => 'Left sidebar', 
                        'img'   => ReduxFramework::$_url.'assets/img/2cl.png'
                    ),
                    'right'      => array(
                        'alt'   => 'Right sidebar', 
                        'img'  => ReduxFramework::$_url.'assets/img/2cr.png'
                    ),
                    'both'      => array(
                        'alt'   => 'Both sidebar', 
                        'img'  => ReduxFramework::$_url.'assets/img/3cm.png'
                    ),
                ),
                'default' => 'fullwidth'
            ),
            array(
                'id' => 'optn_blog_single_sidebar',
                'type' => 'select',
                'title' => esc_html__('Blog single Sidebar', 'piko-construct'),
                'subtitle' => "Choose Your sidebar",
                'data'      => 'sidebars',
                'default' => 'sidebar-1',
                'required' => array( 'optn_blog_single_sidebar_pos', '=', array('left', 'right', 'both'), ),
            ),
            array(
                'id' => 'optn_blog_single_sidebar_left',
                'type' => 'select',
                'title' => esc_html__('Page sidebar left', 'piko-construct'),
                'subtitle' => "Choose Your sidebar",
                'data'      => 'sidebars',
                'required' => array( 'optn_blog_single_sidebar_pos', '=', 'both', ),
            ),
            array(
                'id'       => 'optn_blog_single_sidebar_width',
                'type'     => 'button_set',
                'title'    => esc_html__( 'Sidebar Width', 'piko-construct' ),              
                'default'  => 'small',
                'options'  => array(
                        'large' => esc_html__( 'Large(1/4)', 'piko-construct' ),
                        'small' => esc_html__( 'Small(1/3)', 'piko-construct' ),
                ),
                'required' => array( 'optn_blog_single_sidebar_pos', '=', array('left', 'right', 'both'), ),
            ),
            array(
                'id' => 'enable_author',
                'type' => 'switch',
                'title' => esc_html__('Enable/Disable  Author Biography single post', 'piko-construct'),
                'default' => 1,
                'on' => esc_html__( 'Enable', 'piko-construct' ),
                'off' => esc_html__( 'Disabled', 'piko-construct' ),
                
            ),  
             array(
                'id'      => 'optn_blog_single_social_shear',
                'type'    => 'switch',
                'title'   => esc_html__( 'Enable/Disable Social shear', 'piko-construct' ),
                'default' => 1,
                'on' => esc_html__( 'Enable', 'piko-construct' ),
                'off' => esc_html__( 'Disabled', 'piko-construct' ),
            ),
             array(
                'id'      => 'optn_blog_single_social_text',
                'type'    => 'text',
                'title'   => esc_html__( 'Social Title', 'piko-construct' ),
                'subtitle' => esc_html__( 'if empty nothing show', 'piko-construct' ),
                'default' => esc_html__('Share this story!', 'piko-construct'),
                'required' => array( 'optn_blog_single_social_shear', '=', 1 ),
            ),            
                      
            array(
                'id'       => 'optn_blog_single_single_share_socials',
                'type'     => 'select',
                'multi'    => true,
                'title'    => esc_html__('Choose socials to share single post', 'piko-construct'),
                'required' => array( 'optn_blog_single_social_shear', '=', true, ),
                'options'  => array(
                    'facebook'  => 'Facebook',
                    'gplus'     => 'Google Plus',
                    'twitter'   => 'Twitter',
                    'pinterest' => 'Pinterest',
                    'linkedin'  => 'Linkedin'
                ),
                'sortable' => true,
                'default'  => array( 'facebook', 'gplus', 'twitter', 'pinterest', 'linkedin' ),
            ),
        )
    ));
      Redux::setSection( $opt_name, array(
        'title' => esc_html__( 'Search Page', 'piko-construct' ),
        'subsection' => true,
        'fields'     => array(                      
            array(
                'id'       => 'optn_search_sidebar_pos',
                'type'     => 'image_select',
                'title'    => esc_html__('Blog single Layout', 'piko-construct'),
                'subtitle' => esc_html__( 'Select Page layout No Sidebar, Left, Right and both', 'piko-construct' ),
                'options'  => array(
                    'fullwidth'      => array(
                        'alt'   => 'Full Width', 
                        'img'   => ReduxFramework::$_url.'assets/img/1col.png'
                    ),
                    'left'      => array(
                        'alt'   => 'Left sidebar', 
                        'img'   => ReduxFramework::$_url.'assets/img/2cl.png'
                    ),
                    'right'      => array(
                        'alt'   => 'Right sidebar', 
                        'img'  => ReduxFramework::$_url.'assets/img/2cr.png'
                    ),
                    'both'      => array(
                        'alt'   => 'Both sidebar', 
                        'img'  => ReduxFramework::$_url.'assets/img/3cm.png'
                    ),
                ),
                'default' => 'fullwidth'
            ),
            array(
                'id' => 'optn_search_sidebar',
                'type' => 'select',
                'title' => esc_html__('Blog single Sidebar', 'piko-construct'),
                'subtitle' => "Choose Your sidebar",
                'data'      => 'sidebars',
                'required' => array( 'optn_search_sidebar_pos', '=', array('left', 'right', 'both'), ),
                'default' => 'sidebar-1',
            ),
            array(
                'id' => 'optn_search_sidebar_left',
                'type' => 'select',
                'title' => esc_html__('Page sidebar left', 'piko-construct'),
                'subtitle' => "Choose Your sidebar",
                'data'      => 'sidebars',
                'required' => array( 'optn_search_sidebar_pos', '=', 'both', ),
            ),
            array(
                'id'       => 'optn_search_sidebar_width',
                'type'     => 'button_set',
                'title'    => esc_html__( 'Sidebar Width', 'piko-construct' ),              
                'default'  => 'small',
                'options'  => array(
                        'large' => esc_html__( 'Large(1/4)', 'piko-construct' ),
                        'small' => esc_html__( 'Small(1/3)', 'piko-construct' ),
                ),
                'required' => array( 'optn_search_sidebar_pos', '=', array('left', 'right', 'both'), ),
            ),
             array(
                'id' => 'optn_search_except_word',
                'type' => 'text',
                'title' => esc_html__('Excerpt Word', 'piko-construct'),
                'default' => 55,
            ), 
        )
    ));   
    //portfolio style    
    Redux::setSection( $opt_name, array(
        'title'   => esc_html__( 'Custom Post type', 'piko-construct' ),        
        'icon'    => 'el el-screenshot',
        'fields'  => array(
                    array(
                        'id' => 'optn_cpt_disable',
                        'type' => 'checkbox',
                        'title' => esc_html__('Disable Custom Post Types', 'piko-construct'),
                        'subtitle' => esc_html__( 'Check is Disable or Uncheck is Enable', 'piko-construct' ),
                        'desc' => esc_html__('If you do not needs any custom post just check, Then reloaded WP Admin panel.', 'piko-construct'),
                        'options' => array(
                            'portfolio' => 'Portfolio',
                            'service' => 'Service',
                            'ourteam' => 'Our Team',
                            'testimonial' => 'Testimonial'                            
                        ),
                        'default' => array(
                            'portfolio' => '0',
                            'service' => '0',
                            'ourteam' => '0',
                            'testimonial' => '0'
                            
                        )
                    ),
              )
        )); 
    Redux::setSection( $opt_name, array(
        'title'   => esc_html__( 'Portfolio Setting', 'piko-construct' ),
        'icon'    => 'el el-th-large',
        'subsection' => true,
        'fields'  => array(                  
                        array(
                            'id' => 'portfolio_disable_link_detail',
                            'type' => 'button_set',
                            'title' => esc_html__('Disable link to detail', 'piko-construct'),
                            'subtitle' => esc_html__('Enable/Disable link to detail in Portfolio', 'piko-construct'),
                            'desc' => '',
                            'options' => array('1' => 'On','0' => 'Off'),
                            'default' => '0'
                        ),
                        array(
                                'id' => 'portfolio-single-style',
                                'type' => 'image_select',
                                'title' => esc_html__('Single Portfolio Layout', 'piko-construct'),
                                'subtitle' => esc_html__('Select Single Portfolio Layout', 'piko-construct'),
                                'desc' => '',
                                'options' => array(
                                        'detail-01' => array('title' => '', 'img' => get_template_directory_uri().'/assets/images/theme-options/portfolio-detail-01.jpg'),
                                        'detail-02' => array('title' => '', 'img' => get_template_directory_uri().'/assets/images/theme-options/portfolio-detail-02.jpg'),
                                        'detail-03' => array('title' => '', 'img' => get_template_directory_uri().'/assets/images/theme-options/portfolio-detail-03.jpg'),
                                        'detail-04' => array('title' => '', 'img' => get_template_directory_uri().'/assets/images/theme-options/portfolio-detail-04.jpg'),
                                        'detail-05' => array('title' => '', 'img' => get_template_directory_uri().'/assets/images/theme-options/portfolio-detail-05.jpg'),
                                ),
                                'default' => 'detail-01'
                        ),
                        array(
                            'id'       => 'portfolio_single_sidebar_pos',
                            'type'     => 'image_select',
                            'title'    => esc_html__('Single Portfolio Sideber Position', 'piko-construct'),
                            'subtitle' => esc_html__( 'Select single position Left sidebar and Right sidebar', 'piko-construct' ),
                            'options'  => array(                               
                                'left'      => array(
                                    'alt'   => 'Left sidebar', 
                                    'img'   => ReduxFramework::$_url.'assets/img/2cl.png'
                                ),
                                'right'      => array(
                                    'alt'   => 'Right sidebar', 
                                    'img'  => ReduxFramework::$_url.'assets/img/2cr.png'
                                ),
                            ),
                            'default' => 'right',
                            'required' => array( 'portfolio-single-style', '=', 'detail-05', ),  
                        ),
                        array(
                            'id' => 'portfolio_single_sidebar',
                            'type' => 'select',
                            'title' => esc_html__('Portfolio Single Sidebar', 'piko-construct'),
                            'subtitle' => "Choose Your sidebar",
                            'data'      => 'sidebars',
                            'required' => array( 'portfolio-single-style', '=', 'detail-05', ),                               
                            
                        ),
                        array(
                            'type'  => 'button_set',
                            'title'     => esc_html__( 'AutoPlay', 'piko-construct' ),
                            'id'  => 'autoplay',       
                            'options' => array(
                                'true'  => esc_html__( 'Yes', 'piko-construct' ),
                                'false' => esc_html__( 'No', 'piko-construct' ) 
                            ),
                            'default'         => 'true', 
                            'required' => array( 'portfolio-single-style', '=',  array('detail-01', 'detail-03', 'detail-04', 'detail-05') ), 
                        ),
                    array(
                        'type'  => 'button_set',
                        'title'     => esc_html__( 'Navigation', 'piko-construct' ),
                        'id'  => 'navigation',
                        'subtitle' => esc_html__( "Show buton 'next' and 'prev' buttons.", 'piko-construct' ),
                        'options' => array(
                            'true'  => esc_html__( 'Yes', 'piko-construct' ),
                            'false' => esc_html__( 'No', 'piko-construct' ) 
                        ),
                        'default'         => 'true',
                        'required' => array( 'portfolio-single-style', '=',  array('detail-01', 'detail-03', 'detail-04', 'detail-05') ),
                                                
                    ),                    
                    array(
                        'type'  => 'button_set',
                        'title'     => esc_html__( 'Bullets', 'piko-construct' ),
                        'id'  => 'dots',
                        'subtitle' => esc_html__( "Show Carousel bullets bottom", 'piko-construct' ),
                        'options' => array(
                            'true'  => esc_html__( 'Yes', 'piko-construct' ),
                            'false' => esc_html__( 'No', 'piko-construct' ) 
                        ),
                        'default'         => 'false', 
                        'required' => array( 'portfolio-single-style', '=',  array('detail-01', 'detail-03', 'detail-04', 'detail-05') ),
                    ),
                    array(
                        'type'  => 'button_set',
                        'title'     => esc_html__( 'Loop', 'piko-construct' ),
                        'id'  => 'loop',
                        'subtitle' => esc_html__( "Inifnity loop. Duplicate last and first items to get loop illusion.", 'piko-construct' ),
                        'options' => array(
                            'true'  => esc_html__( 'Yes', 'piko-construct' ),
                            'false' => esc_html__( 'No', 'piko-construct' ) 
                        ),
                        'default'         => 'false',
                        'required' => array( 'portfolio-single-style', '=',  array('detail-01', 'detail-03', 'detail-04', 'detail-05') ),
                        ),
                    array(
                        "type"        => "text",
                        "title"     => esc_html__("Slide Speed", 'piko-construct'),
                        "subtitle" => esc_html__('Slide speed in milliseconds', 'piko-construct'),
                        "id"  => "slidespeed",
                        "default"       => "200",
                        'validate' => 'numeric',
                        'required' => array( 'portfolio-single-style', '=',  array('detail-01', 'detail-03', 'detail-04', 'detail-05') ),                        
                            ),
                    array(
                        'type'  => 'select',
                        'title'     => esc_html__( 'Select Animation', 'piko-construct' ),
                        'id'  => 'sanam',
                        'options' => array(
                            '' =>          esc_html__( '--- No Animation ---', 'piko-construct' ),
                            'fadeiN'    => esc_html__( 'fadeIn', 'piko-construct' ),
                            'slidedowN' => esc_html__( 'slideInDown', 'piko-construct' ),
                            'bounceRighT' => esc_html__( 'bounceInUp', 'piko-construct' ),
                            'zoomiN'    => esc_html__( 'zoomIn', 'piko-construct' ),
                            'zoomin_2'  => esc_html__( 'zoomIn two', 'piko-construct' ),
                            'zoomInDowN'=> esc_html__( 'zoomInDown', 'piko-construct' ),
                            'fadeInLefT'=> esc_html__( 'fadeInLeft', 'piko-construct' ),
                            'fadeInuP'  => esc_html__( 'fadeInUp', 'piko-construct' ),
                            'slideInuP' => esc_html__( 'slideInUp', 'piko-construct' )  ,
                        ),
                        'required' => array( 'portfolio-single-style', '=',  array('detail-01', 'detail-03', 'detail-04', 'detail-05') ),
                                              
                    ),
                    array(
                        'id'      => 'optn_portfolio_social_shear',
                        'type'    => 'switch',
                        'title'   => esc_html__( 'Disable Portfolio Social Shear', 'piko-construct' ),
                        'subtitle' => esc_html__( 'Enable or Disable Social shear', 'piko-construct' ),
                        'default' => 1,
                        'on' => esc_html__( 'Enable', 'piko-construct' ),
                        'off' => esc_html__( 'Disabled', 'piko-construct' ),
                    ),
            
            
            
                    array(
                        'id'       => 'show_portfolio_related',
                        'type'     => 'switch',
                        'title'    => esc_html__( 'Disable Related Portfolio', 'piko-construct' ),
                        'subtitle' => esc_html__( 'Enable or Disable related in single portfolio', 'piko-construct' ),
                        'default' => 1,
                        'on' => esc_html__( 'Enable', 'piko-construct' ),
                        'off' => esc_html__( 'Disabled', 'piko-construct' ),

                    ),
                     array (
                        'id' => 'portfolio_related_option',
                        'icon' => true,
                        'type' => 'info',
                        'required'  => array('show_portfolio_related', '=', array('1')),
                        'raw' => '<h3 style="margin: 0;">' . esc_html__('Related Portfolio Configuration', 'piko-construct') .'</h3>',
                    ),
                    array(
                        'id' => 'portfolio_related_style',
                        'type' => 'button_set',
                        'title' => esc_html__('Select portfolio related style', 'piko-construct'),
                        'options' => array(
                            'grid' => esc_html__('Overlay Grid', 'piko-construct'),
                            'title' => esc_html__('Bottom Title', 'piko-construct')
                        ),
                        'default' => 'grid',
                        'required'  => array('show_portfolio_related', '=', array('1'))
                    ),
            
                    array(
                        'id' => 'portfolio-related-overlay',
                        'type' => 'select',
                        'title' => esc_html__('Portfolio Related Orverlay Style', 'piko-construct'),
                        'subtitle' => esc_html__('Select Portfolio Related Orverlay Style.  Only apply for Portfolio related style is grid', 'piko-construct'),
                        'desc' => '',                        
                        'options' => array(
                            'icon' =>  esc_html__('Icon', 'piko-construct'),
                            'title' =>   esc_html__('Title', 'piko-construct'),
                            'title-category' => esc_html__('Title + Category', 'piko-construct'),
                            'title-category-link'  => esc_html__('Title + Category + Link button', 'piko-construct'),
                            'title-excerpt'  => esc_html__('Title + Excerpt', 'piko-construct'),
                            'title-excerpt-link' => esc_html__('Title + Excerpt + Link button + Align center', 'piko-construct'),
                            'left-title-excerpt-link' => esc_html__('Title + Excerpt + Link button + Align left', 'piko-construct'),
                        ),
                        'default' => 'icon',
                        'required'  => array('portfolio_related_style', '=', array('grid'))
                    ),
                    array(
                        'id' => 'portfolio-related-overlay-zoom',                
                        'type'    => 'switch',
                        'title'   => esc_html__( 'Overlay Zoom effect', 'piko-construct' ),
                        'default' => '0',
                        'on' => esc_html__( 'Yes', 'piko-construct' ),
                        'off' => esc_html__( 'No', 'piko-construct' ),
                        'required'  => array('show_portfolio_related', '=', array('1'))
                    ),
                    array(
                        "type"  => "text",
                        "id"  => "p_related_margin",
                        "default"       => "0",
                        "title" => esc_html__('Margin Right(px) on item', 'piko-construct'),
                        "subtitle" => esc_html__('Margin Right(px) on item', 'piko-construct'),
                        'required'  => array('show_portfolio_related', '=', array('1'))
                      ),
                    array(
                        "type"        => "text",
                        "desc"     => esc_html__("The items on large Device (Screen resolution of device >= 1200px )", 'piko-construct'),
                        'subtitle' => esc_html__('Responsive Portfolio Related Column.', 'piko-construct'),
                        "id"  => "p_related_items_large_device",
                        "default"       => "4",
                        "title" => esc_html__('Large desktop Column', 'piko-construct'),
                        'required'  => array('show_portfolio_related', '=', array('1'))
                      ),
                    array(
                        "type"        => "text",
                        "desc"     => esc_html__("The items on desktop (Screen resolution of device >= 992px < 1199px )", 'piko-construct'),                                
                        "default"       => "3",
                        'subtitle' => esc_html__('Responsive Portfolio Related Column.', 'piko-construct'),
                        "id"  => "p_related_items_desktop",
                        "title" => esc_html__('Desktop Column', 'piko-construct'),
                        'validate' => 'numeric',
                        'required'  => array('show_portfolio_related', '=', array('1'))
                      ),
                    array(
                        "type"        => "text",
                        "desc"     => esc_html__("The items on tablet (Screen resolution of device >=768px and < 992px )", 'piko-construct'),
                        'subtitle' => esc_html__('Responsive Portfolio Related Column.', 'piko-construct'),
                        "id"  => "p_related_items_tablet",
                        "default"       => "2",
                        "title" => esc_html__('Tablet Column', 'piko-construct'),
                        'required'  => array('show_portfolio_related', '=', array('1'))
                    ),
                    array(
                        "type"        => "text",
                        "desc"     => esc_html__("The items on mobile (Screen resolution of device < 768px)", 'piko-construct'),
                        'subtitle' => esc_html__('Responsive Portfolio Related Column.', 'piko-construct'),
                        "id"  => "p_related_items_mobile",
                        "default"       => "1",
                        "title" => esc_html__('Mobile Column', 'piko-construct'),
                        'required'  => array('show_portfolio_related', '=', array('1'))
                    ),
                )
        )); 
    Redux::setSection( $opt_name, array(
        'title'   => esc_html__( 'Service Setting', 'piko-construct' ),
        'icon'    => 'el el-photo-alt',
        'subsection' => true,
        'fields'  => array(
            
            
             array(
                'id'       => 'optn_archive_service_sidebar_pos',
                'type'     => 'image_select',
                'title'    => esc_html__('Service Layout', 'piko-construct'),
                'subtitle' => esc_html__( 'Select Page layout No Sidebar, Left, Right and both', 'piko-construct' ),
                'options'  => array(
                    'fullwidth'      => array(
                        'alt'   => 'Full Width', 
                        'img'   => ReduxFramework::$_url.'assets/img/1col.png'
                    ),
                    'left'      => array(
                        'alt'   => 'Left sidebar', 
                        'img'   => ReduxFramework::$_url.'assets/img/2cl.png'
                    ),
                    'right'      => array(
                        'alt'   => 'Right sidebar', 
                        'img'  => ReduxFramework::$_url.'assets/img/2cr.png'
                    ),
                    'both'      => array(
                        'alt'   => 'Both sidebar', 
                        'img'  => ReduxFramework::$_url.'assets/img/3cm.png'
                    ),
                ),
                'default' => 'left'                
            ),
            array(
                'id' => 'optn_archive_service_sidebar',
                'type' => 'select',
                'title' => esc_html__('Service Sidebar', 'piko-construct'),
                'subtitle' => "Choose Your sidebar",
                'data'      => 'sidebars',
                'default' => 'sidebar-6',
                'required' => array( 'optn_archive_service_sidebar_pos', '=', array('left', 'right', 'both'), ),
            ),
            array(
                'id' => 'optn_archive_service_sidebar_left',
                'type' => 'select',
                'title' => esc_html__('Service sidebar left', 'piko-construct'),
                'subtitle' => "Choose Your sidebar",
                'data'      => 'sidebars',                
                'required' => array( 'optn_archive_service_sidebar_pos', '=', 'both', ),
            ),
            array(
                'id'       => 'optn_archive_service_sidebar_width',
                'type'     => 'button_set',
                'title'    => esc_html__( 'Sidebar Width', 'piko-construct' ),              
                'default'  => 'small',
                'options'  => array(
                        'large' => esc_html__( 'Large(1/4)', 'piko-construct' ),
                        'small' => esc_html__( 'Small(1/3)', 'piko-construct' ),
                ),
                'required' => array( 'optn_archive_service_sidebar_pos', '=', array('left', 'right', 'both'), ),
            ),	        
             array(
                'id' => 'optn_archive_service_display_columns',
                'type' => 'select',
                'title' => esc_html__('Archive Display Columns', 'piko-construct'),
                'subtitle' => esc_html__('Choose the number of columns to display on archive pages.','piko-construct'),
                'options' => array(
                        '2'		=> '2',
                        '3'		=> '3',
                        '4'		=> '4',
                ),
                'default' => '2',
            ),
            array(
                'id' => 'opt_service_overlay_style',
                'type' => 'button_set',
                'title' => esc_html__('Thumbnail overlay effect', 'piko-construct'),
                'options' => array(
                        ''=> esc_html__('Fade', 'piko-construct'),
                        'overlay-style-2' => esc_html__('Zoom', 'piko-construct'),
                ),
                'default' => '',
            ),
            array(
                'id' => 'opt_service_btn_style',
                'type' => 'button_set',
                'title' => esc_html__('button hover effect', 'piko-construct'),
                'options' => array(
                        'advance'=> esc_html__('Default', 'piko-construct'),
                        'default' => esc_html__('Advance', 'piko-construct'),
                ),
                'default' => 'advance',
            ),
            array(
                'id' => 'opt_service_btn_hover_effect',
                'type' => 'select',
                'title' => esc_html__('button hover effect', 'piko-construct'),
                'options' => array(
                    '0'		=> esc_html__('left to right', 'piko-construct'),
                    '1'		=> esc_html__('Swipe left', 'piko-construct'),
                    '2'		=> esc_html__('Swipe bottom', 'piko-construct'),
                    '3'		=> esc_html__('Swipe corner', 'piko-construct'),
                    '4'		=> esc_html__('Swipe center', 'piko-construct'),
                    '5'		=> esc_html__('Swipe corner 2', 'piko-construct'),
                    '6'		=> esc_html__('Swipe top bottom', 'piko-construct'),
                    '7'		=> esc_html__('Swipe corner 3', 'piko-construct'),
                    '8'		=> esc_html__('Shuffle corner', 'piko-construct'),
                    '9'		=> esc_html__('Shuffle top bottom', 'piko-construct'),
                    '10'	=> esc_html__('Aware', 'piko-construct')
                ),
                'default' => '4',
                'required' => array( 'opt_service_btn_style', '=', array('default' ) ), 
            ),
            array(
                'id' => 'opt_service_btn_size',
                'type' => 'button_set',
                'title' => esc_html__('Button hover effect', 'piko-construct'),
                'options' => array(
                        'small'=> esc_html__('Small', 'piko-construct'),
                        'medium' => esc_html__('Medium', 'piko-construct'),
                ),
                'default' => 'medium',
                'required' => array( 'opt_service_btn_style', '=', array('default' ) ), 
            ),
            array(
                'id' => 'opt_service_btn_color',
                'type'     => 'select',
                'multi'    => true,
                'title' => esc_html__('Button more', 'piko-construct'),
                'options' => array(
                        'white'=> esc_html__('Border White', 'piko-construct'),
                        'btn-center' => esc_html__('Button center', 'piko-construct'),
                ),
                'default' => '',
                'required' => array( 'opt_service_btn_style', '=', array('default' ) ), 
            ),
            array(
                'id' => 'opt_service_continue_reading',
                'type' => 'text',
                'title' => esc_html__('Read More Button Text', 'piko-construct'),
                'default' => 'Read More',
            ),
            array(
                'id' => 'optn_archive_service_except_word',
                'type' => 'text',
                'title' => esc_html__('Excerpt Word', 'piko-construct'),
                'default' => '25',
            ), 
             array (
                    'id' => 'service_single_configure',
                    'icon' => true,
                    'type' => 'info',
                    'raw' => '<h3 style="margin: 0;">Service single page Configuration</h3>',
            ),
            array(
                'id'       => 'service_single_sidebar_pos',
                'type'     => 'image_select',
                'title'    => esc_html__('Service Single  Sideber Position', 'piko-construct'),
                'subtitle' => esc_html__( 'Select single position Left sidebar and Right sidebar', 'piko-construct' ),
                'options'  => array(
                                'fullwidth'      => array(
                                    'alt'   => 'Full Width', 
                                    'img'   => ReduxFramework::$_url.'assets/img/1col.png'
                                ),
                                'left'      => array(
                                    'alt'   => 'Left sidebar', 
                                    'img'   => ReduxFramework::$_url.'assets/img/2cl.png'
                                ),
                                'right'      => array(
                                    'alt'   => 'Right sidebar', 
                                    'img'  => ReduxFramework::$_url.'assets/img/2cr.png'
                                ),
                            ),
                            'default' => 'right', 
            ),
                array(
                        'id' => 'service-single-style',
                        'type' => 'image_select',
                        'title' => esc_html__('Single service Layout', 'piko-construct'),
                        'subtitle' => esc_html__('Select Single service Layout', 'piko-construct'),
                        'desc' => '',                                
                        'options'  => array(
                            'detail-01' 	=> array('alt' => 'style 1 ',      'img' => get_template_directory_uri() . '/assets/images/theme-options/service_single_layout_1.jpg'),
                            'detail-02' 	=> array('alt' => 'style 2 ',      'img' => get_template_directory_uri() . '/assets/images/theme-options/service_single_layout_2.jpg'),
                            'detail-03' 	=> array('alt' => 'style 3 ',      'img' => get_template_directory_uri() . '/assets/images/theme-options/service_single_layout_3.jpg'),
                            'detail-04' 	=> array('alt' => 'style 4 ',      'img' => get_template_directory_uri() . '/assets/images/theme-options/service_single_layout_4.jpg'),
                        ),
                        'default' => 'detail-01'
                ),                        
                array(
                    'id' => 'service_single_sidebar',
                    'type' => 'select',
                    'title' => esc_html__('service Single Sidebar', 'piko-construct'),
                    'subtitle' => "Choose Your sidebar",
                    'data'      => 'sidebars',
                    'required' => array( 'service_single_sidebar_pos', '=', array('left', 'right' ), ),                               

                ),
                array(
                    'type'  => 'button_set',
                    'title'     => esc_html__( 'AutoPlay', 'piko-construct' ),
                    'id'  => 'lb_autoplay',       
                    'options' => array(
                        'true'  => esc_html__( 'Yes', 'piko-construct' ),
                        'false' => esc_html__( 'No', 'piko-construct' ) 
                    ),
                    'default'         => 'true',                                             
                ),
            array(
                'type'  => 'button_set',
                'title'     => esc_html__( 'Navigation', 'piko-construct' ),
                'id'  => 'lb_navigation',
                'subtitle' => esc_html__( "Show buton 'next' and 'prev' buttons.", 'piko-construct' ),
                'options' => array(
                    'true'  => esc_html__( 'Yes', 'piko-construct' ),
                    'false' => esc_html__( 'No', 'piko-construct' ) 
                ),
                'default'         => 'true',

            ),                    
            array(
                'type'  => 'button_set',
                'title'     => esc_html__( 'Bullets', 'piko-construct' ),
                'id'  => 'lb_dots',
                'subtitle' => esc_html__( "Show Carousel bullets bottom", 'piko-construct' ),
                'options' => array(
                    'true'  => esc_html__( 'Yes', 'piko-construct' ),
                    'false' => esc_html__( 'No', 'piko-construct' ) 
                ),
                'default'         => 'false',                                              
            ),
            array(
                'type'  => 'button_set',
                'title'     => esc_html__( 'Loop', 'piko-construct' ),
                'id'  => 'lb_loop',
                'subtitle' => esc_html__( "Inifnity loop. Duplicate last and first items to get loop illusion.", 'piko-construct' ),
                'options' => array(
                    'true'  => esc_html__( 'Yes', 'piko-construct' ),
                    'false' => esc_html__( 'No', 'piko-construct' ) 
                ),
                'default'         => 'false',


                ),
            array(
                "type"        => "text",
                "title"     => esc_html__("Slide Speed", 'piko-construct'),
                "subtitle" => esc_html__('Slide speed in milliseconds', 'piko-construct'),
                "id"  => "lb_slidespeed",
                "default"       => "200",
                'validate' => 'numeric',                        
            ),
            array(
                "type"  => "text",
                "id"  => "lb_margin",
                "default"       => "30",
                "title" => esc_html__('Margin Right(px) on item', 'piko-construct'),
                "subtitle" => esc_html__('Margin Right(px) on item', 'piko-construct'),
              ),
            array(
                'id'       => 'lb_use_responsive',
                'type'     => 'switch',
                'title'    => esc_html__( 'Enable multi column slide', 'piko-construct' ),
                'subtitle' => esc_html__( 'Enable or Disable single slide or multi column slide', 'piko-construct' ),
                'default' => 1,
                'on' => esc_html__( 'Enable', 'piko-construct' ),
                'off' => esc_html__( 'Disabled', 'piko-construct' ),
            ),
            array(
                'type'  => 'select',
                'title'     => esc_html__( 'Select Animation', 'piko-construct' ),
                'id'  => 'lb_sanam',
                'options' => array(
                    '' =>          esc_html__( '--- No Animation ---', 'piko-construct' ),
                    'fadeiN'    => esc_html__( 'fadeIn', 'piko-construct' ),
                    'slidedowN' => esc_html__( 'slideInDown', 'piko-construct' ),
                    'bounceRighT' => esc_html__( 'bounceInUp', 'piko-construct' ),
                    'zoomiN'    => esc_html__( 'zoomIn', 'piko-construct' ),
                    'zoomin_2'  => esc_html__( 'zoomIn two', 'piko-construct' ),
                    'zoomInDowN'=> esc_html__( 'zoomInDown', 'piko-construct' ),
                    'fadeInLefT'=> esc_html__( 'fadeInLeft', 'piko-construct' ),
                    'fadeInuP'  => esc_html__( 'fadeInUp', 'piko-construct' ),
                    'slideInuP' => esc_html__( 'slideInUp', 'piko-construct' )  ,
                ),
                'required'  => array('lb_use_responsive', '=', '0')
            ),        

            array(
                "type"        => "text",
                "desc"     => esc_html__("The items on large Device (Screen resolution of device >= 1200px )", 'piko-construct'),
                'subtitle' => esc_html__('Responsive service Related Column.', 'piko-construct'),
                "id"  => "lb_items_large_device",
                "default"       => "4",
                "title" => esc_html__('Large desktop Column', 'piko-construct'),
                'required'  => array('lb_use_responsive', '=', '1')
              ),
            array(
                "type"        => "text",
                "desc"     => esc_html__("The items on desktop (Screen resolution of device >= 992px < 1199px )", 'piko-construct'),                                
                "default"       => "3",
                'subtitle' => esc_html__('Responsive service Related Column.', 'piko-construct'),
                "id"  => "lb_items_desktop",
                "title" => esc_html__('Desktop Column', 'piko-construct'),
                'validate' => 'numeric',
                'required'  => array('lb_use_responsive', '=', '1')
              ),
            array(
                "type"        => "text",
                "desc"     => esc_html__("The items on tablet (Screen resolution of device >=768px and < 992px )", 'piko-construct'),
                'subtitle' => esc_html__('Responsive service Related Column.', 'piko-construct'),
                "id"  => "lb_items_tablet",
                "default"       => "2",
                "title" => esc_html__('Tablet Column', 'piko-construct'),
                'required'  => array('lb_use_responsive', '=', '1')
            ),
            array(
                "type"        => "text",
                "desc"     => esc_html__("The items on mobile (Screen resolution of device < 768px)", 'piko-construct'),
                'subtitle' => esc_html__('Responsive service Related Column.', 'piko-construct'),
                "id"  => "lb_items_mobile",
                "default"       => "1",
                "title" => esc_html__('Mobile Column', 'piko-construct'),
                'required'  => array('lb_use_responsive', '=', '1')
            ),
        )
    ));
    //our team    
    Redux::setSection( $opt_name, array(
        'title'   => esc_html__( 'Our team Setting', 'piko-construct' ),
        'icon'    => 'el el-photo-alt',
        'subsection' => true,
        'fields'  => array(
            array(
                    'id' => 'ourteam-single-layout',
                    'type' => 'image_select',
                    'title' => esc_html__('Ourteam Single Layout', 'piko-construct'),
                    'subtitle' => esc_html__('Select Single team Layout', 'piko-construct'),
                    'desc' => '',                                
                    'options'  => array(
                        '1' 	=> array('alt' => 'style 1 ',      'img' => get_template_directory_uri() . '/assets/images/theme-options/team-single1.jpg'),
                        '2' 	=> array('alt' => 'style 2 ',      'img' => get_template_directory_uri() . '/assets/images/theme-options/team-single2.jpg'),
                        '3' 	=> array('alt' => 'style 3 ',      'img' => get_template_directory_uri() . '/assets/images/theme-options/team-single3.jpg'),
                    ),
                    'default' => '1'
                ),
            array(
                'id'       => 'team_single_sidebar_pos',
                'type'     => 'image_select',
                'title'    => esc_html__('Single page sidebar', 'piko-construct'),
                'subtitle' => esc_html__( 'Select Page layout No Sidebar, Left, Right', 'piko-construct' ),
                'options'  => array(
                    'fullwidth'      => array(
                        'alt'   => 'Full Width', 
                        'img'   => ReduxFramework::$_url.'assets/img/1col.png'
                    ),
                    'left'      => array(
                        'alt'   => 'Left sidebar', 
                        'img'   => ReduxFramework::$_url.'assets/img/2cl.png'
                    ),
                    'right'      => array(
                        'alt'   => 'Right sidebar', 
                        'img'  => ReduxFramework::$_url.'assets/img/2cr.png'
                    ),
                ),
                'required'  => array('ourteam-single-layout', '=', '3'),
                'default' => 'fullwidth'
            ),
            array(
                'id' => 'team_single_sidebar',
                'type' => 'select',
                'title' => esc_html__('Team single Sidebar', 'piko-construct'),
                'subtitle' => "Choose Your sidebar",
                'data'      => 'sidebars',
                'default' => '',
                'required' => array( 'team_single_sidebar_pos', '=', array('left', 'right'), ),
            ),
            )
        )); 
    
    /**
   * Check if WooCommerce is active
    **/
    if ( class_exists( 'WooCommerce' ) ) {
        Redux::setSection( $opt_name, array(
        'title' => esc_html__( 'Woocommerce', 'piko-construct' ),
        'icon'  => 'el el-shopping-cart',
        'fields'     => array(
            array(
                'id'      => 'woo_archive_slide_enable',
                'type'    => 'switch',
                'title'   => esc_html__( 'Archive Slider', 'piko-construct' ),                 
                'default' => 1,
                'on' => esc_html__( 'Enable', 'piko-construct' ),
                'off' => esc_html__( 'Disabled', 'piko-construct' ),
            ),
            array(
                "type"        => "text",
                "title" => esc_html__('Put the slider shortcode', 'piko-construct'),
                'subtitle'  => esc_html__('Enter Slider Revolution shortcode', 'piko-construct'),
                'desc'  => esc_html__('Like as: [rev_slider alias="woocommerce archive"]', 'piko-construct'),
                "id"  => "woo_archive_side_shortcode",
                'required'  => array('woo_archive_slide_enable', '=', 1)
            ),                        
            array( 
                'id'       => 'product-border',
                'type'     => 'border',
                'title'    => esc_html__('Product border Option', 'piko-construct'),
                'subtitle' => esc_html__('if your product background color white', 'piko-construct'),
                'output'   => array('.products .product figure, .product-zoom-wrapper .product-zoom-container, .single-product .product-gallery-carousel .swiper-slide img'),
                'default'  => array(
                    'border-color'  => '#e9e9e9', 
                    'border-style'  => 'solid', 
                    'border-top'    => '1px', 
                    'border-right'  => '1px', 
                    'border-bottom' => '1px', 
                    'border-left'   => '1px'
                )
            ),
            array (
                'title' => esc_html__('Catalog Mode', 'piko-construct'),
                'subtitle' => esc_html__('Enable / Disable the Catalog Mode.', 'piko-construct'),
                'desc' => esc_html__('When enabled, the feature Turns Off the shopping functionality of compare view.', 'piko-construct'),
                'id' => 'catalog_mode',
                'on' => esc_html__('Enabled', 'piko-construct'),
                'off' => esc_html__('Disabled', 'piko-construct'),
                'type' => 'switch',
                'default'  => false,
            ),            
            array (
                    'title' => esc_html__('Topbar tool Products per Page on Grid Allowed Values', 'piko-construct'),
                    'desc' => esc_html__('Comma-separated.','piko-construct'),
                    'id' => 'products_per_page',
                    'type' => 'text',
                    'default' => '20,25,35'
            ),
            array (
                    'title' => esc_html__('Topbar tool Products per Page On Grid Default', 'piko-construct'),
                    'id' => 'products_per_page_default',
                    'min' => '1',
                    'step' => '1',
                    'max' => '80',
                    'type' => 'slider',
                    'edit' => '1',
                    'default' => '20',
            ),
            array (
                    'title' => esc_html__('Topbar tool Products per Page on List Allowed Values', 'piko-construct'),
                    'desc' => esc_html__('Comma-separated.','piko-construct'),
                    'id' => 'products_per_page_list',
                    'type' => 'text',
                    'default' => '20,25,35'
            ),
            array (
                    'title' => esc_html__('Topbar tool Products per Page On List Default', 'piko-construct'),
                    'id' => 'products_per_page_list_default',
                    'min' => '1',
                    'step' => '1',
                    'max' => '80',
                    'type' => 'slider',
                    'edit' => '1',
                    'default' => '20',
            ),
            array(
                'id'      => 'optn_show_new_product_label',
                'type'    => 'switch',
                'title'   => esc_html__( 'New product label', 'piko-construct' ),                 
                'default' => 1,
                'on' => esc_html__( 'Enable', 'piko-construct' ),
                'off' => esc_html__( 'Disabled', 'piko-construct' ),
            ),
            array(
                "type"        => "text",
                "title" => esc_html__('How many days show', 'piko-construct'),
                "id"  => "optn_new_product_label",
                "default"       => "30",
                'validate' => 'numeric',
                'required'  => array('optn_show_new_product_label', '=', 1)
            ),
            array(
                "type"        => "text",
                "title" => esc_html__('Label Text', 'piko-construct'),
                "id"  => "optn_new_product_label_text",
                "default"       => "New",                
                'required'  => array('optn_show_new_product_label', '=', 1)
            ),
            array(
                "type"        => "text",
                "title" => esc_html__('Out of stock label', 'piko-construct'),
                'subtitle'     => esc_html__('if empty dont show label', 'piko-construct'),
                "id"  => "optn_product_out_of_stock_label",
                "default"       => "Out of stock",
            ),
            
        )
    ) );
    //woo Breadcrumb
    Redux::setSection( $opt_name, array(
        'title'   => esc_html__( 'Breadcrumbs', 'piko-construct' ),
        'subsection' => true, 
        'fields'  => array(
            array (
                    'title' => esc_html__('Disable Breadcrumbs Layout', 'piko-construct'),
                    'id' => 'woo_breadcrumbs_disable',
                    'on' => esc_html__('Enabled', 'piko-construct'),
                    'off' => esc_html__('Disabled', 'piko-construct'),
                    'type' => 'switch',
                    'default' => 1,
            ),            
            array(
                'id'       => 'woo_breadcrumb_layout',
                'type'     => 'image_select',
                'title'    => esc_html__('Breadcrumb Layout', 'piko-construct'),
                'subtitle' => esc_html__( 'One Cloumn or Two Cloumn', 'piko-construct' ),
                'required' => array( 'woo_breadcrumbs_disable', '=', 1 ),
                'options'  => array(
                    'one_cols'      => array(
                        'alt'   => 'One Cloumn', 
                        'img'   => ReduxFramework::$_url.'assets/img/1col.png'
                    ),
                    'two_cols'      => array(
                        'alt'   => 'Two Cloumn', 
                        'img'   => get_template_directory_uri() . '/assets/images/theme-options/2columns.png'
                    ),                    
                ),
                'default' => 'two_cols'
            ),
            array(
                'id' => 'optn_archive_header_img',
                'type' => 'media',
                'url' => true,
                'title' => esc_html__('Header Backgrund Image', 'piko-construct'),
                'compiler' => 'true',                        
                'subtitle' => esc_html__('Upload header Backgrund image', 'piko-construct'),
                'default'  => array(
                            'url' => get_template_directory_uri(). '/assets/images/bg/page-title.jpg'
                ),
                'required' => array( 'woo_breadcrumbs_disable', '=', 1 ),
            ),
            array(
                'id'       => 'optn_header_img_bg_color',
                'type'     => 'color',
                'compiler' => true,
                'title'    => esc_html__( 'Bankground Color', 'piko-construct' ),
                'output'    => array('background-color' => '.page-header.woo-breadcrumb'),
                'default'  => '#f5f5f5',
                'required' => array( 'woo_breadcrumbs_disable', '=', 1 ),

            ),
             array(
                'id'      => 'woo_breadcrumb_layout_title',
                'type'    => 'button_set',
                'title'   => esc_html__( 'Title Align', 'piko-construct' ),                    
                'options' => array(
                    'title-left' => esc_html__( 'Left', 'piko-construct' ), 
                    'title-right' => esc_html__( 'Right', 'piko-construct' ),
                    ), 
                'default' => 'title-left',
                'required' => array( 'woo_breadcrumb_layout', '=', 'two_cols' ),
            ),
                array (
                    'title' => esc_html__('Disable title', 'piko-construct'),
                    'id' => 'woo_page_header_title',
                    'on' => esc_html__('Enabled', 'piko-construct'),
                    'off' => esc_html__('Disabled', 'piko-construct'),
                    'type' => 'switch',
                    'default' => 1,
                    'required' => array( 'woo_breadcrumbs_disable', '=', 1 ),
                ),
                array(
                        'id'             => 'woo_page-header_font_size',
                        'type'           => 'typography',
                        'title'          => esc_html__( 'Page title', 'piko-construct' ),
                        'subtitle' => esc_html__( 'Font size and color #333333', 'piko-construct' ),
                        'compiler'       => true,
                        'google'         => false,
                        'font-backup'    => false,
                        'all_styles'     => true,
                        'font-weight'    => false,
                        'font-family'    => true,
                        'text-align'     => false,
                        'font-style'     => true,
                        'subsets'        => false,
                        'font-size'      => true,
                        'line-height'    => true,
                        'word-spacing'   => false,
                        'letter-spacing' => false,
                        'color'          => true,
                        'preview'        => true,
                        'output'         => array( '.page-header.woo-breadcrumb h1' ),
                        'units'          => 'px',
                        'default'        => array(
                                'font-size' => '18px',
                                'color' => '',
                        ),
                        'required' => array( 'woo_page_header_title', '=', 1 ),
                ),
                array (
                    'title' => esc_html__('Disable Breadcrumb', 'piko-construct'),
                    'id' => 'woo_disable_breadcrubm',
                    'on' => esc_html__('Enabled', 'piko-construct'),
                    'off' => esc_html__('Disabled', 'piko-construct'),
                    'type' => 'switch',
                    'default' => 1,
                    'required' => array( 'woo_breadcrumbs_disable', '=', 1 ),
                ),                
                array(
                    'id'             => 'woo_page_title_padding',
                    'type'           => 'spacing',
                    'mode'           => 'padding',
                    'units'          => 'px',
                    'units_extended' => 'false',
                    'title'          => esc_html__('Breadcrumb padding', 'piko-construct'),
                    'subtitle'       => esc_html__('This must be numeric (no px). Leave for default.', 'piko-construct'),
                    'left'          => false,
                    'right'          => false,
                    'output'        => array('.page-header.woo-breadcrumb'),
                    'default'            => array(
                        'padding-top'     => '18px',
                        'padding-bottom'  => '18px',
                        'units'          => 'px',
                    ),
                    'required' => array( 'woo_breadcrumbs_disable', '=', 1 ),
                ),
            array(
                'id'       => 'woo_header_title_text_align',
                'type'     => 'button_set',
                'multi'    => false,
                'title'    => esc_html__('Breadcrubm Text Align', 'piko-construct'),
                'options'  => array(
                    'left'   => esc_html__( 'Left', 'piko-construct' ),
                    'center'  => esc_html__( 'Center', 'piko-construct' ),
                    'right'  => esc_html__( 'Right', 'piko-construct' ),                            
                ),
                'default'   => 'center',
                'required' => array( 'woo_breadcrumb_layout', '=', 'one_cols' ),
            ),
            
            array (
                    'id' => 'woo_single_divide',
                    'icon' => true,
                    'type' => 'info',
                    'raw' => '<h3 style="margin: 0;">Single Page Breadcrumbs Configure</h3>',
            ),
            array (
                    'title' => esc_html__('Disable single Breadcrumbs Layout', 'piko-construct'),
                    'id' => 'woo_single_breadcrumbs_disable',
                    'on' => esc_html__('Enabled', 'piko-construct'),
                    'off' => esc_html__('Disabled', 'piko-construct'),
                    'type' => 'switch',
                    'default' => 1,
            ),
            array(
                'id'       => 'woo_single_breadcrumb_layout',
                'type'     => 'image_select',
                'title'    => esc_html__('Breadcrumb Layout', 'piko-construct'),
                'subtitle' => esc_html__( 'One Cloumn or Two Cloumn', 'piko-construct' ),
                'required' => array( 'woo_single_breadcrumbs_disable', '=', 1 ),
                'options'  => array(
                    'one_cols'      => array(
                        'alt'   => 'One Cloumn', 
                        'img'   => ReduxFramework::$_url.'assets/img/1col.png'
                    ),
                    'two_cols'      => array(
                        'alt'   => 'Two Cloumn', 
                        'img'   => get_template_directory_uri() . '/assets/images/theme-options/2columns.png'
                    ),                    
                ),
                'default' => 'one_cols'
            ),
            array (
                'title' => esc_html__('Disable single page title', 'piko-construct'),
                'id' => 'woo_single_header_title',
                'on' => esc_html__('Enabled', 'piko-construct'),
                'off' => esc_html__('Disabled', 'piko-construct'),
                'type' => 'switch',
                'default' => 0,
                'required' => array( 'woo_single_breadcrumbs_disable', '=', 1 ),
            ),
            array(
                'id'             => 'woo_single_page-header_font_size',
                'type'           => 'typography',
                'title'          => esc_html__( 'Single Page title', 'piko-construct' ),
                'subtitle' => esc_html__( 'Font size and color', 'piko-construct' ),
                'compiler'       => true,
                'google'         => false,
                'font-backup'    => false,
                'all_styles'     => true,
                'font-weight'    => false,
                'font-family'    => false,
                'text-align'     => false,
                'font-style'     => false,
                'subsets'        => false,
                'font-size'      => true,
                'line-height'    => false,
                'word-spacing'   => false,
                'letter-spacing' => false,
                'color'          => true,
                'preview'        => true,
                'output'         => array( '.page-header.woo-single h1' ),
                'units'          => 'px',
                'default'        => array(
                        'font-size' => '18px',
                        'color' => '#777777',
                ),
                'required' => array( 'woo_single_header_title', '=', 1 ),
            ),
            array(
                'id'       => 'woo_single_header_bg_color',
                'type'     => 'color',
                'compiler' => true,
                'title'    => esc_html__( 'Bankground Color', 'piko-construct' ),
                'output'    => array('background-color' => '.page-header.woo-single'),
                'default'  => 'transparent',
                'required' => array( 'woo_single_breadcrumbs_disable', '=', 1 ),

            ),
            array(
                'id'             => 'woo_single_page_title_padding',
                'type'           => 'spacing',
                'mode'           => 'padding',
                'units'          => 'px',
                'units_extended' => 'false',
                'title'          => esc_html__('Breadcrumb padding', 'piko-construct'),
                'subtitle'       => esc_html__('This must be numeric (no px). Leave for default.', 'piko-construct'),
                'left'          => false,
                'right'          => false,
                'output'        => array('.page-header.woo-single'),
                'default'            => array(
                    'padding-top'     => '15px',
                    'padding-bottom'  => '15px',
                    'units'          => 'px',
                ),
                'required' => array( 'woo_single_breadcrumbs_disable', '=', 1 ),
            ),
            array(
                'id'       => 'woo_single_page_link_color',
                'type'     => 'color',
                'compiler' => true,
                'title'    => esc_html__( 'Single Breadcrumb Link Color', 'piko-construct' ),
                'default'  => '#777777',
                'required' => array( 'woo_single_breadcrumbs_disable', '=', 1 ),
                'output'   => array(
                        'color'    => '.page-header.woo-single .breadcrumb a'
                    )
            ),
            array(
                'id'       => 'woo_single_page_link_hover_color',
                'type'     => 'color',
                'compiler' => true,
                'title'    => esc_html__( 'Breadcrumb Link hover Color', 'piko-construct' ),
                'subtitle' => esc_html__( 'Default white color', 'piko-construct' ),
                'default'  => '#45bf55', 
                'required' => array( 'woo_single_breadcrumbs_disable', '=', 1 ),
                'output'   => array(
                        'color'    => '.page-header.woo-single .breadcrumb a:hover, .page-header.woo-single .breadcrumb a:focus'
                    )
            ),
            array(
                'id'       => 'woo_single_page_link_active',
                'type'     => 'color',
                'compiler' => true,
                'title'    => esc_html__( 'Active  Color', 'piko-construct' ),
                'default'  => '#b0afaf',
                'required' => array( 'woo_single_breadcrumbs_disable', '=', 1 ),
                'output'   => array(
                        'color'    => '.page-header.woo-single .breadcrumb > .current, .page-header.woo-single .breadcrumb .prefix, .page-header.woo-single .woocommerce-breadcrumb'
                    )
            ),
            array(
                'id'       => 'woo_single_header_title_text_align',
                'type'     => 'button_set',
                'multi'    => false,
                'title'    => esc_html__('Breadcrubm single Text Align', 'piko-construct'),
                'options'  => array(
                    'left'   => esc_html__( 'Left', 'piko-construct' ),
                    'center'  => esc_html__( 'Center', 'piko-construct' ),
                    'right'  => esc_html__( 'Right', 'piko-construct' ),                            
                ),
                'default'   => 'left',
                'required' => array( 'woo_single_breadcrumb_layout', '=', 'one_cols' ),
            ),
        )
    ));     
        
        Redux::setSection( $opt_name, array(
        'title' => esc_html__( 'Archive Product', 'piko-construct' ),
        'subsection' => true,    
        'fields'     => array(
            array(
                    'id'       => 'catalog_display_type_global',
                    'type'     => 'button_set',
                    'title'    => esc_html__('Catalog Display Type','piko-construct'),
                    'default' => 'grid',
                    'options'  => array(
                            'grid'      => esc_html__('Grid', 'piko-construct'),
                            'list'      => esc_html__('List', 'piko-construct'),
                    )                    
            ),
            array(
                'id'        => 'optn_woo_products_per_row',
                'type'      => 'image_select',
                'compiler'  => true,
                'title'     => esc_html__('Product Column', 'piko-construct'),
                'options'   => array(
                    '2' => array('alt' => '2 Column ',      'img' => get_template_directory_uri() . '/assets/images/theme-options/2columns.png'),
                    '3' => array('alt' => '3 Column ',      'img' => get_template_directory_uri() . '/assets/images/theme-options/3columns.png'),
                    '4' => array('alt' => '4 Column ',      'img' => get_template_directory_uri() . '/assets/images/theme-options/4columns.png'),
                    '5' => array('alt' => '5 Column ',      'img' => get_template_directory_uri() . '/assets/images/theme-options/5columns.png')
                ),
                'default'   => '3',
                'required' => array( 'catalog_display_type_global', '=', 'grid', ), 
            ),            
            array(
                'id'       => 'optn_product_sidebar_pos',
                'type'     => 'image_select',
                'title'    => esc_html__('Archive Product sidebar', 'piko-construct'),
                'subtitle' => esc_html__( 'Select Page layout No Sidebar, Left, Right and both', 'piko-construct' ),
                'options'  => array(
                    'fullwidth'      => array(
                        'alt'   => 'Full Width', 
                        'img'   => ReduxFramework::$_url.'assets/img/1col.png'
                    ),
                    'left'      => array(
                        'alt'   => 'Left sidebar', 
                        'img'   => ReduxFramework::$_url.'assets/img/2cl.png'
                    ),
                    'right'      => array(
                        'alt'   => 'Right sidebar', 
                        'img'  => ReduxFramework::$_url.'assets/img/2cr.png'
                    ),
                    'both'      => array(
                        'alt'   => 'Both sidebar', 
                        'img'  => ReduxFramework::$_url.'assets/img/3cm.png'
                    ),
                ),
                'default' => 'right'
            ),
            array(
                'id' => 'optn_product_sidebar',
                'type' => 'select',
                'title' => esc_html__('Product Sidebar', 'piko-construct'),
                'subtitle' => "Choose Your sidebar",
                'data'      => 'sidebars',
                'default' => 'sidebar-4',
                'required' => array( 'optn_product_sidebar_pos', '=', array('left', 'right', 'both'), ),
            ),
            array(
                'id' => 'optn_product_sidebar_left',
                'type' => 'select',
                'title' => esc_html__('Product sidebar left', 'piko-construct'),
                'subtitle' => "Choose Your sidebar",
                'data'      => 'sidebars',
                'default'  => 'sidebar-4',
                'required' => array( 'optn_product_sidebar_pos', '=', 'both', ),                
            ),
            array(
                'id'       => 'optn_product_sidebar_width',
                'type'     => 'button_set',
                'title'    => esc_html__( 'Sidebar Width', 'piko-construct' ),              
                'default'  => 'small',                
                'options'  => array(
                        'large' => esc_html__( 'Large(1/4)', 'piko-construct' ),
                        'small' => esc_html__( 'Small(1/3)', 'piko-construct' ),
                ),
                'required' => array( 'optn_product_sidebar_pos', '=', array('left', 'right', 'both'), ),
            ),            
            array(
                'id'      => 'optn_show_header_product_cats',
                'type'    => 'switch',
                'title'   => esc_html__( 'Enable Product category list.', 'piko-construct' ),                 
                'default' => 0,
                'on' => esc_html__( 'Enable', 'piko-construct' ),
                'off' => esc_html__( 'Disabled', 'piko-construct' ),
            ),
            array(
                'id'       => 'optn_header_shop_product_cats',
                'type'     => 'select',
                'multi'    => false,
                'title'    => esc_html__( 'Shop product categories', 'piko-construct' ),
                'options'  => array(
                    'auto' => esc_html__( 'Automatically', 'piko-construct' ),
                    'manually'   => esc_html__( 'Manually', 'piko-construct' ),
                ),
                'default'  => 'auto',
                'desc' => esc_html__( 'Show shop product categories on header', 'piko-construct' ),
                'required' => array('optn_show_header_product_cats', '=', 1),
            ),
            array(
                'id'       => 'optn_woo_product_cats_show_on_archive',
                'type'     => 'text',
                'title'    => esc_html__( 'Product categories show on shop archive', 'piko-construct' ),
                'subtitle' => esc_html__( 'Display product categories menu on shop archive page', 'piko-construct' ),
                'desc'     => esc_html__( 'Enter product categories slug, uses a comma to separate categories slug', 'piko-construct' ),
                'required' => array('optn_header_shop_product_cats', '=', 'manually'),
                'validate' => 'no_html'
            ),            
        )
    ));
    Redux::setSection( $opt_name, array(
        'title' => esc_html__( 'Single Product', 'piko-construct' ),
        'subsection' => true,
        'fields'     => array(                      
            array(
                'id'       => 'optn_product_single_sidebar_pos',
                'type'     => 'image_select',
                'title'    => esc_html__('Product single Layout', 'piko-construct'),
                'subtitle' => esc_html__( 'Select Page layout No Sidebar, Left, Right and both', 'piko-construct' ),
                'options'  => array(
                    'fullwidth'      => array(
                        'alt'   => 'Full Width', 
                        'img'   => ReduxFramework::$_url.'assets/img/1col.png'
                    ),
                    'left'      => array(
                        'alt'   => 'Left sidebar', 
                        'img'   => ReduxFramework::$_url.'assets/img/2cl.png'
                    ),
                    'right'      => array(
                        'alt'   => 'Right sidebar', 
                        'img'  => ReduxFramework::$_url.'assets/img/2cr.png'
                    ),
                    'both'      => array(
                        'alt'   => 'Both sidebar', 
                        'img'  => ReduxFramework::$_url.'assets/img/3cm.png'
                    ),
                ),
                'default' => 'right'
            ),
            array(
                'id' => 'optn_product_single_sidebar',
                'type' => 'select',
                'title' => esc_html__('Product single Sidebar', 'piko-construct'),
                'subtitle' => "Choose Your sidebar",
                'data'      => 'sidebars',
                'default' => 'sidebar-4',
                'required' => array( 'optn_product_single_sidebar_pos', '=', array('left', 'right', 'both'), ),
            ),
            array(
                'id' => 'optn_product_single_sidebar_left',
                'type' => 'select',
                'title' => esc_html__('Page sidebar left', 'piko-construct'),
                'subtitle' => "Choose Your sidebar",
                'data'      => 'sidebars',
                'required' => array( 'optn_product_single_sidebar_pos', '=', 'both', ),
            ),
            array(
                'id'       => 'optn_product_single_sidebar_width',
                'type'     => 'button_set',
                'title'    => esc_html__( 'Sidebar Width', 'piko-construct' ),              
                'default'  => 'small',
                'options'  => array(
                        'large' => esc_html__( 'Large(1/4)', 'piko-construct' ),
                        'small' => esc_html__( 'Small(1/3)', 'piko-construct' ),
                ),
                'required' => array( 'optn_product_single_sidebar_pos', '=', array('left', 'right', 'both'), ),
            ),            
            array(
                'id' => 'enable_product_single_post_share',
                'type' => 'switch',
                'title' => esc_html__('Enable/Disable social share links', 'piko-construct'),
                'default' => 1,
                'on' => esc_html__( 'Enable', 'piko-construct' ),
                'off' => esc_html__( 'Disabled', 'piko-construct' ),
                
            ),
            array(
                'id'       => 'single_product_share_socials',
                'type'     => 'select',
                'multi'    => true,
                'title'    => esc_html__('Choose socials to share single product post', 'piko-construct'),
                'required' => array( 'enable_product_single_post_share', '=', true, ),
                'options'  => array(
                    'facebook'  => 'Facebook',
                    'gplus'     => 'Google Plus',
                    'twitter'   => 'Twitter',
                    'pinterest' => 'Pinterest',
                    'linkedin'  => 'Linkedin',
                ),
                'sortable' => true,
                'default'  => array( 'facebook', 'gplus', 'twitter', 'pinterest', 'linkedin' ),
            ),
            array (
                    'title' => esc_html__('Related Products', 'piko-construct'),
                    'subtitle' => esc_html__('Enable / Disable Related Products.', 'piko-construct'),
                    'id' => 'related_products',
                    'on' => esc_html__('Enabled', 'piko-construct'),
                    'off' => esc_html__('Disabled', 'piko-construct'),
                    'type' => 'switch',
                    'default' => 1,
            ),            
            array (
                    'title' => esc_html__('Upsell Products', 'piko-construct'),
                    'subtitle' => esc_html__('Enable / Disable Upsell Products.', 'piko-construct'),
                    'id' => 'upsell_products',
                    'on' => esc_html__('Enabled', 'piko-construct'),
                    'off' => esc_html__('Disabled', 'piko-construct'),
                    'type' => 'switch',
                    'default' => 1,
            ),            
            array (
                    'title' => esc_html__('Review Tab', 'piko-construct'),
                    'subtitle' => esc_html__('Enable / Disable Review Tab.', 'piko-construct'),
                    'id' => 'review_tab',
                    'on' => esc_html__('Enabled', 'piko-construct'),
                    'off' => esc_html__('Disabled', 'piko-construct'),
                    'type' => 'switch',
                    'default' => 1,
            ),
            array (
                    'title' => esc_html__('Custom Tab', 'piko-construct'),
                    'subtitle' => esc_html__('Enable / Disable Custom Tab.', 'piko-construct'),
                    'id' => 'custom_tab',
                    'on' => esc_html__('Enabled', 'piko-construct'),
                    'off' => esc_html__('Disabled', 'piko-construct'),
                    'type' => 'switch',
                    'default' => 1,
            ),
            array(
                    'id'       => 'custom_tab_title',
                    'type'     => 'text',
                    'title'    => esc_html__('Custom Tab Title','piko-construct'),
                    'default'  => 'Custom Tab',
                    'required' => array('custom_tab','=','1')
            ),
            array(
                    'id'       => 'custom_tab_content',
                    'type'     => 'editor',
                    'title'    => esc_html__('Custom Tab Content','piko-construct'),
                    'args'   => array(
                            'teeny'            => true,
                            'textarea_rows'    => 10
                    ),
                    'default' => 'Your custom tab text here dummy text of the printing and typesetting industry.',
                    'required' => array('custom_tab','=','1')
            ),
        )
    ));
}  //end of woocommece 
    
    //typography    
    Redux::setSection( $opt_name, array(
        'title'   => esc_html__( 'Typography', 'piko-construct' ),
        'icon'    => 'el-icon-font',
        'submenu' => true,
        'fields'  => array(
            array(
                    'id'             => 'body_font',
                    'type'           => 'typography',
                    'title'          => esc_html__( 'Body Font', 'piko-construct' ),
                    'subtitle'       => esc_html__( 'Select Google custom font for your main body text. color defatul use #747474', 'piko-construct' ),
                    'compiler'       => true,
                    'google'         => true,
                    'font-backup'    => false,
                    'font-weight'    => false,
                    'all_styles'     => true,
                    'font-style'     => false,
                    'subsets'        => true,
                    'font-size'      => true,
                    'line-height'    => false,
                    'word-spacing'   => false,
                    'letter-spacing' => false,
                    'color'          => true,
                    'preview'        => true,
                    'output'         => array( '.icon-overlay a,.post-navigation a .meta-nav,.post-single .category a,.post-single .share ul li,.sub-footer .piko-newslatter h4,.summary.entry-summary .woocommerce-product-rating .star-rating+.woocommerce-review-link:before,.summary.entry-summary .woocommerce-product-rating .woocommerce-review-link,.testimonial-name a,.testimonial-name span.designation,body,button,div.fancy-select ul.options li,input,select,textarea .posts-wrap .blog-title .title-blog-meta li a' ),
                    'units'          => 'px',                    
                    'default'        => array(
                            'color'       => "",
                    )
            ),
            array(
                    'id'             => 'font_heading',
                    'type'           => 'typography',
                    'title'          => esc_html__( 'Heading', 'piko-construct' ),
                    'subtitle'       => esc_html__( 'Default color #333333 Select custom font for headings', 'piko-construct' ),
                    'compiler'       => true,
                    'google'         => true,
                    'font-backup'    => false,
                    'all_styles'     => true,
                    'font-weight'    => true,
                    'font-style'     => false,
                    'subsets'        => true,
                    'font-size'      => false,
                    'line-height'    => false,
                    'word-spacing'   => false,
                    'letter-spacing' => false,
                    'color'          => true,
                    'preview'        => true,
                    'output'         => array( 'h1, h2, h3, h4, h5, h6,.h1, .h2, .h3, .h4, .h5, .h6' ),
                    'units'          => 'px',
                    'default'        => array(
                            'color'       => "",
                    )
                    
            ),
            array(
                    'id'             => 'h1_params',
                    'type'           => 'typography',
                    'title'          => esc_html__( 'H1', 'piko-construct' ),
                    'compiler'       => true,
                    'google'         => false,
                    'font-backup'    => false,
                    'all_styles'     => true,
                    'font-weight'    => false,
                    'font-family'    => false,
                    'text-align'     => false,
                    'font-style'     => false,
                    'subsets'        => false,
                    'font-size'      => true,
                    'line-height'    => false,
                    'word-spacing'   => false,
                    'letter-spacing' => false,
                    'color'          => false,
                    'preview'        => true,
                    'output'         => array( 'h1,.h1' ),
                    'units'          => 'px',
                    'default'        => array(
                            'font-size' => '40px',
                    )
            ),
            array(
                    'id'             => 'h2_params',
                    'type'           => 'typography',
                    'title'          => esc_html__( 'H2', 'piko-construct' ),
                    'compiler'       => true,
                    'google'         => false,
                    'font-backup'    => false,
                    'all_styles'     => true,
                    'font-weight'    => false,
                    'font-family'    => false,
                    'text-align'     => false,
                    'font-style'     => false,
                    'subsets'        => false,
                    'font-size'      => true,
                    'line-height'    => false,
                    'word-spacing'   => false,
                    'letter-spacing' => false,
                    'color'          => false,
                    'preview'        => true,
                    'output'         => array( 'h2,.h2' ),
                    'units'          => 'px',
                    'default'        => array(
                            'font-size' => '30px',
                    )
            ),
            array(
                    'id'             => 'h3_params',
                    'type'           => 'typography',
                    'title'          => esc_html__( 'H3', 'piko-construct' ),
                    'compiler'       => true,
                    'google'         => false,
                    'font-backup'    => false,
                    'all_styles'     => true,
                    'font-weight'    => false,
                    'font-family'    => false,
                    'text-align'     => false,
                    'font-style'     => false,
                    'subsets'        => false,
                    'font-size'      => true,
                    'line-height'    => false,
                    'word-spacing'   => false,
                    'letter-spacing' => false,
                    'color'          => false,
                    'preview'        => true,
                    'output'         => array( 'h3,.h3' ),
                    'units'          => 'px',
                    'default'        => array(
                            'font-size' => '22px',
                    )
            ),
            array(
                    'id'             => 'h4_params',
                    'type'           => 'typography',
                    'title'          => esc_html__( 'H4', 'piko-construct' ),
                    'compiler'       => true,
                    'google'         => false,
                    'font-backup'    => false,
                    'all_styles'     => true,
                    'font-weight'    => false,
                    'font-family'    => false,
                    'text-align'     => false,
                    'font-style'     => false,
                    'subsets'        => false,
                    'font-size'      => true,
                    'line-height'    => false,
                    'word-spacing'   => false,
                    'letter-spacing' => false,
                    'color'          => false,
                    'preview'        => true,
                    'output'         => array( 'h4,.h4' ),
                    'units'          => 'px',
                    'default'        => array(
                            'font-size' => '16px',
                    )
            ),
            array(
                    'id'             => 'h5_params',
                    'type'           => 'typography',
                    'title'          => esc_html__( 'H5', 'piko-construct' ),
                    'compiler'       => true,
                    'google'         => false,
                    'font-backup'    => false,
                    'all_styles'     => true,
                    'font-weight'    => false,
                    'font-family'    => false,
                    'text-align'     => false,
                    'font-style'     => false,
                    'subsets'        => false,
                    'font-size'      => true,
                    'line-height'    => false,
                    'word-spacing'   => false,
                    'letter-spacing' => false,
                    'color'          => false,
                    'preview'        => true,
                    'output'         => array( 'h5,.h5' ),
                    'units'          => 'px',
                    'default'        => array(
                            'font-size' => '14px',
                    )
            ),
            array(
                    'id'             => 'h6_params',
                    'type'           => 'typography',
                    'title'          => esc_html__( 'H6', 'piko-construct' ),
                    'compiler'       => true,
                    'google'         => false,
                    'font-backup'    => false,
                    'all_styles'     => true,
                    'font-weight'    => false,
                    'font-family'    => false,
                    'text-align'     => false,
                    'font-style'     => false,
                    'subsets'        => false,
                    'font-size'      => true,
                    'line-height'    => false,
                    'word-spacing'   => false,
                    'letter-spacing' => false,
                    'color'          => false,
                    'preview'        => true,
                    'output'         => array( 'h6,.h6' ),
                    'units'          => 'px',
                    'default'        => array(
                            'font-size' => '13px',
                    )
            ),
        )
) );
   //color skin    
    Redux::setSection( $opt_name, array(
        'title'   => esc_html__( 'Color Skin', 'piko-construct' ),
        'icon'    => 'el el-brush',        
        'fields'  => array(
                array(
                    'id'      => 'color_skin',
                    'type'    => 'button_set',
                    'title'   => esc_html__( 'Color Skin', 'piko-construct' ),
                    'default' => 'default',
                    'options' => array(
                            'default'            => esc_html__( 'Default', 'piko-construct' ),                          
                            'skin_custom' => esc_html__( 'Custom color', 'piko-construct' )
                    ),                   
                ),
                array(
                'id'       => 'main_color',
                'type'     => 'color',
                'compiler' => true,
                'title'    => esc_html__( 'Primary Color Scheme', 'piko-construct' ),
                'subtitle'    => esc_html__( 'Default use #45bf55', 'piko-construct' ),
                'default'  => '',
                'required' => array( 'color_skin', '=', 'skin_custom' ),
                'output'   => array(
                        'background-color'    => '
.pricing-box.featured h4,.overlay-style-2 .entry-thumbnail a:not(.prettyPhoto):before,.overlay-style-2 .posts-wrap .entry-thumbnail a.entry-thumbnail_overlay:before,.poverlay-style-2 .entry-thumbnail .entry-thumbnail-hover,.btnwrap:not(.white) a,#yith-quick-view-close:hover,.select2-container--default .select2-results__option--highlighted[aria-selected],.header-layout-3 .site-header .header-3-bottom .social-page-icon a:hover,.ourteam-wrap ul li a:hover,.sub-footer .piko-newslatter .button_newletter,.back-top:hover,.btn-wrap a,.cart-button a.cart-read-more:hover,.cart_totals .wc-proceed-to-checkout .checkout-button,.comment-respond .btn,.comment-respond .btn-custom,.header-dropdown.cart-dropdown>a .cart-items,.icon-block:hover .icon-wrap,.icon-border.white-bg .icon-block:hover .icon-wrap,.icon-overlay a:hover,.layout-outline .btn-wrap a:hover,.main-title-wrap .title-square2:after,.main-title-wrap .title-square2:before,.mega-menu .tip.hot,.nav.nav-pills>li.active>a,.nav.nav-pills>li>a:focus,.nav.nav-pills>li>a:hover,.ourteam-wrap figure figcaption .resume-icon,.owl-carousel .owl-dots .owl-dot.active,.owl-nav-show-hover.owl-carousel .owl-nav>div:hover,.owl-nav-show-inner.owl-carousel .owl-nav>div:hover,.owl-nav-show.owl-carousel .owl-nav>div:hover,.page-links a:focus,.page-links a:hover,.pagination .current,.piko-button.active,.piko-button.color,.piko-button:focus,.piko-button:hover,.piko-contact input[type=submit],.piko-layout-header .piko-show-account.logged-in .link-account,.piko-show-account.logged-in .link-account:hover,.posts-wrap .blog-title .entry-title i,.posts-wrap .entry-thumbnail-wrap .blog-date,.print-button .social-share li:hover,.progress-container .progress .progress-bar,.progress-container .progress .progress-bar .progress-val,.reset_variations:focus,.reset_variations:hover,.seperator span i,.social-page a:focus,.social-page a:hover,.summary .added_to_cart.wc-forward:focus,.summary .added_to_cart.wc-forward:hover,.summary .single_add_to_cart_button,.summary.entry-summary .product-button .yith-wcwl-add-to-wishlist a:hover,.tab-layout-1 .nav.nav-pills>li.active>a,.tab-layout-1 .nav.nav-pills>li>a:focus,.tab-layout-1 .nav.nav-pills>li>a:hover,.tabs-menu .nav>li.active:before,.tagcloud a:focus,.tagcloud a:hover,.widget-area .widget_nav_menu .menu-service-container ul li.current-menu-item,.widget-area .widget_text .brochure li a i,.widget_calendar tbody a,.woocommerce-checkout-payment .form-row.place-order input[type=submit],.works-history .history-left .label-content .square,.works-history .history-left .label-content label,button:focus,button:hover,div.fancy-select ul.options li.selected,input[type=button]:focus,input[type=button]:hover,input[type=reset]:focus,input[type=reset]:hover,input[type=submit]:focus,input[type=submit]:hover,table.shop_table td.actions>input[type=submit]:hover,.social-layout-1 .social-page-icon a,.social-layout-2 .social-page-icon a:hover 
',
                    'color'  => ' 
.btnwrap:not(.white) a,#review_form #commentform .stars>span a.active::before,#review_form #commentform .stars>span a:hover::before,.arrow li:before,.author-info .author-content .more-link:focus,.author-info .author-content .more-link:hover,.author-info a:focus,.author-info a:hover,.cart-button a.add_to_cart_button:hover,.cart-button a.added_to_cart:after,.cart-button a.cart-read-more,.cart_totals table tr.order-total td .amount,.category-product-slide .amount,.comment-reply-title small a:focus,.comment-reply-title small a:focus:before,.comment-reply-title small a:hover,.comment-reply-title small a:hover:before,.comments .comment .comment-date a:hover,.comments .comment h4 a:hover,.entry-footer .tags-links a:focus,.entry-footer .tags-links a:hover,.entry-footer a:focus,.entry-footer a:hover,.error404 .content-area .not-found .number-404 span,.header-boxes-container i,.icon-block .icon-wrap,.info-product .piko-viewdetail:hover,.info-product>h3 a:hover,.mega-menu .tip.hot .tip-arrow:before,.mobile-main-menu li.menu-item.active>a,.mobile-main-menu li.menu-item:hover>a,.nav.nav-pills.nav-bordered.v2>li.active>a,.nav.nav-pills.nav-bordered.v2>li>a:focus,.nav.nav-pills.nav-bordered.v2>li>a:hover,.nav.nav-pills.nav-bordered>li.active>a,.nav.nav-pills.nav-bordered>li>a:focus,.nav.nav-pills.nav-bordered>li>a:hover,.ourteam-wrap ul li a:hover,.page-header.bg-image .breadcrumb li a:focus,.page-header.bg-image .breadcrumb li a:hover,.page-header.parallax .breadcrumb li a:focus,.page-header.parallax .breadcrumb li a:hover,.pagination .next:focus,.pagination .next:hover,.pagination .prev:focus,.pagination .prev:hover,.post-navigation a:focus .post-title,.post-navigation a:hover .post-title,.posts-wrap .blog-title .title-blog-meta li a:hover,.product .new,.product-button .add_to_cart_button .added_to_cart.wc-forward:focus,.product-button .add_to_cart_button .added_to_cart.wc-forward:hover,.product-button .add_to_cart_button a:focus,.product-button .add_to_cart_button a:hover,.product-button .add_to_cart_button:focus,.product-button .add_to_cart_button:hover,.product-button .added_to_cart.wc-forward .added_to_cart.wc-forward:focus,.product-button .added_to_cart.wc-forward .added_to_cart.wc-forward:hover,.product-button .added_to_cart.wc-forward a:focus,.product-button .added_to_cart.wc-forward a:hover,.product-button .added_to_cart.wc-forward:focus,.product-button .added_to_cart.wc-forward:hover,.product-button .compare .added_to_cart.wc-forward:focus,.product-button .compare .added_to_cart.wc-forward:hover,.product-button .compare a:focus,.product-button .compare a:hover,.product-button .compare:focus,.product-button .compare:hover,.product-button .product_type_external .added_to_cart.wc-forward:focus,.product-button .product_type_external .added_to_cart.wc-forward:hover,.product-button .product_type_external a:focus,.product-button .product_type_external a:hover,.product-button .product_type_external:focus,.product-button .product_type_external:hover,.product-button .product_type_grouped .added_to_cart.wc-forward:focus,.product-button .product_type_grouped .added_to_cart.wc-forward:hover,.product-button .product_type_grouped a:focus,.product-button .product_type_grouped a:hover,.product-button .product_type_grouped:focus,.product-button .product_type_grouped:hover,.product-button .yith-wcqv-button .added_to_cart.wc-forward:focus,.product-button .yith-wcqv-button .added_to_cart.wc-forward:hover,.product-button .yith-wcqv-button a:focus,.product-button .yith-wcqv-button a:hover,.product-button .yith-wcqv-button:focus,.product-button .yith-wcqv-button:hover,.product-button .yith-wcwl-add-to-wishlist .added_to_cart.wc-forward:focus,.product-button .yith-wcwl-add-to-wishlist .added_to_cart.wc-forward:hover,.product-button .yith-wcwl-add-to-wishlist a:focus,.product-button .yith-wcwl-add-to-wishlist a:hover,.product-button .yith-wcwl-add-to-wishlist:focus,.product-button .yith-wcwl-add-to-wishlist:hover,.product-category.product h3:hover,.product-footer .category>a:hover,.product-footer .shear-product a:hover i,.product-innercotent .info-product .piko-viewdetail:hover,.product-innercotent .info-product .title-product a:hover,.product_list_widget span.quantity,.products .product .price,.products-list .piko-feature-product .product-footer .product-title:hover,.products-list .piko-feature-product .product-footer .product_meta a:hover,.products.products-list .product-button .add_to_cart_button .added_to_cart.wc-forward:focus,.products.products-list .product-button .add_to_cart_button .added_to_cart.wc-forward:hover,.products.products-list .product-button .add_to_cart_button a:focus,.products.products-list .product-button .add_to_cart_button a:hover,.products.products-list .product-button .add_to_cart_button:focus,.products.products-list .product-button .add_to_cart_button:hover,.products.products-list .product-button .added_to_cart.wc-forward .added_to_cart.wc-forward:focus,.products.products-list .product-button .added_to_cart.wc-forward .added_to_cart.wc-forward:hover,.products.products-list .product-button .added_to_cart.wc-forward a:focus,.products.products-list .product-button .added_to_cart.wc-forward a:hover,.products.products-list .product-button .added_to_cart.wc-forward:focus,.products.products-list .product-button .added_to_cart.wc-forward:hover,.products.products-list .product-button .compare .added_to_cart.wc-forward:focus,.products.products-list .product-button .compare .added_to_cart.wc-forward:hover,.products.products-list .product-button .compare a:focus,.products.products-list .product-button .compare a:hover,.products.products-list .product-button .compare:focus,.products.products-list .product-button .compare:hover,.products.products-list .product-button .product_type_external .added_to_cart.wc-forward:focus,.products.products-list .product-button .product_type_external .added_to_cart.wc-forward:hover,.products.products-list .product-button .product_type_external a:focus,.products.products-list .product-button .product_type_external a:hover,.products.products-list .product-button .product_type_external:focus,.products.products-list .product-button .product_type_external:hover,.products.products-list .product-button .product_type_grouped .added_to_cart.wc-forward:focus,.products.products-list .product-button .product_type_grouped .added_to_cart.wc-forward:hover,.products.products-list .product-button .product_type_grouped a:focus,.products.products-list .product-button .product_type_grouped a:hover,.products.products-list .product-button .product_type_grouped:focus,.products.products-list .product-button .product_type_grouped:hover,.products.products-list .product-button .yith-wcqv-button .added_to_cart.wc-forward:focus,.products.products-list .product-button .yith-wcqv-button .added_to_cart.wc-forward:hover,.products.products-list .product-button .yith-wcqv-button a:focus,.products.products-list .product-button .yith-wcqv-button a:hover,.products.products-list .product-button .yith-wcqv-button:focus,.products.products-list .product-button .yith-wcqv-button:hover,.products.products-list .product-button .yith-wcwl-add-to-wishlist .added_to_cart.wc-forward:focus,.products.products-list .product-button .yith-wcwl-add-to-wishlist .added_to_cart.wc-forward:hover,.products.products-list .product-button .yith-wcwl-add-to-wishlist a:focus,.products.products-list .product-button .yith-wcwl-add-to-wishlist a:hover,.products.products-list .product-button .yith-wcwl-add-to-wishlist:focus,.products.products-list .product-button .yith-wcwl-add-to-wishlist:hover,.shop_table tbody .cart_item .product-name a:hover,.shop_table.order_details td.product-name a:hover,.shop_table.woocommerce-checkout-review-order-table tr.order-total td .amount,.style2 .piko-categories:hover .product-footer .category>a,.sub-footer .widget .comment-author-link a,.summary.entry-summary .price .amount,.swiper-button-next:not(.swiper-button-disabled):hover i,.swiper-button-prev:not(.swiper-button-disabled):hover i,.testimonial-2 h6,.testimonial-3 h6,.testimonial-name .rating,.testimonial-name a:hover,.testimonial-wrap .comment a:hover,.widget-area>.widget.widget_recent_comments ul li span a,.widget.woocommerce .product_list_widget li .amount,.wishlist_table .product-name a:first-child:hover,.wishlist_table .product-stock-status span,.wishlist_table tr td.product-stock-status span.wishlist-in-stock,.woocommerce-MyAccount-navigation ul li.is-active a,.woocommerce-info:before,.woocommerce-toolbar .gridlist-toggle-wrapper a.active,.woocommerce-toolbar .sort-by-wrapper:hover .sort-by-label,a.pp_next:hover,a.pp_previous:hover,a:active,a:focus,a:hover,div.fancy-select ul.options li:not(.selected).hover,div.light_square .pp_close:hover,div.product .summary .product-share ul li a:hover,div.product .summary .product_meta>span.posted_in a:hover
',
                    'border-color' => '
#yith-quick-view-close:hover,.btn-wrap a,.header-layout-3 .site-header .header-3-bottom .tagcloud a:hover,.header-layout-3 .site-header .header-3-bottom .social-page-icon a:hover,.category-product-slide .image-product-gallery img,.icon-border .icon-block:hover .icon-wrap,.icon-overlay a:hover,.layout-outline .btn-wrap a:hover,.nav.nav-pills.nav-bordered.v2>li.active>a,.nav.nav-pills.nav-bordered.v2>li>a:focus,.nav.nav-pills.nav-bordered.v2>li>a:hover,.nav.nav-pills.nav-bordered>li.active>a,.nav.nav-pills.nav-bordered>li>a:focus,.nav.nav-pills.nav-bordered>li>a:hover,.ourteam-wrap ul li a:hover,.owl-carousel .owl-dots .owl-dot.active,.pagination .current,.piko-button.active,.piko-button:focus,.piko-button:hover,.piko-contact input[type=text]:focus,.piko-contact input[type=email]:focus,.piko-contact input[type=url]:focus,.piko-contact input[type=password]:focus,.piko-contact input[type=search]:focus,.piko-contact input[type=tel]:focus,.piko-contact input[type=number]:focus,.piko-contact textarea:focus,.piko-layout-header .piko-show-account.logged-in .link-account,.piko-show-account.logged-in .link-account:hover,.piko-single-post-shear .piko-social-btn a:hover,.product .new,.reply-btn:focus,.reply-btn:hover,.reset_variations:focus,.reset_variations:hover,.sidebar-two .sub-footer .widget .tagcloud a:hover,.social-page a:focus,.social-page a:hover,.summary.entry-summary .yith-wcwl-add-to-wishlist .yith-wcwl-add-button:hover,.summary.entry-summary .yith-wcwl-add-to-wishlist .yith-wcwl-wishlistaddedbrowse:hover,.summary.entry-summary .yith-wcwl-add-to-wishlist .yith-wcwl-wishlistexistsbrowse:hover,.tagcloud a:focus,.tagcloud a:hover,.testimonial-wrap .owl-carousel .owl-dots .owl-dot.active,.threepx .btn-wrap a,.twopx .btn-wrap a,button:focus,button:hover,div.wpcf7-mail-sent-ok,input[type=button]:focus,input[type=button]:hover,input[type=reset]:focus,input[type=reset]:hover,input[type=submit]:focus,input[type=submit]:hover,input[type=text]:focus,input[type=email]:focus,input[type=url]:focus,input[type=password]:focus,input[type=search]:focus,input[type=tel]:focus,input[type=number]:focus,table.shop_table td.actions>input[type=submit]:hover,textarea:focus,.social-layout-1 .social-page-icon a,.social-layout-2 .social-page-icon a:hover 
',  
                    'border-top-color' => '
.navbar-collapse.collapse,.piko-categories .product-footer,.progress-container .progress .progress-bar .progress-val:after,.wishlist_table .product-stock-status span,.wishlist_table tr td.product-stock-status span.wishlist-in-stock
',  
                    'border-left-color' => '
.works-history .history-left .label-content label:after',  
                    'border-right-color' => '
#woof_html_buffer::before,.ribbon span::after',  
                        'border-bottom-color' => '
.wishlist_table .product-stock-status span,.wishlist_table tr td.product-stock-status span.wishlist-in-stock,.woocommerce-error,.woocommerce-info,.woocommerce-message
'
                    )
                ),
            array(
                'id'       => 'main_bg_font_color',
                'type'     => 'color',
                'compiler' => true,
                'transparent' => false,
                'title'    => esc_html__( 'Primary Font color', 'piko-construct' ),
                'subtitle'    => esc_html__( 'When primary background color #45bf55 choose the font color #fff', 'piko-construct' ),
                'default'  => '',
                'required' => array( 'color_skin', '=', 'skin_custom' ),
                'output'   => array(
                        'color'    => '
.works-history .history-left .label-content label,.sub-footer .piko-newslatter .button_newletter:hover,#yith-quick-view-close:before,#yith-quick-view-close:hover:before,.back-top:hover i,.btn-wrap a,.cart_totals .wc-proceed-to-checkout .checkout-button,.header-dropdown.cart-dropdown>a .cart-items,.icon-layout-1:not(.counter-bg) .icon-block:hover .icon-wrap i,.icon-layout-1:not(.counter-bg) .icon-block:hover .icon-wrap span,.icon-layout-2:not(.counter-bg) .icon-block:hover .icon-wrap i,.icon-layout-2:not(.counter-bg) .icon-block:hover .icon-wrap span,.icon-overlay a:hover,.layout-outline .btn-wrap a:hover,.mega-menu .tip,.nav.nav-pills>li.active>a,.nav.nav-pills>li>a:focus,.nav.nav-pills>li>a:hover,.ourteam-wrap figure figcaption .resume-icon,.ourteam-wrap ul li a:hover,.owl-nav-show-hover.owl-carousel .owl-nav>div:hover,.owl-nav-show-inner.owl-carousel .owl-nav>div:hover,.owl-nav-show.owl-carousel .owl-nav>div:hover,.page-links a:focus,.page-links a:hover,.pagination .current,.piko-button.active,.piko-button:focus,.piko-button:hover,.posts-wrap .blog-title .entry-title i,.posts-wrap .entry-thumbnail-wrap .blog-date,.progress-container .progress .progress-bar .progress-val,.reset_variations:focus,.reset_variations:hover,.social-layout-1 .social-page-icon a,.social-layout-2 .social-page-icon a:hover,.social-page a:focus i,.social-page a:hover i,.summary.entry-summary .product-button .yith-wcwl-add-to-wishlist a:hover,.tab-layout-1 .nav.nav-pills>li.active>a,.tab-layout-1 .nav.nav-pills>li>a:focus,.tab-layout-1 .nav.nav-pills>li>a:hover,.tagcloud a:focus,.tagcloud a:hover,.widget-area .widget_nav_menu .menu-service-container ul li.current-menu-item a,.widget-area .widget_text .brochure li a:hover i,.widget_calendar tbody a,button:focus,button:hover,div.fancy-select ul.options li.selected,input[type=button]:focus,input[type=button]:hover,input[type=reset]:focus,input[type=reset]:hover,input[type=submit]:focus,input[type=submit]:hover,table.shop_table td.actions>input[type=submit]:hover
',
                    
                    )
                ),
             array(
                'id'       => 'secondary_color',
                'type'     => 'color',
                'compiler' => true,
                'title'    => esc_html__( 'Secondary Color Scheme', 'piko-construct' ),
                'subtitle'    => esc_html__( 'Default use #333333', 'piko-construct' ),
                'default'  => '',
                'required' => array( 'color_skin', '=', 'skin_custom' ),
                'output'   => array(
                    'background-color'    => '
#mobile_menu_wrapper,#yith-quick-view-close,.back-top,.cart-button a.add_to_cart_button,.cart-button a.added_to_cart,.cart-button a.cart-read-more,.cart_totals .wc-proceed-to-checkout .checkout-button:hover,.owl-nav-show.owl-carousel .owl-nav>div,.panel-default>.panel-heading .badge,.piko-button,.piko-button.color.active,.piko-button.color:focus,.piko-button.color:hover,.piko-contact input[type=submit]:hover,.piko-layout-header .piko-show-account.logged-in .link-account:hover,.piko-my-account .piko-togoleform:hover,.reset_variations,.summary .added_to_cart.wc-forward,.summary .single_add_to_cart_button:hover,.summary.entry-summary .compare-button a:hover:before,.summary.entry-summary .yith-wcwl-add-to-wishlist .yith-wcwl-add-button a:hover:before,.summary.entry-summary .yith-wcwl-add-to-wishlist .yith-wcwl-wishlistaddedbrowse a:hover:before,.summary.entry-summary .yith-wcwl-add-to-wishlist .yith-wcwl-wishlistexistsbrowse a:hover:before,.widget_calendar tbody a:focus,.widget_calendar tbody a:hover,.woocommerce-checkout-payment .form-row.place-order input[type=submit]:hover,button,button[disabled]:focus,button[disabled]:hover,input[type=button],input[type=button][disabled]:focus,input[type=button][disabled]:hover,input[type=reset],input[type=reset][disabled]:focus,input[type=reset][disabled]:hover,input[type=submit],input[type=submit][disabled]:focus,input[type=submit][disabled]:hover,kbd
',                  
                    'color' => '.author-info a,.cart-button a.cart-read-more:hover,.cart_totals table td,.cart_totals table th,.dropdown-menu>li>a,.error404 .content-area .not-found .number-404,.footer-links-menu li a,.info-product .piko-viewdetail,.info-product>h3,.layout-outline .btn-wrap a,.list-categories .active_product_category a,.ourteam-wrap figure figcaption .resume-icon:hover,.owl-nav-show-hover.owl-carousel .owl-nav>div,.owl-nav-show-inner.owl-carousel .owl-nav>div,.pagination .next:after,.pagination .prev:before,.panel-default>.panel-heading,.piko-button.style1,.piko-my-account .piko-togoleform,.piko-show-account.logged-in .link-account,.portfolio:not(.default) .owl-nav-show.owl-carousel .owl-nav>div,.post-navigation a .post-title,.product .sold-out,.product-category.product h3,.product-category.product h3 mark,.product-innercotent .info-product .piko-viewdetail,.product-innercotent .info-product .title-product,.product-innercotent .info-product>h3,.shop_table tbody .cart_item .product-name a,.social-layout-1 .social-page-icon a:hover,.style-layout-5 .icon-title,.style2 .piko-categories .product-footer .category>a,.sub-footer .piko-newslatter .button_newletter:hover,.summary.entry-summary .compare-button,.summary.entry-summary .product-addtocart table.group_table .label,.summary.entry-summary .quantity input[type=text],.summary.entry-summary .yith-wcwl-add-to-wishlist a,.testimonial-name,.thumbnail .caption,.widget .widget-title a,.widget-area>.widget ul li,.wishlist_table .product-name a:first-child,.woocommerce #content table.wishlist_table.cart a.remove:hover,.woocommerce-MyAccount-navigation ul li a,.woocommerce-checkout-payment .payment_methods li label,.woocommerce-ordering div.fancy-select div.trigger,.woocommerce-pagination .page-numbers li .page-numbers.current,.woocommerce-pagination .page-numbers li .page-numbers:hover,.woocommerce-toolbar .sort-by-wrapper .sort-by-label,a,a.list-group-item .list-group-item-heading,a:visited,button.list-group-item .list-group-item-heading,div.fancy-select div.trigger,div.product .summary .product-share ul li a,div.product .summary .product-share>span,pre,table.shop_table td.actions>input[type=submit],td.label 
                    ',
                    'border-color' => '
.cart-button a.add_to_cart_button,.cart-button a.added_to_cart,.cart-button a.cart-read-more,.owl-carousel .owl-dots .owl-dot,.piko-button.color.active,.piko-button.color:focus,.piko-button.color:hover,.piko-layout-header .piko-show-account.logged-in .link-account:hover,.piko-my-account .piko-togoleform,.portfolio-tabs li.active a.active:after,.product .sold-out,.reset_variations,.summary.entry-summary .compare-button:hover,.summary.entry-summary .yith-wcwl-add-to-wishlist a:hover,.tab-btn:hover:after
', 
                    )
                ),
                array(
                    'id'       => 'secondary_font_color',
                    'type'     => 'color',
                    'compiler' => true,
                    'transparent' => false,
                    'title'    => esc_html__( 'Secondary Font Color', 'piko-construct' ),
                    'subtitle'    => esc_html__( 'When Secondary background color #333333 choose the font color #fff', 'piko-construct' ),
                    'default'  => '',
                    'required' => array( 'color_skin', '=', 'skin_custom' ),
                    'output'   => array(
                        'color'    => '
.back-top i,.cart-button a.add_to_cart_button,.piko-contact input[type="submit"]:hover,.cart-button a.added_to_cart,.cart-button a.cart-read-more,.owl-nav-show.owl-carousel .owl-nav>div,.piko-button,.piko-layout-header .piko-show-account.logged-in .link-account:hover,.piko-my-account .piko-togoleform:hover,.reset_variations,.summary.entry-summary .compare-button a:hover:before,.summary.entry-summary .yith-wcwl-add-to-wishlist .yith-wcwl-add-button a:hover:before,.summary.entry-summary .yith-wcwl-add-to-wishlist .yith-wcwl-wishlistaddedbrowse a:hover:before,.summary.entry-summary .yith-wcwl-add-to-wishlist .yith-wcwl-wishlistexistsbrowse a:hover:before,.widget_calendar tbody a:focus,.widget_calendar tbody a:hover,button,button[disabled]:focus,button[disabled]:hover,input[type=button],input[type=button][disabled]:focus,input[type=button][disabled]:hover,input[type=reset],input[type=reset][disabled]:focus,input[type=reset][disabled]:hover,input[type=submit],input[type=submit][disabled]:focus,input[type=submit][disabled]:hover,kbd
',                     

                        )
                    ),
                array(
                    'id'       => 'tertiary_color',
                    'type'     => 'color',
                    'compiler' => true,
                    'title'    => esc_html__( 'Tertiary Color', 'piko-construct' ),
                    'subtitle'    => esc_html__( 'Default use #2fb040. little bit darker change from primary color. it\'s works button hover color', 'piko-construct' ),
                    'default'  => '',
                    'required' => array( 'color_skin', '=', 'skin_custom' ),
                    'output'   => array(
                        'background-color'    => '
.pricing-box .price-head .pricing-ribbon .ribbon,.product .onsale,.widget_price_filter .button:hover,.widget_price_filter .ui-slider .ui-slider-handle,.widget_price_filter .ui-slider .ui-slider-range,.piko-btn-0:before,.piko-btn-6:before, .piko-btn-6:after,.piko-btn-8:before, .piko-btn-8:after,.piko-btn-8 span:before,.piko-btn-8 span:after,.piko-btn-9:before, .piko-btn-9:after,.piko-btn-9 span:before,.piko-btn-9 span:after,.piko-btn-10 .aware,.piko-btn-10:active
',
                        'color' => '.star-rating span,.btnwrap a:hover,.btnwrap.white a:hover',
                        'border-top-color' => '.nav.nav-pills>li>a:after,.product .onsale:after,.widget_price_filter .ui-slider .ui-slider-handle::after,.piko-btn-4:after,.piko-btn-3:after,.piko-btn-5:before, .piko-btn-5:after',
                        'border-right-color' => '#reviews #comments ol.commentlist li .comment-text:after,.piko-btn-7:after',
                        'border-bottom-color' => '.piko-btn-1:before,.piko-btn-2:before, .piko-btn-2:after,.piko-btn-4:before,.piko-btn-3:before,.piko-btn-5:before, .piko-btn-5:after',
                        'border-left-color' => '.piko-btn-7:before',
                        )
                    ),            
                array(
                    'id'       => 'tertiary_font_color',
                    'type'     => 'color',
                    'compiler' => true,
                    'transparent' => false,
                    'title'    => esc_html__( 'Tertiary Font Color', 'piko-construct' ),
                    'subtitle'    => esc_html__( 'When Secondary background color #f5af35 choose the font color #fff', 'piko-construct' ),
                    'default'  => '',
                    'required' => array( 'color_skin', '=', 'skin_custom' ),
                    'output'   => array(                        
                        'color' => '.product .onsale',
                        )
                    ),
                array(
                    'id'        => 'custom_opacity_color',
                    'type'      => 'color_rgba',
                    'title'     => 'Overlay Modal Opacity Color',
                    'required' => array( 'color_skin', '=', 'skin_custom' ),
                    'default'   => array(
                        'color'     => '',
                        'alpha'     => 1
                    ),
                    'output'    => array('background-color' => '#yith-quick-view-modal .yith-quick-view-overlay,#cboxOverlay,div.pp_overlay'),
                ),
                array(
                    'id'       => 'fourth_border_color',
                    'type'     => 'color',
                    'compiler' => true,
                    'transparent' => false,
                    'title'    => esc_html__( 'Border Color', 'piko-construct' ),
                    'subtitle'    => esc_html__( 'if use woocommerce active button border #747474 else leave it', 'piko-construct' ),
                    'default'  => '',
                    'required' => array( 'color_skin', '=', 'skin_custom' ),
                    'output'   => array(                        
                        'border-color' => '.products .product .added_to_cart.wc-forward,.products .product>.added_to_cart:focus,.products .product>.added_to_cart:hover,.products .product>a.button:focus .products.products-list .product .product-list-button>.added_to_cart:hover,.products .product>a.button:hover,.products.products-list .product .product-list-button>.added_to_cart,.products.products-list .product .product-list-button>.added_to_cart:focus,.products.products-list .product .product-list-button>a.button:focus,.products.products-list .product .product-list-button>a.button:hover,.products.products-list .product-button .add_to_cart_button .added_to_cart.wc-forward:focus,.products.products-list .product-button .add_to_cart_button .added_to_cart.wc-forward:hover,.products.products-list .product-button .add_to_cart_button a:focus,.products.products-list .product-button .add_to_cart_button a:hover,.products.products-list .product-button .add_to_cart_button:focus,.products.products-list .product-button .add_to_cart_button:hover,.products.products-list .product-button .added_to_cart.wc-forward .added_to_cart.wc-forward:focus,.products.products-list .product-button .added_to_cart.wc-forward .added_to_cart.wc-forward:hover,.products.products-list .product-button .added_to_cart.wc-forward a:focus,.products.products-list .product-button .added_to_cart.wc-forward a:hover,.products.products-list .product-button .added_to_cart.wc-forward:focus,.products.products-list .product-button .added_to_cart.wc-forward:hover,.products.products-list .product-button .compare .added_to_cart.wc-forward:focus,.products.products-list .product-button .compare .added_to_cart.wc-forward:hover,.products.products-list .product-button .compare a:focus,.products.products-list .product-button .compare a:hover,.products.products-list .product-button .compare:focus,.products.products-list .product-button .compare:hover,.products.products-list .product-button .product_type_external .added_to_cart.wc-forward:focus,.products.products-list .product-button .product_type_external .added_to_cart.wc-forward:hover,.products.products-list .product-button .product_type_external a:focus,.products.products-list .product-button .product_type_external a:hover,.products.products-list .product-button .product_type_external:focus,.products.products-list .product-button .product_type_external:hover,.products.products-list .product-button .product_type_grouped .added_to_cart.wc-forward:focus,.products.products-list .product-button .product_type_grouped .added_to_cart.wc-forward:hover,.products.products-list .product-button .product_type_grouped a:focus,.products.products-list .product-button .product_type_grouped a:hover,.products.products-list .product-button .product_type_grouped:focus,.products.products-list .product-button .product_type_grouped:hover,.products.products-list .product-button .yith-wcqv-button .added_to_cart.wc-forward:focus,.products.products-list .product-button .yith-wcqv-button .added_to_cart.wc-forward:hover,.products.products-list .product-button .yith-wcqv-button a:focus,.products.products-list .product-button .yith-wcqv-button a:hover,.products.products-list .product-button .yith-wcqv-button:focus,.products.products-list .product-button .yith-wcqv-button:hover,.products.products-list .product-button .yith-wcwl-add-to-wishlist .added_to_cart.wc-forward:focus,.products.products-list .product-button .yith-wcwl-add-to-wishlist .added_to_cart.wc-forward:hover,.products.products-list .product-button .yith-wcwl-add-to-wishlist a:focus,.products.products-list .product-button .yith-wcwl-add-to-wishlist a:hover,.products.products-list .product-button .yith-wcwl-add-to-wishlist:focus,.products.products-list .product-button .yith-wcwl-add-to-wishlist:hover,.wishlist_table a.button:focus,.wishlist_table a.button:hover',
                        )
                ),
            )
        ));        
    //    social media    
    Redux::setSection( $opt_name, array(
    'title'   => esc_html__( 'Social Media', 'piko-construct' ),
    'icon'    => 'el el-user',
    'desc'    => esc_html__( 'Enter social media Page urls here as you want. then enable them for header. Please put the full URLs like this "https://facebook.com".', 'piko-construct' ),
    'submenu' => true,
    'fields'  => array(
            array(
                    'id'       => 'facebook',
                    'type'     => 'text',
                    'title'    => esc_html__( 'Facebook', 'piko-construct' ),
                    'subtitle' => '',
                    'default' => 'https://www.facebook.com/',
                    'desc'     => esc_html__( 'Enter your Facebook URL.', 'piko-construct' ),
                    'validate' => 'url'
            ),
            array(
                    'id'       => 'twitter',
                    'type'     => 'text',
                    'title'    => esc_html__( 'Twitter', 'piko-construct' ),
                    'subtitle' => '',
                    'default' => 'https://www.twitter.com/',
                    'desc'     => esc_html__( 'Enter your Twitter URL.', 'piko-construct' ),
                    'validate' => 'url'
            ),
            array(
                    'id'       => 'flickr',
                    'type'     => 'text',
                    'title'    => esc_html__( 'Flickr', 'piko-construct' ),
                    'subtitle' => '',
                    'desc'     => esc_html__( 'Enter your Flickr URL.', 'piko-construct' ),
                    'validate' => 'url'
            ),
            array(
                    'id'       => 'instagram',
                    'type'     => 'text',
                    'title'    => esc_html__( 'Instagram', 'piko-construct' ),
                    'subtitle' => '',
                    'default' => 'https://www.instagram.com/',
                    'desc'     => esc_html__( 'Enter your Instagram URL.', 'piko-construct' ),
                    'validate' => 'url'
            ),
            array(
                    'id'       => 'behance',
                    'type'     => 'text',
                    'title'    => esc_html__( 'Behance', 'piko-construct' ),
                    'subtitle' => '',
                    'desc'     => esc_html__( 'Enter your Behance URL.', 'piko-construct' ),
                    'validate' => 'url'
            ),
            array(
                    'id'       => 'dribbble',
                    'type'     => 'text',
                    'title'    => esc_html__( 'Dribbble', 'piko-construct' ),
                    'subtitle' => '',
                    'desc'     => esc_html__( 'Enter your Dribbble URL.', 'piko-construct' ),
                    'validate' => 'url'
            ),            
            array(
                    'id'       => 'git',
                    'type'     => 'text',
                    'title'    => esc_html__( 'Git', 'piko-construct' ),
                    'subtitle' => '',
                    'desc'     => esc_html__( 'Enter your Git URL.', 'piko-construct' ),
                    'validate' => 'url'
            ),
            array(
                    'id'       => 'linkedin',
                    'type'     => 'text',
                    'title'    => esc_html__( 'Linkedin', 'piko-construct' ),
                    'subtitle' => '',
                    'desc'     => esc_html__( 'Enter your Linkedin URL.', 'piko-construct' ),
                    'validate' => 'url'
            ),
            array(
                    'id'       => 'pinterest',
                    'type'     => 'text',
                    'title'    => esc_html__( 'Pinterest', 'piko-construct' ),
                    'subtitle' => '',
                    'desc'     => esc_html__( 'Enter your Pinterest URL.', 'piko-construct' ),
                    'validate' => 'url'
            ),
            array(
                    'id'       => 'yahoo',
                    'type'     => 'text',
                    'title'    => esc_html__( 'Yahoo', 'piko-construct' ),
                    'subtitle' => '',
                    'desc'     => esc_html__( 'Enter your Yahoo URL.', 'piko-construct' ),
                    'validate' => 'url'
            ),
            array(
                    'id'       => 'delicious',
                    'type'     => 'text',
                    'title'    => esc_html__( 'Delicious', 'piko-construct' ),
                    'subtitle' => '',
                    'desc'     => esc_html__( 'Enter your Delicious URL.', 'piko-construct' ),
                    'validate' => 'url'
            ),
            array(
                    'id'       => 'dropbox',
                    'type'     => 'text',
                    'title'    => esc_html__( 'Dropbox', 'piko-construct' ),
                    'subtitle' => '',
                    'desc'     => esc_html__( 'Enter your Dropbox URL.', 'piko-construct' ),
                    'validate' => 'url'
            ),
            array(
                    'id'       => 'reddit',
                    'type'     => 'text',
                    'title'    => esc_html__( 'Reddit', 'piko-construct' ),
                    'subtitle' => '',
                    'desc'     => esc_html__( 'Enter your Reddit URL.', 'piko-construct' ),
                    'validate' => 'url'
            ),
            array(
                    'id'       => 'soundcloud',
                    'type'     => 'text',
                    'title'    => esc_html__( 'Soundcloud', 'piko-construct' ),
                    'subtitle' => '',
                    'desc'     => esc_html__( 'Enter your Soundcloud URL.', 'piko-construct' ),
                    'validate' => 'url'
            ),
            array(
                    'id'       => 'google',
                    'type'     => 'text',
                    'title'    => esc_html__( 'Google', 'piko-construct' ),
                    'subtitle' => '',
                    'desc'     => esc_html__( 'Enter your Google URL.', 'piko-construct' ),
                    'validate' => 'url'
            ),
            array(
                    'id'       => 'googleplus',
                    'type'     => 'text',
                    'title'    => esc_html__( 'Google+', 'piko-construct' ),
                    'subtitle' => '',
                    'desc'     => esc_html__( 'Enter your Google Plus URL.', 'piko-construct' ),
                    'validate' => 'url'
            ),
            array(
                    'id'       => 'skype',
                    'type'     => 'text',
                    'title'    => esc_html__( 'Skype', 'piko-construct' ),
                    'subtitle' => '',
                    'desc'     => esc_html__( 'Enter your Skype URL.', 'piko-construct' ),
                    'validate' => 'url'
            ),
            array(
                    'id'       => 'youtube',
                    'type'     => 'text',
                    'title'    => esc_html__( 'Youtube', 'piko-construct' ),
                    'subtitle' => '',
                    'desc'     => esc_html__( 'Enter your Youtube URL.', 'piko-construct' ),
                    'validate' => 'url'
            ),
            array(
                    'id'       => 'vimeo',
                    'type'     => 'text',
                    'title'    => esc_html__( 'Vimeo', 'piko-construct' ),
                    'subtitle' => '',
                    'desc'     => esc_html__( 'Enter your vimeo URL.', 'piko-construct' ),
                    'validate' => 'url'
            ),
            array(
                    'id'       => 'tumblr',
                    'type'     => 'text',
                    'title'    => esc_html__( 'Tumblr', 'piko-construct' ),
                    'subtitle' => '',
                    'desc'     => esc_html__( 'Enter your Tumblr URL.', 'piko-construct' ),
                    'validate' => 'url'
            ),
            array(
                    'id'       => 'whatsapp',
                    'type'     => 'text',
                    'title'    => esc_html__( 'Whatsapp', 'piko-construct' ),
                    'subtitle' => '',
                    'desc'     => esc_html__( 'Enter your Whatsapp URL.', 'piko-construct' ),
                    'validate' => 'url'
            ),
            array(
                    'id'       => 'whatsapp',
                    'type'     => 'text',
                    'title'    => esc_html__( 'Whatsapp', 'piko-construct' ),
                    'subtitle' => '',
                    'desc'     => esc_html__( 'Enter your Whatsapp URL.', 'piko-construct' ),
                    'validate' => 'url'
            ),            
        )
    ) );
    
//Newsletter Mailchimp  Settings
    Redux::setSection( $opt_name, array(
        'title' => esc_html__('Newsletter Settings', 'piko-construct'),
        'desc' => esc_html__('Newsletter infomation settings', 'piko-construct'),
        'icon' => 'el-icon-envelope',
        'fields' => array(
            array(
                'id'            => 'mailchimp_api_key',
                'type'          => 'text',
                'title'         => esc_html__('Mailchimp API Key', 'piko-construct'),
                'default'       => '',
                'desc'     => sprintf( wp_kses( __( '<a href="%s" target="__blank"> Click here to get your Mailchimp API key </a>', 'piko-construct' ), array( 'a' => array( 'href' => array(), 'target' => array() ) ) ), 'http://kb.mailchimp.com/accounts/management/about-api-keys' ),
            ),
            array(
                'id'            => 'mailchimp_list_id',
                'type'          => 'text',
                'title'         => esc_html__('Mailchimp List ID', 'piko-construct'),
                'default'       => '',
                'desc'     => sprintf( wp_kses( __( '<a href="%s" target="__blank"> How to find Mailchimp list ID</a>', 'piko-construct' ), array( 'a' => array( 'href' => array(), 'target' => array() ) ) ), 'http://kb.mailchimp.com/lists/managing-subscribers/find-your-list-id' ),
            ),
            array(
                'id'            => 'subscribe_form_title',
                'type'          => 'text',
                'title'         => esc_html__('Subscribe Form Title', 'piko-construct'),
                'default'       => esc_html__( 'Subscribe our newsletter', 'piko-construct' ),
            ),
            array(
                'id'            => 'subscribe_form_input_placeholder',
                'type'          => 'text',
                'title'         => esc_html__('Email Input Placeholder', 'piko-construct'),
                'default'       => esc_html__( 'Your email address...', 'piko-construct' ),
            ),
            array(
                'id'            => 'subscribe_form_submit_text',
                'type'          => 'text',
                'title'         => esc_html__('Submit Text', 'piko-construct'),
                'default'       => esc_html__( 'Submit', 'piko-construct' ),
            ),
            array(
                'id'            => 'subscribe_success_message',
                'type'          => 'text',
                'title'         => esc_html__('Success Message', 'piko-construct'),
                'default'       => esc_html__( 'Your email added our list...', 'piko-construct' ),
            ),
        ),
    ));
    
//    custom css
    Redux::setSection( $opt_name, array(
        'title'   => esc_html__( 'Custom Code', 'piko-construct' ),
        'icon'    => 'el el-icon-css',        
        'fields'  => array(
                array(
                    'id'       => 'custom_css',
                    'type'     => 'ace_editor',
                    'title'    => esc_html__( 'CSS Code', 'piko-construct' ),
                    'subtitle' => esc_html__( 'Wirte or Paste your custom CSS code here.', 'piko-construct' ),
                    'mode'     => 'css',
                    'default'  => " "
                ),
                array(
                    'id'       => 'custom_js',
                    'type'     => 'ace_editor',
                    'title'    => esc_html__( 'Javascript Code', 'piko-construct' ),
                    'subtitle' => esc_html__( 'Wirte or Paste your custom javascript code here.', 'piko-construct' ),
                    'mode'     => 'javascript',
                    'default'  => ""
                )
        )
    ));    
//    import export
    Redux::setSection( $opt_name, array(
        'title'   => esc_html__( 'Import/Export', 'piko-construct' ),
        'icon'    => 'el el-refresh',        
        'fields'    => array(
            array(
                'id'            => 'piko-import-export',
                'type'          => 'import_export',
                'full_width'    => true,
            ),
        ),
    )); 
  
    /*
     * <--- END SECTIONS
     */
    
    function compiler_action($options) {
            
    }
    function pikoworks_redux_update_options_user_can_register( $options ) {
            global $pikoworks;
            $users_can_register = isset( $pikoworks['opt-users-can-register'] ) ? $pikoworks['opt-users-can-register'] : 0;
            update_option( 'users_can_register', $users_can_register );
    }
    function pikoworks_redux_update_options_post_type_portfolio( $options ) {
            global $pikoworks;
            $post_type_portfolio = isset( $pikoworks['optn_enable_portfolio'] ) ? $pikoworks['optn_enable_portfolio'] : 0;
            update_option( 'optn_enable_portfolio', $post_type_portfolio );
    }
    
    
    if ( ! function_exists( 'pikoworks' ) ) {
	function pikoworks( $id, $fallback = false, $key = false ) {
		global $pikoworks;
		if ( $fallback == false ) {
			$fallback = '';
		}
		$output = ( isset( $pikoworks[ $id ] ) && $pikoworks[ $id ] !== '' ) ? $pikoworks[ $id ] : $fallback;
		if ( ! empty( $pikoworks[ $id ] ) && $key ) {
			$output = $pikoworks[ $id ][ $key ];
		}

		return $output;
	}
    }