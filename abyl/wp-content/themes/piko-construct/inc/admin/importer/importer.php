<?php

/* 
 * import data
 */
if(!function_exists('pikoworks_core_admin_css') || !class_exists( 'OCDI_Plugin' )){
    return;
}

function pikoworks_importer_files() {
    $main_domain = 'http://demo.themepiko.com/construct/';
    $import_notice = esc_html__( 'After you import this demo, Need to setup the Slider Revolution separately location: Slider Revolution ->import slider. slider dummy found download package main-> dummy data', 'piko-construct' );
      

  return array(
    array(
      'import_file_name'             => 'Default',
      'categories'                 => array( 'Demo' ),
      'local_import_file'            => trailingslashit( get_template_directory() ) . 'inc/admin/importer/dummy/main/dummy.xml',
      'local_import_widget_file'     => trailingslashit( get_template_directory() ) . 'inc/admin/importer/dummy/main/widgets.json',
      'local_import_redux'           => array(
        array(
          'file_path'   => trailingslashit( get_template_directory() ) . 'inc/admin/importer/dummy/main/options.json',
          'option_name' => 'pikoworks',
        ),
      ),
      'import_preview_image_url'     => pikoworks_theme_uri . 'inc/admin/importer/dummy/main/screenshot.jpg',
      'import_notice'                => $import_notice,
      'preview_url'                  => $main_domain .'main',
    ),
    array(
      'import_file_name'             => 'Main + Product',
      'categories'                 => array( 'Demo' ),  
      'local_import_file'            => trailingslashit( get_template_directory() ) . 'inc/admin/importer/dummy/main_product/dummy.xml',
      'local_import_widget_file'     => trailingslashit( get_template_directory() ) . 'inc/admin/importer/dummy/main_product/widgets.json',
      'local_import_redux'           => array(
        array(
          'file_path'   => trailingslashit( get_template_directory() ) . 'inc/admin/importer/dummy/main_product/options.json',
          'option_name' => 'pikoworks',
        ),
      ),
      'import_preview_image_url'     => pikoworks_theme_uri . 'inc/admin/importer/dummy/main_product/screenshot.jpg',
      'import_notice'                => $import_notice,
      'preview_url'                  => $main_domain .'main',
    ),
    array(
      'import_file_name'             => 'Woocommerce',
      'categories'                 => array( 'Demo' ),  
      'local_import_file'            => trailingslashit( get_template_directory() ) . 'inc/admin/importer/dummy/woocommerce/dummy.xml',
      'local_import_widget_file'     => trailingslashit( get_template_directory() ) . 'inc/admin/importer/dummy/woocommerce/widgets.json',
      'local_import_redux'           => array(
        array(
          'file_path'   => trailingslashit( get_template_directory() ) . 'inc/admin/importer/dummy/woocommerce/options.json',
          'option_name' => 'pikoworks',
        ),
      ),
      'import_preview_image_url'     => pikoworks_theme_uri . 'inc/admin/importer/dummy/woocommerce/screenshot.jpg',
      'import_notice'                => $import_notice,
      'preview_url'                  => $main_domain .'woocommerce',
    ),
    array(
      'import_file_name'             => 'Renovation',
      'categories'                 => array( 'Demo' ), 
      'local_import_file'            => trailingslashit( get_template_directory() ) . 'inc/admin/importer/dummy/renovation/dummy.xml',
      'local_import_widget_file'     => trailingslashit( get_template_directory() ) . 'inc/admin/importer/dummy/renovation/widgets.json',
      'local_import_redux'           => array(
        array(
          'file_path'   => trailingslashit( get_template_directory() ) . 'inc/admin/importer/dummy/renovation/options.json',
          'option_name' => 'pikoworks',
        ),
      ),
      'import_preview_image_url'     => pikoworks_theme_uri . 'inc/admin/importer/dummy/renovation/screenshot.jpg',
      'import_notice'                => $import_notice,
      'preview_url'                  => $main_domain .'renovation',
    ),
    array(
      'import_file_name'             => 'Making (Demo4)',
      'categories'                 => array( 'Demo' ),  
      'local_import_file'            => trailingslashit( get_template_directory() ) . 'inc/admin/importer/dummy/demo4/dummy.xml',
      'local_import_widget_file'     => trailingslashit( get_template_directory() ) . 'inc/admin/importer/dummy/demo4/widgets.json',
      'local_import_redux'           => array(
        array(
          'file_path'   => trailingslashit( get_template_directory() ) . 'inc/admin/importer/dummy/demo4/options.json',
          'option_name' => 'pikoworks',
        ),
      ),
      'import_preview_image_url'     => pikoworks_theme_uri . 'inc/admin/importer/dummy/demo4/screenshot.jpg',
      'import_notice'                => $import_notice,
      'preview_url'                  => $main_domain .'demo4',
    ),
    array(
      'import_file_name'             => 'Construction (Demo5)',
      'categories'                 => array( 'Demo' ),  
      'local_import_file'            => trailingslashit( get_template_directory() ) . 'inc/admin/importer/dummy/demo5/dummy.xml',
      'local_import_widget_file'     => trailingslashit( get_template_directory() ) . 'inc/admin/importer/dummy/demo5/widgets.json',
      'local_import_redux'           => array(
        array(
          'file_path'   => trailingslashit( get_template_directory() ) . 'inc/admin/importer/dummy/demo5/options.json',
          'option_name' => 'pikoworks',
        ),
      ),
      'import_preview_image_url'     => pikoworks_theme_uri . 'inc/admin/importer/dummy/demo5/screenshot.jpg',
      'import_notice'                => $import_notice,
      'preview_url'                  => $main_domain .'demo5',
    ),
    array(
      'import_file_name'             => 'Contact from)',
      'categories'                 => array( 'Other page' ),  
      'local_import_file'            => trailingslashit( get_template_directory() ) . 'inc/admin/importer/dummy/contact/dummy.xml',      
      'import_preview_image_url'     => pikoworks_theme_uri . 'inc/admin/importer/dummy/contact/screenshot.jpg',
      'preview_url'                  => $main_domain .'main/contact',
    ),
    array(
      'import_file_name'             => 'Member',
      'categories'                 => array( 'Other page' ),  
      'local_import_file'            => trailingslashit( get_template_directory() ) . 'inc/admin/importer/dummy/member/dummy.xml',      
      'import_preview_image_url'     => pikoworks_theme_uri . 'inc/admin/importer/dummy/member/screenshot.jpg',
      'preview_url'                  => $main_domain .'main/team',
    ),
    array(
      'import_file_name'             => 'Portfolio',
      'categories'                 => array( 'Other page' ),  
      'local_import_file'            => trailingslashit( get_template_directory() ) . 'inc/admin/importer/dummy/portfolio/dummy.xml',      
      'import_preview_image_url'     => pikoworks_theme_uri . 'inc/admin/importer/dummy/portfolio/screenshot.jpg',
      'preview_url'                  => $main_domain .'main/portfolio-grid',
    ),
    array(
      'import_file_name'             => 'Product',
      'categories'                 => array( 'Other page' ),  
      'local_import_file'            => trailingslashit( get_template_directory() ) . 'inc/admin/importer/dummy/product/dummy.xml',      
      'import_preview_image_url'     => pikoworks_theme_uri . 'inc/admin/importer/dummy/product/screenshot.jpg',
      'preview_url'                  => $main_domain .'main/shop',
    ),
    array(
      'import_file_name'             => 'Services',
      'categories'                 => array( 'Other page' ),  
      'local_import_file'            => trailingslashit( get_template_directory() ) . 'inc/admin/importer/dummy/services/dummy.xml',      
      'import_preview_image_url'     => pikoworks_theme_uri . 'inc/admin/importer/dummy/services/screenshot.jpg',
      'preview_url'                  => $main_domain .'main/service',
    ),
      
    array(
      'import_file_name'             => 'Testimonial',
      'categories'                 => array( 'Other page' ),  
      'local_import_file'            => trailingslashit( get_template_directory() ) . 'inc/admin/importer/dummy/testimonial/dummy.xml',      
      'import_preview_image_url'     => pikoworks_theme_uri . 'inc/admin/importer/dummy/testimonial/screenshot.jpg',
    ),
      
      
  );
}
add_filter( 'pt-ocdi/import_files', 'pikoworks_importer_files' );

function pikoworks_after_importer_setup() {
	// Assign menus to their locations.
	$main_menu = get_term_by( 'name', 'Main menu', 'nav_menu' );
	$top_menu = get_term_by( 'name', 'Top menu', 'nav_menu' );
	$footer_menu = get_term_by( 'name', 'Footer Menu', 'nav_menu' );
	$category_menu = get_term_by( 'name', 'Service', 'nav_menu' );
	$secondary_menu = get_term_by( 'name', 'Secondary Menu', 'nav_menu' );

	set_theme_mod( 'nav_menu_locations', array(
			'primary' => $main_menu->term_id,
			'top_menu' => $top_menu->term_id,
			'footer' => $footer_menu->term_id,
			'service' => $category_menu->term_id,
			'secondary' => $secondary_menu->term_id,
		)
	);

	// Assign front page and posts page (blog page).
	$front_page_id = get_page_by_title( 'Home' );
	$blog_page_id  = get_page_by_title( 'Blog' );

	update_option( 'show_on_front', 'page' );
	update_option( 'page_on_front', $front_page_id->ID );
	update_option( 'page_for_posts', $blog_page_id->ID );

}
add_action( 'pt-ocdi/after_import', 'pikoworks_after_importer_setup' );
