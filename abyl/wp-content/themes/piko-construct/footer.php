<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 */
?>


                    </div><!-- .row -->
		</div><!-- .site-content -->
            </div><!-- .site-inner -->
        </div> <?php //just end div fixed menu layout 3  ?>
            
            
            
            <div id="mobile_menu_wrapper_overlay"></div>
            <div id="mobile_menu_wrapper" class="hidden-md hidden-lg">
            <?php pikoworks_get_mobile_main_menu(); ?>
            </div>
            
            <?php pikoworks_footer_style(); ?>        
            
	
</div><!-- .site -->

<?php wp_footer(); ?>
</body>
</html>
