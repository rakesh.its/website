<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @author themepiko
 */


get_header(); ?>
<?php
$layout_404_number = isset( $GLOBALS['pikoworks']['optn_404_style1'] ) ? $GLOBALS['pikoworks']['optn_404_style1'] : esc_html__('404', 'piko-construct');
$optn_404_heading = isset( $GLOBALS['pikoworks']['optn_404_heading'] ) ? $GLOBALS['pikoworks']['optn_404_heading'] : esc_html__('That page does not exist','piko-construct');
$optn_404_content = isset( $GLOBALS['pikoworks']['optn_404_content'] ) ? $GLOBALS['pikoworks']['optn_404_content'] : esc_html__('The link you clicked might be corrupted or the page may have been removed.. Maybe try a search?', 'piko-construct');
$optn_404_btn = isset( $GLOBALS['pikoworks']['optn_404_btn'] ) ? $GLOBALS['pikoworks']['optn_404_btn'] : esc_html__('GO TO HOME', 'piko-construct');
$optn_404_img = isset( $GLOBALS['pikoworks']['optn_404_img'] ) ? $GLOBALS['pikoworks']['optn_404_img'] : array('url' => get_template_directory_uri(). '/assets/images/404.png');
$allow_html = array(    
    'span' => array('class' => array() ), 'br' => array(), 'strong' => array(), 
);
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<section class="not-found clearfix">                            
                            <div class="col-sm-6 col-sm-offset-6">
                                <img src="<?php echo esc_url( $optn_404_img['url'] ) ?>"  class="img-responsive" alt="<?php echo esc_attr($optn_404_heading); ?>">

                                <div class="page-content">
                                <div class="number-404"><?php  echo wp_kses( $layout_404_number , $allow_html); ?></div>
				<h1 class="page-title"><?php echo esc_attr( $optn_404_heading ); ?></h1>
                                    <p><?php echo esc_attr( $optn_404_content); ?></p>
                                    <?php get_search_form(); ?>
                                    <div class="error-button">
                                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="piko-button"><strong><?php echo esc_attr($optn_404_btn); ?></strong></a>
                                    </div>                                    
				</div><!-- .page-content -->
                            </div>
			</section><!-- .error-404 -->
		</main><!-- .site-main -->
	</div><!-- .content-area -->
<?php get_footer(); ?>
