<?php
/**
 * functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme you can override certain functions (those wrapped
 * in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before
 * the parent theme's file, so the child theme functions would be used.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @link https://codex.wordpress.org/Child_Themes
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters,
 * {@link https://codex.wordpress.org/Plugin_API}
 *
 */

/**
 * Define variables
 */

defined( 'pikoworks_theme_dir' )        or   define( 'pikoworks_theme_dir',          trailingslashit( get_template_directory() ) );
defined( 'pikoworks_theme_uri' )        or   define( 'pikoworks_theme_uri',          trailingslashit(get_template_directory_uri()) );
defined( 'pikoworks_inc_dir' )          or   define( 'pikoworks_inc_dir',            trailingslashit(pikoworks_theme_dir . 'inc' ));
defined( 'pikoworks_js' )               or   define( 'pikoworks_js',                 pikoworks_theme_uri  . 'assets/js' );
defined( 'pikoworks_css' )              or   define( 'pikoworks_css',                pikoworks_theme_uri . 'assets/css' );
defined( 'pikoworks_image' )            or   define( 'pikoworks_image',              pikoworks_theme_uri . 'assets/images' );
defined( 'pikoworks_plugin' )           or   define( 'pikoworks_plugin',             pikoworks_theme_uri . 'assets/plugin' );
defined( 'pikoworks_options_preset' )   or   define( 'pikoworks_options_preset',     pikoworks_theme_dir . 'presets' );
defined( 'pikoworks_theme_version' )    or   define( 'pikoworks_theme_version',      wp_get_theme()->get('Version') );



if (!function_exists('pikoworks_inc_library')) {
    function pikoworks_inc_library(){
        require_once(pikoworks_inc_dir . 'admin/image-resize.php');
        require_once(pikoworks_inc_dir . 'admin/mega-menu.php');
        require_once(pikoworks_inc_dir . 'admin/plugin-activation/class-tgm-plugin-activation.php');
        require_once(pikoworks_inc_dir . 'admin/plugin-activation/plugin-init.php');
        require_once(pikoworks_inc_dir . 'admin/importer/importer.php');
        require_once(pikoworks_inc_dir . 'admin/theme-options.php');
        
        require_once(pikoworks_inc_dir . 'theme-setup.php');
        require_once(pikoworks_inc_dir . 'admin-enqueue.php');
        require_once(pikoworks_inc_dir . 'layout/pre-loader.php');
        require_once(pikoworks_inc_dir . 'layout/wmpl-switcher.php');
        require_once(pikoworks_inc_dir . 'layout/breadcrumbs.php');
        require_once(pikoworks_inc_dir . 'layout/coming-soon-page.php');
        require_once(pikoworks_inc_dir . 'configure/header-configure.php');
        require_once(pikoworks_inc_dir . 'configure/blog-configure.php');
        require_once(pikoworks_inc_dir . 'configure/template-tags.php');
        require_once(pikoworks_inc_dir . 'configure/sidebar-configure.php');
        
        if(function_exists( 'WC' )){            
            require_once(pikoworks_inc_dir . 'woocommerce/function.php');
            require_once(pikoworks_inc_dir . 'woocommerce/hooks.php');
        }
        require_once(pikoworks_inc_dir . 'theme-functions.php');
        require_once(pikoworks_inc_dir . 'theme-filter.php');
        require_once(pikoworks_inc_dir . 'register-sidebar.php');
        require_once(pikoworks_inc_dir . 'customizer.php');
        require_once(pikoworks_inc_dir . 'frontend-enqueue.php'); 
    }
    pikoworks_inc_library();    
}