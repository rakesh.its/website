<?php
/**
 * themeplate | edit
 * 
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;
get_header( 'shop' );



$sidebar_position = pikoworks_get_option_data( 'optn_product_sidebar_pos', 'fullwidth' );
$left_sidebar = pikoworks_get_option_data( 'optn_product_sidebar', 'sidebar-4' );
$right_sidebar = pikoworks_get_option_data( 'optn_product_sidebar_left', '' );
$primary_class = pikoworks_primary_product_class();
$secondary_class = pikoworks_secondary_product_class();


$enable_shop_grid_masonry = pikoworks_get_option_data( 'optn_enable_shop_grid_masonry', '0' );
$products_per_row = pikoworks_get_option_data( 'optn_woo_products_per_row', 3 );
$product_count = 0;

function pikoworks_porduct_description_before(){
        echo '<div class="product-description">';
}
function pikoworks_porduct_description_middle(){
        echo '<div class="product-list-button text-right">';
}
function pikoworks_porduct_description_after(){
        echo '</div>';
}
function pikoworks_porduct_description_after_two(){
        echo '</div>';
}

$mode_view = apply_filters('pikoworks_filter_products_mode_view','grid');
if($mode_view == 'list'){ 
  add_action( 'woocommerce_shop_loop_item_title', 'pikoworks_porduct_description_before',9);
  add_action( 'woocommerce_after_shop_loop_item_title', 'pikoworks_porduct_description_after_two',8);
  add_action( 'woocommerce_after_shop_loop_item_title', 'pikoworks_porduct_description_middle', 9);
  add_action( 'woocommerce_after_shop_loop_item', 'pikoworks_porduct_description_after',13);
}


?>
<?php if ( $sidebar_position == 'both' ): ?>
	<aside id="secondary" class="widget-area <?php echo esc_attr( $secondary_class ); ?>" role="complementary">
		<?php dynamic_sidebar( $right_sidebar ); ?>
	</aside><!-- .sidebar left.widget-area -->
<?php endif; ?>


 <div id="primary" class="content-area <?php echo esc_attr( $primary_class ); ?>">
		<main id="main" class="site-main" role="main">

	<?php
		/**
		 * woocommerce_before_main_content hook.
		 *
		 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
		 * @hooked woocommerce_breadcrumb - 20
		 * @hooked WC_Structured_Data::generate_website_data() - 30
		 */
		do_action( 'woocommerce_before_main_content' );
	?>

    <header class="woocommerce-products-header">

		<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>

			<h1 class="woocommerce-products-header__title page-title"><?php woocommerce_page_title(); ?></h1>

		<?php endif; ?>

		<?php
			/**
			 * woocommerce_archive_description hook.
			 *
			 * @hooked woocommerce_taxonomy_archive_description - 10
			 * @hooked woocommerce_product_archive_description - 10
			 */
			do_action( 'woocommerce_archive_description' );
		?>

    </header>

		<?php if ( woocommerce_product_loop() ) {
				/**
				 * woocommerce_before_shop_loop hook.
				 *
				 * @hooked woocommerce_result_count - 20
				 * @hooked woocommerce_catalog_ordering - 30
				 */
				do_action( 'woocommerce_before_shop_loop' );
                                echo'<div class="clearfix"></div>';
			
                                 woocommerce_product_loop_start();

			?>
                            <div class="column-wrap max-col-<?php echo esc_attr($products_per_row);?>" data-layoutmode="fitRows">
				<?php  
                                if ( wc_get_loop_prop( 'total' ) ) {
                                        while ( have_posts() ) {
                                                the_post();

                                                /**
                                                 * Hook: woocommerce_shop_loop.
                                                 *
                                                 * @hooked WC_Structured_Data::generate_product_data() - 10
                                                 */
                                                do_action( 'woocommerce_shop_loop' );

                                                wc_get_template_part( 'content', 'product' );
                                        }
                                }
                                
                                ?>
                            </div>
			<?php woocommerce_product_loop_end();                         
                        
                        /**
                         * Hook: woocommerce_after_shop_loop.
                         *
                         * @hooked woocommerce_pagination - 10
                         */
                        do_action( 'woocommerce_after_shop_loop' );
			
                }else {
                    /**
                     * Hook: woocommerce_no_products_found.
                     *
                     * @hooked wc_no_products_found - 10
                     */
                    do_action( 'woocommerce_no_products_found' );
                }
            /**
             * Hook: woocommerce_after_main_content.
             *
             * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
             */
            do_action( 'woocommerce_after_main_content' );    
                        
           ?>
 </main>
</div> 

<?php if ( $sidebar_position != 'fullwidth' || $sidebar_position == 'both' ): ?>
	<aside id="secondary" class="widget-area <?php echo esc_attr( $secondary_class ); ?>" role="complementary">
		<?php dynamic_sidebar( $left_sidebar ); ?>
	</aside><!-- .sidebar .widget-area -->
<?php endif; ?>         

<?php get_footer( 'shop' ); ?>
