<?php
/**
* Footer Coming Soon
* @author Themepiko
* 
* Exit if accessed directly
*/

if( !defined( 'ABSPATH' ) ) {
	exit;
}
?>  

</main> <!--.main-->
<!-- End / Site main -->
<?php wp_footer(); ?>
</body>
</html>