<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 */
get_header();


$sidebar_position = isset( $GLOBALS['pikoworks']['optn_archive_service_sidebar_pos'] ) ? trim( $GLOBALS['pikoworks']['optn_archive_service_sidebar_pos'] ) : 'left';
$left_sidebar = isset( $GLOBALS['pikoworks']['optn_archive_service_sidebar'] ) ? trim( $GLOBALS['pikoworks']['optn_archive_service_sidebar'] ) : 'sidebar';
$right_sidebar = isset( $GLOBALS['pikoworks']['optn_archive_service_sidebar_left'] ) ? trim( $GLOBALS['pikoworks']['optn_archive_service_sidebar_left'] ) : '';
$archive_display_columns = isset( $GLOBALS['pikoworks']['optn_archive_service_display_columns'] ) ? trim( $GLOBALS['pikoworks']['optn_archive_service_display_columns'] ) : '3';
$primary_class = pikoworks_primary_service_class();
$secondary_class = pikoworks_secondary_service_class();


if($archive_display_columns == '1'){
    $GLOBALS['pikoworks_archive_loop']['image-size'] = 'image-full-width';
}elseif($archive_display_columns == '2'){
    $GLOBALS['pikoworks_archive_loop']['image-size'] = '2cols-image'; 
}elseif($archive_display_columns == '3'){
    $GLOBALS['pikoworks_archive_loop']['image-size'] = '3cols-image'; 
}elseif($archive_display_columns == '4'){
    $GLOBALS['pikoworks_archive_loop']['image-size'] = '4cols-image'; 
}
$overlay = pikoworks_get_option_data('opt_service_overlay_style', '');

?>
<?php if ( $sidebar_position == 'both' ): ?>
	<aside id="secondary" class="widget-area <?php echo esc_attr( $secondary_class ); ?>" role="complementary">
		<?php dynamic_sidebar( $right_sidebar ); ?>
	</aside><!-- .sidebar left.widget-area -->
<?php endif; ?>

	<div id="primary" class="content-area blog-wrap layout-container grid overlay-style-2 <?php echo esc_attr( $primary_class . ' ' . $overlay ); ?>">
		<main id="main" class="site-main row" role="main">

		<?php if ( have_posts() ) : ?>

			<?php if ( is_home() && ! is_front_page() ) : ?>
				<header>
					<h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
				</header>
			<?php endif; ?>
                        
                        <?php                
                        /**
                         * pikoworks_before_loop_posts hook
                         * 
                         * @hooked pikoworks_before_loop_posts_wrap - 10 (locate in inc/template-tags.php )
                         **/ 
                        do_action( 'pikoworks_before_loop_posts' ); 
                        
			// Start the loop.
			while ( have_posts() ) : the_post(); 
				/*
				 * Include the Post-Format-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
				 */
                                
                                get_template_part( 'template-parts/archive/content-service');                                                                     
                              
			// End the loop.
			endwhile;                           
                        pikoworks_archive_loop_reset();
                        /**
                         * pikoworks_after_loop_posts hook
                         * 
                         * @hooked pikoworks_after_loop_posts_wrap - 10 (locate in inc/template-tags.php )
                         **/ 
                        do_action( 'pikoworks_after_loop_posts' ); 
                        
			echo'<div class="clearfix"></div>';                      
                        // Previous/next page navigation.
			the_posts_pagination( array(
				'prev_text'          => esc_html__( 'Previous page', 'piko-construct'),
				'next_text'          => esc_html__( 'Next page', 'piko-construct'),
				'before_page_number' => '<span class="meta-nav screen-reader-text">' . esc_html__( 'Page', 'piko-construct' ) . ' </span>',
			) );

		// If no content, include the "No posts found" template.
		else :
			pikoworks_get_template( 'archive/content', 'none' );

		endif;
		?>   

		</main><!-- .site-main -->
	</div><!-- .content-area -->

<?php if ( $sidebar_position != 'fullwidth' || $sidebar_position == 'both' ): ?>
	<aside id="secondary" class="widget-area <?php echo esc_attr( $secondary_class ); ?>" role="complementary">
		<?php dynamic_sidebar( $left_sidebar ); ?>
	</aside><!-- .sidebar .widget-area -->
<?php endif; ?>
<?php get_footer(); ?>
