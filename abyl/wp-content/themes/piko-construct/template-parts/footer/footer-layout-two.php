<?php
/*
 * @author themepiko
 */
$prefix = 'pikoworks_';
$footer_width =  get_post_meta(get_the_ID(), $prefix . 'footer_width',true);
if (!isset($footer_width) || $footer_width == '-1' || $footer_width == '') {
    $footer_width = isset( $GLOBALS['pikoworks']['footer-width-content'] ) ? $GLOBALS['pikoworks']['footer-width-content'] : 'container';
} 
$footer_style =  get_post_meta(get_the_ID(), $prefix . 'footer_style',true);
if (!isset($footer_style) || $footer_style == '-1' || $footer_style == '') {
    $footer_style = isset( $GLOBALS['pikoworks']['optn_footer_style'] ) ? $GLOBALS['pikoworks']['optn_footer_style'] : 'style1';
}

 $footer_parallax = isset( $GLOBALS['pikoworks']['footer_parallax'] ) ? $GLOBALS['pikoworks']['footer_parallax'] : '';
 if($footer_parallax == 1){
       $footer_parallax = 'footer-parallax';
    }

$footer_right = isset( $GLOBALS['pikoworks']['optn_footer_right'] ) ? $GLOBALS['pikoworks']['optn_footer_right'] : '';
$footer_copyright_text = isset( $GLOBALS['pikoworks']['sub_footer_text'] ) ? $GLOBALS['pikoworks']['sub_footer_text'] : sprintf( esc_html__( 'Proudly powered by %s', 'piko-construct' ), 'WordPress' );
$allow_html = array(
    'a' => array('href' => array(), 'title' => array(), 'target' => array() ),
    'em' => array(), 'br' => array(), 'strong' => array(), 'i' => array('class' => array()), 'ul' => array('class' => array()), 'li' => array(), 
);
?>
<footer id="colophon" class="site-footer layout2 <?php echo esc_attr($footer_parallax); ?>" role="contentinfo">
    
    <div class="<?php echo esc_attr($footer_width); ?>">
        <div class="footer-perallx-wrap">
    <?php if($footer_style == 'style1'): ?>
        <div class="row">
            <?php pikoworks_footer_sidebar_one(); ?>
            <?php pikoworks_footer_sidebar_two(); ?> 
        </div>
        <div class="info-center-wrap">
            <div class="site-info">
                <p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
                <?php echo '<p>' . wp_kses( $footer_copyright_text , $allow_html) . '</p>'; ?>
            </div><!-- .site-info -->
            <!--back to top-->
            <a href="#0" class="back-top"><i class="lnr-arrow-up"></i></a>
        </div>
        <?php else: ?>
        <div class="row">
            <?php pikoworks_footer_sidebar_one(); ?> 
            <?php pikoworks_footer_sidebar_two(); ?> 
            <div class="main-footer">
                <div class="col-md-6 col-sm-6 info-left">
                    <div class="site-info">
                        <p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
                        <?php echo '<p>' . wp_kses( $footer_copyright_text , $allow_html) . '</p>'; ?>
                    </div><!-- .site-info -->
                </div>                                       
                <div class="col-md-6 col-sm-6">
                    <div class="info-right-wrap">
                   <?php if($footer_right == 1): ?>
                        <div class="social-icon">
                            <?php
                                foreach ($GLOBALS['pikoworks']['footer_social'] as $key => $val){
                                   if(! empty($GLOBALS['pikoworks'][$key]) && $val == 1 ){
                                       echo "<a target='_blank' href='" . esc_url($GLOBALS['pikoworks'][$key]) ."'><i class='social-icon fa fa-" . esc_attr($key) . "'></i></a>";
                                   }
                               }
                            ?>
                        </div><!-- .social-icon -->
                    <?php elseif($footer_right == 2):?>
                        <div class="payments-icon">
                            <ul>
                                <?php
                                $payment_photo_ids = explode( ',', $GLOBALS['pikoworks']['optn_payment_logo_upload']);
                                foreach($payment_photo_ids as $payment_photo_id):                                           
                                ?>                                           
                                <li><img src="<?php echo wp_get_attachment_url( $payment_photo_id ); ?>" alt="<?php esc_attr_e('payment logo', 'piko-construct') ?>"/></li>                                       
                                <?php endforeach; ?>                                      
                            </ul>
                        </div><!-- .payments-icon-->
                    <?php endif; ?>

                    <?php if ( has_nav_menu( 'footer' ) ) : ?>
                        <nav class="footer-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Footer Links Menu', 'piko-construct' ); ?>">
                                <?php
                                        wp_nav_menu( array(
                                                'theme_location' => 'footer',
                                                'menu_class'     => 'footer-links-menu',
                                                'depth'          => 1,

                                        ) );
                                ?>
                        </nav><!-- .footer-navigation -->
                    <?php endif; ?>
                    </div> <!--.info-right-wrap-->
                </div>
                <!--back to top-->
            <a href="#0" class="back-top"><i class="lnr-arrow-up"></i></a>
            </div>                                 
        </div><!-- .row -->
        <?php endif; //$footer_style ?>
    </div>
    </div>
</footer>