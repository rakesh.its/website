<?php
/**
 * The template part for displaying results in search pages
 */
$size = 'full';
if (isset($GLOBALS['pikoworks_archive_loop']['image-size'])) {
    $size = $GLOBALS['pikoworks_archive_loop']['image-size'];
}
$charlength = isset( $GLOBALS['pikoworks']['optn_search_except_word'] ) ? trim( $GLOBALS['pikoworks']['optn_search_except_word'] ) : '65';
?>

<article id="post-<?php the_ID(); ?>">	
        <?php
            $thumbnail = pikoworks_post_format($size);
            if (!empty($thumbnail)) : ?>
                <figure class="entry-thumbnail-wrap">
                    <?php echo wp_kses_post($thumbnail); ?>
                </figure>
            <?php endif; ?>
        <header>
               <?php pikoworks_entry_header_two(); ?>
        </header><!-- .entry-header -->
        <div class="entry-content content-post">
            <?php pikoworks_trim_word_vc($charlength); //remove vc shortcode ?>
            <?php if ( 'page' != get_post_type() ) : ?>
                <?php echo pikoworks_read_more_link();?>
            <?php endif; ?>            
        </div><!-- /.entry-content -->	
	<?php if ( 'post' === get_post_type() ) : ?>               
		<footer class="entry-footer">
			<?php pikoworks_entry_footer(); ?>
		</footer><!-- .entry-footer -->	
	<?php endif; ?>
</article><!-- #post-## -->