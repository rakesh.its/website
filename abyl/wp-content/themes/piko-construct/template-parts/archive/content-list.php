<?php
/**
 * The template part for displaying content
 */

$size = 'full';
if (isset($GLOBALS['pikoworks_archive_loop']['image-size'])) {
    $size = $GLOBALS['pikoworks_archive_loop']['image-size'];
}

$archive_layout_style = isset( $GLOBALS['pikoworks']['optn_archive_layout_style'] ) ? trim( $GLOBALS['pikoworks']['optn_archive_layout_style'] ) : '';
$archive_title_position = isset( $GLOBALS['pikoworks']['optn_archive_title_position'] ) ? trim( $GLOBALS['pikoworks']['optn_archive_title_position'] ) : 'image-bottom';
$except_word = isset( $GLOBALS['pikoworks']['optn_archive_except_word'] ) ? trim( $GLOBALS['pikoworks']['optn_archive_except_word'] ) : '55';
?>


<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="clearfix">
        <div class="col-md-6 col-sm-6">
            <?php
            $thumbnail = pikoworks_post_format($size);
            if (!empty($thumbnail)) : ?>
                <figure class="entry-thumbnail-wrap">
                    <?php echo wp_kses_post($thumbnail); ?>
                </figure>
            <?php endif; ?>
        </div>
        <div class="col-md-6 col-sm-6">
            <header class="entry-header">
                <?php pikoworks_entry_header_two(); ?>
            </header><!-- .entry-header -->
            <div class="entry-content">
                <?php
                    /* translators: %s: Name of current post */                  
                    
                    if ( ! has_excerpt() ) {
                    echo '<p>'. wp_trim_words( get_the_content(), esc_attr($except_word), '...  ' ) . pikoworks_read_more_link() . '</p>';
                    } else { 
                          echo '<p>'. wp_trim_words( the_excerpt(), esc_attr($except_word), '...' ) . pikoworks_read_more_link() . '</p>';
                    }

                    wp_link_pages( array(
                            'before'      => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:', 'piko-construct' ) . '</span>',
                            'after'       => '</div>',
                            'link_before' => '<span>',
                            'link_after'  => '</span>',
                            'pagelink'    => '<span class="screen-reader-text">' . esc_html__( 'Page', 'piko-construct' ) . ' </span>%',
                            'separator'   => '<span class="screen-reader-text">, </span>',
                    ) );
                ?>
            </div><!-- .entry-content -->
            <footer class="entry-footer">
                <?php pikoworks_entry_footer(); ?>
            </footer><!-- .entry-footer -->
        </div>
    </div>     
</article><!-- #post-## -->