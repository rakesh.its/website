<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 */
$size = 'full';
if (isset($GLOBALS['pikoworks_archive_loop']['image-size'])) {
    $size = $GLOBALS['pikoworks_archive_loop']['image-size'];
}


$archive_title_position = isset( $GLOBALS['pikoworks']['optn_archive_title_position'] ) ? trim( $GLOBALS['pikoworks']['optn_archive_title_position'] ) : 'image-bottom';

$prefix = 'pikoworks_';

$quote_content = get_post_meta(get_the_ID(), $prefix.'post_format_quote', true);
$quote_author = get_post_meta(get_the_ID(), $prefix.'post_format_quote_author', true);
$quote_author_url = get_post_meta(get_the_ID(), $prefix.'post_format_quote_author_url', true);
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
   <header class="entry-header">        
    <?php pikoworks_entry_header_two(); ?>
    </header><!-- .entry-header -->        
    <div class="entry-content">
        <?php if (empty($quote_content) || empty($quote_author) || empty($quote_author_url)) : ?>
            <?php the_content(); ?>
        <?php else : ?>
            <blockquote>
                <p><?php echo esc_html($quote_content); ?></p>
                <cite><a href="<?php echo esc_url($quote_author_url) ?>" title="<?php echo esc_attr($quote_author); ?>"><?php echo esc_attr($quote_author); ?></a></cite>
            </blockquote>
        <?php endif; ?>
    </div>
</article>
