<?php
//Post social shear
$social_share = pikoworks_get_option_data('optn_blog_single_single_share_socials', array());
?>

<?php if ( !empty( $social_share ) ): ?>
        <div class="piko-social-btn">

            <?php if ( in_array( 'facebook', $social_share ) ): ?>                    
                <a class="btn-bounce-top" href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" target="_blank">
                    <i class="fa fa-facebook"></i>
                    <span class="text"><?php echo sprintf( esc_html__( 'Share "%s" on Facebook', 'piko-construct' ), get_the_title() ); ?></span>
                </a>
            <?php endif; ?>
            <?php if ( in_array( 'twitter', $social_share ) ): ?>
                <a class="btn-bounce-top" href="https://twitter.com/home?status=<?php the_permalink(); ?>" target="_blank">
                    <i class="fa fa-twitter"></i>
                    <span class="text"><?php echo sprintf( esc_html__( 'Post status "%s" on Twitter', 'piko-construct' ), get_the_title() ); ?></span>
                </a>
            <?php endif; ?>
            <?php if ( in_array( 'gplus', $social_share ) ): ?>
                <a class="btn-bounce-top" href="https://plus.google.com/share?url=<?php the_permalink(); ?>" target="_blank">
                    <i class="fa fa-google-plus"></i>
                    <span class="text"><?php echo sprintf( esc_html__( 'Share "%s" on Google Plus', 'piko-construct' ), get_the_title() ); ?></span>
                </a>
            <?php endif; ?>
            <?php if ( in_array( 'pinterest', $social_share ) ): ?>
                <a class="btn-bounce-top" href="https://pinterest.com/pin/create/button/?url=<?php the_permalink(); ?>&amp;description=<?php echo urlencode( get_the_excerpt() ); ?>" target="_blank">
                    <i class="fa fa-pinterest"></i>
                    <span class="text"><?php echo sprintf( esc_html__( 'Pin "%s" on Pinterest', 'piko-construct' ), get_the_title() ); ?></span>
                </a>
            <?php endif; ?>
            <?php if ( in_array( 'linkedin', $social_share ) ): ?>
                <a class="btn-bounce-top" href="https://www.linkedin.com/shareArticle?mini=true&amp;url=<?php the_permalink(); ?>&amp;title=<?php echo urlencode( get_the_title() ); ?>&amp;summary=<?php echo urlencode( get_the_excerpt() ); ?>&amp;source=<?php echo urlencode( get_bloginfo( 'name' ) ); ?>" target="_blank">
                    <i class="fa fa-linkedin"></i>
                    <span class="text"><?php echo sprintf( esc_html__( 'Share "%s" on LinkedIn', 'piko-construct' ), get_the_title() ); ?></span>
                </a>
            <?php endif; ?>          
        </div>       

<?php endif; // End if ( !empty( $socials_shared ) ) ?>



