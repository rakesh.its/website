<?php
/**
 * The template part for displaying single posts
 *
 */
$size = 'full';
if (isset($GLOBALS['pikoworks_archive_loop']['image-size'])) {
    $size = $GLOBALS['pikoworks_archive_loop']['image-size'];
}
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>	
         <?php
    $thumbnail = pikoworks_post_format($size);
    if (!empty($thumbnail)) : ?>
        <figure class="entry-thumbnail-wrap">
            <?php echo wp_kses_post($thumbnail); ?>
        </figure>
    <?php endif; ?> 
        
            <?php
//            echo '<header class="entry-header">';
//            pikoworks_entry_header_two();
//            echo '</header><!-- .entry-header --> ';
            ?>           
	<div class="entry-content">
            <?php
                the_content();?>
                <div class="blog-title">
                    <?php pikoworks_blog_meta();?>
                </div>
                <?php   
                wp_link_pages( array(
                        'before'      => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:', 'piko-construct' ) . '</span>',
                        'after'       => '</div>',
                        'link_before' => '<span>',
                        'link_after'  => '</span>',
                        'pagelink'    => '<span class="screen-reader-text">' . esc_html__( 'Page', 'piko-construct' ) . ' </span>%',
                        'separator'   => '<span class="screen-reader-text">, </span>',
                ) );                
            ?>
	</div><!-- .entry-content -->
	<footer class="entry-footer">
		 <?php pikoworks_entry_footer(); ?>
        </footer><!-- .entry-footer -->
        <?php
           $enable_shear = pikoworks_get_option_data('optn_blog_single_social_shear', '0');
           $social_text = pikoworks_get_option_data('optn_blog_single_social_text', '');
           $enable_author = pikoworks_get_option_data('enable_author', 1);
            if($enable_shear == 1):
                ?>            
            <div class="piko-single-post-shear clearfix">
                  <h4><?php echo esc_attr($social_text); ?></h4>
                  <?php get_template_part( 'template-parts/post-sharing' ); ?> 
            </div>        
            <?php                
            endif;
            
            if ( '' !== get_the_author_meta( 'description' ) && $enable_author == 1 ) {
                get_template_part( 'template-parts/biography' );
            }
        ?>
</article><!-- #post-## -->