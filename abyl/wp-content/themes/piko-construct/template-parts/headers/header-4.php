<?php
/**
 * Template part for displaying header layout.
 * 
 * @author themepiko
 */
$prefix = 'pikoworks_';
$menu_width =  get_post_meta(get_the_ID(), $prefix . 'manu_width',true);
if (!isset($menu_width) || $menu_width == '-1' || $menu_width == '') {
    $menu_width = isset( $GLOBALS['pikoworks']['full_width_menu'] ) ? $GLOBALS['pikoworks']['full_width_menu'] : 'container';
}
$customtext = pikoworks_get_option_data('menu_custom_text', ''); 
$topbar =  get_post_meta(get_the_ID(), $prefix . 'enable_top_bar',true);
if (!isset($topbar) || $topbar == '-1' || $topbar == 0) {
   $topbar = pikoworks_get_option_data('enable_top_bar', false); 
}
?>
<div class="header-wrapper">
	<header id="header" class="site-header">
		<?php if($topbar){ pikoworks_get_topbar(); } ?>
		<div class="header-main">
                    <div class="<?php echo esc_attr($menu_width); ?>">
			<div class="row">
				<div class="col-xs-12 columns">
					<div class="header-left">
						<?php pikoworks_get_brand_logo(); ?>
						<div class="header-toogle-menu-button">
							<span class="header-toogle-menu-icon"><span></span></span>
						</div>
					</div>
					<div class="header-right">
						<?php pikoworks_get_sticky_logo();?>
						<div class="header-toogle-menu-button">
						<span class="header-toogle-menu-icon"><span></span></span>
						</div>
						<?php pikoworks_get_header_action();
                                                 echo '<div class="header-boxes-container">'. do_shortcode($customtext) .'</div> '; 
                                                ?>
					</div>
				</div>
			</div>
                    </div>
		</div>
	</header>
	<div class="mega-menu-sidebar">
		<div class="main-menu-wrap">
			<div id="main-menu">
				<?php pikoworks_get_main_menu();?>
			</div>
		</div>
	</div>
</div>