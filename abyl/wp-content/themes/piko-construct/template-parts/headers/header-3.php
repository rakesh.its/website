<?php
/**
 * Template part for displaying header layout.
 * 
 * @author themepiko
 */
$prefix = 'pikoworks_';
$menu_width =  get_post_meta(get_the_ID(), $prefix . 'manu_width',true);
if (!isset($menu_width) || $menu_width == '-1' || $menu_width == '') {
    $menu_width = isset( $GLOBALS['pikoworks']['full_width_menu'] ) ? $GLOBALS['pikoworks']['full_width_menu'] : 'container';
}
$menu_sidebar = pikoworks_get_option_data('optn_verticle_menu_sidebar','');
?>
<div class="header-wrapper header-side-nav">
	<header id="header" class="site-header sticky-menu-header">
		<div class="header-main">
                    <div class="<?php echo esc_attr($menu_width); ?>">
			<div class="row">
				<div class="columns">
					<div class="header-left">
						<?php pikoworks_get_brand_logo(); ?>
					</div>
					<div class="header-right">
						<?php pikoworks_get_sticky_logo();?>
						<?php pikoworks_get_header_action();?>
                                            
						<div class="mega-menu-sidebar">
							<div class="main-menu-wrap">
								<div id="main-menu">
									<?php pikoworks_get_main_menu();?>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php
				if(is_active_sidebar($menu_sidebar)){
					echo '<div class="clearfix"></div><div class="header-3-bottom">';
						dynamic_sidebar( $menu_sidebar );
					echo '</div>';
				}
				?>
			</div>
                    </div>
		</div>
	</header>
</div>