<?php
/**
 * Template part for displaying header layout.
 * 
 * @author themepiko
 */
$prefix = 'pikoworks_';
$menu_width =  get_post_meta(get_the_ID(), $prefix . 'manu_width',true);
if (!isset($menu_width) || $menu_width == '-1' || $menu_width == '') {
    $menu_width = pikoworks_get_option_data('full_width_menu', 'container'); 
}
$topbar =  get_post_meta(get_the_ID(), $prefix . 'enable_top_bar',true);
if (!isset($topbar) || $topbar == '-1' || $topbar == 0) {
   $topbar = pikoworks_get_option_data('enable_top_bar', false); 
}
?>
<div class="header-wrapper">
	<header id="header" class="site-header sticky-menu-header">
                <?php if($topbar){ pikoworks_get_topbar(); } ?>
		<div class="header-main">                    
                    <div class="header-left">
                        <div class="<?php echo esc_attr($menu_width); ?>">
                            <div class="row">
                                    <div class="col-md-12 col-lg-12">
                                     <?php pikoworks_get_brand_logo(); ?>
                                    </div>
                            </div>
                        </div>
                    </div>                    
                    <div class="header-right">
                        <div class="<?php echo esc_attr($menu_width); ?>">
                            <div class="main-menu-wrap">
                                <div class="row">
                                    <div class="col-md-12 col-lg-12 columns">
                                        <div id="main-menu">								
                                            <?php pikoworks_get_sticky_logo();?>
                                            <?php pikoworks_get_main_menu();?>
                                            <?php pikoworks_get_header_action();?>	
                                        </div>
                                    </div>
                                </div>
                            </div>
			</div>
                    </div>
		</div>
	</header>
</div>