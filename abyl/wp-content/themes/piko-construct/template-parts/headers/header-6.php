<?php
/**
 * Template part for displaying header layout.
 * 
 * @author themepiko
 */
$prefix = 'pikoworks_';
$menu_width =  get_post_meta(get_the_ID(), $prefix . 'manu_width',true);
if (!isset($menu_width) || $menu_width == '-1' || $menu_width == '') {
    $menu_width = isset( $GLOBALS['pikoworks']['full_width_menu'] ) ? $GLOBALS['pikoworks']['full_width_menu'] : 'container';
}
$topbar =  get_post_meta(get_the_ID(), $prefix . 'enable_top_bar',true);
if (!isset($topbar) || $topbar == '-1' || $topbar == 0) {
   $topbar = pikoworks_get_option_data('enable_top_bar', false); 
}
?>
<div class="header-wrapper">
	<header id="header" class="site-header sticky-menu-header">
		 <?php if($topbar){ pikoworks_get_topbar(); } ?>
		<div class="header-main">
                    <div class="<?php echo esc_attr($menu_width); ?>">			
			<div class="header-right">
				<div class="main-menu-wrap">
					<div class="row">
						<div class="col-md-12 col-lg-12 columns">
							<div id="main-menu">
                                                            <div class="nav-left">
                                                                <div class="logo-left-width"></div>                                                                
                                                                <?php pikoworks_get_main_menu();?>                                                                 
                                                            </div>
                                                                
                                                                <?php pikoworks_get_brand_logo(); ?>
								<?php pikoworks_get_sticky_logo();?>
                                                                
                                                            <div class="nav-right">
                                                                <div class="logo-right-width"></div>
                                                                <?php pikoworks_get_secondary_menu();?>								
								<?php pikoworks_get_header_action();?>
                                                            </div>
							</div>
						</div>
					</div>
				</div>
			</div>
                    </div>
		</div>
	</header>
</div>