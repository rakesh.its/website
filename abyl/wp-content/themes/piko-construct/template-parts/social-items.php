<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

?>

<?php if ( trim( $GLOBALS['pikoworks']['twitter'] ) != '' ): ?>
    <a href="<?php echo esc_url( $GLOBALS['pikoworks']['twitter'] ); ?>">
        <i class="social-icon fa fa-twitter"></i>
        <span class="screen-reader-text"><?php esc_html_e( 'Twitter link', 'piko-construct' ); ?></span>
    </a>
<?php endif; ?>
<?php if ( trim( $GLOBALS['pikoworks']['facebook'] ) != '' ): ?>
    <a href="<?php echo esc_url( $GLOBALS['pikoworks']['facebook'] ); ?>">
        <i class="social-icon fa fa-facebook"></i>
        <span class="screen-reader-text"><?php esc_html_e( 'Facebook link', 'piko-construct' ); ?></span>
    </a>
<?php endif; ?>
<?php if ( trim( $GLOBALS['pikoworks']['googleplus'] ) != '' ): ?>
    <a href="<?php echo esc_url( $GLOBALS['pikoworks']['googleplus'] ); ?>">
        <i class="social-icon fa fa-google-plus"></i>
        <span class="screen-reader-text"><?php esc_html_e( 'Google Plus link', 'piko-construct' ); ?></span>
    </a>
<?php endif; ?>
<?php if ( trim( $GLOBALS['pikoworks']['dribbble'] ) != '' ): ?>
    <a href="<?php echo esc_url( $GLOBALS['pikoworks']['dribbble'] ); ?>">
        <i class="social-icon fa fa-dribbble"></i>
        <span class="screen-reader-text"><?php esc_html_e( 'Dribbble link', 'piko-construct' ); ?></span>
    </a>
<?php endif; ?>
<?php if ( trim( $GLOBALS['pikoworks']['behance'] ) != '' ): ?>
    <a href="<?php echo esc_url( $GLOBALS['pikoworks']['behance'] ); ?>">
        <i class="social-icon fa fa-behance"></i>
        <span class="screen-reader-text"><?php esc_html_e( 'Behance link', 'piko-construct' ); ?></span>
    </a>
<?php endif; ?>
<?php if ( trim( $GLOBALS['pikoworks']['tumblr'] ) != '' ): ?>
    <a href="<?php echo esc_url( $GLOBALS['pikoworks']['tumblr'] ); ?>">
        <i class="social-icon fa fa-tumblr"></i>
        <span class="screen-reader-text"><?php esc_html_e( 'Tumblr link', 'piko-construct' ); ?></span>
    </a>
<?php endif; ?>
<?php if ( trim( $GLOBALS['pikoworks']['instagram'] ) != '' ): ?>
    <a href="<?php echo esc_url( $GLOBALS['pikoworks']['instagram'] ); ?>">
        <i class="social-icon fa fa-instagram"></i>
        <span class="screen-reader-text"><?php esc_html_e( 'Instagram link', 'piko-construct' ); ?></span>
    </a>
<?php endif; ?>
<?php if ( trim( $GLOBALS['pikoworks']['pinterest'] ) != '' ): ?>
    <a href="<?php echo esc_url( $GLOBALS['pikoworks']['pinterest'] ); ?>">
        <i class="social-icon fa fa-pinterest"></i>
        <span class="screen-reader-text"><?php esc_html_e( 'Pinterest link', 'piko-construct' ); ?></span>
    </a>
<?php endif; ?>
<?php if ( trim( $GLOBALS['pikoworks']['youtube'] ) != '' ): ?>
    <a href="<?php echo esc_url( $GLOBALS['pikoworks']['youtube'] ); ?>">
        <i class="social-icon fa fa-youtube"></i>
        <span class="screen-reader-text"><?php esc_html_e( 'Youtube link', 'piko-construct' ); ?></span>
    </a>
<?php endif; ?>
<?php if ( trim( $GLOBALS['pikoworks']['vimeo'] ) != '' ): ?>
    <a href="<?php echo esc_url( $GLOBALS['pikoworks']['vimeo'] ); ?>">
        <i class="social-icon fa fa-vimeo"></i>
        <span class="screen-reader-text"><?php esc_html_e( 'Vimeo link', 'piko-construct' ); ?></span>
    </a>
<?php endif; ?>
<?php if ( trim( $GLOBALS['pikoworks']['linkedin'] ) != '' ): ?>
    <a href="<?php echo esc_url( $GLOBALS['pikoworks']['linkedin'] ); ?>">
        <i class="social-icon fa fa-linkedin"></i>
        <span class="screen-reader-text"><?php esc_html_e( 'LinkedIn link', 'piko-construct' ); ?></span>
    </a>
<?php endif; ?>
<?php if ( trim( $GLOBALS['pikoworks']['flickr'] ) != '' ): ?>
    <a href="<?php echo esc_url( $GLOBALS['pikoworks']['flickr'] ); ?>">
        <i class="social-icon fa fa-flickr"></i>
        <span class="screen-reader-text"><?php esc_html_e( 'Flickr feed', 'piko-construct' ); ?></span>
    </a>
<?php endif; ?>
<?php if ( trim( $GLOBALS['pikoworks']['soundcloud'] ) != '' ): ?>
    <a href="<?php echo esc_url( $GLOBALS['pikoworks']['soundcloud'] ); ?>">
        <i class="social-icon fa fa-soundcloud"></i>
        <span class="screen-reader-text"><?php esc_html_e( 'Soundcloud', 'piko-construct' ); ?></span>
    </a>  
<?php endif; ?>