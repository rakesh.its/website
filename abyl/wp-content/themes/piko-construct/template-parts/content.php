<?php
/**
 * The template part for displaying content
 *
 */
$size = 'full';
if (isset($GLOBALS['pikoworks_archive_loop']['image-size'])) {
    $size = $GLOBALS['pikoworks_archive_loop']['image-size'];
}
$archive_title_position = isset( $GLOBALS['pikoworks']['optn_archive_title_position'] ) ? trim( $GLOBALS['pikoworks']['optn_archive_title_position'] ) : 'image-bottom';
$charlength = isset( $GLOBALS['pikoworks']['optn_archive_except_word'] ) ? trim( $GLOBALS['pikoworks']['optn_archive_except_word'] ) : '55';

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php if($archive_title_position == 'image-top'): ?>
            <header class="entry-header">
                <?php pikoworks_entry_header(); ?>
            </header><!-- .entry-header -->
        <?php endif; ?> 

	<?php pikoworks_excerpt(); ?>
	<?php
            $thumbnail = pikoworks_post_format($size);
            if (!empty($thumbnail)) : ?>
                <figure class="entry-thumbnail-wrap">
                    <?php echo wp_kses_post($thumbnail); ?>
                </figure>
            <?php endif; ?>
        <?php if($archive_title_position == 'image-bottom'): ?>
            <header class="entry-header">
               <?php pikoworks_entry_header_two(); ?>
            </header><!-- .entry-header -->
        <?php endif; ?>

	<div class="entry-content">
	<p><?php pikoworks_trim_word($charlength); ?></p>
		<?php
			/* translators: %s: Name of current post */
			
			wp_link_pages( array(
				'before'      => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:', 'piko-construct' ) . '</span>',
				'after'       => '</div>',
				'link_before' => '<span>',
				'link_after'  => '</span>',
				'pagelink'    => '<span class="screen-reader-text">' . esc_html__( 'Page', 'piko-construct' ) . ' </span>%',
				'separator'   => '<span class="screen-reader-text">, </span>',
			) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
            <?php pikoworks_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
