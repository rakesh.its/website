<?php
/*
Template Name: about
Template Post Type: post, page, event
*/
// Page code here...

get_header(2); ?>

<!--Breadcrumb Area-->
				<section class="breadcrumb-areav2" data-background="images/banner/6.jpg">
					<div class="container">
						<div class="row justify-content-center">
							<div class="col-lg-7">
								<div class="bread-titlev2">
									<h1 class="wow fadeInUp" data-wow-delay=".2s">Need A Premium & Creative Website Designing?</h1>
									<p class="mt20 wow fadeInUp" data-wow-delay=".4s">From Startup to Enterprise be ready and don't worry about design and user experience.</p>
									<a href="#" class="btn-main bg-btn2 lnk mt20 wow zoomInDown" data-wow-delay=".6s">Get Quote <i class="fas fa-chevron-right fa-icon"></i><span class="circle"></span></a>
								</div>
							</div>
						</div>
					</div>
				</section>
  <!--End Breadcrumb Area-->
  <!--Start About-->
  <section class="about-agency pad-tb block-1">
    <div class="container">
      <div class="row">
        <div class="col-lg-6 v-center">
          <div class="about-image">
            <img src="<?php bloginfo('template_url'); ?>/images/about/company-about.png" alt="about us" class="img-fluid"/>
          </div>
        </div>
        <div class="col-lg-6">
          <div class="common-heading text-l ">
            <span>About Us</span>
            <h2>Who we are..</h2>
            <p>Bhrigusoft is a renowned web and mobile app development company & an IT Software Solutions provider based in India. Bhrigusoft is well known to craft the most innovative & eye catchy mobile apps & websites.We offer a wide range of services in mobile apps, website development, and much more. Our skilled team & our products are developed to bring growth to your business. We believe in delivering the services without compromising on client satisfaction and quality.</p>
            <p>Founded and run by young entrepreneurs our center provides project-ready tech experts that will work in full compliance with your needs and objectives.You get superb service and user experience since our team take care of our client reputation and brand.</p>
          </div>
          <div class="row in-stats small about-statistics">
            <div class="col-lg-4 col-sm-4">
              <div class="statistics">
                <div class="statnumb counter-number">
                  <span class="counter">80</span>
                  <p>Happy Clients</p>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-sm-4">
              <div class="statistics">
                <div class="statnumb">
                  <span class="counter">40</span><span>k</span>
                  <p>Hours Worked</p>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-sm-4">
              <div class="statistics mb0">
                <div class="statnumb counter-number">
                  <span class="counter">150</span>
                  <p>Projects Done</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--End About-->
  <!--Start why-choose-->
 <!-- <section class="why-choose pad-tb">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-6">
          <div class="common-heading">
            <span>We Are Awesome</span>
            <h2 class="mb30">Why Choose Us</h2>
          </div>
        </div>
      </div>
      <div class="row upset">
        <div class="col-lg-3 col-sm-6 mt30">
          <div class="s-block up-hor">
            <div class="s-card-icon"><img src="images/icons/research.svg" alt="service" class="img-fluid"/></div>
            <h4>Reasearch and Analysis</h4>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
          </div>
        </div>
        <div class="col-lg-3 col-sm-6 mt30">
          <div class="s-block up-hor">
            <div class="s-card-icon"><img src="images/icons/chat.svg" alt="service" class="img-fluid"/></div>
            <h4>Negotiation and power</h4>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
          </div>
        </div>
        <div class="col-lg-3 col-sm-6 mt30">
          <div class="s-block up-hor">
            <div class="s-card-icon"><img src="images/icons/monitor.svg" alt="service" class="img-fluid"/></div>
            <h4>Creative and innovative solutions</h4>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
          </div>
        </div>
        <div class="col-lg-3 col-sm-6 mt30">
          <div class="s-block up-hor">
            <div class="s-card-icon"><img src="images/icons/trasparency.svg" alt="service" class="img-fluid"/></div>
            <h4>Trasparency and ease of work</h4>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
          </div>
        </div>
      </div>
    </div>
  </section>-->
  <!--End why-choose-->

<?php get_footer(); ?>