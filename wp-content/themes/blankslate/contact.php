<?php
/*
Template Name: contact
Template Post Type: post, page, event
*/
// Page code here...

get_header(2); ?>
<!--Breadcrumb Area-->
				<section class="breadcrumb-areav2" data-background="images/banner/6.jpg">
					<div class="container">
						<div class="row justify-content-center">
							<div class="col-lg-7">
								<div class="bread-titlev2">
									<h1 class="wow fadeInUp" data-wow-delay=".2s">Need A Premium & Creative Website Designing?</h1>
									<p class="mt20 wow fadeInUp" data-wow-delay=".4s">From Startup to Enterprise be ready and don't worry about design and user experience.</p>
									<a href="#" class="btn-main bg-btn2 lnk mt20 wow zoomInDown" data-wow-delay=".6s">Get Quote <i class="fas fa-chevron-right fa-icon"></i><span class="circle"></span></a>
								</div>
							</div>
						</div>
					</div>
				</section>
<!--End Breadcrumb Area-->
<!--Start Enquire Form-->
<section class="contact-page pad-tb">
<div class="container">
<div class="row justify-content-center">
<div class="col-lg-6 v-center">
<div class="common-heading text-l">
<span>Contact Now</span>
<h2 class="mt0 mb0">Have Question? Write a Message</h2>
<p class="mb60 mt20">We will catch you as early as we receive the message</p>
</div>
<div class="form-block">
<form id="contact-form" method="post" action="php/contact.php" data-toggle="validator">
<div class="messages"></div>
<div class="fieldsets row">
<div class="col-md-6 form-group"><input id="form_name" type="text" name="name"  placeholder="Enter your name *" required="required" data-error="Name is required.">
<div class="help-block with-errors"></div></div>
<div class="col-md-6 form-group"><input id="form_email" type="email" name="email"  placeholder="Enter your email *" required="required" data-error="Valid email is required.">
<div class="help-block with-errors"></div></div>
</div>
<div class="fieldsets row">
<div class="col-md-6 form-group"><input id="form_phone" type="text" name="phone"  placeholder="Enter your Phone No *" required="required" data-error="Phone No is required.">
<div class="help-block with-errors"></div></div>
<div class="col-md-6 form-group"><select id="form_need" name="need"  required="required" data-error="Specify your need.">
<option value="">Select Service</option>
<option value="Graphic Design">Web Development</option>
<option value="Web Design">Mobile App Development</option>
<option value="App Design">Digital Marketing</option>
<option value="Other">Other</option>
</select>
<div class="help-block with-errors"></div></div>
</div>
<div class="fieldsets form-group"> <textarea id="form_message" name="message"  placeholder="Message for me *" rows="4" required="required" data-error="Please, leave us a message."></textarea>
<div class="help-block with-errors"></div>
</div>

<div class="custom-control custom-checkbox">
<input type="checkbox" class="custom-control-input" id="customCheck" name="example1" checked="checked">
<label class="custom-control-label" for="customCheck">I agree to the <a href="javascript:void(0)">Terms &amp; Conditions</a> of Bhrigusoft.</label>
</div>
<div class="fieldsets mt20">
<button type="submit" class="lnk btn-main bg-btn">Submit <span class="circle"></span></button>
</div>
<p class="trm"><i class="fas fa-lock"></i>We hate spam, and we respect your privacy.</p>
</form>
</div>
</div>
<div class="col-lg-5 v-center">
<div class="contact-details">
<div class="contact-card wow fadeIn" data-wow-delay=".2s">
<div class="info-card v-center">
<span><i class="fas fa-phone-alt"></i> Phone:</span>
<div class="info-body">
<p>Assistance hours: Monday – Friday, 9:30 am to 6:30 pm</p>
<a href="tel:+10000000000">(+91) 9711 69 6043</a>
</div>
</div>
</div>
<div class="email-card mt30 wow fadeIn" data-wow-delay=".5s">
<div class="info-card v-center">
<span><i class="fas fa-envelope"></i> Email:</span>
<div class="info-body">
<p>Our support team will get back to in 24-h during standard business hours.</p>
<a href="mailto:info@businessname.com">info@bhrigusoft.com</a>
</div>
</div>
</div>
<div class="skype-card mt30 wow fadeIn" data-wow-delay=".9s">
<div class="info-card v-center">
<span><i class="fab fa-skype"></i> Skype:</span>
<div class="info-body">
<p>We Are Online: Monday – Friday, 9 am to 5 pm</p>
<a href="skype:bhrigusoft?call">bhrigusoft</a>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<!--End Enquire Form-->


<?php get_footer(); ?>