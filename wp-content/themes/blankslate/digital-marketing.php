<?php
/*
Template Name: Digital Markeing
Template Post Type: post, page, event
*/
// Page code here...

get_header(2); ?>
<!--Breadcrumb Area-->
<section class="breadcrumb-areav2" data-background="images/banner/9.jpg">
<div class="container">
<div class="row justify-content-center">
<div class="col-lg-7">
<div class="bread-titlev2">
  <h1 class="wow fadeInUp" data-wow-delay=".2s">Professional Digital Marketing Services That Drive Results</h1>
  <p class="mt20 wow fadeInUp" data-wow-delay=".4s">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since. </p>
  <a href="#" class="btn-main bg-btn2 lnk mt20 wow zoomInDown" data-wow-delay=".6s">Get Quote <i class="fas fa-chevron-right fa-icon"></i><span class="circle"></span></a>
</div>
</div>
</div>
</div>
</section>
<div class="statistics-wrap bg-gradient5">
<div class="container">
<div class="row small t-ctr mt0">
<div class="col-lg-3 col-sm-6">
<div class="statistics">
  <div class="statistics-img">
    <img src="images/icons/deal.svg" alt="happy" class="img-fluid">
  </div>
  <div class="statnumb">
    <span class="counter">450</span>
    <p>Happy Clients</p>
  </div>
</div>
</div>
<div class="col-lg-3 col-sm-6">
<div class="statistics">
  <div class="statistics-img">
    <img src="images/icons/computers.svg" alt="project" class="img-fluid">
  </div>
  <div class="statnumb counter-number">
    <span class="counter">48</span><span>k</span>
    <p>Projects Done</p>
  </div>
</div>
</div>
<div class="col-lg-3 col-sm-6">
<div class="statistics">
  <div class="statistics-img">
    <img src="images/icons/worker.svg" alt="work" class="img-fluid">
  </div>
  <div class="statnumb">
    <span class="counter">95</span><span>k</span>
    <p>Hours Worked</p>
  </div>
</div>
</div>
<div class="col-lg-3 col-sm-6">
<div class="statistics mb0">
  <div class="statistics-img">
    <img src="images/icons/customer-service.svg" alt="support" class="img-fluid">
  </div>
  <div class="statnumb">
    <span class="counter">24</span><span>/</span><span class="counter">7</span>
    <p>Support Available</p>
  </div>
</div>
</div>
</div>
</div>
</div>
<!--End Hero-->
<!--Start About-->
<section class="service pad-tb bg-gradient5">
<div class="container">
<div class="row">
<div class="col-lg-4">
<div class="image-block wow fadeIn">
  <img src="images/service/digitalmarketing.png" alt="image" class="img-fluid no-shadow"/>
</div>
</div>
<div class="col-lg-8 block-1">
<div class="common-heading text-l pl25">
  <span>Overview</span>
  <h2>Digital Marketing Services for Growing Your Company</h2>
  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. Lorem Ipsum is simply dummy text of the printing and typesetting industry.  Lorem Ipsum is simply dummy text of the printing and typesetting industry. is simply dummy text of the printing and typesetting industry. </p>

</div>
</div>
</div>
</div>
</section>
<!--End About-->

<!--Start Key points-->
<section class="service pad-tb light-dark">
<div class="container">
<div class="row">
<div class="col-lg-7">
<div class="text-l service-desc- pr25">
  <h3 class="mb20">Online Marketing Services Proven to Increase Leads, Sales, & Revenue</h3>  
  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
  <ul class="service-point-2 mt20">
    <li># 800+ Mobile Delivered</li>
    <li># 200+ Team Strength</li>
    <li># User-Friendly Interface</li>
    <li># 400 Happy Clients</li>
    <li># 95% Repeat business</li>
    <li># Quality Service UX</li>
  </ul>
  <a href="#" class="btn-main bg-btn2 lnk mt30">Request A Quote  <i class="fas fa-chevron-right fa-icon"></i><span class="circle"></span></a>
</div>
</div>
<div class="col-lg-5">
<div class="servie-key-points">
  <h4>Advantages of Digital Marketing</h4>
  <ul class="key-points mt20">
      <li>Strategize with The Valuable Data and Analytics</li>
      <li>Content Performance and Lead Generation</li>
      <li>Reduction in cost and raises standards</li>
      <li>Improved Conversion Rates</li>
      <li>More Cost Effective Than Traditional Marketing</li>
      <li>Higher Revenues</li>
      <li>Higher ROI from Your Campaigns</li>
      <li>Earn People’s Trust and Build Brand Reputation</li>
      <li>Know All About Your Competitors</li>
  </ul>
</div>
</div>
</div>
</div>
</section>
<!--End Key points-->
<!--Start Service-->
<section class="service-block bg-gradient6 pad-tb">
<div class="container">
<div class="row justify-content-center">
<div class="col-lg-6">
<div class="common-heading ptag">
  <span>Service</span>
  <h2>Our Services</h2>
  <p class="mb30">We think big and have hands in all leading technology platforms to provide you wide array of services.</p>
</div>
</div>
</div>
<div class="row upset link-hover">

<div class="col-lg-6 col-sm-6 mt30 wow fadeInUp" data-wow-delay=".2s">
<div class="s-block wide-sblock">
  <div class="s-card-icon-large"><img src="images/service/seo.png" alt="service" class="img-fluid"/></div>
  <div class="s-block-content-large">
  <h4>Search Engine Optimization</h4>
  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
</div></div>
</div>

<div class="col-lg-6 col-sm-6 mt30 wow fadeInUp" data-wow-delay=".4s">
<div class="s-block wide-sblock">
  <div class="s-card-icon-large"><img src="images/service/smo.png" alt="service" class="img-fluid"/></div>
  <div class="s-block-content-large">
  <h4>Social Media Marketing</h4>
  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
</div></div>
</div>

<div class="col-lg-6 col-sm-6 mt30 wow fadeInUp" data-wow-delay=".6s">
<div class="s-block wide-sblock">
  <div class="s-card-icon-large"><img src="images/service/ppc.png" alt="service" class="img-fluid"/></div>
  <div class="s-block-content-large">
  <h4>Pay per Click</h4>
  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
</div></div>
</div>

<div class="col-lg-6 col-sm-6 mt30 wow fadeInUp" data-wow-delay=".8s">
<div class="s-block wide-sblock">
  <div class="s-card-icon-large"><img src="images/service/emails.png" alt="service" class="img-fluid"/></div>
  <div class="s-block-content-large">
  <h4>Email Marketing</h4>
  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
</div></div>
</div>


</div>
<div class="-cta-btn mt70">
<div class="free-cta-title v-center wow zoomInDown" data-wow-delay="1.1s">
<p>Hire a <span>Dedicated Developer</span></p>
<a href="#" class="btn-main bg-btn2 lnk">Hire Now<i class="fas fa-chevron-right fa-icon"></i><span class="circle"></span></a>
</div>
</div>
</div>
</section>
<!--End Service-->
  <!--Start Why Choose-->
  <section class="service-block pad-tb bg-gradient7">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-8">
          <div class="common-heading ptag">
            <span>We Deliver Our Best</span>
            <h2>Why Choose Niwax</h2>
            <p class="mb30">Donec metus lorem, vulputate at sapien sit amet, auctor iaculis lorem. In vel hendrerit nisi. Vestibulum eget risus velit.</p>
          </div>
        </div>
      </div>
      <div class="row justify-content-center">
        <div class="col-lg-4 col-sm-6 mt30  wow fadeIn" data-wow-delay=".2s">
          <div class="s-block wide-sblock">
            <div class="s-card-icon"><img src="images/icons/teama.svg" alt="service" class="img-fluid"></div>
            <div class="s-block-content">
              <h4>Reliable Service. In House Team</h4>
              <p>In vel hendrerit nisi. Vestibulum eget risus velit.</p>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-sm-6 mt30 wow fadeIn" data-wow-delay=".5s">
          <div class="s-block wide-sblock">
            <div class="s-card-icon"><img src="images/icons/link.svg" alt="service" class="img-fluid"></div>
            <div class="s-block-content">
              <h4>Trusted by People Like You</h4>
              <p>In vel hendrerit nisi. Vestibulum eget risus velit.</p>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-sm-6 mt30 wow fadeIn" data-wow-delay=".8s">
          <div class="s-block wide-sblock">
            <div class="s-card-icon"><img src="images/icons/tech.svg" alt="service" class="img-fluid"></div>
            <div class="s-block-content">
              <h4>Complete Technical Competency</h4>
              <p>In vel hendrerit nisi. Vestibulum eget risus velit.</p>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-sm-6 mt30 wow fadeIn" data-wow-delay="1.1s">
          <div class="s-block wide-sblock">
            <div class="s-card-icon"><img src="images/icons/hi.svg" alt="service" class="img-fluid"></div>
            <div class="s-block-content">
              <h4>Friendly & Cordial in Nature</h4>
              <p>In vel hendrerit nisi. Vestibulum eget risus velit.</p>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-sm-6 mt30 wow fadeIn" data-wow-delay="1.4s">
          <div class="s-block wide-sblock">
            <div class="s-card-icon"><img src="images/icons/badge.svg" alt="service" class="img-fluid"></div>
            <div class="s-block-content">
              <h4>Excellent Quality Delivered on Time</h4>
              <p>In vel hendrerit nisi. Vestibulum eget risus velit.</p>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-sm-6 mt30 wow fadeIn" data-wow-delay="1.7s">
          <div class="s-block wide-sblock">
            <div class="s-card-icon"><img src="images/icons/tin.svg" alt="service" class="img-fluid"></div>
            <div class="s-block-content">
              <h4>Effective & Continuous Communication</h4>
              <p>In vel hendrerit nisi. Vestibulum eget risus velit.</p>
            </div>
          </div>
        </div>
      </div>
      <div class="-cta-btn mt70">
        <div class="free-cta-title v-center wow zoomInDown" data-wow-delay="1.8s">
          <p>Let's Start a <span>New Project</span> Together</p>
          <a href="#" class="btn-main bg-btn2 lnk">Inquire Now<i class="fas fa-chevron-right fa-icon"></i><span class="circle"></span></a>
        </div>
      </div>
    </div>
  </section>
  <!--End Why Choose-->




<?php get_footer(); ?>