<?php
/*
Template Name: Web Development
Template Post Type: post, page, event
*/
// Page code here...

get_header(2); ?>
<!--Breadcrumb Area-->
				<section class="breadcrumb-areav2" data-background="images/banner/6.jpg">
					<div class="container">
						<div class="row justify-content-center">
							<div class="col-lg-7">
								<div class="bread-titlev2">
									<h1 class="wow fadeInUp" data-wow-delay=".2s">Need A Premium & Creative Website Designing?</h1>
									<p class="mt20 wow fadeInUp" data-wow-delay=".4s">From Startup to Enterprise be ready and don't worry about design and user experience.</p>
									<a href="#" class="btn-main bg-btn2 lnk mt20 wow zoomInDown" data-wow-delay=".6s">Get Quote <i class="fas fa-chevron-right fa-icon"></i><span class="circle"></span></a>
								</div>
							</div>
						</div>
					</div>
				</section>
				<div class="statistics-wrap bg-gradient5">
					<div class="container">
						<div class="row small t-ctr mt0">
							<div class="col-lg-3 col-sm-6">
								<div class="statistics">
									<div class="statistics-img">
										<img src="images/icons/deal.svg" alt="happy" class="img-fluid">
									</div>
									<div class="statnumb">
										<span class="counter">450</span>
										<p>Happy Clients</p>
									</div>
								</div>
							</div>
							<div class="col-lg-3 col-sm-6">
								<div class="statistics">
									<div class="statistics-img">
										<img src="images/icons/computers.svg" alt="project" class="img-fluid">
									</div>
									<div class="statnumb counter-number">
										<span class="counter">48</span><span>k</span>
										<p>Projects Done</p>
									</div>
								</div>
							</div>
							<div class="col-lg-3 col-sm-6">
								<div class="statistics">
									<div class="statistics-img">
										<img src="images/icons/worker.svg" alt="work" class="img-fluid">
									</div>
									<div class="statnumb">
										<span class="counter">95</span><span>k</span>
										<p>Hours Worked</p>
									</div>
								</div>
							</div>
							<div class="col-lg-3 col-sm-6">
								<div class="statistics mb0">
									<div class="statistics-img">
										<img src="images/icons/customer-service.svg" alt="support" class="img-fluid">
									</div>
									<div class="statnumb">
										<span class="counter">24</span><span>/</span><span class="counter">7</span>
										<p>Support Available</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!--End Hero-->
				<!--Start About-->
				<section class="service pad-tb">
					<div class="container">
						<div class="row">
							<div class="col-lg-4">
								<div class="image-block upset bg-shape wow fadeIn">
									<img src="images/about/square-image-1.jpg" alt="image" class="img-fluid"/>
								</div>
							</div>
							<div class="col-lg-8 block-1">
								<div class="common-heading text-l pl25">
									<span>Overview</span>
									<h2>Creative Web Design Service</h2>
									<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. Lorem Ipsum is simply dummy text of the printing and typesetting industry.  Lorem Ipsum is simply dummy text of the printing and typesetting industry. is simply dummy text of the printing and typesetting industry. </p>
									<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.  Lorem Ipsum is simply dummy text of the printing and typesetting industry. is simply dummy text of the printing and typesetting industry.</p>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!--End About-->
				<!--Start Tech-->
				<div class="techonology-used-">
					<div class="container">
						<ul class="h-scroll tech-icons">
							<li><a href="#"><img src="images/icons/stack-icon1.png" alt="icon"></a></li>
							<li><a href="#"><img src="images/icons/stack-icon2.png" alt="icon"></a></li>
							<li><a href="#"><img src="images/icons/stack-icon3.png" alt="icon"></a></li>
							<li><a href="#"><img src="images/icons/stack-icon4.png" alt="icon"></a></li>
							<li><a href="#"><img src="images/icons/stack-icon5.png" alt="icon"></a></li>
							<li><a href="#"><img src="images/icons/stack-icon6.png" alt="icon"></a></li>
							<li><a href="#"><img src="images/icons/stack-icon7.png" alt="icon"></a></li>
							<li><a href="#"><img src="images/icons/stack-icon8.png" alt="icon"></a></li>
						</ul>
					</div>
				</div>
				<!--End Tech-->
				<!--Start Service-->
				<section class="service-block bg-gradient6 pad-tb">
					<div class="container">
						<div class="row justify-content-center">
							<div class="col-lg-6">
								<div class="common-heading ptag">
									<span>Service</span>
									<h2>Our Services</h2>
									<p class="mb30">We think big and have hands in all leading technology platforms to provide you wide array of services.</p>
								</div>
							</div>
						</div>
						<div class="row upset link-hover">
							<div class="col-lg-4 col-sm-6 mt30 wow fadeInUp" data-wow-delay=".2s">
								<div class="s-block">
									<div class="s-card-icon"><img src="images/icons/logo-and-branding.svg" alt="service" class="img-fluid"/></div>
									<h4>Graphic Designing Services</h4>
									<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
								</div>
							</div>
							<div class="col-lg-4 col-sm-6 mt30 wow fadeInUp" data-wow-delay=".4s">
								<div class="s-block">
									<div class="s-card-icon"><img src="images/icons/service2.svg" alt="service" class="img-fluid"/></div>
									<h4>Responsive Web Designing</h4>
									<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
								</div>
							</div>
							<div class="col-lg-4 col-sm-6 mt30 wow fadeInUp" data-wow-delay=".6s">
								<div class="s-block">
									<div class="s-card-icon"><img src="images/icons/service3.svg" alt="service" class="img-fluid"/></div>
									<h4>Static Website Designing</h4>
									<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
								</div>
							</div>
							<div class="col-lg-4 col-sm-6 mt30 wow fadeInUp" data-wow-delay=".8s">
								<div class="s-block">
									<div class="s-card-icon"><img src="images/icons/service4.svg" alt="service" class="img-fluid"/></div>
									<h4>Dynamic Website Designing</h4>
									<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
								</div>
							</div>
							<div class="col-lg-4 col-sm-6 mt30 wow fadeInUp" data-wow-delay="1s">
								<div class="s-block">
									<div class="s-card-icon"><img src="images/icons/service5.svg" alt="service" class="img-fluid"/></div>
									<h4>Psd to HTML Service</h4>
									<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
								</div>
							</div>
							<div class="col-lg-4 col-sm-6 mt30 wow fadeInUp" data-wow-delay="1.2s">
								<div class="s-block">
									<div class="s-card-icon"><img src="images/icons/service6.svg" alt="service" class="img-fluid"/></div>
									<h4>Website Redesign Service</h4>
									<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
								</div>
							</div>
						</div>
						<div class="-cta-btn mt70">
							<div class="free-cta-title v-center wow zoomInDown" data-wow-delay="1.3s">
								<p>Hire a <span>Dedicated Developer</span></p>
								<a href="#" class="btn-main bg-btn2 lnk">Hire Now<i class="fas fa-chevron-right fa-icon"></i><span class="circle"></span></a>
							</div>
						</div>
					</div>
				</section>
				<!--End Service-->
				<!--Start Process-->
				<section class="service-block pad-tb light-dark">
					<div class="container">
						<div class="row justify-content-center">
							<div class="col-lg-8">
								<div class="common-heading ptag">
									<span>Process</span>
									<h2>Our App Development Process</h2>
									<p>Our design process follows a proven approach. We begin with a deep understanding of your needs and create a planning template.</p>
								</div>
							</div>
						</div>
						<div class="row upset justify-content-center mt60">
							<div class="col-lg-4 v-center order1">
								<div class="image-block1">
									<img src="images/process/process-1.jpg" alt="Process" class="img-fluid"/>
								</div>
							</div>
							<div class="col-lg-7 v-center order2">
								<div class="ps-block">
									<span>1</span>
									<h3>Requirement Gathering</h3>
									<p>Donec metus lorem, vulputate at sapien sit amet, auctor iaculis lorem. In vel hendrerit nisi. Vestibulum eget risus velit. Aliquam tristique libero at dui sodales, et placerat orci lobortis. Maecenas ipsum neque, elementum id dignissim et, imperdiet vitae mauris.</p>
								</div>
							</div>
						</div>
						<div class="row upset justify-content-center mt60">
							<div class="col-lg-7 v-center order2">
								<div class="ps-block">
									<span>2</span>
									<h3>Prototype</h3>
									<p>Donec metus lorem, vulputate at sapien sit amet, auctor iaculis lorem. In vel hendrerit nisi. Vestibulum eget risus velit. Aliquam tristique libero at dui sodales, et placerat orci lobortis. Maecenas ipsum neque, elementum id dignissim et, imperdiet vitae mauris.</p>
								</div>
							</div>
							<div class="col-lg-4 v-center order1">
								<div class="image-block1">
									<img src="images/process/process-2.jpg" alt="Process" class="img-fluid"/>
								</div>
							</div>
						</div>
						<div class="row upset justify-content-center mt60">
							<div class="col-lg-4 v-center order1">
								<div class="image-block1">
									<img src="images/process/process-3.jpg" alt="Process" class="img-fluid"/>
								</div>
							</div>
							<div class="col-lg-7 v-center order2">
								<div class="ps-block">
									<span>3</span>
									<h3>Deployment</h3>
									<p>Donec metus lorem, vulputate at sapien sit amet, auctor iaculis lorem. In vel hendrerit nisi. Vestibulum eget risus velit. Aliquam tristique libero at dui sodales, et placerat orci lobortis. Maecenas ipsum neque, elementum id dignissim et, imperdiet vitae mauris.</p>
								</div>
							</div>
						</div>
						<div class="row upset justify-content-center mt60">
							<div class="col-lg-7 v-center order2">
								<div class="ps-block">
									<span>4</span>
									<h3>Support & Maintenance</h3>
									<p>Donec metus lorem, vulputate at sapien sit amet, auctor iaculis lorem. In vel hendrerit nisi. Vestibulum eget risus velit. Aliquam tristique libero at dui sodales, et placerat orci lobortis. Maecenas ipsum neque, elementum id dignissim et, imperdiet vitae mauris.</p>
								</div>
							</div>
							<div class="col-lg-4 v-center order1">
								<div class="image-block1">
									<img src="images/process/process-4.jpg" alt="Process" class="img-fluid"/>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!--End Process-->
				<!--Start Why Choose-->
				<section class="service-block pad-tb bg-gradient7">
					<div class="container">
						<div class="row justify-content-center">
							<div class="col-lg-8">
								<div class="common-heading ptag">
									<span>We Deliver Our Best</span>
									<h2>Why Choose Niwax</h2>
									<p class="mb30">Donec metus lorem, vulputate at sapien sit amet, auctor iaculis lorem. In vel hendrerit nisi. Vestibulum eget risus velit.</p>
								</div>
							</div>
						</div>
						<div class="row justify-content-center">
							<div class="col-lg-4 col-sm-6 mt30  wow fadeIn" data-wow-delay=".2s">
								<div class="s-block wide-sblock">
									<div class="s-card-icon"><img src="images/icons/teama.svg" alt="service" class="img-fluid"></div>
									<div class="s-block-content">
										<h4>Reliable Service. In House Team</h4>
										<p>In vel hendrerit nisi. Vestibulum eget risus velit.</p>
									</div>
								</div>
							</div>
							<div class="col-lg-4 col-sm-6 mt30 wow fadeIn" data-wow-delay=".5s">
								<div class="s-block wide-sblock">
									<div class="s-card-icon"><img src="images/icons/link.svg" alt="service" class="img-fluid"></div>
									<div class="s-block-content">
										<h4>Trusted by People Like You</h4>
										<p>In vel hendrerit nisi. Vestibulum eget risus velit.</p>
									</div>
								</div>
							</div>
							<div class="col-lg-4 col-sm-6 mt30 wow fadeIn" data-wow-delay=".8s">
								<div class="s-block wide-sblock">
									<div class="s-card-icon"><img src="images/icons/tech.svg" alt="service" class="img-fluid"></div>
									<div class="s-block-content">
										<h4>Complete Technical Competency</h4>
										<p>In vel hendrerit nisi. Vestibulum eget risus velit.</p>
									</div>
								</div>
							</div>
							<div class="col-lg-4 col-sm-6 mt30 wow fadeIn" data-wow-delay="1.1s">
								<div class="s-block wide-sblock">
									<div class="s-card-icon"><img src="images/icons/hi.svg" alt="service" class="img-fluid"></div>
									<div class="s-block-content">
										<h4>Friendly & Cordial in Nature</h4>
										<p>In vel hendrerit nisi. Vestibulum eget risus velit.</p>
									</div>
								</div>
							</div>
							<div class="col-lg-4 col-sm-6 mt30 wow fadeIn" data-wow-delay="1.4s">
								<div class="s-block wide-sblock">
									<div class="s-card-icon"><img src="images/icons/badge.svg" alt="service" class="img-fluid"></div>
									<div class="s-block-content">
										<h4>Excellent Quality Delivered on Time</h4>
										<p>In vel hendrerit nisi. Vestibulum eget risus velit.</p>
									</div>
								</div>
							</div>
							<div class="col-lg-4 col-sm-6 mt30 wow fadeIn" data-wow-delay="1.7s">
								<div class="s-block wide-sblock">
									<div class="s-card-icon"><img src="images/icons/tin.svg" alt="service" class="img-fluid"></div>
									<div class="s-block-content">
										<h4>Effective & Continuous Communication</h4>
										<p>In vel hendrerit nisi. Vestibulum eget risus velit.</p>
									</div>
								</div>
							</div>
						</div>
						<div class="-cta-btn mt70">
							<div class="free-cta-title v-center wow zoomInDown" data-wow-delay="1.8s">
								<p>Let's Start a <span>New Project</span> Together</p>
								<a href="#" class="btn-main bg-btn2 lnk">Inquire Now<i class="fas fa-chevron-right fa-icon"></i><span class="circle"></span></a>
							</div>
						</div>
					</div>
				</section>
				<!--End Why Choose-->

				<!--Stat Projects-->
				<section class="featured-project pad-tb">
					<div class="container">
						<div class="row justify-content-center">
							<div class="col-lg-6">
								<div class="common-heading ptag">
									<span>Our Projects</span>
									<h2>Some of Our Works</h2>
									<p class="mb0">We think big and have hands in all leading technology platforms to provide you wide array of services.</p>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-4 wow fadeInUp" data-wow-delay=".2s">
								<div class="isotope_item hover-scale">
									<div class="item-image">
										<a href="#"><img src="images/portfolio/image-1.jpg" alt="portfolio" class="img-fluid"/> </a>
									</div>
									<div class="item-info">
										<h4><a href="#">Creative </a></h4>
										<p>ios, design</p>
									</div>
								</div>
							</div>
							<div class="col-lg-4 wow fadeInUp" data-wow-delay=".4s">
								<div class="isotope_item hover-scale">
									<div class="item-image">
										<a href="#"><img src="images/portfolio/image-2.jpg" alt="portfolio" class="img-fluid"/> </a>
									</div>
									<div class="item-info">
										<h4><a href="#">Brochure Design</a></h4>
										<p>Graphic, Print</p>
									</div>
								</div>
							</div>
							<div class="col-lg-4 wow fadeInUp" data-wow-delay=".6s">
								<div class="isotope_item hover-scale">
									<div class="item-image">
										<a href="#"><img src="images/portfolio/image-3.jpg" alt="portfolio" class="img-fluid"/> </a>
									</div>
									<div class="item-info">
										<h4><a href="#">Ecommerce Development</a></h4>
										<p>Web application</p>
									</div>
								</div>
							</div>
							<div class="col-lg-4 wow fadeInUp" data-wow-delay=".8s">
								<div class="isotope_item hover-scale">
									<div class="item-image">
										<a href="#"><img src="images/portfolio/image-4.jpg" alt="portfolio" class="img-fluid"/> </a>
									</div>
									<div class="item-info">
										<h4><a href="#">Icon Pack</a></h4>
										<p>Android & iOs, Design</p>
									</div>
								</div>
							</div>
							<div class="col-lg-4 wow fadeInUp" data-wow-delay="1s">
								<div class="isotope_item hover-scale">
									<div class="item-image">
										<a href="#"><img src="images/portfolio/image-5.jpg" alt="portfolio" class="img-fluid"/> </a>
									</div>
									<div class="item-info">
										<h4><a href="#">Smart Watch</a></h4>
										<p>UI/UX Design</p>
									</div>
								</div>
							</div>
							<div class="col-lg-4 wow fadeInUp" data-wow-delay="1.2s">
								<div class="isotope_item hover-scale">
									<div class="item-image">
										<a href="#"><img src="images/portfolio/image-6.jpg" alt="portfolio" class="img-fluid"/> </a>
									</div>
									<div class="item-info">
										<h4><a href="#">Brochure Design</a></h4>
										<p>Graphic, Print</p>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12 maga-btn mt60">
								<a href="javascript:void(0)" class="btn-outline">View More Projects <i class="fas fa-chevron-right fa-icon"></i></a>
							</div>
						</div>
					</div>
				</section>
				<!--End Projects-->


<?php get_footer(); ?>