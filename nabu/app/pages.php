<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class pages extends Model
{
    public $timestamps = false;
    protected $table = 'pages';
}
