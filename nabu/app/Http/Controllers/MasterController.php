<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Auth;
use App\pages;
use App\products;
use Crypt;
use Session;
use Hash;
use DB;

class MasterController extends Controller
{
    function list_pages()
    {
        $pages = pages::get();
        return view('admin.pages')->with('pages', $pages);
    }
    // Save pages Details
    public function save_pages(Request $request)
    {
        // return $request;
        if ($request->pages_id != "") {
            $validator =  $request->validate([
                'page_name' => 'required',
                'page_title' => 'required',
                'status' => 'required',
            ]);
            $pages = pages::find($request->pages_id);
            $message = "Page Updated";
        } else {
            $validator =  $request->validate([
                'page_name' => 'required',
                'page_title' => 'required',
                'status' => 'required',
            ]);
            $pages = new pages();
            $message = "Page Added";
        }
        $pages->page_name = $request->page_name;
        $pages->page_title = $request->page_title;
        $pages->page_position = $request->page_position;
        $pages->page_contain = $request->page_contain;
        $pages->status = $request->status;
        $pages->save();
        Session::put('success', $message);
        return redirect('admin/pages');
    }
    // Fetch pages Details
    public function fetchpages($pages_id)
    {
        $pages = pages::where('id', $pages_id)->first();
        return response()->json($pages);
    }
    // Update Status For Active and Deactive
    public function updateStatuspages($pages_id)
    {
        $pages = pages::where('id', $pages_id)->first();
        if ($pages->status == 1) {
            $pages->status = 0;
            $message = "Page Deactived";
        } else {
            $pages->status = 1;
            $message = "Page Actived";
        }
        $pages->save();
        Session::put('success', $message);
        return redirect('admin/pages');
    }
    function list_products()
    {
        $products = products::get();
        return view('admin.products')->with('products', $products);
    }
    // Save products Details
    public function save_products(Request $request)
    {
        // return $request;
        if ($request->products_id != "") {
            $validator =  $request->validate([
                'product_name' => 'required',
                'product_title' => 'required',
                'status' => 'required',
            ]);
            $products = products::find($request->products_id);
            $message = "Product Updated";
        } else {
            $validator =  $request->validate([
                'product_name' => 'required',
                'product_title' => 'required',
                'status' => 'required',
            ]);
            $products = new products();
            $message = "Product Added";
        }
        $products->product_name = $request->product_name;
        $products->product_title = $request->product_title;
        $products->product_position = $request->product_position;
        $products->product_contain = $request->product_contain;
        $products->status = $request->status;
        $products->save();
        Session::put('success', $message);
        return redirect('admin/products');
    }
    // Fetch products Details
    public function fetchproducts($products_id)
    {
        $products = products::where('id', $products_id)->first();
        return response()->json($products);
    }
    // Update Status For Active and Deactive
    public function updateStatusproducts($products_id)
    {
        $products = products::where('id', $products_id)->first();
        if ($products->status == 1) {
            $products->status = 0;
            $message = "Product Deactived";
        } else {
            $products->status = 1;
            $message = "Product Actived";
        }
        $products->save();
        Session::put('success', $message);
        return redirect('admin/products');
    }
}
