<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('clear-cache', function () {
    $exitCode = Artisan::call('config:clear');
    $exitCode = Artisan::call('cache:clear');
    $exitCode = Artisan::call('route:clear');
    $exitCode = Artisan::call('view:clear');
    $exitCode = Artisan::call('config:cache');
    $exitCode = Artisan::call('config:cache');
    Session::flash('success', 'All Clear');
    echo "DONE";
});
Route::get('/', function () {
    return view('website.home');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/dashboard', function () {
    return view('admin.dashboard');
});
Route::get('/homepages', function () {
    return view('admin.homepage');
});
Route::get('/pages', function () {
    return view('admin.pages');
});

// Admin Routes
Route::get('/admin/pages', 'MasterController@list_pages'); //pages List
Route::post('/admin/pages/save', 'MasterController@save_pages'); // pages Save
Route::get('/admin/pages/fetch/{sub_id}', 'MasterController@fetchpages'); //pages Fetch For Edit
Route::get('admin/pages/statusUpdate/{id}', 'MasterController@updateStatuspages'); // pages guardians Status

Route::get('/admin/products', 'MasterController@list_products'); //products List
Route::post('/admin/products/save', 'MasterController@save_products'); // products Save
Route::get('/admin/products/fetch/{sub_id}', 'MasterController@fetchproducts'); //products Fetch For Edit
Route::get('admin/products/statusUpdate/{id}', 'MasterController@updateStatusproducts'); // products guardians Status
