<!DOCTYPE html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="keywords"
content="">
<meta name="author" content="">
<title>NABU SUPPORT AND TRADING INC</title>
<!-- favicon -->
<link rel="icon" type="image/x-icon" href="{{url('public/WebsiteAsset/images/1561451980service-icon-1.png')}}" />
<!-- bootstrap -->
<link rel="stylesheet" href="{{url('public/WebsiteAsset/css/bootstrap.min.css')}}">
<!-- Plugin css -->
<link rel="stylesheet" href="{{url('public/WebsiteAsset/css/plugin.css')}}">
<link rel="stylesheet" href="{{url('public/WebsiteAsset/css/pignose.calender.css')}}">
<!-- stylesheet -->
<link rel="stylesheet" href="{{url('public/WebsiteAsset/css/style.css')}}">
<link rel="stylesheet" href="{{url('public/WebsiteAsset/css/custom.css')}}">
<link rel="stylesheet" href="{{url('public/WebsiteAsset/css/megamenu.css')}}">
<!-- responsive -->
<link rel="stylesheet" href="{{url('public/WebsiteAsset/css/responsive.css')}}">
<!--Updated CSS-->
<link rel="stylesheet" href="{{url('public/WebsiteAsset/css/stylesf00a.html?color=4a1ec7')}}">
<style>
:root {--blue:#1242C9;}
/* ==== Site Banner ==== */
.hero-area {height:none;padding:0;}
#site-banner {position:relative;left:0;top:0;width:100%;}
#site-banner .carousel-item {width:100%;}
#site-banner .carousel-item img {width:100%;height:100vh;object-fit:cover;}
.banner-carousel-caption {bottom:43%;}
.banner-carousel-caption h1 {font-weight:900;color:#fff;}
.banner-slider-arrows {width:15%!important;}
/* ==== Section First ==== */
.section-first{display:block;padding:80px 0 0;}
.slider-heading-left {margin:;font-size:15px;font-weight:600;text-transform:uppercase;color:var(--blue);}
.slider-arrow-box-right{width:61px;background:#e2e5ef;position:relative;top:0px;height:31px;}
.slider-arrow-box-right .carousel-control-prev,
.slider-arrow-box-right .carousel-control-next {width:30px;height:30px;line-height:30px;text-align:center;background-color:var(--blue);color:#fff;opacity:0.9}
.sec-1-img {width:100%;height:350px;object-fit:cover;}
.slider-cover {position:absolute;left:0;bottom:-120px;right:0;width:100%;padding:100px 15px 15px;background:linear-gradient(#0000 10%,#051b57);color:#fff;text-align:center;transition:all 0.5s ease-in-out;}
.slider-cover h6 {font-size:16px;font-weight:600;text-transform:uppercase;color:#fff;}
.slider-cover p {font-size:13px;color:#fff;margin-bottom:2px;}
.slider-cover a {font-size:13px;color:#fff;}
.section-first .carousel-item {overflow:hidden;}
.section-first .carousel-item:hover .slider-cover {bottom:0;opacity:1}

.site-footer {font-size:13px;background-color:#F6F6F6;}
.site-footer h5 {font-size:14px;font-weight:600;color:var(--blue);}
.site-footer .site-f-social {display:flex;}
.site-footer .site-f-social > a {width:35px;height:35px;line-height:32px;text-align:center;background-color:var(--blue);color:#fff;margin-right:0.3rem;border-radius:50%;border:2px solid royalblue;}
.site-footer .site-f-social > a > i {color:#fff;}
.n_f_newletter {display:block;width:100%;background:#dcdcdc;padding:10px 0px 10px 15px;}
.n_f_newletter h6 {position:relative;font-size:13px;color:var(--blue);margin-bottom:0.3em;}
.n_f_newletter .form-control {height:38px;line-height:38px;border-radius:0;box-shadow:none;background-color:#ccc;border:1px solid var(--blue);}
.n_f_newletter .form-control::placeholder {font-size:13px;}
.n_f_newletter button{background:var(--blue);color:#fff;border-radius:0;display:inline-block;width:100px;border:1px solid var(--blue);height:38px;line-height:38px;}
.n_f_newletter button:hover {color:#fff;}
.copy {background-color:var(--blue);color:#fff;padding:30px 0;}


@media (max-width:900px) {
 .banner-carousel-caption {bottom:18%;}
}
</style>
</head>
<body>
<div class="preloader" id="preloader" style="background: url({{url('public/WebsiteAsset/images/1558779995loader.gif')}}) no-repeat scroll center center #FFF;"></div>
<header class="d-block">
   <div class="top_nav">
      <div class="top">
         <div class="container">
            <div class="links">
            	<div class="links_left">
            		<a href="javascript:void(0)" style="cursor:default;">Follow Us:</a>
            		<a href=""><i class="fab fa-facebook-f"></i></a>
            		<a href=""><i class="fab fa-google-plus"></i></a>
            		<a href=""><i class="fab fa-twitter"></i></a>
            		<a href=""><i class="fab fa-linkedin"></i></a>
            	</div>
            	<div class="links_right">
            		<div class="language-selector">
            			<i class="fas fa-globe"></i>
            			<select name="language" class="language selectors">
            				<option value="#" selected="">English</option>
            			</select>
            		</div>
            		<div class="currency-selector">
            			<select name="currency" class="currency selectors">
            				<option value="#" selected="">USD</option>
            				<option value="#">BDT</option>
            				<option value="#">EUR</option>
            			</select>
            		</div>
            	</div>
            </div>
         </div>
      </div>
      <nav class="navbar navbar-expand-lg navbar-light mainNav">
         <div class="container">
         	<a class="navbar-brand" href="index.html" style="pointer-events:none;">
         		<img src="{{url('public/WebsiteAsset/images/nabu-white-logo.png')}}" alt="" class="img-fluid" style="pointer-events:all;">
         	</a>
         	<div class="collapse navbar-collapse" id="mainMenu">
         		<ul class="navbar-nav ml-auto">
         			<li class="nav-item">
         				<a class="nav-link" href="#">About</a>
         			</li>
         			<li class="nav-item">
         				<a class="nav-link" href="#">Product</a>
         				<div class="mega-menu">
         					<div class="n-b-container">

         						<div class="n-b-img">
         							<ul class="list-unstyled n-b-box">
         								<li>
         									<a href="#!">
         										<span>Snefro Pearl</span>
         										<img src="{{url('public/WebsiteAsset/images/banner1.jpg')}}" alt="" width="80" height="50">
         									</a>
         									<ul class="list-unstyled d-block">
         										<li>
         											<a href="#!" class="b-1">
         												<span>Boat aaa</span>
         											</a>
         										</li>
         										<li>
         											<a href="#!" class="b-2">
         												<span>Cabins</span>
         											</a>
         										</li>
         										<li>
         											<a href="#!" class="b-3">
         												<span>Sun Deck</span>
         											</a>
         										</li>
         										<li>
         											<a href="#!" class="b-4">
         												<span>Dive Deck</span>
         											</a>
         										</li>
         										<li>
         											<a href="#!" class="b-5">
         												<span>Living Areas</span>
         											</a>
         										</li>
         									</ul>
         								</li>
         								<li>
         									<a href="#!">
         										<span>Snefro Spirit</span>
         										<img src="{{url('public/WebsiteAsset/images/banner2.jpg')}}" alt="" width="80" height="50">
         									</a>
         									<ul class="list-unstyled">
         										<li>
         											<a href="#!" class="" onmousemove="$('.n-b-box a').removeClass('active');$(this).addClass('active');">
         												<span>Snefro Spirit</span>
         											</a>
         										</li>
         										<li>
         											<a href="#!" onmousemove="$('.n-b-box a').removeClass('active');$(this).addClass('active');">
         												<span>Snefro Spirit</span>
         											</a>
         										</li>
         									</ul>
         								</li>
         								<li>
         									<a href="#!">
         										<span>Snefro Love</span>
         										<img src="{{url('public/WebsiteAsset/images/banner1.jpg')}}" alt="" width="80" height="50">
         									</a>
         								</li>
         								<li>
         									<a href="#!">
         										<span>Snefro Target</span>
         										<img src="{{url('public/WebsiteAsset/images/banner2.jpg')}}" alt="" width="80" height="50">
         									</a>
         								</li>
         								<li>
         									<a href="#!">
         										<span>King Snefro 5</span>
         										<img src="{{url('public/WebsiteAsset/images/banner1.jpg')}}" alt="" width="80" height="50">
         									</a>
         								</li>
         								<li>
         									<a href="#!">
         										<span>King Snefro 6</span>
         										<img src="{{url('public/WebsiteAsset/images/banner2.jpg')}}" alt="" width="80" height="50">
         									</a>
         								</li>
         							</ul>
         						</div>
         						<div class="n-b-img w-50">
         							<!-- Boat -->
         							<div class="n-b-img-inner s-pearl _1" style="background-image:url(assets/images/Slide0.jpg);">
         								<div class="n-b-caption w-100">
         									<div class="row mb-3 w-100">
         										<div class="col-md-6">
         											<h4>Length / Width</h4>
         											<p>29 m / 7 m</p> <br>

         											<h4>Guest</h4>
         											<p>up to 12</p> <br>

         											<h4>Nitrox</h4>
         											<p>free</p> <br>

         											<h4>Wifi</h4>
         											<p>free</p> <br>

         										</div>
         									</div>
         									<div class="row mb-3">
         										<div class="col-md-12">
         											<a href="#!" class="n-b-btn"><i class="fa fa-angle-right"></i> Explore </a>
         											<a href="#!" class="n-b-btn"><i class="fa fa-angle-right"></i> Compare </a>
         										</div>
         									</div>
         									<div class="row w-100">
         										<div class="col-md-12 w-100">
         											<p class="small"></p>
         										</div>
         									</div>
         								</div>
         							</div>

         							<!-- Cabin -->
         							<div class="n-b-img-inner s-pearl _2" style="background-image:url(assets/images/Slide1.jpg);">
         								<div class="n-b-caption">
         									<div class="row mb-3">
         										<div class="col-md-12">
         											<h4>Cabin</h4>
         											<p>4</p> <br>

         											<h4>Guest</h4>
         											<p>2 in bunk bed</p> <br>

         											<h4>Bathroom</h4>
         											<p>ensuite</p> <br>

         											<h4>AC</h4>
         											<p>Yes</p> <br>
         										</div>
         									</div>
         									<div class="row mb-3">
         										<div class="col-md-12">
         											<a href="#!" class="n-b-btn"><i class="fa fa-angle-right"></i> Explore </a>
         											<a href="#!" class="n-b-btn"><i class="fa fa-angle-right"></i> Compare </a>
         										</div>
         									</div>
         									<div class="row">
         										<div class="col-md-12">
         											<p class="small"></p>
         										</div>
         									</div>
         								</div>
         							</div>

         							<!-- Sundeck -->

         							<div class="n-b-img-inner s-pearl _3" style="background-image:url(assets/images/Slide2.jpg);">
         								<div class="n-b-caption">
         									<div class="row mb-3">
         										<div class="col-md-12">
         											<h4>Sundeck</h4>
         											<p>1</p> <br>

         											<h4>Bar</h4>
         											<p>1</p> <br>

         											<h4>Sunbeds</h4>
         											<p>Yes</p> <br>

         											<h4>Shaded Area</h4>
         											<p>Yes</p> <br>
         										</div>
         									</div>
         									<div class="row mb-3">
         										<div class="col-md-12">
         											<a href="#!" class="n-b-btn"><i class="fa fa-angle-right"></i> Explore </a>
         											<a href="#!" class="n-b-btn"><i class="fa fa-angle-right"></i> Compare </a>
         										</div>
         									</div>
         									<div class="row">
         										<div class="col-md-12">
         											<p class="small"></p>
         										</div>
         									</div>
         								</div>
         							</div>

         							<!-- Dive Deck -->
         							<div class="n-b-img-inner s-pearl _4" style="background-image:url(assets/images/Slide3.jpg);">
         								<div class="n-b-caption">
         									<div class="row mb-3">
         										<div class="col-md-12">
         											<h4>12 L Tanks</h4>
         											<p>all DIN/INT, included</p> <br>

         											<h4>15 L Tanks</h4>
         											<p>all DIN/INT, surcharge</p> <br>

         											<h4>Compressor</h4>
         											<p>2 Air, EAN Membrane</p> <br>

         											<h4>Zodiac</h4>
         											<p>1</p> <br>

         										</div>
         									</div>
         									<div class="row mb-3">
         										<div class="col-md-12">
         											<a href="#!" class="n-b-btn"><i class="fa fa-angle-right"></i> Explore </a>
         											<a href="#!" class="n-b-btn"><i class="fa fa-angle-right"></i> Compare </a>
         										</div>
         									</div>
         									<div class="row">
         										<div class="col-md-12">
         											<p class="small"></p>
         										</div>
         									</div>
         								</div>
         							</div>

         							<!-- Living areas -->
         							<div class="n-b-img-inner s-pearl _5" style="background-image:url(assets/images/Slide4.jpg);">
         								<div class="n-b-caption">
         									<div class="row mb-3">
         										<div class="col-md-12">

         											<h4>Salon</h4>
         											<p>1 main deck / 1 upper deck</p> <br>

         											<h4>Audio & Media</h4>
         											<p>yes</p> <br>

         											<h4>Dining Area</h4>
         											<p>1</p> <br>

         											<h4>Food & Drinks</h4>
         											<p>Buffet Style</p> <br>
         										</div>
         									</div>
         									<div class="row mb-3">
         										<div class="col-md-12">
         											<a href="#!" class="n-b-btn"><i class="fa fa-angle-right"></i> Explore </a>
         											<a href="#!" class="n-b-btn"><i class="fa fa-angle-right"></i> Compare </a>
         										</div>
         									</div>
         									<div class="row">
         										<div class="col-md-12">
         											<p class="small"></p>
         										</div>
         									</div>
         								</div>
         							</div>
         						</div>
         					</div>
         				</div>
         			</li>
         			<li class="nav-item">
         				<a class="nav-link" href="#">Blog</a>
         			</li>
         			<li class="nav-item">
         				<a class="nav-link" href="#">Contact</a>
         			</li>
         		</ul>
         	</div>
         </div>
      </nav>
   </div>
   <div class="mobile_menu">
   	<div class="mobile_menu_btn" onclick="$('.mobile_menu_slide').toggleClass('active');">Menu</div>
   	<div class="mobile_menu_slide">
   		<ul>
   			<li class="dropdown-divider mt-0"></li>
   			<li><a href="#!" onclick="$('.mobile_menu_slide').removeClass('active');">Close</a></li>
   			<li class="dropdown-divider"></li>
   			<li><a href="">Home</a></li>
   			<li><a href="">About</a></li>
   			<li><a href="">Product</a></li>
   			<li><a href="">Blog</a></li>
   			<li><a href="">Contact</a></li>
   		</ul>
   	</div>
   </div>
</header>