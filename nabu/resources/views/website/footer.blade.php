<!-- Footer Area Start -->
<footer class="site-footer pt-5">
	<div class="container">
		<div class="row">
			<div class="col-lg-3 mb-2">
				<a href="" class="d-inline-block" style="max-width:180px">
					<img src="{{url('public/WebsiteAsset/images/nabu-blue-logo.png')}}" alt="" class="img-fluid">
				</a>
			</div>
			<div class="col-lg-6">
				<div class="row">
					<div class="col-md-4 mb-2">
						<h5>Discover More</h5>
						<div class="d-block mb-1">
							<a href="">Discover Sinai Peninsula</a>
						</div>
						<div class="d-block mb-1">
							<a href="">Discover Red Sea</a>
						</div>
						<div class="d-block mb-1">
							<a href="">Discover Sharm El Sheikh</a>
						</div>
						<div class="d-block mb-1">
							<a href="">Company Profile</a>
						</div>
					</div>
					<div class="col-md-4 mb-2">
						<h5>Trusted Partners</h5>
						<div class="d-block mb-1">
							<a href="">Europe & Eastern Europe</a>
						</div>
						<div class="d-block mb-1">
							<a href="">North & South America</a>
						</div>
						<div class="d-block mb-1">
							<a href="">Middle East & Africa</a>
						</div>
						<div class="d-block mb-1">
							<a href="">Asia Pacific & Far East</a>
						</div>
					</div>
					<div class="col-md-4 mb-2">
						<h5>For Clients</h5>
						<div class="d-block mb-1">
							<a href="">Terms & Conditions</a>
						</div>
						<div class="d-block mb-1">
							<a href="">Covid 19 Policy</a>
						</div>
						<div class="d-block mb-1">
							<a href="">FAQ/ Help</a>
						</div>
						<div class="d-block mb-1">
							<a href="">Downloads</a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-3 mb-2">
				<h5 class="font-weight-bol">Contact Us</h5>
				<address>
					Club El Faraana Reef <br>
					Marine Sports Club Street <br>
					Hadabah Om Sid <br>
					46619 Sharm El Sheikh <br>
					South Sinai - Egypt <br>
					Mobile 002 - 0122 - 315 0612
				</address>
			</div>
		</div>
		<div class="row mb-3">
			<div class="col-lg-3">
				<a href="" class="d-inline mr-3">
					<img src="{{url('public/WebsiteAsset/images/logo2_1588420140.png')}}" alt="" class="img-fluid">
				</a>
				<a href="" class="d-inline mr-3">
					<img src="{{url('public/WebsiteAsset/images/logo3_1588420140.png')}}" alt="">
				</a>
			</div>
			<div class="col-lg-3">
				<h5 class="ml-0 ml-md-4 pl-0 pl-md-3">FIND US ON</h5>
				<div class="site-f-social mb-3">
					<a href=""><i class="fab fa-facebook-f"></i></a>
					<a href=""><i class="fab fa-linkedin"></i></a>
					<a href=""><i class="fab fa-instagram"></i></a>
					<a href=""><i class="fab fa-youtube"></i></a>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="n_f_newletter f-news">
					<h6>GET OUR NEWSLETTER</h6>
					<form id="frmNewsLetter" method="post">
						<div class="input-group mb-2" style="padding-right:20px!important;">
							<input type="email" class="form-control" placeholder="Enter your email address"required="">
							<div class="input-append">
								<button class="" id="btn-news-letter" type="button">SIGN UP</button>
							</div>
						</div>
						<div id="newsletter-email-error">

						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="copy">
		<div class="container">
			<span class="d-inline text-light">|</span>
			<a href="" class="d-inline text-white mx-2">&copy; 2020 - Nabu Support and Trading</a>
			<span class="d-inline text-light">|</span>
			<a href="" class="d-inline text-white mx-2">Privacy Policy</a>
			<span class="d-inline text-light">|</span>
			<a href="" class="d-inline text-white mx-2">Dosclaimer</a>
			<span class="d-inline text-light">|</span>
			<a href="" class="d-inline text-white mx-2">Cookie Policy</a>
		</div>
	</div>
</footer>


<!-- Back to Top Start -->
<div class="bottomtotop">
<i class="fas fa-chevron-right"></i>
</div>
<!-- Back to Top End -->
<script type="text/javascript">
var mainurl = "#";
var gs = { "id": "1", "logo": "1561442662logo.png", "favicon": "1561451980service-icon-1.png", "loader": "1558779995loader.gif", "admin_loader": "1562578944loader.gif", "banner": "1563960965Slider.jpg", "title": "Corporate Business", "header_email": "Info@example.com", "header_phone": "0123 456789", "footer": "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae", "copyright": "COPYRIGHT \u00a9 2019. All Rights Reserved By <a href=\"http:\/\/geniusocean.com\/\">GeniusOcean.com<\/a>", "colors": "#4a1ec7", "is_talkto": "0", "talkto": "<script type=\"text\/javascript\">\r\nvar Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();\r\n(function(){\r\nvar s1=document.createElement(\"script\"),s0=document.getElementsByTagName(\"script\")[0];\r\ns1.async=true;\r\ns1.src='https:\/\/embed.tawk.to\/5bc2019c61d0b77092512d03\/default';\r\ns1.charset='UTF-8';\r\ns1.setAttribute('crossorigin','*');\r\ns0.parentNode.insertBefore(s1,s0);\r\n})();\r\n<\/script>", "is_language": "1", "is_loader": "1", "map_key": "AIzaSyB1GpE4qeoJ__70UZxvX9CTMUTZRZNHcu8", "is_disqus": "1", "disqus": "<div id=\"disqus_thread\">         \r\n    <script>\r\n    \/**\r\n    *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.\r\n    *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https:\/\/disqus.com\/admin\/universalcode\/#configuration-variables*\/\r\n    \/*\r\n    var disqus_config = function () {\r\n    this.page.url = PAGE_URL;  \/\/ Replace PAGE_URL with your page's canonical URL variable\r\n    this.page.identifier = PAGE_IDENTIFIER; \/\/ Replace PAGE_IDENTIFIER with your page's unique identifier variable\r\n    };\r\n    *\/\r\n    (function() { \/\/ DON'T EDIT BELOW THIS LINE\r\n    var d = document, s = d.createElement('script');\r\n    s.src = 'https:\/\/junnun.disqus.com\/embed.js';\r\n    s.setAttribute('data-timestamp', +new Date());\r\n    (d.head || d.body).appendChild(s);\r\n    })();\r\n    <\/script>\r\n    <noscript>Please enable JavaScript to view the <a href=\"https:\/\/disqus.com\/?ref_noscript\">comments powered by Disqus.<\/a><\/noscript>\r\n    <\/div>", "is_contact": "1", "is_faq": "1", "paypal_check": "1", "stripe_check": "1", "cod_check": "1", "paypal_business": "shaon143-facilitator-1@gmail.com", "stripe_key": "pk_test_UnU1Coi1p5qFGwtpjZMRMgJM", "stripe_secret": "sk_test_QQcg3vGsKRPlW6T3dXcNJsor", "smtp_host": "in-v3.mailjet.com", "smtp_port": "587", "smtp_user": "0e05029e2dc70da691aa2223aa53c5be", "smtp_pass": "5df1b6242e86bce602c3fd9adc178460", "from_email": "admin@geniusocean.com", "from_name": "GeniusOcean", "is_smtp": "1", "coupon_found": "Coupon Found", "already_coupon": "Coupon Already Applied", "order_title": "THANK YOU FOR YOUR PURCHASE.", "service_subtitle": "<h5 class=\"sub-title\" style=\"margin-bottom: 2px; font-weight: 600; line-height: 28px; font-size: 18px; color: rgb(5, 14, 51);\">WE WORK FOR BEST RESULTS<\/h5>", "service_title": "Welcome to About Section", "service_text": "<span style=\"color: rgb(5, 14, 51);\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis.&nbsp;<\/span><div><div><span style=\"color: rgb(5, 14, 51);\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis.&nbsp;<\/span><span style=\"color: rgb(5, 14, 51);\"><br><\/span><\/div><div><span style=\"color: rgb(5, 14, 51);\"><br><\/span><\/div><div><span style=\"color: rgb(5, 14, 51);\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis.&nbsp;<\/span><span style=\"color: rgb(5, 14, 51);\"><br><\/span><\/div><\/div><div><span style=\"color: rgb(5, 14, 51);\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis.&nbsp;<\/span><span style=\"color: rgb(5, 14, 51);\"><br><\/span><\/div>", "service_image": "1563961003details.png", "order_text": "We'll email you an order confirmation with details and tracking info.", "is_currency": "1", "currency_format": "0", "price_title": "Choose Plans & Pricing", "price_subtitle": "Choose the best for yourself", "price_text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.", "subscribe_success": "You are subscribed Successfully.", "subscribe_error": "This email has already been taken.", "error_title": "<div><h4 class=\"heading\"><\/h4><\/div><div><h4 class=\"heading\" style=\"margin: 40px 0px 25px; font-weight: 700; line-height: 46px; font-size: 36px; color: rgb(40, 56, 79); text-align: center;\">Oops! You're lost...<\/h4><\/div>", "error_text": "<span style=\"color: rgb(51, 51, 51); text-align: center;\">The page you are looking for might have been moved, renamed, or might never existed.<\/span><br>", "error_photo": "1561878540404.png", "breadcumb_banner": "1562486531breadcrumb-bg.jpg" };
</script>
<!-- jquery -->
<script src="{{url('public/WebsiteAsset/js/jquery.js')}}"></script>
<script src="{{url('public/WebsiteAsset/jquery-ui/jquery-ui.min.js')}}"></script>
<!-- bootstrap -->
<script src="{{url('public/WebsiteAsset/js/bootstrap.min.js')}}"></script>
<!-- popper -->
<script src="{{url('public/WebsiteAsset/js/popper.min.js')}}"></script>
<!-- Moment js -->
<script src="{{url('public/WebsiteAsset/js/moment.min.js')}}"></script>
<!-- Pignose Calender -->
<script src="{{url('public/WebsiteAsset/js/pignose.calender.js')}}"></script>
<!-- plugin js-->
<script src="{{url('public/WebsiteAsset/js/plugin.js')}}"></script>
<!-- notify js-->
<script src="{{url('public/WebsiteAsset/js/notify.js')}}"></script>
<!-- main -->
<script src="{{url('public/WebsiteAsset/js/main.js')}}"></script>
<!-- custom -->
<script src="{{url('public/WebsiteAsset/js/custom.js')}}"></script>
</body>
<!-- Mirrored from demo.geniusocean.com/myagency/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 23 Nov 2020 10:55:28 GMT -->
</html>
<script>
$(document).mouseup(function (e) {
	if ($(e.target).closest(".prod_dropdown, .prod_dropdown_btn").length === 0) { 
		$(".prod_dropdown").hide(); 
	}
});
$(document).ready(function(){
	$('.prod_dropdown_btn').click(function(){
		$('.prod_dropdown').toggle();
	});
});

</script>

<script>
   // $(document).ready(function(){
      $('.n-b-box .b-1').hover(function(){
         $('.s-pearl').hide();
         $('.s-pearl._1').show();
      });
      $('.n-b-box .b-2').hover(function(){
         $('.s-pearl').hide();
         $('.s-pearl._2').show();
      });
      $('.n-b-box .b-3').hover(function(){
         $('.s-pearl').hide();
         $('.s-pearl._3').show();
      });
      $('.n-b-box .b-4').hover(function(){
         $('.s-pearl').hide();
         $('.s-pearl._4').show();
      });
      $('.n-b-box .b-5').hover(function(){
         $('.s-pearl').hide();
         $('.s-pearl._5').show();
      });
   // });
</script>